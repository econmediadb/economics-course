## 1.4. L'offre

**Mots-clés** de la section 1.4 : *offre, ...*

*L’offre détermine les quantités que les vendeurs sont prêts à offrir sur le marché pour chaque niveau de prix donné. L’offre de marché s’obtient en additionnant les offres individuelles. L’offre est une fonction croissante des prix, car plus le prix augmente et plus les perspectives de profit sont importantes pour les vendeurs. La pente d’une courbe d’offre nous renseigne sur la sensibilité de l’offre à la suite d’une variation des prix.*

Pour un modèle simple d’un marché comptant de nombreux acheteurs et vendeurs (on parle ici d’*atomicité*), prenons l’exemple d’une ville où un grand nombre de petites boulangeries fabriquent du pain et le vendent directement aux consommateurs. La courbe d’offre de marché indique la quantité totale produite par l’ensemble des boulangeries à chaque niveau de prix donné.

L’offre (dans notre exemple, l’offre quotidienne totale de pains de la part de l’ensemble des boulangers de la ville) est une fonction croissante du prix : plus le prix est élevé, plus les boulangers (offreurs) sont disposés à produire et à vendre, car cela constitue une perspective de gain plus élevé, augmentant l’offre de marché. L’offre de marché est donc la somme des offres de tous les producteurs. De plus, ces prix plus élevés incitent de nouveaux individus à devenir boulangers, augmentant le nombre d’offreurs et l’offre totale.

Voici les déterminants qui influencent les quantités offertes : les coûts de production (par exemple, l’évolution du prix des matières premières ou bien le changement de prix des facteurs de production), le nombre d’offreurs, les évolutions technologiques, un changement des anticipations des producteurs.

---
**EXERCICE**

- Exercice : L’offre et le prix
  - Qu’est-ce que l’offre ? 
  - Qu’est-ce que l’offre de marché ?
  - Pourquoi l’offre est-elle une fonction croissante du prix ? 

---


Le Tableau 1.9 et le Graphique 1.1 montrent les quantités offertes en fonction du prix du pain qui vont permettre de construire la courbe d’offre.

Tableau 1.9 Prix et quantité de pains offerte. 
| Prix en euro | Quantité de <br> pains offerte |
|:------------:|:-------------------------:|
| 1,10          | 1.000                     |
| 1,50          | 3.000                     |
| 2,00            | 5.000                     |
| 2.35         | 6.000                     |
| 3,77         | 9.000                     |


<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-01.jpg" />
    <br>
    Graphique 1.1 La courbe d’offre sur le marché du pain.


---
**APPLICATION**

*Interpréter la pente de la courbe d’offre.*

La pente d’une courbe d’offre nous renseigne sur la sensibilité de l’offre par rapport aux variations des prix.

Quand une variation des prix entraîne une variation plus que proportionnelle des quantités offertes, on dit que l’offre est très sensible à la variation des prix (pente de la courbe peu importante). Quand une variation des prix entraîne une variation moins que proportionnelle des quantités offertes, on dit que l’offre est peu sensible à la variation des prix (pente de la courbe très importante). Enfin, si une variation des prix n’entraîne pas de modification des quantités offertes, on dit que l’offre est insensible à la variation des prix.

---
