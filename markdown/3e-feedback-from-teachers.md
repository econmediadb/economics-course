# Retour d'information des enseignants

## 1. Regroupement entre pairs

### 1.1. Présentation générale

1. Au cas où nous devrions abandonner le chapitre envisagé pour la 2e "marché des biens et services", le contenu de ce chapitre peut être traité en partie en 3e et 1e.
2. Le contenu de 1e "échanges internationaux" et "intervention de l'autorité publique" permet d'introduire la *culture de débat* parmi les élèves (p.ex. jeu de rôle en classe).
3. Le cours (de 2e et 1e) et l'examen de fin d'études secondaires doit permettre d'établir une distinction entre l'épreuve orale et écrite dans l'examen.

###  1.2. Présentation des concepts

#### Chapitre: Introduction

1. La notion de *système économique: économie de marchée et économie planifié* peut être introduite à l'aide d'une vidéo (existant dans le cours de GCG?). Ce contenu est utile dans le cadre de "flipped classroom". Cette partie du cours/texte peut être minimiser. (*agréable à avoir*)
2.  La notion d' *économie normative et économie positive* peut-être introduit plus tard dans le cours. Cette matière est peut-être trop compliqué en 3e dans le cadre de ce chapitre. (*peut être évité*)
3. Certains enseignants parlent de la pyramide de Maslov lorsqu'ils introduisent la notion des *besoins*
4. Le paragraphe "Or, ces besoins ... son bien-être." (p.10) devrait se trouver après le paragraphe "Pour satisfaire ces besoins, ... doivent faire des choix." (p.10). Raison: Il faudrait introduire ceci après que la notion de *ressources rares* a été introduite.
5. "Les économistes étudient comment les individus prennent ces décisions." devrait plutôt être "Les économistes étudient comment les individus *font leurs choix*." (p.11)
6. Où se trouve les "ressources" dans le schéma (p.11). Est-ce que le terme *bien* regroupe les notions de *biens et services*?
7. La citation de Eisenhower (p.12) devrait être en italique et ce paragraphe devrait se situer tout de suite au début de 2.1 (p.12)
8. Donner une définition de *libéralisme économique*, éventuellement en footnote (p.14). On pourrait introduire le schéma à cet endroit (p.ex. [schéma du système économique](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220531-systeme-economique-pays.png)  )
9. Introduction de la vidéo sur l'économie planifiée et l'économie du marché (cours précédent?).
10. Introduire du matériel (p.ex. paragraphe ou un extrait d'article) parlant du système économique (l'économie mixte) en Europe ou au Luxembourg (source possible Alteréco, ...)


#### Chapitre 1: Comment crée-t-on des richesses et comment les mesure-t-on?

1. Dans la notion *biens de consommation*, il faudrait ajouter la notion de "bien de production". Cette notion manque par rapport au cours précédent
2. Concernant la notion *indicateurs complémentaires*, cette partie peut être facultative ou elle peut être regroupé ensemble avec les notions "d'inégalités". La notion de LIW peut être regroupée dans un document séparé (p.ex. *pour profs*).
3. La section 6.1. devrait être raccourcie et éventuellement le titre devrait changer ou être adapté - le même titre est déjà utilisé ailleurs.
4. Les *critères de performances* peuvent être mis à la fin de 4. une fois que les facteurs de substitutions ont été introduites (A CLARIFIER!) 
5. Mettre le mot marché en gras (première ligne du paragraphe "Pour qu'il y ait ... pour ce service." (p.22)). Dans cette partie les élèves peuvent être engagés dans une discussion "Quels types de marchés connaissez-vous?".
6. Rajouter un paragraphe sur les *biens de production* (p.23) tout de suite après le paragraphe sur les *biens intermédiaires*
7. Remplacer dans le paragraphe "Une unité de production ... leurs coûts de production." (p.23-24) la partie "prix couvrant plus de la moitié des coûts de production" par une tournure du genre "les prix ne couvrant pas totalement les coûts de production".
Tout le contenu à la p.26 peut être mis en italique ou bien transformé en *cas appliqué: illustration pour le Luxembourg*
8. Remplacer dans le paragraphe "Pour transformer les ressources naturelles ... sans intervention humaine." (p.28). le terme de "besoin de deux facteurs" à "des deux autres facteurs".
9. Dans le paragraphe "Il faut néanmoins .... richesse créée par l'entreprise." (p.34) à partir de la deuxième phrase doit être placé plus tard dans le cours.
10. Les deux boîtes sont identiques! Il faut enlever la première boîte. (p.34)
11. En bas de la p.34: CHIDA - cons.intermédiaires = VA - autres coûts = Résultats
Dans le paragraphe "La valeur ajoutée mesure ... dans son ensemble." (p.36) il faut simplifier les deux dernières phrase en incluant éventuellement le terme *consommation intermédiaire*.
12. On pourrait inclure comme expression $PIB=\sum VA$ juste avant "4.2. La croissance économique" (p.37)
13. Corriger l'année 1960 et adapter au graphique (1995) (p.38)
14. Corriger la description du graphique en "Figure 1.3 : Taux de croissance du PIB au Luxembourg entre 1992 et 2021. Source: Banque mondiale" --> ceci doit être fait dans Overleaf! (Tarik)
15. Il faudrait rendre le graphique plus claire pour les élèves puissent faire la question "4. Complétez la phrase suivante ..." (p.39)
16. Simplifier la présentation du graphique â la p.40.
17. Corriger la phrase dans le paragraphe "La croissance économique ... la production globale." (p.40) Remplacer "La récession correspond à un ralentissement de la *croissance*." en "La récession correspond à un ralentissement de *l'économie*." 
18. Rajouter une parenthèse manquante dans le paragraphe "Pour comparer ... du pays (PIB par habitant)." (p.41)
19. Les enseignants ont tendance à laisser de côté "5.1. La crosse de hockey" (p.43). On devrait inclure ceci mais éventuellement proposer une application pour faciliter l'utilisation de ce concept en classe.
20. On pourrait éventuellement introduire la vidéo de la FEDIL concernant l'histoire de l'industrie au Luxembourg (lien et source ?)


#### Chapitre 2: La répartition de la richesse

1. Les élèves sont très intéressés par les inégalités, il serait dommage d'omettre tout le chapitre. Ce chapitre permet aux enseignants d'avoir de bonnes discussions en classe.
2. Les notions suivantes sont considérées comme *indispensable*: 
   - *partage de la richesse*
   - *valeur ajoutée brute*
   - *revenu primaire: revenus du travail et revenus du capital*
   - *inégalité de revenu: part de la richesse des 1% les plus riches*
3. Il faudrait regrouper les inégalités (p.ex. en avançant la partie "répartition des valeurs ajoutées" de la page 59-62, cette partie peut être ajoutée après la section 4.4 du chapitre 1 en créant une nouvelle section "4.5. Répartition de la valeur ajoutée"). Ceci permet de traiter le sujet plus tôt dans le cours.
4. La partie "3.3. Mesurer les inégalités" de ce chapitre peut être laissée de côté. Ce sujet sera traité plus tard (en 2e ou 1e?)

#### Chapitre 3: Comment se forment les prix sur un marché?

Le contenu de ce chapitre sera discuté ultérieurement lors d'un regroupement entre pairs.
