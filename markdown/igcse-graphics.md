# IGCSE Economics Keywords

## Chapter 1: The nature of the economic problem

- basic economic problem
- economic agents
- economic goods
- free goods
- goods
- needs
- private sector
- public sector
- services
- wants

<!-- People experiencing homelessness are a stark reminder that scarcity of resources is real. (Credit: "Pittsburgh Homeless" by "daveyinn"/Flickr Creative Commons, CC BY 2.0) -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/fad0e7644528235aaebe7f8ad5ec7e3602706328" alt="Basic Economic Problem - Scarcity of Resources" style="width:50%">
  <figcaption>Fig.X - Basic Economic Problem - Scarcity of Resources. (Credit: "Pittsburgh Homeless" by "daveyinn"/Flickr Creative Commons, CC BY 2.0)</figcaption>
</figure> 

<!-- Adam Smith -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/abc76451c77edc0569aa2af879956b25fa17c129" alt="Adam Smith" style="width:50%">
  <figcaption>Fig.X - Adam Smith introduced the idea of dividing labor into discrete tasks. (Credit: "Adam Smith" by Cadell and Davies (1811), John Horsburgh (1828), or R.C. Bell (1872)/Wikimedia Commons, Public Domain)</figcaption>
</figure>

[The Wealth of Nations by Adam Smith (audio and transcript)](https://www.britannica.com/topic/the-Wealth-of-Nations)

<!-- La Richesse des Nations par Adam Smith (Gallica) -->
<div style="display: block; "><iframe style="width:500px; height: 554.8919753086419px; border: 0;" src="https://gallica.bnf.fr/ark:/12148/bpt6k75319v/f9.double.mini"></iframe></div>

<!--  Division of Labor -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/64ca456471b6d936cb0f53a84006cf29997e84bb" alt="Division of Labor" style="width:50%">
  <figcaption>Fig.X - Division of Labor: Workers on an assembly line are an example of the divisions of labor. (Credit: "Red Wing Shoe Factory Tour" by Nina Hale/Flickr Creative Commons, CC BY 2.0)</figcaption>
</figure>


<!-- John Maynard Keynes -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/89ad7d87595dd610af33d1aef44f896e46587e52" alt="John Maynard Keynes" style="width:50%">
  <figcaption>Fig.X - John Maynard Keynes One of the most influential economists in modern times was John Maynard Keynes. (Credit: “John Maynard Keynes” by IMF/Wikimedia Commons, Public Domain)</figcaption>
</figure>

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZRvaxUNDTKY?si=RXq5nzFIpuEr09OE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>









## Chapter 2: The factors of production

- capital
- ceteris paribus
- enterprise
- factors of production
- geographical mobility
- labour
- land
- occupational mobility

<!-- The Circular Flow Diagram -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/626d1c7cf155ffff8f72475d68ac15fe4da4eb75" alt="The Circular Flow Diagram" style="width:50%">
  <figcaption>Fig.X - The Circular Flow Diagram The circular flow diagram shows how households and firms interact in the goods and services market, and in the labor market. The direction of the arrows shows that in the goods and services market, households receive goods and services and pay firms for them. In the labor market, households provide labor and receive payment from firms through wages, salaries, and benefits.</figcaption>
</figure>



## Chapter 3: Opportunity cost

- consumers
- decision makers
- firms
- government
- opportunity cost

<!-- Choices and Tradeoffs -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/5a1c044deac5acfdd49ba024da1c17ba361ac9e2" alt="Choices and Tradeoffs" style="width:50%">
  <figcaption>Fig.X - Choices and Tradeoffs In general, the higher the degree, the higher the salary, so why aren’t more people pursuing higher degrees? The short answer: choices and tradeoffs. (Credit: modification of "College of DuPage Commencement 2018 107" by COD Newsroom/Flickr, CC BY 2.0)</figcaption>
</figure>

<!-- The Budget Constraint -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/1e6acbb4ab3f597a104c28cc3ba208dc5debdaf3" alt="The Budget Constraint" style="width:50%">
  <figcaption>Fig.X - The Budget Constraint: Alphonso’s Consumption Choice Opportunity Frontier Each point on the budget constraint represents a combination of burgers and bus tickets whose total cost adds up to Alphonso’s budget of $10. The relative price of burgers and bus tickets determines the slope of the budget constraint. All along the budget set, giving up one burger means gaining four bus tickets.</figcaption>
</figure>



## Chapter 4: Production possibility curve

- efficiency
- inefficiency
- movement
- production possibility curve (PPC)
- productive capacity
- shift
- the PPC diagram

<!-- Production possibilities frontier (PPF) -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/b5a33337ada51f6d64ae40d7e3f97fec2659e108" alt="Production possibilities frontier" style="width:50%">
  <figcaption>Fig.X - A Healthcare vs. Education Production Possibilities Frontier This production possibilities frontier shows a tradeoff between devoting social resources to healthcare and devoting them to education. At A all resources go to healthcare and at B, most go to healthcare. At D most resources go to education, and at F, all go to education.</figcaption>
</figure>

<!-- Productive and Allocative Efficiency -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/79dd80a2684689ad1140ba93aa07462b2b857cb5" alt="Productive and Allocative Efficiency" style="width:50%">
  <figcaption>Fig.X - Productive and Allocative Efficiency Productive efficiency means it is impossible to produce more of one good without decreasing the quantity that is produced of another good. Thus, all choices along a given PPF like B, C, and D display productive efficiency, but R does not. Allocative efficiency means that the particular mix of goods being produced—that is, the specific choice along the production possibilities frontier—represents the allocation that society most desires.</figcaption>
</figure>

<!-- Production Possibility Frontier for the U.S. and Brazil -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/32b5df814819fcabd18b2a8203f7a7aef51c8fd0" alt="Production Possibility Frontier for the U.S. and Brazil " style="width:50%">
  <figcaption>Fig.X -  Production Possibility Frontier for the U.S. and Brazil The U.S. PPF is flatter than the Brazil PPF implying that the opportunity cost of wheat in terms of sugar cane is lower in the U.S. than in Brazil. Conversely, the opportunity cost of sugar cane is lower in Brazil. The U.S. has comparative advantage in wheat and Brazil has comparative advantage in sugar cane.</figcaption>
</figure>

<!-- The Tradeoff Diagram -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/e1c35f7fad20ff4499ff70c7a1c1e202ed7ce268" alt="The Tradeoff Diagram" style="width:50%">
  <figcaption>Fig.X -   The Tradeoff Diagram Both the individual opportunity set (or budget constraint) and the social production possibilities frontier show the constraints under which individual consumers and society as a whole operate. Both diagrams show the tradeoff in choosing more of one good at the cost of less of the other.</figcaption>
</figure>



## Chapter 5: Microeconomics and macroeconomics

- consumers
- households
- microeconomics
- macroeconomics

<!-- A Choice between Consumption Goods -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/3108eb94a783d4e9fb3415caa8ff83a3c8d61aac" alt="A Choice between Consumption Goods" style="width:50%">
  <figcaption>Fig.X - A Choice between Consumption Goods José has income of $56. Movies cost $7 and T-shirts cost $14. The points on the budget constraint line show the combinations of affordable movies and T-shirts.</figcaption>
</figure>

<!-- How a Change in Income Affects Consumption Choices -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/894492c1efc968fec9cf4314a5873c6458259c0a" alt="How a Change in Income Affects Consumption Choices" style="width:50%">
  <figcaption>Fig.X - How a Change in Income Affects Consumption Choices The utility-maximizing choice on the original budget constraint is M. The dashed horizontal and vertical lines extending through point M allow you to see at a glance whether the quantity consumed of goods on the new budget constraint is higher or lower than on the original budget constraint. On the new budget constraint, Kimberly will make a choice like N if both goods are normal goods. If overnight stays is an inferior good, Kimberly will make a choice like P. If concert tickets are an inferior good, Kimberly will make a choice like Q.</figcaption>
</figure>

<!-- How a Change in Price Affects Consumption Choices -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/c1e0ab3e0970ee44ffd6a79e1918a5aec62f1636" alt="How a Change in Price Affects Consumption Choices" style="width:50%">
  <figcaption>Fig.X - How a Change in Price Affects Consumption Choices The original utility-maximizing choice is M. When the price rises, the budget constraint rotates clockwise. The dashed lines make it possible to see at a glance whether the new consumption choice involves less of both goods, or less of one good and more of the other. The new possible choices would be fewer baseball bats and more cameras, like point H, or less of both goods, as at point J. Choice K would mean that the higher price of bats led to exactly the same quantity of bat consumption, but fewer cameras. Theoretically possible, but unlikely in the real world, we rule out choices like L because they would mean that a higher price for baseball bats means a greater consumption of baseball bats.</figcaption>
</figure>

<!-- The Foundations of a Demand Curve: An Example of Housing -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/b6923db15fef4b4ba3daab72b3db124583bc108a" alt="The Foundations of a Demand Curve: An Example of Housing" style="width:50%">
  <figcaption>Fig.X - The Foundations of a Demand Curve: An Example of Housing (a) As the price increases from P0 to P1 to P2 to P3, the budget constraint on the upper part of the diagram rotates clockwise. The utility-maximizing choice changes from M0 to M1 to M2 to M3. As a result, the quantity demanded of housing shifts from Q0 to Q1 to Q2 to Q3, ceteris paribus. (b) The demand curve graphs each combination of the price of housing and the quantity of housing demanded, ceteris paribus. The quantities of housing are the same at the points on both (a) and (b). Thus, the original price of housing (P0) and the original quantity of housing (Q0) appear on the demand curve as point E0. The higher price of housing (P1) and the corresponding lower quantity demanded of housing (Q1) appear on the demand curve as point E1.</figcaption>
</figure>


## Chapter 6: The role of markets in allocating resources

- market disequilibrium
- market equilibrium
- market system
- price mechanism





## Chapter 7: Demand

- complements
- contraction in demand
- decrease in demand
- demand
- extension in demand
- increase in demand
- law of demand
- market demand
- quantity demanded
- substitutes


<!-- Farmer’s Market  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/768a247caf0478bbf20c8020fac1c5cf6e0cee89" alt="Farmer’s Market " style="width:50%">
  <figcaption>Fig.X -   Farmer’s Market Organic vegetables and fruits that are grown and sold within a specific geographical region should, in theory, cost less than conventional produce because the transportation costs are less. That is not, however, usually the case. (Credit: modification of "Old Farmers' Market" by NatalieMaynor/Flickr, CC BY 2.0)</figcaption>
</figure>


[Weirdest Celebrity Items Sold At Auction: Britney Spears' Gum, Brad Pitt's Breath And More (Huffpost)](https://www.huffpost.com/entry/weirdest-celebrity-items-sold-at-auction_n_1791850)


<!-- A Demand Curve for Gasoline  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/a591b2927eda14cd9c8692084ac24c9074441bbb" alt="A Demand Curve for Gasoline" style="width:50%">
  <figcaption>Fig.X -   A Demand Curve for Gasoline The demand schedule shows that as price rises, quantity demanded decreases, and vice versa. We graph these points, and the line connecting them is the demand curve (D). The downward slope of the demand curve again illustrates the law of demand—the inverse relationship between prices and quantity demanded.</figcaption>
</figure>


## Chapter 8: Supply

- contraction in supply
- decrease in supply
- extension in supply
- increase in supply
- law of supply
- market supply
- quantity supplied
- supply

<!-- A Supply Curve for Gasoline   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/dc5552e05117d2233339d960336aa4c2f8d25251" alt="A Supply Curve for Gasoline " style="width:50%">
  <figcaption>Fig.X - A Supply Curve for Gasoline The supply schedule is the table that shows quantity supplied of gasoline at each price. As price rises, quantity supplied also increases, and vice versa. The supply curve (S) is created by graphing the points from the supply schedule and then connecting them. The upward slope of the supply curve illustrates the law of supply—that a higher price leads to a higher quantity supplied, and vice versa.</figcaption>
</figure>

## Chapter 9: Price determination

- equilibrium price
- excess demand
- excess supply
- market disequilibrium
- market equilibrium
- shortages
- surpluses

<!-- Demand and Supply for Gasoline   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/57f3b3c40b3db192b5e93cb8454cceb72f1fc1d9" alt="Demand and Supply for Gasoline" style="width:50%">
  <figcaption>Fig.X - Demand and Supply for Gasoline The demand curve (D) and the supply curve (S) intersect at the equilibrium point E, with a price of $1.40 and a quantity of 600. The equilibrium price is the only price where quantity demanded is equal to quantity supplied. At a price above equilibrium like $1.80, quantity supplied exceeds the quantity demanded, so there is excess supply. At a price below equilibrium such as $1.20, quantity demanded exceeds quantity supplied, so there is excess demand.</figcaption>
</figure>

<!-- Consumer and Producer Surplus -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/07416e1de46955bdc4cc7317fc83fc1d71bb8a9b" alt="Consumer and Producer Surplus" style="width:50%">
  <figcaption>Fig.X - Consumer and Producer Surplus The somewhat triangular area labeled by F shows the area of consumer surplus, which shows that the equilibrium price in the market was less than what many of the consumers were willing to pay. Point J on the demand curve shows that, even at the price of $90, consumers would have been willing to purchase a quantity of 20 million. The somewhat triangular area labeled by G shows the area of producer surplus, which shows that the equilibrium price received in the market was more than what many of the producers were willing to accept for their products. For example, point K on the supply curve shows that at a price of $45, firms would have been willing to supply a quantity of 14 million.</figcaption>
</figure>

<!--  Efficiency and Price Floors and Ceilings  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/6fce0024f9af1cfd314ff1261969037ea3ddb350" alt=" Efficiency and Price Floors and Ceilings " style="width:50%">
  <figcaption>Fig.X -  Efficiency and Price Floors and Ceilings (a) The original equilibrium price is $600 with a quantity of 20,000. Consumer surplus is T + U, and producer surplus is V + W + X. A price ceiling is imposed at $400, so firms in the market now produce only a quantity of 15,000. As a result, the new consumer surplus is T + V, while the new producer surplus is X. (b) The original equilibrium is $8 at a quantity of 1,800. Consumer surplus is G + H + J, and producer surplus is I + K. A price floor is imposed at $12, which means that quantity demanded falls to 1,400. As a result, the new consumer surplus is G, and the new producer surplus is H + I.

</figcaption>
</figure>

## Chapter 10: Price changes

- decrease in demand
- decrease in supply
- increase in demand
- increase in supply
- non-price factors
- sales tax
- subsidy

### Shift in demand

<!-- Shifts in Demand: A Car Example   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/a2858fa4eb32f68eb064c1650c9ba5af639fe314" alt="Shifts in Demand: A Car Example" style="width:50%">
  <figcaption>Fig.X -  Shifts in Demand: A Car Example Increased demand means that at every given price, the quantity demanded is higher, so that the demand curve shifts to the right from D0 to D1. Decreased demand means that at every given price, the quantity demanded is lower, so that the demand curve shifts to the left from D0 to D2.</figcaption>
</figure>


<!-- Demand Curve   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/7a1f445af14b01a553c20783d7631baead44e57a" alt="Demand Curve" style="width:50%">
  <figcaption>Fig.X -  Demand Curve We can use the demand curve to identify how much consumers would buy at any given price.</figcaption>
</figure>


<!-- Demand Curve with Income Increase  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/e03ad357b0f3dac3c54940c216906b99fec36263" alt="Demand Curve with Income Increase " style="width:50%">
  <figcaption>Fig.X -  Demand Curve with Income Increase With an increase in income, consumers will purchase larger quantities, pushing demand to the right.</figcaption>
</figure>


<!-- Demand Curve Shifted Right   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/a585d748d13ffb41a35c44fb859412e4f6c1a816" alt="Demand Curve Shifted Right " style="width:50%">
  <figcaption>Fig.X -  Demand Curve Shifted Right With an increase in income, consumers will purchase larger quantities, pushing demand to the right, and causing the demand curve to shift right.</figcaption>
</figure>


<!-- Factors That Shift Demand Curves   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/65c17584fc369f62c1977f893fd35d1abd563538" alt="Factors That Shift Demand Curves" style="width:50%">
  <figcaption>Fig.X -  Factors That Shift Demand Curves (a) A list of factors that can cause an increase in demand from D0 to D1. (b) The same factors, if their direction is reversed, can cause a decrease in demand from D0 to D1.</figcaption>
</figure>


### Shift in supply

<!-- Shifts in Supply: A Car Example   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/11a4586c6228af210af8f3a8bbdcebca25d7ea4f" alt="Shifts in Supply: A Car Example" style="width:50%">
  <figcaption>Fig.X -  Shifts in Supply: A Car Example Decreased supply means that at every given price, the quantity supplied is lower, so that the supply curve shifts to the left, from S0 to S1. Increased supply means that at every given price, the quantity supplied is higher, so that the supply curve shifts to the right, from S0 to S2.</figcaption>
</figure>


<!-- Supply Curve   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/1899990f90f0ad5efc0f580b649a618ae9b02af5" alt="Supply Curve" style="width:50%">
  <figcaption>Fig.X -  Supply Curve You can use a supply curve to show the minimum price a firm will accept to produce a given quantity of output.</figcaption>
</figure>


<!-- Setting Prices    -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/7e9369217e462de874c03fc34677917b564933d7" alt="Setting Prices " style="width:50%">
  <figcaption>Fig.X - Setting Prices The cost of production and the desired profit equal the price a firm will set for a product.</figcaption>
</figure>


<!--  Increasing Costs Leads to Increasing Price    -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/472e2358a4cfe78a26e23af812d1470445c13e1f" alt=" Increasing Costs Leads to Increasing Price" style="width:50%">
  <figcaption>Fig.X -  Increasing Costs Leads to Increasing Price Because the cost of production and the desired profit equal the price a firm will set for a product, if the cost of production increases, the price for the product will also need to increase.</figcaption>
</figure>


<!--  Supply Curve Shifts  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/e0f2ca47dbc3ca8f0943d4a3edf33e985f2a33ed" alt="Supply Curve Shifts" style="width:50%">
  <figcaption>Fig.X - Supply Curve Shifts When the cost of production increases, the supply curve shifts upwardly to a new price level.</figcaption>
</figure>


<!-- Factors That Shift Supply Curves   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/498e7d7cbc00a885faee08544c39cc295ac72c07" alt="Factors That Shift Supply Curves " style="width:50%">
  <figcaption>Fig.X - Factors That Shift Supply Curves (a) A list of factors that can cause an increase in supply from S0 to S1. (b) The same factors, if their direction is reversed, can cause a decrease in supply from S0 to S1. </figcaption>
</figure>


### Changes in equilibrium price

<!-- Good Weather for Salmon Fishing: The Four-Step Process   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/60b9a29b260b77e9a91e1ec966d41109f13bfe1f" alt="Good Weather for Salmon Fishing: The Four-Step Process" style="width:50%">
  <figcaption>Fig.X - Good Weather for Salmon Fishing: The Four-Step Process Unusually good weather leads to changes in the price and quantity of salmon.</figcaption>
</figure>


<!-- The Print News Market: A Four-Step Analysis   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/9490d677e30ecfd05e5a684e2b14eb022a5e2fc3" alt="The Print News Market: A Four-Step Analysis" style="width:50%">
  <figcaption>Fig.X -  The Print News Market: A Four-Step Analysis A change in tastes from print news sources to digital sources results in a leftward shift in demand for the former. The result is a decrease in both equilibrium price and quantity.</figcaption>
</figure>


<!--  Higher Compensation for Postal Workers: A Four-Step Analysis   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/05073030c65fdcc05e9705f210261b445ec69bbc" alt=" Higher Compensation for Postal Workers: A Four-Step Analysis" style="width:50%">
  <figcaption>Fig.X - Higher Compensation for Postal Workers: A Four-Step Analysis (a) Higher labor compensation causes a leftward shift in the supply curve, a decrease in the equilibrium quantity, and an increase in the equilibrium price. (b) A change in tastes away from Postal Services causes a leftward shift in the demand curve, a decrease in the equilibrium quantity, and a decrease in the equilibrium price.</figcaption>
</figure>


<!--  Combined Effect of Decreased Demand and Decreased Supply    -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/402c09fa371559c3c38d5545bdd136173a0e2f87" alt="Combined Effect of Decreased Demand and Decreased Supply " style="width:50%">
  <figcaption>Fig.X - Combined Effect of Decreased Demand and Decreased Supply Supply and demand shifts cause changes in equilibrium price and quantity.</figcaption>
</figure>


<!--  Shifts of Demand or Supply versus Movements along a Demand or Supply Curve -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/1f4d706301bc3e5b60dc501a6fa1ca5ab9f024eb" alt="Shifts of Demand or Supply versus Movements along a Demand or Supply Curve" style="width:50%">
  <figcaption>Fig.X -  Shifts of Demand or Supply versus Movements along a Demand or Supply Curve A shift in one curve never causes a shift in the other curve. Rather, a shift in one curve causes a movement along the second curve.</figcaption>
</figure>

### Apply demand and supply models to analyze prices and quantities

<!--  Demand and Supply Curves -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/57c1e6c5d8548e4b0d193854c7b78d7b2b61114e" alt="Demand and Supply Curves" style="width:50%">
  <figcaption>Fig.X -   Demand and Supply Curves The figure displays a generic demand and supply curve. The horizontal axis shows the different measures of quantity: a quantity of a good or service, a quantity of labor for a given job, or a quantity of financial capital. The vertical axis shows a measure of price: the price of a good or service, the wage in the labor market, or the rate of return (like the interest rate) in the financial market. We can use the demand and supply curves explain how economic events will cause changes in prices, wages, and rates of return.</figcaption>
</figure>

<!--  Impact of Increasing Demand for Nurses 2020–2030  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/fa00e3d9bba2f37a1561388f37290d3fc9a25e9d" alt="Impact of Increasing Demand for Nurses 2020–2030 " style="width:50%">
  <figcaption>Fig.X - Impact of Increasing Demand for Nurses 2020–2030 In 2020, the median salary for nurses was $75,330. As demand for services increases, the demand curve shifts to the right (from D0 to D1) and the equilibrium quantity of nurses increases from Qe0 to Qe1. The equilibrium salary increases from Pe0 to Pe1.</figcaption>
</figure>

<!--   Impact of Decreasing Supply of Nurses between 2020 and 2030  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/0d30879730d653f4acb8eb7d9aca3fa007ea2fec" alt=" Impact of Decreasing Supply of Nurses between 2020 and 2030" style="width:50%">
  <figcaption>Fig.X -  Impact of Decreasing Supply of Nurses between 2020 and 2030 The increase in demand for nurses shown in Figure 4.10 leads to both higher prices and higher quantities demanded. As nurses retire from the work force, the supply of nurses decreases, causing a leftward shift in the supply curve and higher salaries for nurses at Pe2. The net effect on the equilibrium quantity of nurses is uncertain, which in this representation is less than Qe1, but more than the initial Qe0.</figcaption>
</figure>



## Chapter 11: Price elasticity of demand

- perfectly price elastic
- perfectly price inelastic
- price discrimination
- price elastic demand
- price elasticity of demand (PED)
- price inelastic demand
- profit
- sales revenue
- unitary price elasticity of demand

<!--   On-Demand Media Pricing  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/d12176907a8d98b800db4a2317e8cd9d1e108bde" alt="On-Demand Media Pricing" style="width:50%">
  <figcaption>Fig.X - On-Demand Media Pricing Many on-demand Internet streaming media providers, such as Netflix, have introduced tiered pricing for levels of access to services, begging the question, how will these prices affect buyer’s purchasing choices? (Credit: modification of “160906_FF_CreditCardAgreements” by kdiwavvou/Flickr, Public Domain)</figcaption>
</figure>

<!--  Calculating the Price Elasticity of Demand -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/b5d88d1502a1f5326699d34bd79701f0ea6878e9" alt="Calculating the Price Elasticity of Demand" style="width:50%">
  <figcaption>Fig.X - Calculating the Price Elasticity of Demand We calculate the price elasticity of demand as the percentage change in quantity divided by the percentage change in price.</figcaption>
</figure>

<!-- Price Elasticity of Supply -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/9e2781c68f0707c6f7fce8bdae829f3965b904e3" alt="Price Elasticity of Supply" style="width:50%">
  <figcaption>Fig.X - Price Elasticity of Supply We calculate the price elasticity of supply as the percentage change in quantity divided by the percentage change in price.</figcaption>
</figure>

<!--  Infinite Elasticity -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/75e75c9367579225bb31ff1719c693add22895ca" alt=" Infinite Elasticity" style="width:50%">
  <figcaption>Fig.X -  Infinite Elasticity The horizontal lines show that an infinite quantity will be demanded or supplied at a specific price. This illustrates the cases of a perfectly (or infinitely) elastic demand curve and supply curve. The quantity supplied or demanded is extremely responsive to price changes, moving from zero for prices close to P to infinite when prices reach P.</figcaption>
</figure>

<!-- Zero Elasticity -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/ff50743942da09db765fc483531fcfdbc0313613" alt="Zero Elasticity" style="width:50%">
  <figcaption>Fig.X - Zero Elasticity The vertical supply curve and vertical demand curve show that there will be zero percentage change in quantity (a) demanded or (b) supplied, regardless of the price.</figcaption>
</figure>

<!-- A Constant Unitary Elasticity Demand Curve -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/9e571c5725473c6f41e69ab78ebe5cba674d1707" alt="A Constant Unitary Elasticity Demand Curve" style="width:50%">
  <figcaption>Fig.X -  A Constant Unitary Elasticity Demand Curve A demand curve with constant unitary elasticity will be a curved line. Notice how price and quantity demanded change by an identical percentage amount between each pair of points on the demand curve.</figcaption>
</figure>

<!--  A Constant Unitary Elasticity Supply Curve -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/88a69ad2b37ade499271f0d4c3f886f27b0301cb" alt=" A Constant Unitary Elasticity Supply Curve" style="width:50%">
  <figcaption>Fig.X - A Constant Unitary Elasticity Supply Curve A constant unitary elasticity supply curve is a straight line reaching up from the origin. Between each pair of points, the percentage increase in quantity supplied is the same as the percentage increase in price.</figcaption>
</figure>

<!--  Passing along Cost Savings to Consumers -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/275f0f84b10779198a2cbf3e4dbcf8c303242404" alt=" Passing along Cost Savings to Consumers" style="width:50%">
  <figcaption>Fig.X -  Passing along Cost Savings to Consumers Cost-saving gains cause supply to shift out to the right from S0 to S1; that is, at any given price, firms will be willing to supply a greater quantity. If demand is inelastic, as in (a), the result of this cost-saving technological improvement will be substantially lower prices. If demand is elastic, as in (b), the result will be only slightly lower prices. Consumers benefit in either case, from a greater quantity at a lower price, but the benefit is greater when demand is inelastic, as in (a).</figcaption>
</figure>

<!-- Passing along Higher Costs to Consumers -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/02911944317a54d9546fc07f565f147f1af7462f" alt="Passing along Higher Costs to Consumers" style="width:50%">
  <figcaption>Fig.X - Passing along Higher Costs to Consumers Higher costs, like a higher tax on cigarette companies for the example we gave in the text, lead supply to shift to the left. This shift is identical in (a) and (b). However, in (a), where demand is inelastic, companies largely can pass the cost increase along to consumers in the form of higher prices, without much of a decline in equilibrium quantity. In (b), demand is elastic, so the shift in supply results primarily in a lower equilibrium quantity. Consumers do not benefit in either case, but in (a), they pay a higher price for the same quantity, while in (b), they must buy a lower quantity (and presumably needing to shift their consumption elsewhere).</figcaption>
</figure>

<!-- Elasticity and Tax Incidence -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/b08cfd88b4611466a9bc7e9b82e0990e7d985304" alt="Elasticity and Tax Incidence" style="width:50%">
  <figcaption>Fig.X -  Elasticity and Tax Incidence An excise tax introduces a wedge between the price paid by consumers (Pc) and the price received by producers (Pp). The vertical distance between Pc and Pp is the amount of the tax per unit. Pe is the equilibrium price prior to introduction of the tax. (a) When the demand is more elastic than supply, the tax incidence on consumers Pc – Pe is lower than the tax incidence on producers Pe – Pp. (b) When the supply is more elastic than demand, the tax incidence on consumers Pc – Pe is larger than the tax incidence on producers Pe – Pp. The more elastic the demand and supply curves, the lower the tax revenue. </figcaption>
</figure>

<!-- How a Shift in Supply Can Affect Price or Quantity -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/992148af109c6342c9339b2572791461d0aa0dd0" alt="How a Shift in Supply Can Affect Price or Quantity" style="width:50%">
  <figcaption>Fig.X - How a Shift in Supply Can Affect Price or Quantity The intersection (E0) between demand curve D and supply curve S0 is the same in both (a) and (b). The shift of supply to the left from S0 to S1 is identical in both (a) and (b). The new equilibrium (E1) has a higher price and a lower quantity than the original equilibrium (E0) in both (a) and (b). However, the shape of the demand curve D is different in (a) and (b), being more elastic in (b) than in (a). As a result, the shift in supply can result either in a new equilibrium with a much higher price and an only slightly smaller quantity, as in (a), with more inelastic demand, or in a new equilibrium with only a small increase in price and a relatively larger reduction in quantity, as in (b), with more elastic demand. </figcaption>
</figure>

## Chapter 12: Price elasticity of supply

- price elastic supply
- price elasticity of supply (PES)
- price inelastic supply
- stocks
- unitary price elasticity of supply

## Chapter 13: Market economic system

- economic system
- market economy
- mixed economy
- planned economy
- private sector
- public sector

<!-- A Command Economy -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/2a6af7aa6dce6a6e2f047d2d2835a506618adea4" alt="A Command Economy" style="width:50%">
  <figcaption>Fig.X - A Command Economy Ancient Egypt was an example of a command economy. (Credit: "Pyramids at Giza" by Jay Bergesen/Flickr Creative Commons, CC BY 2.0)</figcaption>
</figure>


<!-- A Market Economy -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/156ff3bfad3dafb431ef8afb9c61a2248e7df9b1" alt="A Market Economy" style="width:50%">
  <figcaption>Fig.X - A Market Economy Nothing says “market” more than The New York Stock Exchange. (Credit: work by Erik Drost/Flickr Creative Commons, CC BY 2.0)</figcaption>
</figure>

<!-- Index of Economic Freedom -->
[2023 Index of Economic Freedom - Interactive Heat Map](https://www.heritage.org/index/heatmap)

 <figure>
  <img src="../images/index-of-economic-freedom-heatmap-2023.png" alt="Index of Economic Freedom" style="width:50%">
  <figcaption>Fig.X - 2023 Index of Economic Freedom - Heat Map</figcaption>
</figure>



## Chapter 14: Market failure

- demerit goods
- external benefits
- external costs
- externalities (spillover effects)
- free riders
- market failure
- merit goods
- private benefits
- private costs
- public goods
- social benefits
- social costs

## Chapter 15: Mixed economic system

- direct provision
- maximum price
- minimum price
- mixed economy
- nationalisation
- privatisation
- rules and regulations
- subsidy
- taxation

<!-- A Price Ceiling Example—Rent Control  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/3ab9f77c4631ab9876a0da3842ff58365f69257f" alt="A Price Ceiling Example—Rent Control " style="width:50%">
  <figcaption>Fig.X -  A Price Ceiling Example—Rent Control The original intersection of demand and supply occurs at E0. If demand shifts from D0 to D1, the new equilibrium would be at E1—unless a price ceiling prevents the price from rising. If the price is not permitted to rise, the quantity supplied remains at 15,000. However, after the change in demand, the quantity demanded rises to 19,000, resulting in a shortage.</figcaption>
</figure>

<!-- European Wheat Prices: A Price Floor Example  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/88d49e5ca66783c0b7a3543a10b0ec657ce53a80" alt="European Wheat Prices: A Price Floor Example" style="width:50%">
  <figcaption>Fig.X -  European Wheat Prices: A Price Floor Example The intersection of demand (D) and supply (S) would be at the equilibrium point E0. However, a price floor set at Pf holds the price above E0 and prevents it from falling. The result of the price floor is that the quantity supplied Qs exceeds the quantity demanded Qd. There is excess supply, also called a surplus.</figcaption>
</figure>


## Chapter 16: Money and banking

- bank deposits
- bartering
- cash
- central bank
- commercial banks
- functions of money
- money
- stock exchange

<!-- Banks as Financial Intermediaries  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/65df689e54aec99608ea873c17fd242a73f3b5b0" alt="Banks as Financial Intermediaries" style="width:50%">
  <figcaption>Fig.X -  Banks as Financial Intermediaries Banks are a financial intermediary because they stand between savers and borrowers. Savers place deposits with banks, and then receive interest payments and withdraw money. Borrowers receive loans from banks, and repay the loans with interest.</figcaption>
</figure>

<!--  Interest Rates on Six-Month, One-Year, and Five-Year Certificates of Deposit   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/e6be32ba607297718e6ec1a7ff4ea339789ce20c" alt=" Interest Rates on Six-Month, One-Year, and Five-Year Certificates of Deposit " style="width:50%">
  <figcaption>Fig.X -  Interest Rates on Six-Month, One-Year, and Five-Year Certificates of Deposit The interest rates on certificates of deposit have fluctuated over time. The high interest rates of the early 1980s are indicative of the relatively high inflation rate in the United States at that time. Interest rates fluctuate with the business cycle, typically increasing during expansions and decreasing during a recession. Note the steep decline in CD rates since 2008, the beginning of the Great Recession.</figcaption>
</figure>

<!--  Interest Rates for Corporate Bonds and Ten-Year U.S. Treasury Bonds  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/903d4a4e0419a9b82ff9b96de11331291ac9748a" alt=" Interest Rates for Corporate Bonds and Ten-Year U.S. Treasury Bonds" style="width:50%">
  <figcaption>Fig.X - Interest Rates for Corporate Bonds and Ten-Year U.S. Treasury Bonds The interest rates for corporate bonds and U.S. Treasury bonds (officially “notes”) rise and fall together, depending on conditions for borrowers and lenders in financial markets for borrowing. The corporate bonds always pay a higher interest rate, to make up for the higher risk they have of defaulting compared with the U.S. government.</figcaption>
</figure>

<!-- The Dow Jones Industrial Index and the Standard & Poor’s 500, 1965–2021 -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/ab0e6d5804fd67204635067c8801942fa682bbf1" alt="The Dow Jones Industrial Index and the Standard & Poor’s 500, 1965–2021" style="width:50%">
  <figcaption>Fig.X - The Dow Jones Industrial Index and the Standard & Poor’s 500, 1965–2021 Stock prices rose dramatically from the 1980s up to about 2000. From 2000 to 2013, stock prices bounced up and down, but ended up at about the same level. Since 2009, both indexes have for the most part increased.</figcaption>
</figure>

<!-- Cowrie Shell or Money? -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/a53ce397f0c16020247ea333dee61e9af45cf899" alt="Cowrie Shell or Money?" style="width:50%">
  <figcaption>Fig.X - Cowrie Shell or Money? Is this an image of a cowrie shell or money? The answer is: Both. For centuries, people used the extremely durable cowrie shell as a medium of exchange in various parts of the world. (Credit: modification of “Cowry Shell (Cypraeidae)” by Silke Baron/Flickr Creative Commons, CC BY 2.0)</figcaption>
</figure>

<!-- A Silver Certificate and a Modern U.S. Bill -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/5d0bade81d7fbf72619dbe3ada2a56c277f1a5b1" alt="A Silver Certificate and a Modern U.S. Bill" style="width:50%">
  <figcaption>Fig.X -  A Silver Certificate and a Modern U.S. Bill Until 1958, silver certificates were commodity-backed money—backed by silver, as indicated by the words “Silver Certificate” printed on the bill. Today, The Federal Reserve backs U.S. bills, but as fiat money (inconvertible paper money made legal tender by a government decree). (Credit: "One Dollar Bills" by “The.Comedian”/Flickr Creative Commons, CC BY 2.0)</figcaption>
</figure>

[Finance: The History of Money (video 11min)](https://www.youtube.com/watch?v=YCN2aTlocOw)

<!-- The Relationship between M1 and M2 Money -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/4a6d20035187eebdf301afc64af7c17408da03b3" alt="The Relationship between M1 and M2 Money" style="width:50%">
  <figcaption>Fig.X - The Relationship between M1 and M2 Money M1 and M2 money have several definitions, ranging from narrow to broad. M1 = coins and currency in circulation + checkable (demand) deposit + savings deposits. M2 = M1 + money market funds + certificates of deposit + other time deposits.</figcaption>
</figure>

[Sweden's Cashless Experiment: Is It Too Much Too Fast? (article and audio 4min)](https://www.npr.org/2019/02/11/691334123/swedens-cashless-experiment-is-it-too-much-too-fast)

<!--  A Run on the Bank -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/f99e5d234266ef45aa83648013300fad2577c595" alt="A Run on the Bank" style="width:50%">
  <figcaption>Fig.X -  A Run on the Bank Bank runs during the Great Depression only served to worsen the economic situation. (Credit: “Depression: "Runs on Banks” by National Archives and Records Administration, Public Domain)</figcaption>
</figure>


[Monetary Policy (U.S. Federal Reserve System)](https://www.federalreserve.gov/monetarypolicy.htm)


## Chapter 17: Household

- bad debts
- borrowing
- capital expenditure
- collateral
- conspicuous consumption
- consumer spending
- current expenditure
- disposable income
- dissaving
- income
- mortgage
- saving
- savings ratio
- wealth

## Chapter 18: Workers

- backward-bending supply of labour curve
- demand for labour
- derived demand
- division of labour
- equilibrium wage rate
- fringe benefits (perks)
- geographical mobility of labour
- labour force participation rate
- national minimum wage
- non-wage factors
- occupational mobility of labour
- piece rate
- productivity of labour
- salaries
- specialisation
- supply of labour
- wage determination
- wage factors
- wages


<!-- Labor market  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/ef0ab9fad84f8a6dd7dac20cd751abb6498b20a9" alt="Labor market" style="width:50%">
  <figcaption>Fig.X -  People often think of demand and supply in relation to goods, but labor markets, such as the nursing profession, can also apply to this analysis. (Credit: modification of "Hospital do Subúrbio" by Jaques Wagner Governador/Flickr Creative Commons, CC BY 2.0)</figcaption>
</figure>


<!-- Labor Market Example: Demand and Supply for Nurses in Minneapolis-St. Paul-Bloomington  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/c0b696ebdb7d2f68fcccb751c0cdf327af6b7b2b" alt="Labor Market Example: Demand and Supply for Nurses in Minneapolis-St. Paul-Bloomington" style="width:50%">
  <figcaption>Fig.X -   Labor Market Example: Demand and Supply for Nurses in Minneapolis-St. Paul-Bloomington The demand curve (D) of those employers who want to hire nurses intersects with the supply curve (S) of those who are qualified and willing to work as nurses at the equilibrium point (E). The equilibrium salary is $85,000 and the equilibrium quantity is 41,000 nurses. At an above-equilibrium salary of $90,000, quantity supplied increases to 45,000, but the quantity of nurses demanded at the higher pay declines to 40,000. At this above-equilibrium salary, an excess supply or surplus of nurses would exist. At a below-equilibrium salary of $75,000, quantity supplied declines to 34,000, while the quantity demanded at the lower wage increases to 47,000 nurses. At this below-equilibrium salary, excess demand or a shortage exists.</figcaption>
</figure>


<!-- Technology and Wages: Applying Demand and Supply  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/2a8eaafaf4246abd72a0daff0618e9a050ea4ad6" alt="Technology and Wages: Applying Demand and Supply" style="width:50%">
  <figcaption>Fig.X - Technology and Wages: Applying Demand and Supply (a) The demand for low-skill labor shifts to the left when technology can do the job previously done by these workers. (b) New technologies can also increase the demand for high-skill labor in fields such as information technology and network administration.</figcaption>
</figure>


<!-- A Living Wage: Example of a Price Floor -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/502b6ba4c069fdd35983691d21d055bfc1e26496" alt="A Living Wage: Example of a Price Floor" style="width:50%">
  <figcaption>Fig.X - A Living Wage: Example of a Price Floor The original equilibrium in this labor market is a wage of $10/hour and a quantity of 1,200 workers, shown at point E. Imposing a wage floor at $12/hour leads to an excess supply of labor. At that wage, the quantity of labor supplied is 1,600 and the quantity of labor demanded is only 700.</figcaption>
</figure>


## Chapter 19: Trade unions

- collective bargaining
- craft union
- general union
- go-slow
- industrial action
- industrial union
- sit-in
- strike
- trade union
- white-collar union
- works-to-rule

## Chapter 20: Firms

- average costs
- conglomerate integration
- demerger
- diseconomies of scale
- economies of scale
- external economies of scale
- external growth
- franchise
- horizontal integration
- interdependence
- internal economies of scale
internal growth
- market share
- merger
- primary sector
- private sector
- public sector
- secondary sector

<!-- Firm: Amazon -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/9619a3d0cee4b5d07f633bf1493558b0b304b153" alt="Amazon" style="width:50%">
  <figcaption>Fig.X -  Amazon is an American international electronic commerce company that sells books, among many other things, shipping them directly to the consumer. Until recently there were no brick and mortar Amazon stores. (Credit: modification of “Amazon Prime Delivery Van (50072389511)” by Tony Webster/Wikimedia Commons, CC BY 2.0)</figcaption>
</figure>

<!-- Production: Pizza -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/790ffa059cd702eee36567b2091f18144e9adebb" alt="Pizza" style="width:50%">
  <figcaption>Fig.X - The production process for pizza includes inputs such as ingredients, the efforts of the pizza maker, and tools and materials for cooking and serving. (Credit: “Grilled gluten-free BBQ chicken pizza” by Keith McDuffee/Flickr, CC BY 2.0)</figcaption>
</figure>

<!-- Production: Lumberjacks -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/3b5bdb81b11ec7a8ea29c728d5d663f0079a623a" alt="Lumberjacks" style="width:50%">
  <figcaption>Fig.X - Production in the short run may be explored through the example of lumberjacks using a two-person saw. (Credit: “DO - Apple Day Civilian Conservation Corps Demonstration Crosscut Saw (Gladden)” by Virginia State Parks/Flickr, CC BY 2.0)</figcaption>
</figure>

<!-- Production: Lumberjacks and the Law of Diminishing Marginal Product -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/22448d1895bea2a28dd42da43bcebf0a4b407c07" alt="Lumberjacks: Law of Diminishing Marginal Product" style="width:50%">
  <figcaption>Fig.X - Law of Diminishing Marginal Product</figcaption>
</figure>

<!-- How Output Affects Total Costs -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/70b0bce117ef73da731f6cc2642dd51df5d2c5ac" alt="How Output Affects Total Costs" style="width:50%">
  <figcaption>Fig.X -  How Output Affects Total Costs At zero production, the fixed costs of $160 are still present. As production increases, variable costs are added to fixed costs, and the total cost is the sum of the two.</figcaption>
</figure>

<!-- Cost Curves at the Clip Joint -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/5a1250aa0bdd6e14b156f81a7bac0d0bdf8efcef" alt="Cost Curves at the Clip Joint" style="width:50%">
  <figcaption>Fig.X - Cost Curves at the Clip Joint We can also present the information on total costs, fixed cost, and variable cost on a per-unit basis. We calculate average total cost (ATC) by dividing total cost by the total quantity produced. The average total cost curve is typically U-shaped. We calculate average variable cost (AVC) by dividing variable cost by the quantity produced. The average variable cost curve lies below the average total cost curve and is also typically U-shaped. We calculate marginal cost (MC) by taking the change in total cost between two levels of output and dividing by the change in output. The marginal cost curve is upward-sloping.</figcaption>
</figure>

<!-- Economies of Scale -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/a93be10c584b33f8414dec662b3000e8f896932a" alt="Economies of Scale" style="width:50%">
  <figcaption>Fig.X - Economies of Scale A small factory like S produces 1,000 alarm clocks at an average cost of $12 per clock. A medium factory like M produces 2,000 alarm clocks at a cost of $8 per clock. A large factory like L produces 5,000 alarm clocks at a cost of $4 per clock. Economies of scale exist when the larger scale of production leads to lower average costs.</figcaption>
</figure>

<!--  From Short-Run Average Cost Curves to Long-Run Average Cost Curves -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/668289b1dec2012250f8ec43fbe736795bee0f94" alt=" From Short-Run Average Cost Curves to Long-Run Average Cost Curves" style="width:50%">
  <figcaption>Fig.X - From Short-Run Average Cost Curves to Long-Run Average Cost Curves The five different short-run average cost (SRAC) curves each represents a different level of fixed costs, from the low level of fixed costs at SRAC1 to the high level of fixed costs at SRAC5. Other SRAC curves, not in the diagram, lie between the ones that are here. The long-run average cost (LRAC) curve shows the lowest cost for producing each quantity of output when fixed costs can vary, and so it is formed by the bottom edge of the family of SRAC curves. If a firm wished to produce quantity Q3, it would choose the fixed costs associated with SRAC3.</figcaption>
</figure>

<!--  The LRAC Curve and the Size and Number of Firms -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/c9d6991f95b6c1e230143fe82ed0596fdaf543c1" alt="The LRAC Curve and the Size and Number of Firms" style="width:50%">
  <figcaption>Fig.X - The LRAC Curve and the Size and Number of Firms (a) Low-cost firms will produce at output level R. When the LRAC curve has a clear minimum point, then any firm producing a different quantity will have higher costs. In this case, a firm producing at a quantity of 10,000 will produce at a lower average cost than a firm producing, say, 5,000 or 20,000 units. (b) Low-cost firms will produce between output levels R and S. When the LRAC curve has a flat bottom, then firms producing at any quantity along this flat bottom can compete. In this case, any firm producing a quantity between 5,000 and 20,000 can compete effectively, although firms producing less than 5,000 or more than 20,000 would face higher average costs and be unable to compete.</figcaption>
</figure>

## Chapter 21: Firms and production

- capital-intensive
- derived demand
- innovation
- labour productivity
- labour-intensive
- production
- productivity

## Chapter 22: Firms' costs, revenue and objectives

- average fixed cost
- average revenue
- average total cost
- average variable cost
- costs of production
- fixed costs
- objectives
- profit
- profit maximisation
- sales revenue
- total cost
- total revenue
- variable costs

## Chapter 23: Market structure

- barriers to entry
- competitive markets
- market structure
- monopoly
- price maker [price setter]
- price takers

<!-- The Spectrum of Competition -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/320100f958f814fa7b782cc9cc42b6a4856e2bcd" alt="The Spectrum of Competition" style="width:50%">
  <figcaption>Fig.X - The Spectrum of Competition Firms face different competitive situations. At one extreme—perfect competition—many firms are all trying to sell identical products. At the other extreme—monopoly—only one firm is selling the product, and this firm faces no competition. Monopolistic competition and oligopoly fall between the extremes of perfect competition and monopoly. Monopolistic competition is a situation with many firms selling similar, but not identical products. Oligopoly is a situation with few firms that sell identical or similar products.</figcaption>
</figure>

<!-- Soybean -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/eca531ae918b4263366ff37e44dc2e0cb41d2c6a" alt="Soybean" style="width:50%">
  <figcaption>Fig.X - Depending on the competition and prices offered, a soybean farmer may choose to grow a different crop. (Credit: modification “Agronomist & Farmer Inspecting Weeds” by United Soybean Board/Flickr, CC BY 2.0)</figcaption>
</figure>


[Current value of various commodities](https://edition.cnn.com/business/markets/commodities)

<!-- Total Cost and Total Revenue at the Raspberry Farm -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/15d35d83b9faab368acc229f86372c953b2ba69d" alt="Total Cost and Total Revenue at the Raspberry Farm" style="width:50%">
  <figcaption>Fig.X -  Total Cost and Total Revenue at the Raspberry Farm Total revenue for a perfectly competitive firm is a straight line sloping up. The slope is equal to the price of the good. Total cost also slopes up, but with some curvature. At higher levels of output, total cost begins to slope upward more steeply because of diminishing marginal returns. The maximum profit will occur at the quantity where the difference between total revenue and total cost is largest.</figcaption>
</figure>

<!-- Marginal Revenues and Marginal Costs at the Raspberry Farm: Individual Farmer -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/342c46971d030c95dabe4f39c0d2455ab0a795ff" alt="Marginal Revenues and Marginal Costs at the Raspberry Farm: Individual Farmer" style="width:50%">
  <figcaption>Fig.X - Marginal Revenues and Marginal Costs at the Raspberry Farm: Individual Farmer For a perfectly competitive firm, the marginal revenue (MR) curve is a horizontal line because it is equal to the price of the good, which is determined by the market, as Figure 8.4 illustrates. The marginal cost (MC) curve is sometimes initially downward-sloping, if there is a region of increasing marginal returns at low levels of output, but is eventually upward-sloping at higher levels of output as diminishing marginal returns kick in.</figcaption>
</figure>

<!-- Marginal Revenues and Marginal Costs at the Raspberry Farm: Individual Farmer -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/342c46971d030c95dabe4f39c0d2455ab0a795ff" alt="Marginal Revenues and Marginal Costs at the Raspberry Farm: Individual Farmer" style="width:50%">
  <figcaption>Fig.X - Marginal Revenues and Marginal Costs at the Raspberry Farm: Individual Farmer For a perfectly competitive firm, the marginal revenue (MR) curve is a horizontal line because it is equal to the price of the good, which is determined by the market, as Figure 8.4 illustrates. The marginal cost (MC) curve is sometimes initially downward-sloping, if there is a region of increasing marginal returns at low levels of output, but is eventually upward-sloping at higher levels of output as diminishing marginal returns kick in.</figcaption>
</figure>

<!-- Supply, Demand, and Equilibrium Price in the Market for Raspberries -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/9b6e25c75e91744b3e7f3ee08ad8a12dc9a1aa5a" alt="Supply, Demand, and Equilibrium Price in the Market for Raspberries" style="width:50%">
  <figcaption>Fig.X - Supply, Demand, and Equilibrium Price in the Market for Raspberries The equilibrium price of raspberries is determined through the interaction of market supply and market demand at $4.00.</figcaption>
</figure>

<!-- Price and Average Cost at the Raspberry Farm -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/60e61d0929ca2f692d2a93c5094c46d0cd957374" alt="Price and Average Cost at the Raspberry Farm" style="width:50%">
  <figcaption>Fig.X - Price and Average Cost at the Raspberry Farm In (a), price intersects marginal cost above the average cost curve. Since price is greater than average cost, the firm is making a profit. In (b), price intersects marginal cost at the minimum point of the average cost curve. Since price is equal to average cost, the firm is breaking even. In (c), price intersects marginal cost below the average cost curve. Since price is less than average cost, the firm is making a loss.</figcaption>
</figure>

<!-- The Shutdown Point for the Raspberry Farm -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/c38eb0206039354811ffa75e0cbfe1ea0063009b" alt="The Shutdown Point for the Raspberry Farm" style="width:50%">
  <figcaption>Fig.X - The Shutdown Point for the Raspberry Farm In (a), the farm produces at a level of 65. It is making losses of $47.50, but price is above average variable cost, so it continues to operate. In (b), total revenues are $90 and total cost is $165, for overall losses of $75. If the farm shuts down, it must pay only its fixed costs of $62. Shutting down is preferable to selling at a price of $1.50 per pack.</figcaption>
</figure>

<!-- Profit, Loss, Shutdown -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/a1db3550f70c2bfd31e5f23aba9e773adfdc9634" alt="Profit, Loss, Shutdown" style="width:50%">
  <figcaption>Fig.X - Profit, Loss, Shutdown We can divide the marginal cost curve into three zones, based on where it is crossed by the average cost and average variable cost curves. We call the point where MC crosses AC the break even point. If the firm is operating where the market price is at a level higher than the break even point, then price will be greater than average cost and the firm is earning profits. If the price is exactly at the break even point, then the firm is making zero profits. If price falls in the zone between the shutdown point and the break even point, then the firm is making losses but will continue to operate in the short run, since it is covering its variable costs, and more if price is above the shutdown-point price. However, if price falls below the price at the shutdown point, then the firm will shut down immediately, since it is not even covering its variable costs.</figcaption>
</figure>


[Video that addresses how drought in the United States can impact food prices across the world.](https://www.pbs.org/video/pbs-newshour-food-prices-rise-across-the-us-due-to-drought/)

find a similar video for the Ukraine war and the price of grain across the world


<!--  Political Power from a Cotton Monopoly -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/d8454a78d655e62fafed33d1dcd3d4243b66907d" alt=" Political Power from a Cotton Monopoly" style="width:50%">
  <figcaption>Fig.X - Political Power from a Cotton Monopoly In the mid-nineteenth century, the United States, specifically the Southern states, had a near monopoly in the cotton that they supplied to Great Britain. These states attempted to leverage this economic power into political power—trying to sway Great Britain to formally recognize the Confederate States of America. (Credit: modification of "cotton!" by ashley/Flickr, CC BY 2.0)</figcaption>
</figure>

<!--  Economies of Scale and Natural Monopoly -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/d114b8a9c86f3f924bbdea622fb48c43e53fab4d" alt="Economies of Scale and Natural Monopoly" style="width:50%">
  <figcaption>Fig.X -  Economies of Scale and Natural Monopoly In this market, the demand curve intersects the long-run average cost (LRAC) curve at its downward-sloping part. A natural monopoly occurs when the quantity demanded is less than the minimum quantity it takes to be at the bottom of the long-run average cost curve.</figcaption>
</figure>

<!--  The Perceived Demand Curve for a Perfect Competitor and a Monopolist -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/667320caef202e61dccce159a45557c97cddc7e3" alt="The Perceived Demand Curve for a Perfect Competitor and a Monopolist" style="width:50%">
  <figcaption>Fig.X -   The Perceived Demand Curve for a Perfect Competitor and a Monopolist (a) A perfectly competitive firm perceives the demand curve that it faces to be flat. The flat shape means that the firm can sell either a low quantity (Ql) or a high quantity (Qh) at exactly the same price (P). (b) A monopolist perceives the demand curve that it faces to be the same as the market demand curve, which for most goods is downward-sloping. Thus, if the monopolist chooses a high level of output (Qh), it can charge only a relatively low price (PI). Conversely, if the monopolist chooses a low level of output (Ql), it can then charge a higher price (Ph). The challenge for the monopolist is to choose the combination of price and quantity that maximizes profits.</figcaption>
</figure>





## Sources

- OpenStax : [Principles of Economics](https://openstax.org/books/principles-economics-3e) 
