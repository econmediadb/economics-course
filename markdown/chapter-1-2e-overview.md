
# 1. Le marché des biens et services

```mermaid
flowchart LR
  subgraph MARCHES-DES-BIENS-ET-SERVICES
    direction TB
    subgraph SCIENCES-ECO-3e
        direction TB
            A[L'économie en tant que <br> science sociale]-->B[Affirmations économiques <br> positives et normatives]
            B --> C[problème économique]
            C --> D[Frontières des <br> possibilités de production]
            D --> E[division du travail]
            E --> F[Économie de marché, <br> économie mixte et <br> économie planifiée]
    end
    direction LR
    subgraph MARCHES-2e
        direction TB
            c1[décision rationnelle]-->c2[demand et offre]
            c2-->c3[élasticité]
            c3-->c4[mécanisme des prix]
            c4 --> c5[surplus du <br> consommateur et <br> du producteur]
            c5 --> c6[impôts et subventions]
    end
    subgraph DEFAILLANCE-INTERVENTION-1e
        direction TB
            b1[défaillance du marché]-->b2[intervention publique]
    end
  end
  SCIENCES-ECO-3e --> MARCHES-2e
  MARCHES-2e --> DEFAILLANCE-INTERVENTION-1e
```


## 1.1. Les sciences économiques


Les notions suivantes seront acquises en **3e**. Ces notions sont requises pour l'avancement aux sections 1.2. et 1.3.

1. L'économie en tant que *science sociale*
    1. Raisonner comme un économiste : le processus de développement de modèles en économie, y compris la nécessité de faire des hypothèses.
    2. L'utilisation de l'hypothèse *ceteris paribus* dans la construction de modèles
    3. L'incapacité de l'économie à réaliser des expériences scientifiques.

2. Affirmations économiques *positives* et *normatives*
    1. Distinction entre les énoncés économiques positifs et normatifs
    2. Le rôle des jugements de valeur dans l'influence des décisions et des politiques économiques

3. Le problème économique
    1. Le problème de la *rareté* - lorsque les besoins sont illimités et les ressources finies
    2. La distinction entre ressources *renouvelables* et *non renouvelables*
    3. L'importance des *coûts d'opportunité* pour les agents économiques (consommateurs, producteurs et gouvernement).

4. *Frontières des possibilités de production*
    1. L'utilisation des frontières des possibilités de production pour représenter :
        - le potentiel productif maximal d'une économie 
        - le coût d'opportunité (par l'analyse marginale)
        - la croissance ou le déclin économique
        - l'allocation efficace ou inefficace des ressources
        - la production possible et non réalisable
    2. La distinction entre les mouvements le long des courbes des possibilités de production et les déplacements de ces courbes, en considérant les causes possibles de ces changements.
    3. La distinction entre les biens d'équipement et les biens de consommation

5. La spécialisation et la *division du travail*
    1. Spécialisation et division du travail : référence à Adam Smith
    2. Les avantages et les inconvénients de la spécialisation et de la division du travail dans l'organisation de la production
    3. Les avantages et les inconvénients de la spécialisation dans la production de biens et de services destinés au commerce
    4. Les fonctions de la monnaie (en tant que moyen d'échange, mesure de la valeur, réserve de valeur, méthode de paiement différé).

6. Économie de marché, économie mixte et économie planifiée
    1. La distinction entre économie de marché, économie mixte et économie dirigée : référence à Adam Smith, Friedrich Hayek et Karl Marx.
    2. Les avantages et les inconvénients de l'économie de marché et de l'économie dirigée.
    3. Le rôle de l'État dans une économie mixte


## 1.2. Le fonctionnement des marchés


```mermaid
flowchart LR
  subgraph MARCHES
    direction TB
    subgraph PRIX
        direction TB
            A[décision <br> rationnelle]-->B[demande <br> et <br> offre];
            B-->C[élasticité de la <br> demande et de l'offre];
            C-->F[détermination <br> des prix];
            F-->G[surplus du <br> consommateur et <br> producteur];
            G-->H[impôts indirects <br> et subventions];
            style F fill:#ceeeec,stroke:#333
            click A "https://www.core-econ.org/the-economy/book/fr/text/03.html" "Lien vers CORE-ECON pour la décision rationnelle"
            click B "https://www.core-econ.org/the-economy/book/fr/text/08.html" "Lien vers CORE-ECON pour la demande, offre et concurrence parfaite"
            click C "https://www.core-econ.org/the-economy/book/fr/text/07.html#78-%C3%A9lasticit%C3%A9-de-la-demande" "Lien vers CORE-ECON pour l'élasticité de la demande"
            click F "https://www.core-econ.org/the-economy/book/fr/text/08.html#84-offre-de-march%C3%A9-et-%C3%A9quilibre" "Lien vers CORE-ECON pour l'équilibre sur le marché"
            click G "https://www.core-econ.org/the-economy/book/fr/text/08.html#85-%C3%A9quilibre-concurrentiel-gains-tir%C3%A9s-de-l%C3%A9change-allocation-et-r%C3%A9partition" "Lien vers CORE-ECON pour le surplus du consommateur"
            click H "https://www.core-econ.org/the-economy/book/fr/text/08.html#87-effets-de-la-fiscalit%C3%A9" "Lien vers CORE-ECON pour l'effet des taxes"
    end
    direction LR
    subgraph CONCURRENCE
        direction TB
            c1[coût de <br> production]-->c2[preneur <br> de prix]
            c2-->c3[maximisation <br> du profit]
            c3-->c4[marchés concurrentiels]
            style c4 fill:#92dce7,stroke:#333
    end
    subgraph MONOPOLE
        direction TB
            b1[pouvoir <br> de marché]-->b2[barrière <br> à l'entrée]
            b2-->b3[monopole]
            b3-->b4[concurrence <br> imparfaite]
            style b4 fill:#92dce7,stroke:#333
    end
  end
  PRIX --> CONCURRENCE
  CONCURRENCE --> MONOPOLE
```



Les notions suivantes seront acquises en **2e**. Les notions acquises en 3e sont nécessaires pour la compréhension des nouvelles notions.

**KEYWORDS**
- décision rationnelle, maximiser l'utilité, maximiser les profits



**SANDBOX**
- [VIDEO: La concurrence](https://www.youtube.com/watch?v=ZYkbXe_lmBo)
- [VIDEO: Pas d'économie sans confiance!](https://www.youtube.com/watch?v=AO6171YCx-Q)


**ISSUES**
- When should we introduce market structure?
- When should we introduce all the consumption related topics? [SOURCE 1](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/text/07.html), [SOURCE 2](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/text/08.html), [SOURCE 3](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/text/09.html)


1. La prise de décision rationnelle
    1. Les hypothèses sous-jacentes de la prise de décision économique rationnelle :
        - les consommateurs cherchent à maximiser leur utilité 
        - les entreprises cherchent à maximiser leurs profits 

    - [SOURCE: décisions du consommateur et du producteur](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/text/02.html)
    - [SOURCE: maximisation du profit](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/text/01.html#15-comment-le-producteur-maximise-t-il-son-profit)


2. La demande
    1. La distinction entre les mouvements *le long d'une courbe* de demande et les *déplacements* d'une courbe de demande.
    2. Les facteurs qui peuvent provoquer un déplacement de la courbe de la demande (les conditions de la demande)
    3. Le concept d'utilité marginale décroissante et son influence sur la forme de la courbe de demande

    - [SOURCE: déplacement de la courbe, déplacement sur la courbe, effet d'une taxe forfaitaire](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/text/01.html#14-comment-interpr%C3%A9ter-les-variations-des-quantit%C3%A9s-offertes-et-des-quantit%C3%A9s-demand%C3%A9es)

3. Elasticités de la demande par rapport au prix, au revenu et à l'inverse <br>
    
    1. Compréhension de l'élasticité de la demande par rapport au prix, au revenu et à l'élasticité croisée.
    2. Utiliser des formules pour calculer les élasticités de prix, de revenu et les élasticités croisées de la demande.
    3. Interpréter les valeurs numériques de :
        - élasticité de la demande par rapport au prix : élastique unitaire, parfaitement et relativement élastique, et parfaitement et relativement inélastique
        - élasticité de la demande par rapport au revenu : produits inférieurs, normaux et de luxe ; relativement élastique et relativement inélastique
        - élasticité croisée de la demande : biens substituables, complémentaires et non liés.
    4. Les facteurs qui influencent les élasticités de la demande.
    5. La signification des élasticités de la demande pour les entreprises et le gouvernement en termes de :
        - l'imposition d'impôts indirects et de subventions
        - des changements dans le revenu réel
        - des changements dans les prix des biens de substitution et des biens complémentaires.
    6. La relation entre l'élasticité de la demande par rapport au prix et le revenu total (y compris le calcul).

    - [SOURCE: élasticité prix-demande](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/text/12.html#129-l%C3%A9lasticit%C3%A9-prix-demande)
    - [SOURCE: élasticité croisée](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/text/12.html#1210-l%C3%A9lasticit%C3%A9-crois%C3%A9e)
    - [SOURCE: définition, interprétation, exemple, élasticitié *en générale*](https://www.core-econ.org/the-economy/book/fr/text/leibniz-07-08-01.html)

4. L'offre
    1. La distinction entre les mouvements le long d'une courbe d'offre et les déplacements d'une courbe d'offre.
    2. Les facteurs qui peuvent provoquer un déplacement de la courbe d'offre (les conditions d'offre).

    - [SOURCE: prix-offre, prix-demande, offre-demande](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/text/01.html#13-comment-la-confrontation-de-loffre-et-la-demande-dun-bien-ou-dun-service-d%C3%A9termine-t-elle-l%C3%A9quilibre-sur-un-march%C3%A9-concurrentiel)
    - [SOURCE: déplacement de la courbe, déplacement sur la courbe, effet d'une taxe forfaitaire](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/text/01.html#14-comment-interpr%C3%A9ter-les-variations-des-quantit%C3%A9s-offertes-et-des-quantit%C3%A9s-demand%C3%A9es)

5. Elasticité de l'offre
    1. Compréhension de l'élasticité de l'offre par rapport au prix
    2. Utiliser une formule pour calculer l'élasticité de l'offre par rapport au prix
    3. Interpréter les valeurs numériques de l'élasticité de l'offre par rapport au prix :
        - parfaitement et relativement élastique, et parfaitement et relativement inélastique.
    4. Facteurs qui influencent l'élasticité de l'offre par rapport au prix
    5. La distinction entre le court terme et le long terme en économie et sa signification pour l'élasticité de l'offre.

6. Détermination des prix
    1. Prix et quantité d'équilibre et comment ils sont déterminés
    2. L'utilisation de diagrammes d'offre et de demande pour représenter l'offre et la demande excédentaires.
    3. Le fonctionnement des forces du marché pour éliminer la demande et l'offre excédentaires.
    4. Utilisation de diagrammes d'offre et de demande pour montrer comment les déplacements des courbes de demande et d'offre entraînent une modification du prix et de la quantité d'équilibre dans des situations réelles.

7. Le mécanisme des prix
    1. Fonctions du mécanisme des prix pour allouer les ressources : 
        - rationnement
        - incitation
        - signalisation
    2. Le mécanisme des prix dans le contexte de différents types de marchés, y compris les marchés locaux, nationaux et mondiaux.

8. Le surplus du consommateur et du producteur
    1. La distinction entre le surplus du consommateur et le surplus du producteur
    2. L'utilisation de diagrammes d'offre et de demande pour illustrer le surplus du consommateur et du producteur
    3. Comment les changements de l'offre et de la demande peuvent-ils affecter le surplus du consommateur et du producteur ?

    - [SOURCE: coût de production, coût marginal, courbe d'offre du producteur](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/text/01.html#15-comment-le-producteur-maximise-t-il-son-profit)

9. Impôts indirects et subventions
    1. Analyse de l'offre et de la demande, élasticités, et :
        - l'incidence des impôts indirects sur les consommateurs, les producteurs et le gouvernement
        - l'incidence des impôts indirects sur les consommateurs et les producteurs
        - l'impact des subventions sur les consommateurs, les producteurs et le gouvernement
        - la zone qui représente la subvention au producteur et la subvention au consommateur.

10. Vues alternatives du comportement des consommateurs
    1. Les raisons pour lesquelles les consommateurs peuvent ne pas avoir un comportement rationnel :
        - la prise en compte de l'influence du comportement d'autrui
        - l'importance du comportement habituel 
        - la faiblesse du consommateur en matière de calcul

## 1.3. La défaillance du marché

Les notions suivantes seront acquises en **1e**. Les notions acquises en 3e et 2e sont nécessaires pour la compréhension des nouvelles notions.


1. Types de défaillance du marché
    1. Compréhension de la défaillance du marché 
    2. Types de défaillance du marché
        - externalités
        - sous-production de biens publics 
        - lacunes en matière d'information

2. Externalités
    1. Distinction entre coûts privés, coûts externes et coûts sociaux
    2. Distinction entre les bénéfices privés, les bénéfices externes et les bénéfices sociaux.
    3. Utilisation d'un diagramme pour illustrer
        - les coûts externes de production à l'aide de l'analyse marginale
        - la distinction entre l'équilibre du marché et la position optimale sociale
        - l'identification de la zone de perte de bien-être 
    4. Utilisation d'un diagramme pour illustrer
        - les bénéfices externes de la consommation en utilisant l'analyse marginale
        - la distinction entre l'équilibre du marché et la position d'optimum social
        - l'identification de la zone de gain de bien-être
    5. L'impact sur les agents économiques des externalités et de l'intervention de l'État sur différents marchés.

3. Biens publics
    1. Distinction entre biens publics et biens privés à l'aide des concepts de non-rivalité et de non-exclusivité
    2. Pourquoi les biens publics ne peuvent-ils pas être fournis par le secteur privé : le problème du passager clandestin ?

4. Lacunes en matière d'information
    1. La distinction entre information symétrique et asymétrique
    2. Comment une information imparfaite sur le marché peut conduire à une mauvaise allocation des ressources.

## 1.4. Intervention publique

Les notions suivantes seront acquises en **1e**. Les notions acquises en 3e et 2e sont nécessaires pour la compréhension des nouvelles notions.

1. Intervention des pouvoirs publics sur les marchés
    1. Objectif de l'intervention par rapport à la défaillance du marché et utilisation de diagrammes dans différents contextes :
        - fiscalité indirecte (ad valorem et spécifique) 
        - subventions
        - prix maximum et minimum
    2. Autres méthodes d'intervention des pouvoirs publics : o échanges de permis de polluer
        - fourniture par l'État de biens publics
        - fourniture d'informations
        - réglementation

2. L'échec des gouvernements
    1. Compréhension de l'échec de l'État comme une intervention qui entraîne une perte nette de bien-être.
    2. Causes de la défaillance des pouvoirs publics :
        - distorsion des signaux de prix
        - conséquences involontaires
        - coûts administratifs excessifs o lacunes en matière d'information
    3. Défaillance de l'État dans les marchés variables






# Sources
- [A/AS Level Syllabus](https://qualifications.pearson.com/en/qualifications/edexcel-a-levels/economics-a-2015.html#%2Ftab-ASlevel)
- [Econofides: L'économie pour le Lycée](https://www.sciencespo.fr/department-economics/econofides/index.html)
- [Core-Econ - Econofides](https://www.sciencespo.fr/department-economics/econofides/)

TEST




