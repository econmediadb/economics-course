## 1.6. Le surplus du consommateur et du producteur

### 1.6.1. La notion de coût de production

Prenons l’exemple du marché des avions privés et imaginons une entreprise fictive qui produit des avions similaires à ceux des grandes marques, comme Cessna, Bombardier, Gulfstream ou encore Dassault. Nous appellerons cette entreprise Supers Jets. Si cette entreprise produit des avions identiques à ses concurrents, rappelez-vous qu’elle est preneuse de prix : elle ne peut pas vendre un avion, de la même gamme, à un prix supérieur à celui de ses concurrents.

Pensez aux coûts de production et de vente des avions. Tout d’abord, l’entreprise a besoin de mettre en œuvre un processus de recherche et développement pour concevoir son appareil. Elle a également besoin de locaux (une usine) équipés de machines pour la confection des appareils. Elle peut les louer à une autre entreprise ou rassembler le capital financier nécessaire pour investir dans ses propres locaux et équipements. Ensuite, elle doit acheter les matières premières et les composants, et rémunérer les salariés (des ouvriers notamment) qui font fonctionner les équipements. Elle a aussi besoin d’autres salariés (des cadres, des professions intermédiaires ou des employés) pour gérer le processus d’achat et de production ainsi que pour vendre les avions.

Pour produire, l’entreprise fait donc face d’une part à des coûts fixes qui ne dépendent pas de son activité ou encore des quantités produites, d’autre part à des coûts variables qui dépendent des quantités produites. Les coûts fixes sont donc à acquitter même si aucune quantité n’a été produite et aucun chiffre d’affaires n’a été réalisé.

Le coût total noté $CT$ se compose de l’ensemble des coûts supportés par l’entreprise.

Le coût moyen ($CM$) de production de Supers Jets correspond au coût total pour une quantité d’avions produits $CT$ divisé par cette quantité ($Q$).

$CM(Q) = \frac{CT(Q)}{Q}$

Le coût marginal ($Cm$) correspond au coût de production d’une unité supplémentaire (ici l’unité est l’avion) ; la quantité produite passe alors de $Q$ à $Q+1$.

$Cm(Q+1)=CT(Q + 1)−CT(Q)$




### 1.6.2. Quelle quantité le producteur décide-t-il de produire en situation de « preneur de prix » ?

Le profit total augmente tant que le coût marginal est inférieur au prix de marché, car le producteur réalise un profit marginal (le solde « prix de marché – coût marginal » est positif). À la quantité pour laquelle le coût marginal égalise le prix de marché, le profit total est à son maximum.

Une entreprise preneuse de prix produit donc une quantité telle que $Cm = P^*$ (ou $P^*$ est le prix de marché). En effet, si l’entreprise augmentait sa production jusqu’à un niveau où $Cm > P^*$, le coût de fabrication de la dernière unité produite serait supérieur à $P^*$, de telle sorte que l’entreprise réaliserait une perte sur cette unité. Si l’entreprise produisait à un niveau où $Cm < P^*$, elle pourrait produire davantage, tant qu’elle réalise un profit marginal positif. À la quantité pour laquelle $Cm = P^*$, le profit total est maximal.

Supposez maintenant que vous soyez le chef d’entreprise de Supers Jets et que vous deviez déterminer la quantité d’avions à produire. Comme tout chef d’entreprise, vous avez comme objectif non seulement de faire des profits, mais également de maximiser votre profit. Le profit d’une entreprise est la différence entre ses recettes et ses coûts totaux.

$profit = recettes - coûts = PQ - CT(Q)$

De manière équivalente, le profit est le nombre d’unités produites multiplié par le profit unitaire, qui est la différence entre le prix et le coût moyen.

$profit=Q \left( P - \frac{CT(Q)}{Q} \right)  $

On suppose que compte tenu de l’offre et de la demande globale d’avions privés le prix d’équilibre du marché est de 340 000 euros. On rappelle que vos principaux concurrents produisent des avions strictement identiques aux vôtres. Si vous choisissiez un prix plus élevé, les clients potentiels se tourneraient vers d’autres concurrents. Vous êtes donc preneur de prix.

---
**APPLICATION**

*Pour un prix de marché de 340.000 euros, quelle quantité allez-vous accepter de produire ? Déterminez la quantité à produire pour laquelle votre profit est maximal.*

| Prix du <br> marché <br> (€) | Quantité <br> produite | Coût moyen <br> (€) | Coût marginal <br> (€) | Chiffres <br> d'affaires <br> (€) | Coût total <br> (€) | Profit <br> (€) |
|:------------------:|:-----------------:|---------------:|------------------:|------------------------:|---------------:|-----------:|
| 340.000            | 1                 | 750.000        | 300.000           | 340.000                 | 750.000        | −410.000   |
| 340.000            | 2                 | 475.000        | 200.000           | 680.000                 | 950.000        | −270.000   |
| 340.000            | 3                 | 367.000        | 151.000           | 1.020.000               | 1.101.000      | -81.000    |
| 340.000            | 4                 | 305.500        | 121.000           | 1.360.000               | 1.222.000      | 138.000    |
| 340.000            | 5                 | 273.000        | 143.000           | 1.700.000               | 1.365.000      | 335.000    |
| 340.000            | 6                 | 256.500        | 174.000           | 2.040.000               | 1.539.000      | 501.000    |
| 340.000            | 7                 | 250.000        | 211.000           | 2.380.000               | 1.750.000      | 630.000    |
| 340.000            | 8                 | 250.000        | 250.000           | 2.720.000               | 2.000.000      | 720.000    |
| 340.000            | 9                 | 255.000        | 295.000           | 3.060.000                | 2.295.000      | 765.000    |
| 340.000            | 10                | 263.500        | 340.000           | 3.400.000               | 2.635.000      | 765.000    |
| 340.000            | 11                | 275.000        | 390.000           | 3.740.000               | 3.025.000      | 715.000    |
| 340.000            | 12                | 288.750        | 440.000           | 4.080.000               | 3.465.000      | 615.000    |
| 340.000            | 13                | 305.000        | 500.000           | 4.420.000               | 3.965.000      | 455.000    |
| 340.000            | 14                | 324.000        | 571.000           | 4.760.000               | 4.536.000      | 224.000    |
| 340.000            | 15                | 346.000        | 654.000           | 5.100.000               | 5.190.000      | −90.000    |
| 340.000            | 16                | 370.500        | 738.000           | 5.440.000               | 5.928.000      | −488.000   |
| 340.000            | 17                | 398.000        | 838.000           | 5.780.000               | 6.766.000      | −986.000   |
| 340.000            | 18                | 428.000        | 938.000           | 6.120.000               | 7.704.000      | −1.584.000 |
| 340.000            | 19                | 461.000        | 1.055.000         | 6.460.000               | 8.759.000      | −2.299.000 |
| 340.000            | 20                | 497.000        | 1.181.000         | 6.800.000               | 9.940.000      | −3.140.000 |

---

<br>
<br>

---
**APPLICATION**

*On suppose désormais que compte tenu de l’offre et de la demande globale le prix d’équilibre du marché soit de 500.000 euros.*

| Prix du <br> marché <br> (€) | Quantité <br> produite | Coût moyen <br> (€) | Coût marginal <br> (€) | Chiffres <br> d’affaires <br> (€) | Coût total <br> (€) | Profit <br> (€) |
|:--------------------:|:-----------------:|-----------------:|--------------------:|--------------------------:|-----------------:|-------------:|
| 500.000              | 1                 | 750.000           | 300.000              | 500.000                   | 750.000          | −250.000     |
| 500.000              | 2                 | 475.000           | 200.000              | 1.000.000                 | 950.000          | 50.000       |
| 500.000              | 3                 | 367.000           | 151.000              | 1.500.000                 | 1.101.000        | 499.000      |
| 500.000              | 4                 | 305.500          | 121.000             | 2.000.000                  | 1.222.000        | 778.000      |
| 500.000              | 5                 | 273.000          | 143.000             | 2.500.000                 | 1.365.000        | 1.135.000    |
| 500.000              | 6                 | 256.500          | 174.000             | 3.000.000                 | 1.539.000        | 1.461.000    |
| 500.000              | 7                 | 250.000          | 211.000             | 3.500.000                 | 1.750.000        | 1.750.000    |
| 500.000              | 8                 | 250.000          | 250.000             | 4.000.000                 | 2.000.000        | 2.000.000     |
| 500.000              | 9                 | 255.000          | 295.000             | 4.500.000                 | 2.295.000        | 2.205.000    |
| 500.000              | 10                | 263.500          | 340.000             | 5.000.000                 | 2.635.000        | 2.365.000    |
| 500.000              | 11                | 275.000          | 390.000             | 5.500.000                 | 3.025.000        | 2.475.000    |
| 500.000              | 12                | 288.750          | 440.000             | 6.000.000                 | 3.465.000        | 2.535.000    |
| 500.000              | 13                | 305.000          | 500.000             | 6.500.000                 | 3.965.000        | 2.535.000    |
| 500.000              | 14                | 324.000          | 571.000             | 7.000.000                 | 4.536.000        | 2.464.000    |
| 500.000              | 15                | 346.000          | 654.000             | 7.500.000                 | 5.190.000        | 2.310.000    |
| 500.000              | 16                | 370.500          | 738.000             | 8.000.000                 | 5.928.000        | 2.072.000    |
| 500.000              | 17                | 398.000          | 838.000             | 8.500.000                 | 6.766.000        | 1.734.000    |
| 500.000              | 18                | 428.000          | 938.000             | 9.000.000                 | 7.704.000        | 1.296.000    |
| 500.000              | 19                | 461.000          | 1.055.000           | 9.500.000                 | 8.759.000        | 741.000      |
| 500.000              | 20                | 497.000          | 1.181.000           | 10.000.000                | 9.940.000        | 60.000       |

---


Tableau 1.4 La maximisation du profit pour un niveau de prix donné (P* = 340.000 euros) 
| Prix du <br> marché <br> (€) | Quantité <br> produite | Coût moyen <br> (CM, €) | Coût marginal <br> (Cm, €) | Chiffres d’affaires <br> (€) | Coût total <br> (CT, €) | Profit <br> (€) |
|----------------------|-------------------|----------------------|-------------------------|---------------------------|----------------------|--------------|
| 340.000              | 8                 | 250.000              | 250.000                 | 2.720.000                 | 2.000.000            | 720.000      |
| 340.000              | 9                 | 255.000              | 295.000                 | 3.060.000                  | 2.295.000            | 765.000      |
| 340.000              | 10                | 263.500              | 340.000                 | 3.400.000                 | 2.635.000            | 765.000      |
| 340.000              | 11                | 275.000              | 390.000                 | 3.740.000                 | 3.025.000            | 715.000      |
| 340.000              | 12                | 288.750              | 440.000                 | 4.080.000                 | 3.465.000            | 615.000      |



Tableau 1.5 La maximisation du profit pour un niveau de prix donné (P* = 500.000 euros) 
| Prix du <br> marché <br> (€) | Quantité <br> produite | Coût moyen <br> (CM, €) | Coût marginal <br> (Cm, €) | Chiffres d’affaires <br> (€) | Coût total <br> (CT, €) | Profit <br> (€) |
|----------------------|-------------------|----------------------|-------------------------|---------------------------|----------------------|--------------|
| 500.000              | 10                | 263.500              | 340.000                 | 5.000.000                 | 2.635.000            | 2.365.000    |
| 500.000              | 11                | 275.000              | 390.000                 | 5.500.000                 | 3.025.000            | 2.475.000    |
| 500.000              | 12                | 288.750              | 440.000                 | 6.000.000                 | 3.465.000            | 2.535.000    |
| 500.000              | 13                | 305.000              | 500.000                 | 6.500.000                 | 3.965.000            | 2.535.000    |
| 500.000              | 14                | 324.000              | 571.000                 | 7.000.000                 | 4.536.000            | 2.464.000    |
| 500.000              | 15                | 346.000              | 654.000                 | 7.500.000                 | 5.190.000            | 2.310.000    |


### 1.6.3. Comment déduire la courbe d’offre du producteur de la maximisation du profit ?

*Pour une entreprise en situation de preneuse de prix, la courbe d’offre débute au minimum du coût moyen et se confond avec la courbe de coût marginal. La courbe d’offre détermine donc à partir du coût moyen minimal la quantité à produire pour laquelle le profit est maximal.*

Rappel : le producteur cherche à faire du profit et à le maximiser. Le profit est maximal lorsque la dernière unité produite rapporte autant que ce qu’elle coûte.

Lorsque les prix baissent (du fait de la concurrence que se livrent les entreprises présentes sur le marché), vous choisissez de produire des quantités différentes. Vous choisissez la quantité pour laquelle $Cm = \text{prix de marché}$ ; en effet, nous avons vu que pour un prix de marché donné ($P^*$) le profit est maximal pour $P^* = Cm$.

On remarque néanmoins que si le prix baissait jusqu’à devenir inférieur à 250.000 euros, votre entreprise Supers Jets accuserait des pertes. En effet, le coût moyen minimal est de 250.000 euros, ce qui suppose qu’en deçà de ce prix votre entreprise ferait forcément des pertes. Vous refuseriez alors de produire au-dessous de ce prix. D’autres entreprises, plus compétitives, pourraient continuer à produire. Le Graphique 1.8 représente les courbes de coût marginal et de coût moyen de l’entreprise Supers Jets.

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-08.jpg" />
    <br>
    Graphique 1.8 La représentation graphique de la courbe d’offre de Supers Jets.


### 1.6.4. Que sont le surplus du producteur et le surplus du consommateur ?

Quand le marché du pain est à l’équilibre avec la quantité de pains offerte sur le marché égalisant la quantité demandée, le surplus total est représenté par l’aire sous la courbe de demande et au-dessus de la courbe d’offre.

La quantité d’équilibre de 5.000 pains en situation de concurrence est telle que le surplus total est maximisé. Si une quantité inférieure à 5 000 pains était produite, les gains totaux issus de l’échange seraient plus faibles.

À l’équilibre, tous les gains potentiels à l’échange sont exploités. Cette propriété – le fait que le surplus combiné des consommateurs et des producteurs soit maximisé au point où l’offre égale la demande – est vraie de manière générale dans ce modèle : si les acheteurs comme les vendeurs sont preneurs de prix, la quantité d’équilibre maximise la somme des gains issus de l’échange sur le marché.

Les participants à ce marché sont preneurs de prix, les acheteurs et vendeurs sont bien sûr libres de choisir un autre prix, mais ils n’en tireraient aucun avantage.

Cependant, dans l’exemple du marché du pain, même si tous les agents acceptent le prix de 2 euros par pain, certains vont tirer plus de gains de cet échange. En effet, certains consommateurs (la demande) étaient prêts à payer plus de 2 euros et certains producteurs (l’offre) étaient prêts à vendre moins cher que 2 euros.

#### 1.6.4.1. Le surplus du consommateur

Souvenez-vous que la courbe de demande indique la disposition à payer de chaque client potentiel. Un client disposé à payer plus que le prix de vente achètera le pain et gagnera à cet échange un surplus, car la valeur qu’il attribue au pain est supérieure au prix qu’il doit payer pour l’acquérir. Le Graphique 1.9a illustre le surplus du consommateur.

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-09-a-d.jpg" />
    <br>
    Graphique 1.9a Le surplus du consommateur.

Le surplus total des consommateurs est l’aire colorée qui représente le gain à l’échange des consommateurs au prix de marché donné, c’est-à-dire la différence entre ce que ces consommateurs étaient disposés à payer et le prix (ce qu’ils payent vraiment).

#### 1.6.4.2. Le surplus du producteur

Souvenez-vous que la courbe d’offre correspond à la courbe de coût marginal, c’est-à-dire le coût de la dernière unité produite. Un producteur supportant un coût marginal inférieur au prix continue à produire pour vendre à un prix qui lui permet de gagner un profit, le surplus du producteur.

Comme le prix d’équilibre est unique et que le coût marginal est croissant, le surplus du producteur va être de plus en plus réduit. Il est même nul lorsque le coût marginal a rejoint le niveau du prix. Le Graphique 1.9b illustre le surplus du producteur.

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-09-b-c.jpg" />
    <br>
    Graphique 1.9b Le surplus du producteur


Le surplus total du producteur est l’aire colorée qui représente le gain à l’échange des producteurs au prix de marché donné, c’est-à-dire la différence entre le prix payé pour chaque pain et le coût marginal de chacun de ces pains.


#### 1.6.4.3. Pourquoi le surplus est-il maximisé à l’équilibre ?

Les consommateurs (demandeurs) et les producteurs (offreurs) de pain qui décident volontairement d’échanger tirent tous des gains de cet échange. Les consommateurs qui sont prêts à payer un prix supérieur ou égal au prix du marché gagnent le surplus du consommateur. Les producteurs qui ont des coûts marginaux inférieurs ou égaux au prix du marché gagnent le surplus du producteur. Le surplus total mesure les gains à l’échange ou gains générés par le commerce pour tous les agents économiques qui y participent.

Le Graphique 1.9c montre comment calculer le surplus total (les gains tirés de l’échange) à l’équilibre concurrentiel du marché du pain.

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-09-d-b.jpg" />
    <br>
    Graphique 1.9d La perte sèche.

Ces deux exemples de perte sèche montrent que le surplus total est réduit s’il s’éloigne du prix d’équilibre de 2 euros et de la quantité d’équilibre de 5.000 pains. Pour un prix supérieur (prix plancher), les producteurs sont prêts à vendre plus de pains, mais il y a moins de demandes. Pour un prix inférieur (prix plafond), les consommateurs sont disposés à acheter plus de pains, mais les boulangers en offrent moins.

