# Notions et concepts utilisés dans le cours de 2e

## Sociologie

### 1. Les inégalités socioéconomiques

Dans cette section, nous discutons la perception des inégalités dans la société.

```mermaid
graph TD;
A[inégalités économiques] --> |s'observe par la présence de| B[3 phénomènes];
B --> B1[élite économique];
B --> B2[classes moyennes];
B --> B3[pauvreté];
B1 --> |observable à l'| C[échelle];
B2 --> |observable à l'| C;
B3 --> |observable à l'| C;
C --> C1[mondiale];
C --> C2["nationale (Luxembourg)"];
A --> |sont analysées par quels| D[acteurs];
D --> D1[chercheurs en <br> sciences sociales];
D --> D2[statisticiens officiels];
D --> D3[chargés d'études];
D1 --> E[indicateurs utilisés];
D2 --> E;
D3 --> E;
E --> F1[seuils de faible revenu];
E --> F2[quintiles de revenu];
E --> F3[coefficient de Gini];
F1 --> |peuvent mesurer| G[2 types de pauvretés];
G --> G1[pauvreté relative];
G --> G2[pauvreté absolue];
```

### 2. Les théories sur les classes sociales

Dans cette section, nous présentons les différents modèles de société.

```mermaid
graph TD;
A[concept de classe sociale] --> |repose sur la| B[hiérarchie et division des classes sociales];
B --> |mais les diverses théories on des| C[visions distinctes de la société];
C --> C1["les classes sociales <br> selon la vision marxiste : <br> <br> Karl Marx (1818-1883)" ];
C --> C2["les classes sociales <br> selon la vision fonctionnaliste <br> <br> Talcott Parsons (1902-1979)"]; 
C1 --> |divergences des classes provient du| D1[mode de production capitaliste];
D1 --> |les fondements des relations <br> humaines proviennent des| D11[rapports de production];
D1 --> |les rapports de classes <br> sont basés sur l'| D12[exploitation];
D12 --> D121["les capitalistes <br> (ou bourgeoisie) <br> exploitent <br> le proletariat"];
D11 --> D111[mode de production <br> esclavagiste : <br> esclaves vs. maîtres];
D11 --> D112[mode de production <br> féodale : <br> chevaliers vs. serfs];
D11 --> D113[mode de production <br> capitaliste : <br> capitalistes vs. prolétariat];
D1 --> |principal facteur <br> d'évolution dans <br> la société est la| D13[lutte des classes];
D13 --> D131[conflit social <br> pour le <br> contrôle du pouvoir <br> et la richesse];
C2 --> |groupe les personnes <br> ayant les mêmes <br> caractéristiques <br> communes dans| D2["strate sociale <br> (stratification)" ];
D2 --> |exemples de| D21[critères pour <br> désigner les <br> strates sociales];
D21 --> D211[revenu];
D21 --> D212[occupation];
D21 --> D213[niveau de <br> scolarité]
D212 --> D22[indice de stratification : <br> statut socioéconomique];
D213 --> D22;
```

### 3. La mobilité sociale

```mermaid
graph TD;
A[mobilité sociale] --> |peut s'expliquer par des| B[facteurs];
B --> B1[individuels et familiaux];
B --> B2[socioéconomiques];
A --> |peut aller dans <br> différents| C[sens];
C --> C1[descendante]
C --> C2[ascendante]
A --> |étudiée surtout <br> par les| D[fonctionnalistes]
```

<br>

<br>

```mermaid
graph TD;
A[reproduction sociale] --> |étudiée surtout <br> par les| B[marxistes];
A --> |peut prendre des| C[formes différentes];
C --> C1[sociétés libérales];
C1 --> |où elle <br> s'effectue <br> subtilement| C11["à l'école <br> <br> Bourdieu (1930-2002)"]
C --> C2[systèmes des castes]
C2 --> |où elle <br> s'effectue <br> explicitement| C21[par l'imposition <br> de règles et <br> d'interdits <br> très stricts];
```


