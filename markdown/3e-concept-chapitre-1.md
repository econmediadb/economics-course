# Chapitre 1 : La science économique – une science de choix

## 1. Concept fondamental : la rareté

```mermaid
graph TD;
A[besoins] -->  B[primaires: besoins vitaux];
A -->  C[secondaires: désirs non-nécessaire];
B --> D[manger]
B --> E[dormir]
B --> F[se loger]
C --> G[voiture climatisée]
style A fill:#C0C0C0
```

<br>

<br>

<br>

```mermaid
graph TD;
A[besoins] -->  B[satisfaction des besoins]
B -->  |est mesurée <br> en utilisant|B1[utilité]
B1 --> |pour satisfaire ces besoins|C[ressources disponibles]
C --> |or ces ressources <br> sont rares|D[rareté]
D --> E[prise de décision]
E --> F[choix]
E--> G[coût d'opportunité]
style B1 fill:#C0C0C0
style G fill:#C0C0C0
```

## 2. Les trois problèmes économiques fondamentaux

```mermaid
graph TD;
A[3 problèmes économiques fondamentaux] --> B1[que produire?]
A --> B2[comment produire?]
A --> B3[pour qui produire?]
B1 --> B11[coût d'opportunité]
B1 --> B12[coût de production]
B2 --> B21[combinaison productive]
B21 --> B211[efficacité productive]
B211 --> B2111[frontière des possibilités de production]
B2111 --> B21111[modèle économique]
B3 --> B31[biens et services produits]
B31 --> B311[facteurs de production]
B311 --> B3111[répartition des revenus]
B3111 --> B31111[circuit économique]
```

## 3. Les systèmes économiques

```mermaid
graph TD;
A[système économique] -->  B1[économie de marché]
A --> B2[économie planifiée]
B1 --> B11[marché & prix = régulateur]
B11 --> B11a[libéralisme économique]
B11a --> B111[capitalisme]
B111 --> B1111[fluctuations des prix]
B1111 --> |sont fonction de| B11111[offre & demande]
B2 --> B21[plan de l'Etat = régulateur]
B21 --> B211[exemple URSS]
B211 --> B2111[investissement]
B211 --> B2112[production]
B211 --> B2113[prix]
B2111 --> B212[régulé par l'Etat]
B2112 --> B212
B2113 --> B212
style A fill:#C0C0C0
style B1 fill:#C0C0C0
style B2 fill:#C0C0C0
```

<br>

<br>

<br>

[code source LaTeX](https://www.overleaf.com/read/jcdtgbxbkyxh)

![Systeme Economique](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20230525-systeme-economique-pays.png "Systeme Economique")

