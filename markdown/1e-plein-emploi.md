# Chapitre ??? : Chômage et plein-emploi

### Vidéos

- [Dessine-moi l'éco : Comment mesure-t-on le chômage ?](https://www.youtube.com/watch?v=0AJLLsL2mZg)
- [Dessine-moi l'éco : Y a-t-il un remède au chômage ?](https://www.youtube.com/watch?v=s3cM2Kx-jUY)
- [Expliquez-nous... les allocations chômage dégressives](https://www.youtube.com/watch?v=dsq1_S9pYwg)
- [Dessine-moi l'éco : La création monétaire, un taux d'inflation à contrôler](https://www.youtube.com/watch?v=o2u7Xa57y8A)
- [La France est-elle trop généreuse avec les chômeurs ?](https://www.youtube.com/watch?v=8V-yvXGIx8c)
- [Pourquoi un faible taux de chômage n'est pas toujours bon signe](https://www.youtube.com/watch?v=1X5CtBHYj30)
- [Dessine-moi l'éco : Pourquoi la déflation peut-elle être dangereuse ?](https://www.youtube.com/watch?v=U0oti-8Wc6E)
- [Expliquez-nous... les cotisations sociales](https://www.youtube.com/watch?v=VxFop1QjRQ8)
- [Les 3 formes de CHÔMAGE](https://www.youtube.com/watch?v=Emtn9_GKQ6Q)
- [Le marché du travail](https://www.youtube.com/watch?v=3q_h00oUsLU)
- [Approche-t-on vraiment du plein emploi ?](https://www.youtube.com/watch?v=f02EsHVeBg8)
- [Campus de l'innovation pour les lycées](https://www.youtube.com/@campusdelinnovationpourles7129/videos)

## 1. Définition du Chômage

- Expliquer ce qu'est le chômage.
- Types de chômage : frictionnel, structurel, cyclique et saisonnier.
- Le concept de force de travail et le taux de chômage.

```mermaid
graph TD;
A[Définition du Chômage] -->  B1[Types de Chômage];
A --> B2[Force de Travail];
A --> B3[Taux de Chômage];
B1 --> C11[Chômage <br> Frictionnel];
B1 --> C12[Chômage <br> Structurel];
B1 --> C13[Chômage <br> Cyclique];
B1 --> C14[Chômage <br> Saisonnier];
B2 --> C21[Population <br> Active];
B3 --> C31[Calcul];
B3 --> C32[Limitations];
```

<br>

<br>

[Figure XX. Taux de chômage pour le Luxembourg](https://data.oecd.org/chart/7eRl)

### Types de Chômage

- Chômage Frictionnel:
    - Transition naturelle entre les emplois.
    - Courte durée.
- Chômage Structurel:
    - Mismatch des compétences.
    - Changements technologiques.
- Chômage Cyclique:
    - Ralentissement économique.
    - Récessions.
- Chômage Saisonnier:
    - Variations saisonnières de la demande de travail.
    - Secteurs tels que le tourisme ou l'agriculture.

> Le chômage, phénomène complexe et multifacette, se manifeste sous différents types, chacun résultant de causes et de circonstances particulières. Premièrement, le chômage cyclique, qui est directement lié aux fluctuations économiques. Durant les périodes de récession, la demande globale de biens et services diminue, entraînant une réduction de la production et, par conséquent, des effectifs.

> Le chômage structurel survient lorsque l'économie évolue, modifiant la demande de certaines compétences. L'automatisation, l'innovation technologique et les délocalisations contribuent à ce déséquilibre entre les compétences disponibles et celles requises par le marché. Ce type de chômage nécessite souvent une reconversion professionnelle ou des ajustements dans la formation des travailleurs.

> Le chômage frictionnel est temporaire et fait partie du fonctionnement normal du marché du travail. Il se produit lorsqu'il y a un décalage entre la fin d'un emploi et le début d'un autre, période durant laquelle les individus cherchent activement et sélectionnent des opportunités d'emploi.

> Enfin, le chômage saisonnier est inhérent à certaines industries où la demande pour le travail fluctue avec les saisons, comme l'agriculture ou le tourisme. Chacun de ces types de chômage requiert des politiques économiques spécifiques pour être adressé efficacement.

**Questions de révision**:
- Expliquez comment le chômage cyclique diffère du chômage structurel et donnez un exemple de circonstance économique qui pourrait conduire à chacun de ces types de chômage.
- Pourquoi est-il important que les politiques économiques soient spécifiques au type de chômage abordé et quelles stratégies pourraient être efficaces pour lutter contre le chômage frictionnel et saisonnier?


<div style="text-align:center;">
  <img src="https://www.core-econ.org/the-economy/v1/book/fr/images/web/figure-09-02.jpg" 
       alt="Le marché du travail. (CORE)"
       style="max-width:75%; height:auto;" 
       title="Le marché du travail" />
  <figcaption>Figure: Le marché du travail. (CORE) </figcaption>    
</div>



### Force de Travail

Population Active: Individus employés ou cherchant activement un emploi.

> La force de travail désigne l'ensemble des individus disponibles pour occuper un emploi dans une économie à un moment donné. Elle comprend tous les travailleurs actifs, c'est-à-dire les personnes employées, ainsi que les chômeurs actifs recherchant un emploi. La taille et les caractéristiques de la force de travail sont déterminées par plusieurs facteurs, dont les démographiques, comme l'âge et le sexe, l'éducation, et la formation professionnelle. La force de travail est un indicateur clé de la capacité productive d'une économie et un facteur essentiel pour le calcul du taux de chômage.

<div style="text-align:center;">
  <img src="../inkscape-files/diagrams/demand-offre-travail.svg" alt="La courbe des salaires : salaire d’efficience et chômage dans l’économie agrégée."
       style="max-width:75%; height:auto;" title="La courbe des salaires" />
  <figcaption> Figure: La courbe des salaires : salaire d’efficience et chômage dans l’économie agrégée. </figcaption>     
</div>

<br>

<br>

**Questions de révision**:
- Quels éléments constituent la force de travail dans une économie et comment ces éléments influencent-ils le calcul du taux de chômage?
- Expliquez en quoi les facteurs démographiques peuvent affecter la taille et les caractéristiques de la force de travail et pourquoi ces facteurs sont importants pour évaluer la capacité productive d'une économie.

### Taux de Chômage

- Calcul: (Nombre de chômeurs / Population active) x 100.
- Limitations: 
    - Sous-emploi.
    - Travailleurs découragés.

> Le taux de chômage est une mesure cruciale qui reflète la proportion de la force de travail qui est sans emploi et cherche activement un travail. Pour le calculer, on divise le nombre de chômeurs par la force de travail totale (la somme des personnes employées et des chômeurs), et on multiplie le résultat par 100 pour obtenir un pourcentage.

> $\text{Taux de chômage} = \frac{ \text{Nombre de chômeurs} }{ \text{Force de travail totale} } \times 100$

> Toutefois, ce taux présente des limitations. Il ne tient pas compte du sous-emploi, où les individus travaillent moins qu'ils ne le souhaiteraient ou sont surqualifiés pour leur poste actuel. De plus, il exclut les travailleurs découragés qui ont cessé de chercher activement du travail. Ces facteurs peuvent donner une image imparfaite de la santé réelle du marché du travail et masquer des problèmes économiques sous-jacents.

**Questions de révision**:
- Expliquez comment est calculé le taux de chômage et pourquoi ce taux est exprimé en pourcentage. Qu'indique ce pourcentage sur l'état du marché du travail?
- Quelles sont les limitations du taux de chômage en tant qu'indicateur de la santé du marché du travail et quelles catégories de la population sont exclues de ce calcul? Comment cela pourrait-il affecter la perception de l'économie?

## 2. Mesure du Chômage

- Présenter comment le chômage est mesuré.
- Discuter de l'importance du taux de chômage.
- Limitations des données sur le chômage (par exemple, le sous-emploi, les travailleurs découragés).

```mermaid
graph TD;
A[Mesure du Chômage] -->  B1[Calcul du <br> Taux de Chômage];
A --> B2[Population <br> Active];
A --> B3[Nombre de <br> Chômeurs];
A --> B4[Limitations <br> des Données];
A --> B5[Indicateurs <br> Complémentaires];
```

### Calcul du Taux de Chômage

- Formula: (Nombre de chômeurs/Population active)×100
- Importance: Indicateur économique clé

> Le calcul du taux de chômage se fait à travers une formule spécifique: le nombre de personnes sans emploi activement en recherche de travail est divisé par la force de travail totale (la somme des employés et des chômeurs), le résultat étant ensuite converti en pourcentage.

> $\text{Taux de chômage} = \frac{ \text{Chômeurs} }{ \text{Employés} + \text{Chômeurs} } \times 100 $

> Cependant, cette formule exclut certaines catégories d'individus: les travailleurs découragés qui ne cherchent plus activement du travail, les personnes en sous-emploi, et ceux travaillant à temps partiel par nécessité, non par choix. Ces exclusions peuvent conduire à une sous-estimation du taux de chômage réel.

**Questions de révision**:
- Quelles informations sont nécessaires pour utiliser la formule du taux de chômage et comment interprétez-vous le pourcentage résultant ?
- Pourquoi des groupes comme les travailleurs découragés ou ceux en sous-emploi ne sont-ils pas inclus dans le calcul standard du taux de chômage et quelle est l'implication de ces exclusions sur l'interprétation du taux de chômage ?

### Population Active

- Définition: Individus employés + ceux cherchant activement un emploi
- Exclusion: Retraités, étudiants, personnes ne cherchant pas activement un emploi

> La population active, ou main-d'oeuvre, englobe l'ensemble des personnes en âge de travailler qui occupent un emploi ou qui en cherchent activement un. Elle est définie par l'âge de travail habituellement établi entre 15 et 64 ans, mais cela peut varier selon les normes et régulations nationales.

> Cette population est donc constituée de deux segments: les employés, c'est-à-dire les personnes travaillant à plein temps ou à temps partiel, et les chômeurs, ceux qui n'ont pas d'emploi mais qui sont disponibles et en recherche de travail.

> Cependant, la définition standard exclut plusieurs groupes: les étudiants à plein temps, les retraités, les personnes s'occupant exclusivement des tâches ménagères, et les individus non actifs pour d'autres raisons telles que des limitations de santé. Ces exclusions sont significatives car elles omettent une portion de la population qui pourrait potentiellement offrir ou rechercher du travail sous différentes circonstances économiques ou personnelles.

> $ \text{taux de participation} = \frac{ \text{population active} }{ \text{population en âge de travailler} } $

**Questions de révision**:
- Expliquez ce qui constitue la population active et comment les personnes sont classées comme employées ou chômeurs. Pourquoi certains groupes sont-ils exclus de cette définition?
- Quelles pourraient être les conséquences de l'exclusion des étudiants, des retraités, et des personnes non actives pour des raisons de santé dans le calcul de la population active?

### Nombre de Chômeurs

- Critères: Sans emploi, disponible pour travailler, cherchant activement un emploi
- Sources de Données (STATEC): Enquêtes sur la population active, registres de l'emploi, statistiques gouvernementales

> Le nombre de chômeurs fait référence à l'ensemble des individus actifs, sans emploi, qui sont en recherche active d'un travail. Les critères pour classer une personne comme chômeur comprennent l'absence d'emploi rémunéré, la disponibilité pour prendre un emploi dans les deux semaines, et avoir cherché activement du travail au cours des quatre dernières semaines.

> Pour assurer la précision, les sources de données comme le STATEC, l'institut national de la statistique du Luxembourg, collectent les informations via des enquêtes, telles que l'Enquête sur la Force de Travail (EFT), et enregistrent également les inscriptions auprès des agences pour l'emploi. Le STATEC se conforme aux définitions et méthodologies de l'Organisation Internationale du Travail (OIT) pour permettre la comparabilité des données au niveau international. Les données du STATEC fournissent ainsi un aperçu essentiel de la santé économique du Luxembourg et sont utilisées pour élaborer des politiques économiques et sociales pertinentes.

**Questions de révision**:
- Quels sont les trois critères principaux utilisés par STATEC pour classifier une personne comme étant au chômage et pourquoi est-il important que ces critères soient respectés?
- Comment le STATEC collecte-t-il les données sur le chômage et pourquoi est-il important que ces méthodes soient conformes aux standards de l'Organisation Internationale du Travail?

### Limitations des Données

- Sous-Emploi: Individus travaillant moins qu'ils ne le souhaiteraient
- Travailleurs Découragés: Individus ayant cessé de chercher un emploi
- Emploi Non Déclaré: Travail au noir non comptabilisé

> Les données sur le chômage recèlent des limitations intrinsèques qui peuvent masquer certaines réalités du marché du travail. Le sous-emploi, par exemple, concerne les individus travaillant moins d'heures qu'ils ne le souhaiteraient ou sous-utilisant leurs compétences à cause de la conjoncture économique; ces cas ne sont pas pleinement capturés dans les statistiques standards du chômage.

> Les travailleurs découragés, qui ont renoncé à chercher du travail après de longues périodes sans succès, sont également exclus des calculs du taux de chômage. Leur absence fausse le tableau de l'emploi en sous-estimant le besoin réel en opportunités de travail.

> De plus, l'emploi non déclaré, caractérisé par le travail rémunéré qui n'est pas rapporté aux autorités fiscales ou de surveillance du travail, reste invisible dans les statistiques officielles. Cette omission entrave la capacité des analystes et des décideurs à évaluer précisément la situation économique et à élaborer des politiques adéquates pour améliorer les conditions du marché du travail.

**Questions de révision**:
- En quoi le sous-emploi pose-t-il un défi pour l'évaluation précise du chômage et quel impact cela a-t-il sur l'interprétation des statistiques du marché du travail?
- Pourquoi l'exclusion des travailleurs découragés et de l'emploi non déclaré des calculs du taux de chômage peut-elle conduire à une évaluation erronée des besoins du marché du travail et quelles en sont les conséquences pour la politique économique?

### Indicateurs Complémentaires

- Taux de participation: Proportion de la population active par rapport à la population en âge de travailler
- Durée du chômage: Temps moyen passé en chômage
- Emploi précaire: Emplois à faible sécurité d'emploi ou à court terme

> Pour appréhender la dynamique du marché du travail avec plus de nuance, il est essentiel de considérer des indicateurs complémentaires au taux de chômage. Le taux de participation de la force de travail, par exemple, mesure la proportion de la population en âge de travailler qui est active sur le marché de l'emploi, offrant un aperçu de l'engagement économique des citoyens. Une baisse de ce taux peut indiquer un désengagement croissant face au marché du travail ou des changements démographiques comme le vieillissement de la population.

> La durée du chômage est un autre indicateur significatif. Elle permet de comprendre la persistance du chômage dans une économie, révélant si les périodes sans emploi sont principalement de courte durée ou si elles s'inscrivent dans le long terme, ce qui peut signaler des problèmes structurels.

> Enfin, l'emploi précaire, caractérisé par des contrats temporaires, à temps partiel ou informels, peut refléter l'instabilité de l'emploi et une insécurité professionnelle, éléments qui ne sont pas directement mesurés par le taux de chômage standard. Ces indicateurs offrent une perspective élargie de la santé et des défis du marché du travail.

**Questions de révision**:
- Comment le taux de participation de la force de travail complète-t-il notre compréhension du taux de chômage et quels facteurs peuvent influencer une variation de ce taux?
- Pourquoi est-il important de considérer la durée du chômage et l'emploi précaire en plus du taux de chômage standard pour évaluer la santé du marché du travail?

## 3. Causes du Chômage

- Concepts économiques de base menant au chômage (par exemple, l'offre et la demande de main-d'oeuvre).
- Le rôle des cycles économiques.
- Changements structurels dans l'économie (par exemple, avancements technologiques, mondialisation).
- Politiques gouvernementales et régulations pouvant impacter les niveaux d'emploi.

```mermaid
graph TD;
A[Causes du Chômage] -->  B1[Causes <br> Économiques];
A --> B2[Facteurs <br> Structurels];
A --> B3[Politiques et <br> Réglementations];
A --> B4[Facteurs Sociaux <br> et Technologiques];
A --> B5[Influences <br> Internationales];
```

<br>

<br>


<div style="text-align:center;">
  <img src="../inkscape-files/diagrams/demande-offre-travail-exces-a.svg" 
       alt="Exemple de marché du travail : L'offre et la demande d'infirmières."
       style="max-width:75%; height:auto;" 
       title="Deséquilibre sur le marché du travail" />
  <figcaption>L'offre et la demande d'infirmières: Salaire d'équilibre</figcaption>     
</div>


<div style="text-align:center;">
  <img src="../inkscape-files/diagrams/demande-offre-travail-exces-b.svg" 
       alt="Exemple de marché du travail : L'offre et la demande d'infirmières."
       style="max-width:75%; height:auto;" 
       title="Deséquilibre sur le marché du travail" />
  <figcaption>L'offre et la demande d'infirmières: Offre excédentaire de main d'oeuvre.</figcaption>     
</div>

<div style="text-align:center;">
  <img src="../inkscape-files/diagrams/demande-offre-travail-exces-c.svg" 
       alt="Exemple de marché du travail : L'offre et la demande d'infirmières."
       style="max-width:75%; height:auto;" 
       title="Deséquilibre sur le marché du travail" />
  <figcaption>L'offre et la demande d'infirmières: Demande excédentaire de main d'oeuvre.</figcaption>     
</div>


### Causes Économiques

- Fluctuations Cycliques
    - Récessions économiques
    - Baisse de la demande globale
- Offre et Demande de Main-d'oeuvre
    - Automatisation
    - Délocalisations industrielles

> Les causes économiques du chômage sont souvent profondément ancrées dans les fluctuations cycliques et les dynamiques d'offre et de demande de main-d'oeuvre. Les fluctuations cycliques correspondent aux variations naturelles de l'économie, passant par des périodes d'expansion et de récession. Durant une récession, la demande globale pour les biens et services diminue, entraînant les entreprises à réduire leur production et, par conséquent, à diminuer leur main-d'oeuvre, ce qui augmente le chômage.

> D'autre part, la correspondance entre l'offre et la demande de main-d'oeuvre peut être déséquilibrée par des changements structurels, comme l'évolution technologique ou les modifications dans les patterns commerciaux internationaux, qui altèrent les besoins en compétences et en types d'emplois. Si la main-d'oeuvre ne possède pas les compétences demandées ou si elle est géographiquement éloignée des zones où les emplois sont créés, un chômage structurel peut s'installer. Ainsi, les disparités entre les qualifications disponibles et celles requises par les employeurs peuvent mener à un chômage prolongé, même lorsque des emplois sont vacants.

**Questions de révision**:
- Comment les fluctuations cycliques de l'économie influencent-elles le niveau de chômage, et pourquoi une récession peut-elle entraîner une augmentation du taux de chômage?
- En quoi les changements structurels, tels que l'évolution technologique et les changements dans le commerce international, peuvent-ils créer un déséquilibre entre l'offre et la demande de main-d'oeuvre, et quel impact cela a-t-il sur le chômage structurel?

### Facteurs Structurels

- Mismatch de Compétences
    - Évolution des besoins de l'industrie
    - Systèmes éducatifs inadaptés
- Changements Démographiques
    - Vieillissement de la population
    - Mobilité géographique limitée

> Les facteurs structurels du chômage s'attardent sur les aspects fondamentaux et durables de l'économie qui influencent l'emploi indépendamment des fluctuations conjoncturelles. Parmi eux, le mismatch de compétences est prépondérant. Il survient lorsque les compétences de la main-d'oeuvre ne correspondent pas aux besoins du marché du travail, souvent exacerbé par l'évolution rapide des technologies et l'innovation qui transforment les industries. L'éducation et la formation professionnelle peinent parfois à suivre le rythme, laissant des postes vacants malgré un nombre élevé de chômeurs.

> En parallèle, les changements démographiques ont un impact significatif. Les variations dans la composition par âge de la population, les taux de natalité et les mouvements migratoires peuvent modifier la taille et la nature de la force de travail. Par exemple, le vieillissement de la population dans de nombreux pays développés pose le défi de maintenir un marché du travail dynamique malgré une part croissante de retraités. De même, l'immigration peut à la fois combler les pénuries de compétences et intensifier la concurrence pour les emplois disponibles, influant ainsi sur le niveau de chômage structurel.

> Ces facteurs structurels nécessitent des réponses politiques adaptées, telles que des réformes dans l'éducation, des incitations à la mobilité professionnelle et géographique, ainsi que des politiques d'intégration efficaces pour gérer l'impact des migrations sur le marché du travail.

**Questions de révision**:
- Qu'est-ce que le mismatch de compétences et comment l'évolution des technologies et de l'innovation dans les industries contribue-t-il à ce phénomène? En quoi les défis associés à l'éducation et à la formation professionnelle peuvent-ils aggraver le problème du chômage structurel?
- Comment les changements démographiques, tels que le vieillissement de la population et les mouvements migratoires, influencent-ils la taille et la composition de la force de travail? Discutez de l'impact potentiel de ces changements sur le chômage structurel et des politiques qui pourraient être mises en oeuvre pour répondre à ces défis.

### Politiques et Réglementations

- Coût du Travail
    - Charges sociales élevées
    - Salaire minimum
- Protection de l'Emploi
    - Législation du travail rigide
    - Indemnisations chômage prolongées

> Les politiques et réglementations en matière de travail sont des leviers essentiels pour les gouvernements visant à influencer le marché de l'emploi et, par conséquent, le taux de chômage. En ce qui concerne le coût du travail, les décisions gouvernementales sur la fiscalité, les cotisations sociales, et le salaire minimum ont un impact direct sur l'emploi. Des coûts élevés peuvent dissuader les employeurs d'embaucher ou inciter à la recherche d'alternatives automatisées, tandis que des charges allégées peuvent encourager l'embauche.

> Par ailleurs, la protection de l'emploi est un autre domaine clé de réglementation. Des lois strictes sur la protection de l'emploi, incluant les procédures de licenciement complexes et coûteuses, peuvent sécuriser l'emploi des travailleurs mais aussi rendre les employeurs réticents à créer de nouveaux postes. À l'inverse, une réglementation plus flexible peut faciliter l'ajustement de la main-d'oeuvre aux besoins changeants des entreprises, mais elle peut aussi mener à une précarité de l'emploi accrue.

> Les politiques en matière de coût et de protection du travail doivent donc trouver un équilibre: suffisamment protectrices pour offrir une sécurité aux travailleurs, mais assez flexibles pour permettre aux entreprises de s'adapter rapidement aux conditions économiques changeantes et de rester compétitives sur le marché mondial. C'est dans cette tension entre sécurité de l'emploi et flexibilité du marché que les gouvernements doivent naviguer pour concevoir des politiques efficaces.

<div style="text-align:center;">
  <img src="../inkscape-files/diagrams/demande-offre-travail-prix-plancher.svg" 
       alt="Exemple de prix plancher."
       style="max-width:75%; height:auto;" 
       title="Deséquilibre sur le marché du travail" />
  <figcaption>Exemple de prix plancher.</figcaption>     
</div>


**Questions de révision**:
- Comment les décisions gouvernementales concernant la fiscalité, les cotisations sociales et le salaire minimum affectent-elles l'emploi et quels peuvent être les effets contradictoires de coûts de travail élevés sur les décisions d'embauche des employeurs?
- Quels sont les avantages et les inconvénients des lois strictes sur la protection de l'emploi pour les travailleurs et les employeurs, et comment un équilibre peut-il être atteint entre la sécurité de l'emploi et la flexibilité du marché du travail?

### Facteurs Sociaux et Technologiques

- Progrès Technologiques
    - Remplacement de la main-d'oeuvre par la technologie
- Réseaux Sociaux et Professionnels
    - Importance du réseau dans la recherche d'emploi
    - Exclusion sociale

> Les facteurs sociaux et technologiques influencent fortement la dynamique de l'emploi et du chômage. Le progrès technologique, tout en étant un moteur de croissance économique et d'efficience, peut aussi mener au phénomène de "destruction créatrice" où de nouveaux produits ou méthodes de production rendent les anciennes technologies obsolètes, impactant directement le marché de l'emploi. L'automatisation et la numérisation peuvent substituer le travail humain dans certains secteurs, conduisant à une redéfinition des besoins en compétences et potentiellement à un chômage structurel si la main-d'oeuvre n'est pas requalifiée pour répondre aux nouvelles exigences.

> En parallèle, les réseaux sociaux et professionnels jouent un rôle croissant dans le marché de l'emploi. Ils permettent une mise en relation plus fluide entre les offres et les demandes d'emploi, facilitant les opportunités de networking qui peuvent s'avérer déterminantes dans la recherche d'emploi. L'accès à l'information sur les postes vacants ou les compétences demandées se fait désormais en temps réel, permettant une réactivité accrue des chercheurs d'emploi. Toutefois, l'inégalité d'accès à ces réseaux peut creuser le fossé entre ceux qui en bénéficient et les autres, accentuant les disparités sur le marché du travail. Ainsi, les politiques d'emploi doivent tenir compte de ces facteurs sociaux et technologiques pour favoriser une intégration équitable et efficace dans l'ère numérique.

<div style="text-align:center;">
  <img src="../inkscape-files/diagrams/demande-offre-travail-prix-hausse-technologie-travailleurs-qualifies-nonqualifies.svg" alt="Figure: Technologie et salaires - Application de l'offre et de la demande"
       style="max-width:75%; height:auto;" title="La courbe des salaires" />
  <figcaption>Figure: Technologie et salaires - Application de l'offre et de la demande</figcaption>     
</div>

<br>

<br>

**Questions de révision**:
- Comment le concept de 'destruction créatrice' se rapporte-t-il au progrès technologique, et quelles sont les implications potentielles pour l'emploi et la nécessité de requalification de la main-d'oeuvre ?
- De quelle manière les réseaux sociaux et professionnels ont-ils transformé le marché de l'emploi, et comment un accès inégal à ces réseaux pourrait-il affecter les disparités en matière d'emploi ?

### Influences Internationales

- Mondialisation
    - Concurrence internationale
    - Échanges commerciaux
- Crises Économiques Mondiales
    - Crises financières
    - Chocs pétroliers

> Les influences internationales jouent un rôle prépondérant dans la dynamique de l'emploi à l'échelle nationale. La mondialisation, caractérisée par une intégration économique accrue entre les nations à travers le commerce, l'investissement et la technologie, a remodelé les marchés du travail. Elle offre des opportunités d'expansion et de diversification économique, mais expose également les économies locales à la concurrence internationale, pouvant entraîner des délocalisations d'entreprises et une pression à la baisse sur les salaires dans certains secteurs.

> Les crises économiques mondiales, telles que la crise financière de 2008 ou la pandémie de COVID-19, ont également des répercussions profondes sur l'emploi. Ces crises provoquent des chocs économiques qui se traduisent par des licenciements massifs, une consommation réduite et un investissement d'entreprise en berne, engendrant une augmentation du chômage à une échelle souvent difficile à gérer rapidement par les politiques nationales seules.

> Face à ces influences, les gouvernements sont contraints de développer des stratégies qui non seulement protègent leurs marchés du travail contre les effets négatifs de la mondialisation et des récessions mondiales, mais qui exploitent également les aspects positifs de l'intégration économique internationale pour stimuler la création d'emplois et la croissance économique durable.

**Questions de révision**:
- De quelle manière la mondialisation a-t-elle affecté les marchés du travail locaux, et quels sont les avantages et les inconvénients qui en résultent pour l'emploi national ?
- Quelles stratégies peuvent être adoptées par les gouvernements pour atténuer les impacts négatifs des crises économiques mondiales sur l'emploi national, tout en maximisant les bénéfices de l'intégration économique internationale ?

## 4. Impact du Chômage

- Coûts économiques (perte de PIB, augmentation des dépenses gouvernementales en prestations).
- Coûts sociaux (pauvreté, inégalité, troubles sociaux).
- Impact psychologique sur les individus (santé mentale, stress).

```mermaid
graph TD;
A[Impact du Chômage] -->  B1[Coûts <br> Économiques];
A --> B2[Effets <br> Sociaux];
A --> B3[Conséquences sur <br> le Marché du Travail];
A --> B4[Impact <br> Psychologique];
A --> B5[Effets à <br> Long Terme];
```

> L'impact du chômage dépasse largement les simples statistiques économiques, affectant profondément la société et les individus. Sur le plan économique, le chômage entraîne une perte de revenus pour les individus et réduit la consommation des ménages, ce qui diminue la demande globale et peut freiner la croissance économique. Pour les pouvoirs publics, il représente un double fardeau financier : les recettes fiscales baissent tandis que les dépenses pour les indemnités chômage et les aides sociales augmentent.

> Les coûts sociaux du chômage sont également importants. Il peut exacerber les inégalités, marginaliser certaines couches de la population et augmenter la dépendance aux aides publiques. Les tensions sociales peuvent s'intensifier, notamment lorsque le chômage affecte de manière disproportionnée certaines régions ou groupes démographiques.

> Sur le plan individuel, l'impact psychologique du chômage est profond. Il peut affecter l'estime de soi, provoquer stress et anxiété, et mener à des problèmes de santé mentale et physique. Le manque de structure quotidienne et la perte de relations sociales liées au travail peuvent entraîner un sentiment d'isolement et de désespoir.

> Les gouvernements sont donc confrontés au défi de gérer non seulement les conséquences économiques du chômage, mais aussi ses répercussions sociales et individuelles, ce qui nécessite une approche holistique allant au-delà des simples mesures économiques.

**Questions de révision**:
- Quelles sont les conséquences économiques directes du chômage sur les individus et l'économie globale, et comment cela influence-t-il les finances publiques ?
- En quoi le chômage affecte-t-il la santé mentale et le bien-être des individus, et quelles stratégies les gouvernements peuvent-ils adopter pour répondre aux défis économiques, sociaux et individuels posés par le chômage ?

### Coûts Économiques

- Perte de Production
    - Baisse du PIB potentiel
- Pression sur les Finances Publiques
    - Augmentation des dépenses pour les allocations chômage
    - Réduction des recettes fiscales

> Le chômage engendre d'importants coûts économiques pour une société, parmi lesquels la perte de production et la pression accrue sur les finances publiques sont particulièrement saillants. Lorsque les travailleurs sont au chômage, leurs compétences ne sont pas utilisées, ce qui représente une perte de production potentielle pour l'économie. Cette sous-utilisation de la force de travail réduit le PIB potentiel et la croissance économique, entravant ainsi la prospérité à long terme du pays.

> D'un autre côté, le chômage exerce une pression significative sur les finances publiques. Avec l'augmentation du nombre de personnes dépendantes des allocations chômage et d'autres formes d'assistance sociale, les dépenses gouvernementales s'accroissent. Simultanément, il y a une baisse des recettes fiscales car moins de personnes travaillent et contribuent à la base d'imposition. Ce double impact peut conduire à des déficits budgétaires plus importants et à une accumulation de la dette publique, restreignant la capacité du gouvernement à investir dans d'autres domaines essentiels comme l'éducation, la santé ou l'infrastructure.

> La gestion de ces coûts économiques exige des politiques qui stimulent la création d'emplois et la reprise économique, tout en assurant un filet de sécurité adéquat pour soutenir les individus pendant les périodes de transition ou de recherche d'emploi.

**Questions de révision**:
- Quelle est l'incidence du chômage sur le produit intérieur brut (PIB) et quel double effet le chômage a-t-il sur les finances publiques ?
- Quelles stratégies politiques peuvent être mises en oeuvre pour atténuer les effets économiques négatifs du chômage et comment ces stratégies contribuent-elles à la reprise économique et à la sécurité sociale ?

### Effets Sociaux

- Pauvreté et Exclusion Sociale
    - Risque accru de pauvreté
    - Marginalisation sociale
- Santé et Bien-être
    - Problèmes de santé mentale
    - Stress et anxiété

> Le chômage est souvent à l'origine d'effets sociaux préoccupants, notamment en matière de pauvreté et d'exclusion sociale, ainsi que sur la santé et le bien-être des individus. Quand une personne perd son emploi, non seulement ses revenus diminuent, la rendant plus vulnérable à la pauvreté, mais elle risque également de souffrir d'une exclusion sociale. Le travail étant une source d'identité et d'appartenance, son absence peut conduire à l'isolement et à l'érosion du lien social.

> En ce qui concerne la santé et le bien-être, les effets du chômage sont multiples et peuvent être graves. Les études montrent que les taux de dépression, d'anxiété et d'autres troubles mentaux sont plus élevés chez les chômeurs. De plus, la perte de revenus peut entraîner des difficultés d'accès aux soins de santé et une détérioration de l'alimentation ou du logement, exacerbant ainsi les problèmes de santé physique.

> Les gouvernements sont donc confrontés à la tâche complexe de mettre en place des mesures qui réduisent la pauvreté et favorisent l'inclusion sociale, tout en veillant à la santé et au bien-être des citoyens. Des programmes de soutien du revenu, des services de santé mentale accessibles et des initiatives de réintégration professionnelle sont essentiels pour atténuer les effets sociaux négatifs du chômage.

**Questions de révision**:
- Expliquez comment le chômage peut conduire à la pauvreté et à l'exclusion sociale, et quel est le rôle du travail dans le sentiment d'appartenance et d'identité d'un individu.
- Quels sont les impacts du chômage sur la santé mentale et physique des individus ? De quelle manière la perte de revenus due au chômage peut-elle affecter l'accès aux soins de santé et à la qualité de vie d'une personne ?

**Questions de révision**:
- Quelles sont les conséquences du chômage sur la situation sociale des individus et de quelle manière peut-il affecter leur identité et leur sentiment d'appartenance à la société ?
- Quelles mesures les gouvernements peuvent-ils prendre pour prévenir la pauvreté, promouvoir l'inclusion sociale et assurer la santé et le bien-être des personnes au chômage ?

### Conséquences sur le Marché du Travail

- Déqualification: Perte de compétences sur le long terme
- Hystérésis: Effet persistant sur le taux de chômage même après la reprise économique

> Les répercussions du chômage sur le marché du travail peuvent être durables et profondes, affectant non seulement les individus actuellement sans emploi, mais également la dynamique générale de l'emploi. Un phénomène particulièrement inquiétant est celui de la déqualification, qui se produit lorsque les compétences des travailleurs sans emploi deviennent obsolètes en raison du manque d'utilisation ou de l'évolution rapide des technologies et des besoins du marché. Cela peut diminuer leur employabilité future et conduire à un cercle vicieux où le chômage conduit à une moindre qualification, réduisant encore plus les chances de retrouver un emploi.

> Un autre concept important est l'hystérésis sur le marché du travail, une situation où le chômage élevé d'aujourd'hui contribue à un taux de chômage plus élevé à l'avenir, même après que l'économie se soit améliorée. Cela peut se produire parce que les employeurs peuvent préférer embaucher des travailleurs qui n'ont pas été hors du travail pendant de longues périodes, percevant les chômeurs de longue durée comme moins désirables ou moins compétents. En outre, les travailleurs eux-mêmes peuvent perdre la motivation ou la confiance nécessaire pour chercher activement un emploi.

> Face à ces conséquences, il est essentiel que les politiques du marché du travail visent non seulement à réduire le chômage, mais aussi à mettre en place des programmes de formation continue et de reconversion professionnelle pour maintenir et améliorer les compétences des travailleurs, ainsi que des mesures pour éviter la stigmatisation des chômeurs de longue durée.

**Questions de révision**:
- Comment la déqualification affecte-t-elle l'employabilité des chômeurs, et quelles sont les conséquences d'une évolution rapide des technologies et des besoins du marché sur les compétences des travailleurs ?
- Qu'est-ce que l'hystérésis sur le marché du travail, et quelles peuvent être les conséquences d'une période de chômage prolongée sur la perception des employeurs et la motivation des travailleurs ?

### Impact Psychologique

- Sur les Individus
    - Perte d'estime de soi
    - Démotivation professionnelle
- Sur les Familles
    - Tensions familiales
    - Réduction du niveau de vie

> L'impact psychologique du chômage s'étend profondément dans la vie des individus et des familles. Pour les personnes sans emploi, les conséquences vont au-delà de la perte de revenu. Le chômage peut éroder l'estime de soi et l'identité professionnelle, entraînant stress, anxiété, et parfois dépression. Les recherches montrent une corrélation entre le chômage et une augmentation du risque de troubles psychologiques, voire de morbidité ou de mortalité prématurée. L'absence d'une routine quotidienne et le manque de buts professionnels peuvent également conduire à un sentiment d'isolement et de désengagement social.

> Au sein de la famille, les tensions peuvent s'exacerber sous l'effet de l'incertitude financière et du stress émotionnel lié au chômage. Les rôles au sein du foyer peuvent se trouver bouleversés, affectant la dynamique familiale et le bien-être des enfants. Le stress parental peut impacter négativement le développement et la santé mentale des enfants, ainsi que leur performance académique.

> Ainsi, l'impact psychologique du chômage appelle une réponse comprenant un soutien psychosocial aux individus et aux familles. Les services d'aide à l'emploi devraient être complétés par un soutien en santé mentale, des programmes de conseil familial et des initiatives communautaires pour favoriser la résilience face à l'adversité économique.

**Questions de révision**:
- Quelles sont les répercussions du chômage sur la santé mentale et l'estime de soi des individus, et comment la perte de routine quotidienne peut-elle contribuer au sentiment d'isolement et de désengagement social ?
- De quelle manière le chômage influence-t-il les tensions familiales et le bien-être des enfants, et quel rôle le stress parental peut-il jouer dans le développement des enfants ?

### Effets à Long Terme

- Sur la Société
    - Inégalités croissantes
    - Cohésion sociale affaiblie
- Sur l'Économie
    - Investissement en capital humain réduit
    - Frein à l'innovation et à la croissance

> Les effets à long terme du chômage sur la société et l'économie sont multiples et interconnectés, marquant profondément le tissu social et la dynamique économique d'un pays. Sociétalement, un taux de chômage élevé peut engendrer une fracture sociale, avec une augmentation de la pauvreté, de l'exclusion et de la marginalisation de certains groupes. Ces effets sont susceptibles d'accroître les clivages sociaux et de diminuer la cohésion sociale, menant à des tensions accrues et à une détérioration du capital social.

> Économiquement, les répercussions se manifestent par une réduction du potentiel de croissance et par l'altération de la compétitivité à long terme. Les travailleurs au chômage prolongé peuvent voir leurs compétences se déprécier, ce qui limite leur contribution future à l'économie et freine l'innovation. Le manque d'investissements dans le capital humain pendant les périodes de chômage élevé peut également conduire à une perte de productivité et à un déclin dans l'attractivité d'un pays pour les investisseurs.

> Pour atténuer ces effets à long terme, il est crucial d'adopter des politiques actives de marché du travail, d'encourager la formation continue et de veiller à la réinsertion rapide des chômeurs dans le marché du travail. Cela exige des investissements stratégiques dans l'éducation et la formation professionnelle, ainsi que des mesures visant à maintenir l'équilibre entre la flexibilité du marché du travail et la sécurité des travailleurs.

**Questions de révision**:
- Quelles sont les implications d'un taux de chômage élevé sur la cohésion sociale et le capital social d'une société, et comment cela peut-il influencer les clivages sociaux au sein du pays ?
- En quoi le chômage prolongé peut-il nuire à la croissance économique future et à la compétitivité d'un pays, et quelles stratégies politiques peuvent être mises en oeuvre pour contrer ces effets négatifs à long terme ?

## 5. Cadres Théoriques

- Aperçu succinct de différentes théories économiques concernant le chômage (économie keynésienne, économie classique, perspective monétariste).
- Taux naturel de chômage et le concept de plein emploi.

```mermaid
graph TD;
A[Cadres Théoriques du Chômage] -->  B1[Théorie <br> Classique];
A --> B2[Théorie <br> Keynésienne];
A --> B3[Théorie du <br> Chômage de Recherche];
A --> B4[Théorie de <br> l'Inadéquation des <br> Compétences];
A --> B5[Modèles <br> Monétaristes];
A --> B6[Nouvelle <br> Économie <br> Classique];
A --> B7[Institutionnalisme];
```

### Théorie Classique

- Marché du travail auto-régulé
- Chômage volontaire
- Politique recommandée: Non-intervention

> La théorie classique du chômage se fonde sur l'idée que le marché du travail est auto-régulé, et que les déséquilibres tels que le chômage sont temporaires et s'ajustent par des modifications de salaires. Selon cette perspective, le chômage est souvent considéré comme volontaire, résultant de la décision des individus de ne pas accepter des emplois aux taux de salaires courants. La politique économique recommandée par les classiques est donc celle de la non-intervention, en suggérant que toute tentative de contrôler le marché, par des salaires minimums ou des régulations, peut en fait provoquer des distorsions et entraver le fonctionnement efficient du marché.

<div style="text-align:center;">
  <img src="../inkscape-files/diagrams/demande-offre-travail-theorie-classique.svg" alt="Figure: Theorie Classique"
       style="max-width:75%; height:auto;" title="La courbe des salaires" />
  <figcaption>Figure: Les niveaux de salaire équilibrent l'offre et la demande de main-d'oeuvre, conduisant à un plein emploi sous les hypothèses classiques</figcaption>     
</div>



### Théorie Keynésienne

- Insuffisance de la demande globale
- Chômage involontaire
- Politique recommandée: Intervention de l'État (politique fiscale, dépenses publiques)

> La théorie keynésienne attribue le chômage à une insuffisance de la demande globale pour les biens et services. Contrairement à l'approche classique, elle reconnaît l'existence d'un chômage involontaire, où des individus sont prêts et capables de travailler au salaire en vigueur, mais ne peuvent trouver d'emploi en raison du ralentissement économique. Keynésiens recommandent une intervention active de l'État pour stimuler l'économie par le biais de la politique fiscale et de l'augmentation des dépenses publiques. Ces mesures visent à augmenter la demande globale et, par conséquent, à réduire le taux de chômage.



### Théorie du Chômage de Recherche

- Processus de recherche d’emploi
- Frictions sur le marché du travail
- Politique recommandée: Formation, aides à la mobilité

> La théorie du chômage de recherche met en lumière les frictions inhérentes au marché du travail. Elle reconnaît que la recherche d'emploi est un processus qui prend du temps et que des déséquilibres temporaires peuvent survenir car les travailleurs cherchent à améliorer leur adéquation à un emploi ou à négocier de meilleurs salaires. Les chômeurs sont en phase de transition plutôt qu'en situation d'inactivité complète. En réponse à ces frictions, la politique recommandée consiste à soutenir la formation continue et à fournir des aides à la mobilité pour accélérer le processus de mise en adéquation entre les employeurs et les employés potentiels, réduisant ainsi la durée du chômage.

### Théorie de l'Inadéquation des Compétences

- Déséquilibre compétences-emplois
- Changement technologique rapide
- Politique recommandée: Formation continue et éducation

> La théorie de l'inadéquation des compétences souligne le déséquilibre entre les compétences des travailleurs et les besoins du marché du travail, exacerbé par l'évolution rapide des technologies. Elle suggère que le chômage résulte souvent d'un manque de compétences requises pour les postes disponibles, plutôt que d'une absence d'emplois. Pour remédier à cette inadéquation, la politique recommandée est d'investir dans la formation continue et l'éducation, afin de permettre aux travailleurs d'acquérir les compétences nécessaires pour répondre aux exigences changeantes des emplois et ainsi favoriser une meilleure adéquation entre l'offre et la demande de travail.

### Modèles Monétaristes

- Importance de la politique monétaire
- Chômage naturel
- Politique recommandée: Stabilité des prix, faible inflation

> Les modèles monétaristes mettent en avant l'importance de la politique monétaire dans la régulation de l'économie et le maintien d'un taux de chômage dit "naturel", où toutes les fluctuations de l'emploi sont dues à des changements dans la structure du marché plutôt qu'à des cycles économiques. Selon cette école de pensée, le rôle de l'État devrait se concentrer sur la maîtrise de l'inflation par une politique monétaire prudente et une expansion monétaire modérée. La politique recommandée vise la stabilité des prix comme moyen d'assurer la santé économique et de réduire l'incertitude, favorisant ainsi un environnement propice à l'emploi et à la croissance économique.

### Nouvelle Économie Classique

- Salaires rigides
- Imperfections du marché
- Politique recommandée: Flexibilisation du marché du travail

> La Nouvelle Économie Classique reconnaît l'existence de salaires rigides et d'imperfections sur le marché du travail, qui peuvent entraver l'ajustement naturel du marché et la création d'emplois. Elle postule que ces rigidités sont souvent le résultat de réglementations excessives, d'interventions étatiques ou de pratiques de négociation collective qui fixent les salaires à des niveaux supérieurs à ceux du marché. La politique préconisée par les économistes de cette école est la flexibilisation du marché du travail : diminuer les contraintes réglementaires, favoriser une plus grande négociation individuelle des salaires et des conditions de travail, dans le but de réduire le chômage et d'accroître l'efficacité économique.

### Institutionnalisme

- Rôle des institutions
- Contrats, syndicats, législation
- Politique recommandée: Réformes structurelles

> L'institutionnalisme met l'accent sur le rôle crucial des institutions telles que les contrats de travail, les syndicats et la législation du travail dans la détermination des résultats économiques, y compris le niveau de chômage. Cette approche souligne que ces institutions peuvent souvent rigidifier le marché du travail, influençant les niveaux de salaire et les conditions d'embauche ou de licenciement. Les institutionnalistes recommandent des réformes structurelles visant à moderniser ces institutions pour les rendre plus adaptatives aux conditions économiques changeantes, afin de promouvoir l'emploi et d'équilibrer la protection des travailleurs avec la nécessité d'une économie flexible et réactive.


## 6. Détermination des salaires

A compléter : p. 417, p. 464 du Cambridge A-level

## 7. La relation entre l'inflation et le chômage

A compléter : p. 505 du Cambridge A-level


**Questions de révision**:
- Théorie Classique :
    - Comment la théorie classique explique-t-elle le phénomène du chômage et quel type de politique économique recommande-t-elle ?
    - Selon la perspective classique, quel est le principal facteur qui conduit au chômage et pourquoi la non-intervention est-elle préconisée ?
- Théorie Keynésienne :
    - Quelle est la principale cause du chômage selon la théorie keynésienne, et quelle intervention est suggérée pour y remédier ?
    - En quoi la vision du chômage de Keynésiens diffère-t-elle de celle de la théorie classique ?
- Théorie du Chômage de Recherche :
    - Qu'est-ce que la théorie du chômage de recherche identifie comme la cause principale du chômage et quelles politiques recommande-t-elle ?
    - Comment la théorie du chômage de recherche perçoit-elle le rôle de la formation et des aides à la mobilité dans la résolution du chômage ?
- Théorie de l'Inadéquation des Compétences :
    - Quel déséquilibre la théorie de l'inadéquation des compétences met-elle en avant comme cause du chômage ?
    - Quelles solutions la théorie de l'inadéquation des compétences propose-t-elle pour répondre aux défis posés par le chômage ?
- Modèles Monétaristes :
    - Quelle est la vision des monétaristes concernant le rôle de la politique monétaire dans la gestion du chômage ?
    - Quel est le concept de chômage naturel dans le cadre des modèles monétaristes et quelle est la politique économique préconisée ?
- Nouvelle Économie Classique :
    - Quelles sont les imperfections du marché du travail identifiées par la Nouvelle Économie Classique et quelle politique est recommandée pour y remédier ?
    - Comment la Nouvelle Économie Classique justifie-t-elle la flexibilisation du marché du travail et quelles en sont les conséquences attendues sur le chômage ?
- Institutionnalisme :
    - Selon l'institutionnalisme, comment les institutions affectent-elles le marché du travail et le chômage ?
    - Quelles réformes structurelles sont recommandées par l'approche institutionnaliste pour améliorer les conditions de l'emploi ?


## 6. Graphiques

1. **Théorie Classique** :
- **Diagramme de l'offre et de la demande pour le marché du travail** : Illustrer comment les niveaux de salaire équilibrent l'offre et la demande de main-d'oeuvre, conduisant à un plein emploi sous les hypothèses classiques. 
- **Marché du travail avec salaire minimum** : Montrer comment la fixation d'un salaire minimum au-dessus de l'équilibre peut entraîner un excédent de main-d'oeuvre (chômage). <br> [Figure: chômage classique](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSbaR0_XUcCTAJOO78hhWNlqhDLhChvYm5ZEQ&usqp=CAU)
2. **Théorie Keynésienne** :
- **Modèle de la demande agrégée et de l'offre agrégée (AD-AS)** : Mettre en évidence comment une demande agrégée insuffisante peut conduire au chômage à l'équilibre.
- **Croix Keynésienne** : Démontrer la relation entre la dépense agrégée et le PIB réel, en soulignant l'effet multiplicateur. <br> [Figure: Comment le modèle AD/AS intègre la croissance, le chômage et l'inflation](https://socialsci.libretexts.org/@api/deki/files/111/CNX_Econ_C24_015.jpg)
3. **Théorie du Chômage de Recherche** :
- **Modèle de recherche d'emploi et d'appariement** : Une représentation graphique des chercheurs d'emploi et des postes vacants, montrant le processus d'appariement.
- **Courbe de Beveridge** : Représenter la relation entre les offres d'emploi et le taux de chômage, en mettant en évidence la nature frictionnelle du chômage. <br> [Figure: La courbe de Beveridge](https://www.alternatives-economiques.fr/sites/default/files/public/media/20120501/A313052B.GIF)
4. **Théorie de l'Inadéquation des Compétences** :
- **Diagramme de l'écart de compétences** : Montrer la discordance entre les compétences que possèdent les travailleurs et les compétences nécessaires pour les emplois disponibles.
- **Investissement en Capital Humain** : Illustrer l'impact de l'éducation et de la formation sur la productivité et l'employabilité des travailleurs. <br> [Figure: Taux de retour sur investissement dans le capital humain](https://ses.ens-lyon.fr/ses/images/articles/1240479794609.jpg)
5. **Modèles Monétaristes** :
- **Courbe de Phillips** : Présenter le compromis supposé à court terme entre le chômage et l'inflation.
- **Graphique de l'offre de monnaie et de l'inflation** : Expliquer le rôle de la politique monétaire et l'effet de l'offre de monnaie sur les niveaux de prix et l'activité économique. <br> [Figure: La courbe de Phillips](https://theothereconomy.com/media/images/Courbe_de_Phillips.max-600x600.png)
6. **Nouvelle Économie Classique** :
- **Marché du travail avec rigidité des salaires** : Un diagramme qui montre comment la rigidité des salaires peut conduire au chômage, avec l'offre de travail ne répondant pas à la demande de travail à un taux de salaire plus élevé.
- **Équilibre du marché du travail et salaire flexible** : Illustrer comment la suppression de la rigidité des salaires peut conduire à un nouvel équilibre et réduire le chômage. <br> [Figure: Rigidité du salaire](http://annotations.blog.free.fr/public/marche_du_travail_neoclassique__salaire_retour_a_l__equilibre__Martin_Anota_.png)
7. **Institutionnalisme** :
- **Diagramme de l'impact du cadre institutionnel** : Représenter comment les institutions du marché du travail (comme les syndicats, les réglementations) impactent la fixation des salaires et les niveaux d'emploi.
- **Impact des réformes structurelles** : Un diagramme avant-après montrant les effets potentiels des réformes structurelles sur le marché du travail. <br> [Figure: L'institutionnalisation sur le marché du travail ](https://e.educlever.com/img/6/0/1/7/601773.png)



## 7. Approches microéconomiques sur l'emploi

### 7.1. La demande de travail

De nombreux principes présentés dans le chapitre sur l'offre et la demande de biens et services peuvent être appliqués au marché du travail. Il existe toutefois une différence fondamentale. La demande de travail est une demande dérivée. Cela signifie que la demande de main-d'oeuvre d'une entreprise est due à sa décision de produire certains biens ou services. Le travail n'est donc pas demandé pour lui-même, mais parce qu'il est essentiel à la production de biens ou de services. 
Par exemple, les employés de bureau sont employés parce qu'ils sont nécessaires à une entreprise pour mener à bien ses activités quotidiennes, les rues ont besoin d'être nettoyées par conséquent les éboueurs sont employés pour les nettoyer.
Les jeunes ont besoin d'être éduqués, il faut donc des enseignants. Les entreprises ont besoin de gestionnaires pour atteindre leurs objectifs. Ces exemples peuvent sembler évidents, mais ils sous-tendent l'ensemble des fondements de l'économie du travail. Un petit nombre de stars du cinéma, de pop stars et de personnalités sportives ont des talents exceptionnels. La demande pour leurs services est élevée et ils peuvent exiger une rémunération importante pour leurs services.

#### 7.1.1. Le revenu marginal du produit du travail

Une entreprise qui cherche à maximiser son profit souhaite connaître la valeur de ce qu'elle produit.
Le tableau ci-dessous indique la production qui peut être réalisée si l'entreprise emploie jusqu'à jusqu'à six travailleurs. Le prix de la production est de 10 euros par unité ; le taux de salaire est fixé à 300 euros par semaine. Il s'agit du coût marginal de l'emploi d'un travailleur supplémentaire. On suppose que le travail est le seul facteur de production variable. Le revenu marginal du travail est la recette supplémentaire gagné par l'entreprise lorsqu'elle emploie un travailleur supplémentaire. Il est calculé en multipliant le produit marginal d'un travailleur supplémentaire par le prix du produit. 

$ \text{Productivité marginal du travail} = \frac{ \Delta \text{Production} }{ \Delta \text{Emploi} } $  <br> 
<br> 
où $\Delta \text{Production} = \text{Production}_t - \text{Production}_{t-1}$ <br> 
<br>
et $\Delta \text{Emploi} = \text{Emploi}_t - \text{Emploi}_{t-1}$



| nombre de <br> travailleurs | production <br> totale | produit <br> marginal | prix <br> (en euro) | revenu marginal <br> du travail | taux de <br> salaire | contribution |
|-----------------------:|------------------:|-----------------:|---------------:|---------------------------:|----------------:|-------------:|
| 1                      | 20                | 20               | 10             | 200                        | 300             | -100         |
| 2                      | 55                | 35               | 10             | 350                        | 300             | 50           |
| 3                      | 99                | 44               | 10             | 440                        | 300             | 140          |
| 4                      | 129               | 30               | 10             | 300                        | 300             | 0            |
| 5                      | 153               | 24               | 10             | 240                        | 300             | -60          |
| 6                      | 165               | 12               | 10             | 120                        | 300             | -180         |



```python
### Python code to plot values from the table

import matplotlib.pyplot as plt

# Data
num_workers = [1, 2, 3, 4, 5, 6]
marginal_revenue = [200, 350, 440, 300, 240, 120]

# Plot
plt.figure(figsize=(8, 6))  # Set the figure size for a textbook format
plt.plot(num_workers, marginal_revenue, marker='o', linestyle='-')  # Plot with markers and solid line

# Horizontal line at 300 (dashed)
plt.axhline(y=300, color='r', linestyle='--', label='MR = 300')

# Labels and title
plt.xlabel("nombre de travailleurs")
plt.ylabel("revenu marginal du travail")
plt.title("Revenu Marginal vs. Nombre de travailleurs")

# Legend
plt.legend()

# Grid
plt.grid(True)

# Show the plot
plt.tight_layout()
plt.show()
```

<img src="../images/20240407-marginal-revenue-vs-number-of-workers.png" alt="marginal revenue vs number of workers" width="500" >


La courbe du revenu marginal augmente jusqu'au point où trois travailleurs sont employés. Elle diminue ensuite fortement, conformément à la loi des rendements décroissants. Il arrive un moment, après l'emploi du quatrième travailleur, où un travailleur supplémentaire augmente davantage les coûts que les recettes. L'emploi de ce travailleur coûte encore 300 euros, mais la production supplémentaire n'est que de 240 euros. Ainsi, au-delà de ce niveau d'emploi, la valeur du produit marginal est inférieure au salaire payé. Cela n'a manifestement aucun sens pour l'entreprise. On peut donc en déduire que l'entreprise doit embaucher des travailleurs jusqu'à ce que la valeur du produit marginal soit égale au taux de salaire payé. C'est à ce stade que quatre travailleurs sont employés. La courbe de revenu margial est donc la courbe de demande de travail.
Par conséquent, une entreprise doit continuer à embaucher de la main-d'oeuvre tant que le travailleur supplémentaire ajoute plus aux recettes qu'aux coûts de production de l'entreprise.

<b>Remarque : </b>Le <em>raisonnement à la marge</em> est au coeur de la prise de décision des firmes : revenu marginal = coût marginal

#### Activité

Le tableau ci-dessous indique la production hebdomadaire totale d'une entreprise utilisant un nombre différent de travailleurs. L'entreprise vend son produit 15 euros par unité et paie ses travailleurs 300 euros par semaine.

| Nombre de <br> travailleurs | Production <br> totale |
|:---------------------:|------------------:|
| 1                      | 8                 |
| 2                      | 20                |
| 3                      | 40                |
| 4                      | 55                |
| 5                      | 65                |
| 6                      | 72                |

1. Calculer le produit marginal et le revenu marginal.
2. Combien de travailleurs l'entreprise doit-elle embaucher pour maximiser ses bénéfices ?
3. Si le taux de salaire diminue de 25 %, combien de travailleurs supplémentaires l'entreprise devrait-elle employer ?


#### 7.1.2. La demande de main-d'oeuvre dans une entreprise

La demande de travail d'une entreprise repose sur deux hypothèses :

- L'entreprise qui souhaite embaucher de la main-d'oeuvre opère sur un marché parfaitement concurrentiel. Il y a de nombreux acheteurs et vendeurs de main-d'oeuvre, et aucune entreprise ni aucun travailleur ne peut influer sur le salaire versé.
- L'entreprise maximise son profit. Sa demande de main-d'oeuvre est basée sur la maximisation de la différence entre les recettes totales et les coûts totaux.


Trois facteurs principaux déterminent la demande de main-d'oeuvre dans une entreprise :
- <b>Taux de salaire</b>. Le taux de salaire est un type de prix. Il détermine la quantité de travail demandée par une entreprise. Une hausse du taux de salaire augmentera le coût du travail et entraînera probablement une baisse de la quantité de main-d'oeuvre demandée par une entreprise.
- <b>Productivité</b>. Un changement dans la productivité marginale du travail affecte la position de la courbe de demande de travail. Si les travailleurs sont plus productifs, la production augmente. Le travail devient une ressource plus attrayante pour l'entreprise et la demande de travailleurs augmente.
- <b>La demande pour le produit</b>. Compte tenu de la demande de travail dérivée, il s'ensuit qu'un changement dans la demande pour les produits d'une entreprise affectera sa demande de travail. Lorsque la demande d'un produit est élevée, une entreprise embauchera davantage de travailleurs. La courbe de La courbe de demande de main-d'oeuvre se déplacera vers la droite.



### 7.2. L'offre de travail

L'offre de main-d'oeuvre est le nombre total d'heures que la main-d'oeuvre est capable et désireuse de travailler pour un taux de salaire donné. Les principes généraux de l'offre présentés dans le chapitre sur l'offre des biens et services s'appliquent au marché du travail. Il convient de rappeler que le marché du travail implique des personnes et leur volonté de participer (ou non) au marché du travail. Cette décision dépend généralement du taux de salaire qui leur est offert pour leurs services, bien que pour certains travailleurs, le taux de salaire n'ait qu'une influence mineure sur leur décision de travailler ou non - d'autres facteurs comportementaux ont une influence plus importante.

Il est utile de considérer l'offre de main-d'oeuvre à trois niveaux : celui du travailleur individuel, celui d'une entreprise ou d'un secteur et celui de l'économie dans son ensemble. Différents facteurs affectent l'offre de main-d'oeuvre en fonction du niveau considéré.

#### 7.2.1. L'offre de travail des individus

Comme pour toute offre, le prix (ou le salaire dans ce cas) a une influence importante sur la décision d'un travailleur individuel d'entrer sur le marché du travail. Si le salaire est trop bas, un individu peut estimer qu'il ne vaut pas la peine d'aller travailler et décidera de rester à la maison. Peu de gens sont dans cette situation - la plupart d'entre nous ont besoin de travailler pour vivre.

La théorie économique suppose qu'il existe une relation positive entre l'offre de travail et le taux de salaire. Ainsi, lorsque le taux de salaire augmente, davantage de personnes sont disposées à offrir leurs services aux employeurs. Ce phénomène est représenté par la courbe d'offre de travail de l'individu, qui est principalement orientée vers le haut.

<img src="../inkscape-files/diagrams/taux-de-salaire-individuel.svg" alt="offre de travail individuel" width="500" >

Au-delà d'un certain point, il y a un compromis entre le travail et les loisirs. Les individus peuvent décider qu'ils préfèrent travailler moins et avoir plus de temps pour les loisirs. Ce point est indiqué par la courbe à partir du point X. Avant ce point, un travailleur individuel est davantage disposé à fournir de la main-d'oeuvre à mesure que le taux de salaire augmente. Il convient de souligner que ce point dépend de l'attitude de l'individu à l'égard du travail et des loisirs - le point X de la courbe d'offre de chaque individu variera. Il dépend également du type d'économie et de la situation personnelle et familiale du travailleur, en particulier du revenu dont il a besoin.

Un autre facteur susceptible d'affecter l'offre de travail d'un individu est le taux d'imposition sur le revenu. Dans tous les pays, celui-ci tend à être progressif. Les travailleurs à bas salaires ne paient que peu ou pas d'impôt sur le revenu. Au fur et à mesure que les salaires augmentent, une part plus importante de l'augmentation des salaires est versée à l'État sous forme d'impôt sur le revenu. Une structure typique de l'impôt sur le revenu est un taux standard de 20 %, avec un taux maximum de 45 % pour les revenus les plus élevés. Dans certains pays où les revenus sont les plus élevés, ce taux supérieur est souvent bien supérieur à 50 %. L'inconvénient est qu'un taux d'imposition élevé supprime l'incitation au travail. Les gouvernements doivent veiller à éviter cette situation, car elle risque de nuire aux perspectives économiques futures.


#### 7.2.2. L'offre de main-d'oeuvre à une entreprise 

La courbe d'offre de main-d'oeuvre d'une entreprise ou d'une branche d'activité est la somme des courbes d'offre individuelles de tous les travailleurs employés dans l'entreprise ou la branche d'activité. Elle est généralement orientée à la hausse. Comme pour un travailleur individuel, le nombre de travailleurs désireux d'offrir leur travail augmente avec le taux de salaire offert. La pente de cette courbe d'offre est mesurée par l'élasticité de l'offre de main-d'oeuvre, c'est-à-dire la mesure dans laquelle l'offre de main-d'oeuvre réagit à une variation du taux de salaire.
La figure ci-dessous présente deux courbes d'offre différentes - l'une inélastique (L1) et l'autre élastique (L2) - en fonction des salaires versés.

<img src="../inkscape-files/diagrams/offre-travail-elastique-inelastique.svg" alt="offre de travail individuel elastique et inelastique" width="500" >

Il y a plusieurs raisons à cette différence. L'une d'entre elles est évidente : les compétences requises pour exercer une profession donnée. En général, plus les compétences requises sont nombreuses, plus l'offre de main-d'oeuvre est inélastique. Cet argument s'applique également au niveau d'éducation et de formation requis pour exercer un emploi particulier. Normalement, un enseignant doit passer quatre ans après l'université pour obtenir les qualifications nécessaires. Cela signifie que l'offre d'enseignants est plus inélastique que celle des éboueurs, par exemple, pour lesquels peu d'études ou de formation sont requises.

La courbe d'offre L2 de la figure ci-dessus est donc probablement celle d'un secteur où les salaires sont bas et où il existe une offre abondante de main-d'oeuvre sans compétences ni formation particulières.

Dans un marché du travail concurrentiel, les taux de salaire offerts dans toutes les autres industries seront importants pour déterminer l'offre de main-d'oeuvre dans une industrie particulière. Dans les pays à revenu intermédiaire, un exemple pourrait être la différence entre les taux de salaire dans l'agriculture et les taux de salaire dans les industries manufacturières ou de transformation des aliments, qui paient généralement mieux leurs travailleurs.

#### Activité

Une entreprise emploie deux types de main-d'oeuvre différents. Les courbes d'offre de chacun d'entre eux sont présentées dans la figure ci-dessus. L'entreprise connaît une pénurie de ces deux types de travailleurs. Elle décide de dépenser de l'argent pour une reconversion professionnelle des travailleurs qualifiés au chômage et d'augmenter le taux de salaire des travailleurs non qualifiés au chômage.

1. À l'aide de diagrammes de l'offre et de la demande, comparez les effets de ces politiques sur le marché de l'entreprise pour chaque type de travailleur.
2. Examinez si la stratégie de l'entreprise a des chances de réussir.


#### 7.2.3. L'offre de travail à long terme

L'offre de main-d'oeuvre à long terme revêt une importance particulière pour l'économie dans son ensemble. Il existe des contrastes entre les pays à revenu intermédiaire de la tranche inférieure et les pays à revenu élevé, qui sont liés à des facteurs économiques plus larges.

Ces facteurs sont notamment les suivants :

- <b>La taille de la population.</b> Dans certains pays à revenu élevé, la population totale est relativement stable ; dans d'autres, elle augmente à un rythme modeste, principalement en raison de l'accroissement de l'immigration. Avec l'allongement de l'espérance de vie, les personnes en âge de travailler sont relativement moins nombreuses. En revanche, dans la plupart des pays à faible revenu et à revenu intermédiaire de la tranche inférieure, on observe une augmentation de l'offre globale de main-d'oeuvre, principalement due à des taux de natalité plus élevés et à l'amélioration des soins médicaux pour les jeunes enfants. Par conséquent, un nombre croissant de jeunes entrent sur le marché du travail. Cela signifie que la courbe d'offre de travail à long terme se déplace vers la droite, indiquant que davantage de travailleurs sont disposés à fournir leur travail à un taux de salaire donné.
- <b>Le taux de participation au marché du travail.</b> Ce terme est utilisé pour décrire la proportion de la population en âge de travailler qui occupe effectivement un emploi. Dans certains pays à revenu élevé, les travailleurs choisissent souvent de quitter le marché du travail en prenant une "retraite anticipée". Cette dernière intervient avant l'âge normal de la retraite, ce qui réduit le taux de participation au marché du travail. De plus en plus d'étudiants poursuivent leurs études à l'université, ce qui constitue une autre cause de la baisse du taux de participation au marché du travail. Une réduction du taux de participation au marché du travail déplace la courbe d'offre de travail à long terme vers la gauche.
- <b>Les niveaux d'imposition et de prestations.</b>  Comme le montre la figure illustrant l'offre de travail individuel, il arrive un moment où l'arbitrage entre travail et loisir affecte l'offre de travail d'un individu. Cela affecte également l'offre de main-d'oeuvre pour l'économie, en particulier dans les pays à revenu élevé. Les gouvernements doivent être très prudents dans leurs politiques d'imposition et d'indemnisation afin de s'assurer que l'offre de main-d'oeuvre à long terme n'est pas affectée par une réduction de la volonté de travailler. Au Royaume-Uni, le taux supérieur de l'impôt sur le revenu a été ramené de 50 % à 45 % en 2013. Vingt ans plus tôt, il était beaucoup plus progressif, les taux marginaux d'imposition pouvant atteindre 80 % de l'augmentation des revenus pour les plus hauts salaires. Dans ces conditions, il est clair qu'un individu est fortement incité à se retirer du marché du travail. Le niveau du chômage et des prestations sociales peut également affecter l'offre de travail à long terme de la même manière qu'il peut affecter l'offre de travail d'un individu. Par le biais de leurs politiques d'offre, les gouvernements cherchent à inciter certains types de travailleurs à rester actifs sur le marché du travail.
- <b>Immigration et émigration.</b>  L'immigration et l'émigration affectent l'offre de main-d'oeuvre à long terme dans une économie. En cas de pénurie de main-d'oeuvre, comme ce fut le cas au Royaume-Uni à la fin des années 1950 et au début des années 1960, des migrants ont quitté des pays du Commonwealth tels que la Barbade, la Jamaïque, le Bangladesh, l'Inde et le Pakistan, souvent pour travailler dans des secteurs relativement peu rémunérateurs et dans les services publics. L'offre de main-d'oeuvre s'en est trouvée accrue. L'émigration en provenance de ces pays a, à son tour, atténué les pressions exercées sur les marchés du travail de ces pays. Par le passé, le Royaume-Uni a été confronté à des pénuries de main-d'oeuvre dans les secteurs des soins infirmiers, de l'enseignement et des industries de haute technologie, ainsi que dans d'autres professions manuelles qualifiées. Depuis 2004, l'élargissement géographique de l'UE a entraîné un afflux massif d'environ deux millions de travailleurs migrants au Royaume-Uni. Ces travailleurs sont originaires des nouveaux États membres d'Europe centrale et orientale. Avant le retrait du Royaume-Uni de l'UE, il y avait environ un million de travailleurs polonais au Royaume-Uni. Il est probable que ce flux diminuera fortement après le retrait du Royaume-Uni.

Les facteurs mentionnés auparavant déterminent l'offre de main-d'oeuvre à long terme. Les glissements vers la gauche et vers la droite de l'offre de travail à long terme sont illustrés dans la figure ci-dessous.

<img src="../inkscape-files/diagrams/offre-travail-changement-long-terme.svg" alt="les variations de l'offre de travail à long terme" width="500" >

À long terme, l'offre de main-d'oeuvre à une entreprise particulière, distincte de l'économie, est influencée par les avantages nets d'un emploi. Ceux-ci comprennent les avantages monétaires et non monétaires. Pour la plupart des travailleurs, le salaire hebdomadaire ou mensuel est la raison la plus importante de l'offre de travail. D'autres facteurs monétaires peuvent être pris en compte, tels que les primes, les possibilités de travailler plus longtemps et les régimes de retraite. Les cadres supérieurs, en particulier, seront attirés par les emplois qui leur promettent d'énormes bonus s'ils réussissent. Il en va de même pour les sportifs professionnels de haut niveau.

Les avantages non monétaires sont nombreux et variés. Ils sont généralement pris en compte lorsqu'une personne envisage de rester dans un emploi particulier ou de changer d'entreprise ou de profession. Les avantages non monétaires comprennent les heures de travail, la sécurité de l'emploi, le droit aux vacances, les perspectives de promotion, l'emplacement du lieu de travail, la satisfaction au travail et la possibilité de travailler à domicile.

Un travailleur rationnel est susceptible de prendre en compte les deux types d'avantages avant de décider s'il doit ou non fournir de la main-d'oeuvre. Pour de nombreux travailleurs, les avantages non monétaires tendent à avoir une influence substantielle sur le choix de leur profession.

<b>Remarque : </b> L'offre de main-d'oeuvre à long terme est essentielle pour la croissance économique future d'une économie. Elle dépend de nombreux facteurs, qui ne peuvent pas toujours être facilement prévus par les économistes.

#### Application

1. A l'aide de diagrammes, expliquez l'effet sur un marché du travail lorsque :
- il y a un afflux de travailleurs migrants
- un grand nombre de travailleurs migrants retournent dans leur pays d'origine
2. Discutez des coûts et des avantages pour l'économie luxembourgeoise des tendances en matière de migration de la main-d'oeuvre mentionnées ci-dessus.


### 7.3. Détermination des salaires sur des marchés parfaits

Deux caractéristiques importantes du fonctionnement des marchés du travail sont désormais établies.

Il s'agit de :

- le salaire versé au travail est égal à la valeur du produit marginal du travail, le produit marginal de la recette
- la volonté de la main-d'oeuvre de fournir ses services au marché du travail dépend du taux de salaire offert. 

À certains égards, il peut sembler surprenant que le salaire puisse faire ces deux choses en même temps. Cela s'explique par la manière dont les salaires sont déterminés sur un marché du travail concurrentiel.

<img src="../inkscape-files/diagrams/demande-offre-travail-equilibre.svg" alt="équilibre sur le marché du travail" width="500" >

Le prix du travail, le salaire, n'est pas différent de tout autre prix dans la mesure où il dépend de l'offre et de la demande. La figure ci-dessus montre comment le salaire et la quantité de travail s'ajustent pour équilibrer l'offre et la demande. La courbe de demande reflète la valeur de la productivité marginale du travail. À l'équilibre, les travailleurs reçoivent la valeur de leur contribution à la production de biens et de services. Chaque entreprise achète donc de la main-d'oeuvre jusqu'à ce que la valeur du produit marginal soit égale au taux de salaire. Par conséquent, le salaire payé sur le marché doit être égal à la valeur du produit marginal du travail une fois qu'il a permis d'équilibrer l'offre et la demande. Le marché s'équilibre donc au salaire d'équilibre.

Le marché du travail est dynamique comme tout autre marché - tout changement dans l'offre ou la demande de main-d'œuvre modifie le salaire d'équilibre. La valeur du produit marginal du travail changera également du même montant puisque, par définition, elle doit toujours être égale au taux de salaire.

Prenons le cas de l'industrie de l'habillement. Une augmentation du revenu disponible des consommateurs dans les pays à haut revenu déplace la courbe de la demande de vêtements vers la droite, ce qui indique qu'une plus grande quantité sera demandée à n'importe quel prix. Cela affecte à son tour la demande de travailleurs produisant des vêtements, comme le montre la figure ci-dessous par un déplacement vers la droite de la courbe de demande de main-d'œuvre. Le résultat est que le salaire d'équilibre passe de W à W1 et que l'emploi augmente de L à L1. Le changement du taux de salaire reflète donc un changement de la valeur de la productivité marginale du travail.

<img src="../inkscape-files/diagrams/demande-offre-travail-hausse-de-la-demande.svg" alt="les effets d'une augmentation de la demande de main-d'œuvre" width="500" >

Une modification de l'offre de main-d'œuvre affectera également l'équilibre du marché. Supposons qu'il y ait une augmentation du nombre de travailleurs migrants et que cela augmente le nombre de travailleurs capables de produire des vêtements. Dans ce cas, la courbe d'offre de main-d'œuvre se déplace vers la droite. L'augmentation de l'offre de main-d'œuvre a un effet à la baisse sur les salaires, ce qui fait qu'il est plus rentable pour les entreprises produisant des vêtements d'embaucher davantage de main-d'œuvre. Plus le nombre de travailleurs augmente, plus leur productivité marginale diminue, de même que la valeur de leur produit marginal. Dans ce cas, les salaires sont réduits pour tous les travailleurs, bien que le niveau d'emploi augmente. Cette situation est illustrée dans la figure ci-dessous.

<img src="../inkscape-files/diagrams/demande-offre-travail-hausse-de-l-offre.svg" alt="les effets d'une augmentation de l'offre de main-d'œuvre" width="500" >


### 7.4. Détermination des salaires sur des marchés imparfaits

Jusqu'à présent, l'analyse du mode de détermination des salaires a supposé que les forces respectives de l'offre et de la demande opéraient librement, sans intervention. Cette hypothèse n'est pas réaliste car, sur de nombreux marchés du travail, la demande et l'offre de main-d'œuvre sont influencées par les actions des syndicats et des gouvernements. Ces interventions produisent ce que l'on appelle parfois des imperfections sur le marché du travail.

#### 7.4.1. Influence des syndicats sur la détermination des salaires et l'emploi

Les syndicats sont des organisations qui cherchent à représenter les travailleurs sur leur lieu de travail. Ils ont été créés et continuent d'exister parce que les individus (les travailleurs) ont très peu de pouvoir pour influencer les conditions d'emploi, y compris les salaires. Par le biais de la négociation collective, où les représentants syndicaux se réunissent avec les employeurs, les syndicats visent à :

- augmenter les salaires de leurs membres
- améliorer les conditions de travail
- maintenir les écarts de rémunération entre les travailleurs qualifiés et non qualifiés
- lutter contre les pertes d'emploi
- assurer un environnement de travail sûr
- garantir des avantages professionnels supplémentaires
- prévenir les licenciements abusifs.

Traditionnellement, les syndicats sont très présents dans le secteur public et dans la plupart des industries manufacturières. Ils sont moins importants dans le secteur des services. Les syndicats sont particulièrement puissants dans l'industrie des transports et dans les professions qui requièrent des compétences spécialisées. Les syndicats ont tendance à avoir le plus grand pouvoir de négociation lorsque la main-d'œuvre possède des compétences spécialisées ou est en pénurie.

L'analyse économique suggère que, sur un marché du travail concurrentiel, un syndicat puissant est en mesure de garantir à ses membres des salaires supérieurs au taux d'équilibre. Le fondement de cette affirmation est illustré par la figure ci-dessous. Au salaire d'équilibre, la quantité de main-d'œuvre employée est L. Si un syndicat puissant peut faire monter les salaires à Wu, par exemple, ce qui est supérieur au salaire d'équilibre, le nombre de travailleurs auxquels les employeurs offrent un emploi tombe à Lu. À ce salaire, le nombre de personnes souhaitant travailler est plus élevé. C'est ce que montre Lc.

<img src="../inkscape-files/diagrams/demande-offre-travail-salaire-minimum.svg" alt="L'effet d'un syndicat fort sur un marché du travail concurrentiel" width="500" >

Par conséquent, il y a un déficit entre ceux qui veulent travailler et ceux qui peuvent effectivement travailler en raison de l'influence du syndicat. Ce phénomène est illustré dans la figure ci-dessus par la différence horizontale entre Lc et Lu.

En pratique, il est difficile de prouver que cette théorie s'applique réellement aux marchés du travail dans le monde réel. Un exemple souvent cité est celui des acteurs au Royaume-Uni et aux États-Unis, où il existe des syndicats très puissants qui limitent le nombre de personnes pouvant travailler dans les films, à la télévision et dans les théâtres. Les salaires des membres des syndicats sont censés être soutenus de cette manière. D'autres exemples sont susceptibles d'apparaître sur les marchés du travail où un syndicat détient un monopole sur les travailleurs possédant un type particulier de compétences. Dans de nombreux pays, de telles pratiques ont été rendues illégales, limitant ainsi leur pouvoir d'agir de la sorte.

Les syndicats qui tentent de se comporter de la sorte sont accusés de jouer un jeu dangereux avec les employeurs. La crainte est qu'en raison des coûts élevés de la main-d'œuvre et des pratiques restrictives, les employeurs fassent faillite ou transfèrent leur production dans des pays où les niveaux de salaire sont moins élevés. Cette menace a été particulièrement grave pour certains constructeurs automobiles européens et américains - la production a été transférée de la France et de l'Allemagne vers d'autres pays de l'UE, tels que la République tchèque, la Pologne, la Roumanie et la Slovaquie, où les coûts de main-d'œuvre peuvent être inférieurs d'un cinquième à ceux des pays à revenu plus élevé. Il semblerait que les syndicats n'aient eu que très peu d'influence réelle dans ces circonstances.


#### 7.4.2. Influence du gouvernement sur la détermination des salaires et l'emploi en cas de salaire minimum

Le marché du travail a fait l'objet d'une intervention explicite des pouvoirs publics par l'introduction d'un salaire minimum. Au Royaume-Uni, par exemple, un salaire minimum s'applique depuis le début de 1999 à tous les travailleurs âgés de plus de 21 ans. Des salaires minimums ont également été adoptés dans la plupart des pays à revenu élevé et dans certaines parties de l'Amérique du Sud. Aux États-Unis, chaque État fixe son propre salaire minimum. Il n'y a pas d'exemple en Afrique et en Asie. L'objectif d'un salaire minimum est de réduire la pauvreté et l'exploitation des travailleurs qui ont peu ou pas de pouvoir de négociation avec leurs employeurs. En particulier, de nombreuses femmes employées dans des magasins, des petites entreprises et des emplois peu qualifiés, tels que les travaux à domicile et le nettoyage, ont reçu des salaires très bas. L'introduction d'un salaire minimum a revêtu une importance particulière pour elles.

La question de l'existence d'un salaire minimum est controversée. Certains affirment que l'introduction d'un salaire minimum entraîne une réduction du montant des allocations publiques versées aux familles à faible revenu. Il pourrait également y avoir une légère augmentation des recettes fiscales. Les opposants au salaire minimum ne sont pas convaincus par ces arguments ; ils estiment que des emplois sont perdus et que d'autres travailleurs faiblement rémunérés cherchent à obtenir une augmentation de salaire pour maintenir l'écart avec les plus bas salaires. Une inflation par les coûts peut se produire lorsqu'un salaire minimum est mis en œuvre, affectant l'économie dans son ensemble.

Le raisonnement graphique est similaire à celui présenté dans la section précédente.

### 7.5. Écarts de salaires

Il est évident que dans toute économie, il existe des différences de salaires entre les professions. Un pilote de ligne, par exemple, sera mieux payé qu'un agent de bord ; à son tour, l'agent de bord sera mieux payé que le travailleur qui nettoie la cabine après un vol. Les écarts salariaux existent entre les secteurs d'activité, entre les entreprises d'un même secteur et entre les différentes régions d'un pays. Dans une certaine mesure, les différences salariales peuvent s'expliquer en termes d'offre et de demande. Une profession qui fait l'objet d'une forte demande et d'une faible offre sera mieux rémunérée qu'une profession où l'offre de travailleurs est abondante.

Outre les forces du marché de l'offre et de la demande, les différences de salaires ont d'autres causes :

- Force de négociation
- Éducation et formation
- Travailleurs qualifiés et non qualifiés
- Travailleurs masculins et féminins
- Heures de travail
- Politique du gouvernement.


## 8. Approches mqcroéconomiques sur l'emploi

### 8.1. Le plein-emploi

Le plein emploi est le niveau d'emploi le plus élevé possible. On considère souvent que le plein emploi est atteint lorsque le taux de chômage tombe à 3 %, bien que ce taux puisse varier d'un pays à l'autre. Cela peut paraître surprenant, car on pourrait s'attendre à ce que le plein emploi ne soit atteint qu'avec un taux de chômage de 0 %. Toutefois, dans la pratique, à tout moment, certaines personnes peuvent connaître une période de chômage en passant d'un emploi à un autre.

#### Application

95 % de la main-d'œuvre du Qatar sont des travailleurs migrants. Beaucoup d'entre eux travaillent dans l'hôtellerie et la construction. Le taux de chômage du pays en 2019 était de 0,15 %.

En tant qu'économiste, comment feriez vous pour :
- mesurer le taux de chômage d'un pays
- expliquer pourquoi, malgré un faible taux de chômage, le taux de salaire des travailleurs de l'hôtellerie n'a pas augmenté de manière significative au cours des dernières années
- conseiller un gouvernement sur la manière dont son équilibre budgétaire serait affecté par le plein emploi.

### 8.2. Le chômage d'équilibre et de déséquilibre

Le chômage d'équilibre est le chômage qui existe lorsque la demande globale de main-d'œuvre est égale à l'offre globale de main-d'œuvre. À ce stade, il n'y a pas de pression pour que le taux de salaire réel change. Les personnes désireuses et capables de travailler au taux de salaire actuel auront un emploi. Toutefois, certaines personnes faisant partie de la population active n'auront pas d'emploi. Cela s'explique par le fait qu'elles ne sont pas disposées à accepter un emploi au taux de salaire actuel, qu'elles manquent d'informations sur les emplois vacants, qu'elles n'ont pas les compétences et les qualifications requises ou qu'elles ne sont pas en mesure de se rendre là où il y a du travail.

La figure ci-dessous illustre l'équilibre du marché du travail d'un pays. La demande globale de travail (ADL) correspond à l'offre globale de travail (ASL) à un taux de salaire réel de W. La main-d'œuvre globale (ALF) comprend les personnes prêtes à travailler au taux de salaire réel de W et capables de trouver un emploi. Elle comprend également les personnes à la recherche d'un emploi mieux rémunéré, celles qui se trouvent entre deux emplois et celles qui ne sont pas en mesure d'occuper les emplois proposés parce qu'elles vivent dans une autre région du pays ou qu'elles n'ont pas les compétences ou les qualifications requises. La courbe ALF se rapproche de la courbe ASL à mesure que le taux de salaire réel augmente. Cela s'explique par le fait qu'une plus grande proportion de la main-d'œuvre sera disposée à accepter le taux de salaire.

<img src="../inkscape-files/diagrams/ADL-ASL-ALF-equilibre-chomage.svg" alt="Chômage d'équilibre" width="500" >

- ADL : aggregate demande for labour
- ASL : aggregate supply for labour
- ALF . aggregate labour force

La figure ci-dessus montre qu'à un taux de salaire de W, Y travailleurs sont employés. YZ travailleurs sont au chômage. Ces travailleurs connaissent le **chômage volontaire**, le chômage frictionnel et le chômage structurel. Dans ce cas, le chômage est dû à des problèmes du côté de l'offre.

Les économistes keynésiens affirment que le marché du travail peut être en déséquilibre pendant de longues périodes. La figure ci-dessous montre le chômage de XY résultant d'une offre globale de travail supérieure à la demande globale de travail. Ce chômage de déséquilibre est équivalent au chômage cyclique.

<img src="../inkscape-files/diagrams/ADL-ASL-ALF-desequilibre-chomage.svg" alt="Chômage de déséquilibre" width="500" >


Le taux de salaire peut rester supérieur au niveau d'équilibre pour un certain nombre de raisons. L'une d'entre elles est que la demande globale de main-d'œuvre a diminué, mais que le taux de salaire réel reste inchangé parce que les travailleurs résistent aux baisses de salaire. Le taux de salaire peut également avoir été poussé au-dessus du niveau d'équilibre par l'action des syndicats ou par la fixation d'un salaire minimum national par le gouvernement. Le problème peut ne pas être résolu par une simple baisse du taux de salaire. En effet, une baisse du taux pourrait entraîner une diminution de la demande globale et, par conséquent, de la demande globale de main-d'œuvre. Une spirale descendante pourrait être créée.

Il est possible qu'à un moment donné, une économie connaisse à la fois un chômage d'équilibre et un chômage de déséquilibre. La figure ci-dessous montre une économie qui connaît un taux de chômage XZ. XY correspond à un chômage de déséquilibre et YZ à un chômage d'équilibre.

<img src="../inkscape-files/diagrams/ADL-ASL-ALF-equilibre-desequilibre-chomage.svg" alt="Chômage d'équilibre et de déséquilibre" width="500" >

### 8.3. Le chômage volontaire et involontaire

Le chômage volontaire, comme indiqué précédemment, se produit lorsque les travailleurs choisissent de ne pas accepter d'emploi au taux de salaire actuel. Ils pourraient avoir un emploi, mais préfèrent rester au chômage jusqu'à ce qu'ils puissent obtenir un emploi à un taux de salaire plus élevé. En revanche, le chômage involontaire survient lorsque les travailleurs sont prêts à travailler au salaire actuel mais ne trouvent pas d'emploi.

La plupart des chômages structurels et cycliques sont des chômages involontaires. Une partie du chômage frictionnel, en particulier le chômage de recherche, peut être volontaire. Les travailleurs peuvent prendre plus de temps à chercher un emploi mieux rémunéré si le salaire qu'ils peuvent obtenir actuellement est proche ou inférieur à l'allocation de chômage qu'ils pourraient recevoir.

En pratique, il peut être difficile de déterminer si le chômage est volontaire ou involontaire. Les personnes qui perçoivent des allocations de chômage ne sont pas susceptibles d'admettre qu'elles pourraient trouver un emploi. Il peut également être difficile de décider si une scientifique de haut niveau qui vient de perdre son emploi doit être classée comme chômeuse si un emploi d'éboueur est disponible.

### 8.4. Le taux naturel de chômage

Le taux de chômage naturel existe lorsque le marché du travail est en équilibre, la demande globale de main-d'œuvre étant égale à l'offre globale de main-d'œuvre. Toutefois, ce concept va un peu plus loin que le chômage d'équilibre. Il s'agit du taux auquel les <b>nouveaux économistes classiques</b> pensent que l'économie reviendra à long terme et qui est compatible avec un taux d'inflation constant. Si, par exemple, un gouvernement augmente la demande globale pour réduire le chômage, la demande globale de main-d'œuvre augmentera. Cela entraînera une hausse du taux de salaire et rapprochera l'ASL de l'ALF. Toutefois, à long terme, cela peut entraîner une augmentation du niveau des prix. Des coûts plus élevés et une baisse du taux de salaire réel entraîneront un retour du chômage au taux naturel.

Les facteurs qui déterminent le taux naturel de chômage dans un pays à un moment donné sont des facteurs liés à l'offre. Ils sont liés aux causes du chômage volontaire, frictionnel et structurel. Les facteurs liés à l'offre comprennent :

- la valeur des allocations de chômage par rapport à la valeur des bas salaires
- la législation nationale sur le salaire minimum
- la qualité de l'éducation et de la formation
- la manière dont les travailleurs sont affectés par les périodes de chômage
- la quantité et la qualité des informations sur les offres d'emploi et les compétences et qualifications des travailleurs
- le degré de mobilité de la main-d'œuvre 
- la flexibilité des travailleurs et des entreprises.

#### Implications en terme de politiques économiques du taux naturel de chômage

Pour réduire le taux naturel de chômage, un gouvernement cherchera à accroître à la fois la volonté et la capacité des travailleurs à travailler au taux de salaire actuel. Parmi les approches et les outils politiques qu'il peut utiliser, citons :

- <b>Creuser l'écart entre les bas salaires et les allocations de chômage.</b> Cet objectif pourrait être atteint en réduisant les allocations de chômage et/ou le taux de base de l'impôt sur le revenu.
- <b>Supprimer toute restriction sur le montant que les entreprises peuvent verser à leurs travailleurs.</b> Certains économistes affirment que les entreprises sont moins susceptibles de réduire le nombre d'emplois qu'elles offrent si elles peuvent réduire les salaires qu'elles versent. Un salaire minimum national pourrait empêcher les entreprises de le faire.
- <b>Améliorer l'éducation et la formation.</b> Une main-d'œuvre plus qualifiée aura probablement plus de facilité à passer d'un emploi à l'autre et souffrira donc moins du chômage structurel. La formation des chômeurs peut être importante pour surmonter le problème de l'hystérésis. Les travailleurs qui connaissent de longues périodes de chômage peuvent perdre confiance dans la possibilité de retrouver un emploi et leurs compétences peuvent devenir obsolètes. Les entreprises peuvent également être réticentes à embaucher une personne qui a été sans emploi pendant un certain temps, car elles peuvent penser que le chômeur de longue durée a perdu l'habitude du travail et qu'il doit être recyclé aux frais de l'entreprise.
- <b>Augmenter la quantité et la qualité des informations sur le marché du travail.</b> Si les entreprises en savent plus sur les personnes à la recherche d'un emploi et si les travailleurs connaissent mieux les emplois proposés, le chômage frictionnel est susceptible de diminuer.
- <b>Améliorer la mobilité de la main-d'œuvre.</b> Si les travailleurs sont plus mobiles, il est probable qu'un plus grand nombre d'offres d'emploi correspondront aux chômeurs.
- <b>Accroître la flexibilité de la main-d'œuvre.</b> Les nouveaux économistes classiques sont favorables à la réduction du pouvoir des syndicats et à l'élimination de la législation nationale sur le salaire minimum. Ils affirment que les syndicats et un salaire minimum national peuvent pousser le taux de salaire au-dessus du niveau d'équilibre. Ils pensent également que les syndicats peuvent restreindre les tâches que les travailleurs sont prêts à accomplir, ce qui peut réduire la productivité du travail. Une plus grande flexibilité des entreprises peut également augmenter l'emploi. Plus les entreprises sont flexibles, par exemple en ce qui concerne les heures de travail et le lieu de travail, plus les travailleurs peuvent être disposés à accepter les emplois proposés au taux de salaire actuel.

### 8.5. Modèles et tendances en matière de chômage

Le chômage n'est généralement pas réparti uniformément. Il est plus élevé dans les secteurs et les professions en déclin. Le chômage peut également varier d'une région à l'autre au sein d'un même pays. En effet, il peut y avoir des différences dans les liaisons de transport, les infrastructures et les coûts du logement.

Dans la plupart des pays, le chômage est plus élevé chez les jeunes travailleurs. Les entreprises peuvent être réticentes à employer des jeunes travailleurs en raison de leur manque d'expérience et de la nécessité de les former. Certains groupes, notamment les femmes, les personnes souffrant de problèmes de santé ou de handicaps et les personnes appartenant à des minorités ethniques, peuvent faire l'objet de discriminations sur le marché du travail et connaître ainsi un taux de chômage supérieur à la moyenne.

Une tendance à la hausse du nombre de chômeurs et du taux de chômage est un sujet de préoccupation pour un gouvernement. Ce qui est particulièrement préoccupant, c'est que les chômeurs restent sans emploi pendant un certain temps. En effet, ces travailleurs risquent de perdre l'habitude de travailler et de connaître l'hystérésis.


### 8.6. Modèles et tendances en matière d'emploi

La structure de l'emploi varie d'un pays à l'autre, bien qu'il existe certaines similitudes dans les tendances. Elle peut être examinée de différentes manières :

- <b>Structure industrielle.</b> Au fur et à mesure que de nombreux pays se sont développés, l'emploi s'est déplacé du secteur primaire au secteur secondaire, puis au secteur tertiaire. Par exemple, au Mali, un pays à faible revenu, 80 % de la main-d'œuvre était employée dans le secteur primaire, 15 % dans le secteur secondaire et 5 % dans le secteur tertiaire en 2019. En revanche, en Suède, 2 % de la main-d'œuvre est employée dans le secteur primaire, 10 % dans le secteur secondaire et 88 % dans le secteur tertiaire.
- <b>La proportion de femmes dans la population active.</b> La tendance générale est à l'augmentation de la part des femmes dans la population active d'un pays. Par exemple, alors qu'en 1990, 20 % de la population active du Bangladesh était composée de femmes, en 2019, cette proportion était passée à 31 %. Un certain nombre de raisons expliquent pourquoi davantage de femmes travaillent ou cherchent à travailler. Il s'agit notamment de l'augmentation des salaires que les femmes peuvent percevoir, de l'accroissement des possibilités d'emploi, de l'augmentation des qualifications acquises par les femmes, de l'amélioration des services de garde d'enfants et de la réduction de la discrimination fondée sur le sexe.
- <b>Salarié et indépendant.</b> La plupart des gens travaillent pour quelqu'un d'autre. Ils ont un employeur qui décide, par exemple, des heures qu'ils travaillent et des tâches qu'ils accomplissent. Toutefois, une proportion croissante de travailleurs sont indépendants. Par exemple, un ancien enseignant peut s'installer comme consultant en éducation et conseiller les écoles sur la manière d'améliorer leurs performances.
- <b>Temps plein et temps partiel.</b> La proportion de personnes qui travaillent à temps plein et à temps partiel varie dans le temps. Certaines personnes choisissent de travailler à temps partiel pour pouvoir aider à élever leurs enfants, s'occuper de parents âgés ou malades ou pratiquer des activités de loisirs. D'autres sont obligées de travailler à temps partiel parce qu'il n'y a pas d'emploi à temps plein. Cette situation est plus susceptible de se produire en période de ralentissement économique.
- <b>L'emploi dans l'économie formelle et l'économie informelle.</b> Les travailleurs de l'économie formelle sont protégés par la législation sur l'emploi, mais ceux de l'économie informelle ne sont pas couverts, par exemple, par la législation nationale sur le salaire minimum, les restrictions sur le temps de travail et les normes de santé et de sécurité. Les travailleurs de l'économie informelle ne sont pas représentés par des syndicats et ont peu de chances de bénéficier d'indemnités de maladie ou d'une pension.
- <b>Emploi sûr et emploi précaire.</b> Dans un certain nombre de pays, on a récemment constaté une augmentation des emplois précaires. De plus en plus de travailleurs sont employés dans le cadre de l'économie parallèle. Ces travailleurs ne savent souvent pas combien d'heures ils travailleront la semaine prochaine. Comme les travailleurs de l'économie informelle, les travailleurs de l'économie parallèle ne bénéficient pas d'indemnités de maladie ou de pension. Certains travailleurs apprécient de pouvoir choisir quand et où ils travaillent ; d'autres ne font ce travail que parce qu'ils ne trouvent pas d'emploi stable. Ces travailleurs peuvent trouver ce type de travail assez stressant en raison de l'incertitude qu'il implique. Le manque de fiabilité de leurs revenus peut également les empêcher d'acheter ou de louer un logement.
- <b>L'emploi dans les secteurs privé et public.</b> La qualité et la disponibilité relatives des emplois du secteur privé et du secteur public varient d'un pays à l'autre. Dans certains pays, travailler pour le gouvernement est très prestigieux et bien rémunéré. Dans un certain nombre de pays, la proportion de travailleurs employés dans le secteur privé a augmenté avec la privatisation.


### 8.7. Les formes de mobilité de la main-d'œuvre

La mobilité de la main-d'œuvre est la capacité des travailleurs à passer d'une profession à une autre ou d'un lieu à un autre. Il existe deux formes principales de mobilité de la main-d'œuvre. La première est la mobilité professionnelle. Par exemple, un comptable devra probablement obtenir un diplôme d'enseignement pour devenir professeur de comptabilité. Il devrait toutefois trouver plus facile de changer d'emploi qu'une personne essayant de passer d'un emploi de jardinier à un emploi d'ingénieur en informatique. L'autre type de mobilité est la mobilité géographique. Il peut y avoir des chômeurs dans une région du pays et des postes vacants, pour lesquels les chômeurs ont les compétences et les qualifications appropriées, dans une autre région du pays ou dans un autre pays. Cependant, il peut y avoir des raisons pour lesquelles les chômeurs ne peuvent pas changer de lieu de résidence.

#### 8.7.1. Facteurs affectant la mobilité de la main-d'œuvre

Parmi les facteurs qui influencent la mobilité professionnelle, on peut citer :
- <b>Qualité de l'éducation et de la formation.</b> Plus les travailleurs sont instruits et formés, plus ils sont susceptibles d'exercer différents types de professions.
- <b>Informations disponibles.</b> Pour que les travailleurs puissent passer sans heurts d'une profession dont la demande diminue à une profession dont la demande augmente, ils doivent savoir où se trouvent les possibilités d'emploi.
- <b>Barrières à l'entrée et à la sortie.</b> Les organismes professionnels et les syndicats peuvent chercher à restreindre l'offre dans une profession afin d'en augmenter le taux de rémunération. Les travailleurs peuvent également avoir signé des contrats à long terme qui les empêchent de changer de profession.
- <b>Le temps.</b> Plus la période est longue, plus les travailleurs auront la possibilité d'acquérir la formation, les qualifications et les compétences nécessaires pour obtenir un emploi dans une autre profession.

#### 8.7.2. Facteurs affectant la mobilité géographique

Les facteurs influençant la mobilité géographique de la main-d'œuvre peuvent être les suivants :
- <b>Le prix et la disponibilité du logement.</b> Les travailleurs peuvent ne pas être en mesure de changer de lieu de travail s'il n'y a pas de logements abordables dans cette région.
- <b>L'information disponible.</b> Comme pour la mobilité professionnelle, les travailleurs ont besoin de savoir où se trouvent les postes vacants, de connaître les détails de la rémunération accordée et d'autres aspects liés à l'emploi.
- <b>Liens personnels.</b> Les travailleurs peuvent être réticents à quitter les membres de leur famille élargie et leurs amis pour s'installer ailleurs.

D'autres facteurs influencent les déplacements entre les pays :
- <b>Les contrôles de l'immigration.</b> Le fait de s'installer et de travailler dans un autre pays peut être restreint par les limites imposées aux visas de travail et aux autorisations de séjour.
- <b>Les barrières linguistiques.</b> Les travailleurs peuvent ne pas être en mesure d'accepter un emploi dans un autre pays parce qu'ils ne parlent pas la langue.
- <b>Différences culturelles.</b> Les travailleurs peuvent trouver la culture d'un autre pays peu attrayante.
- <b>Différences de salaire et de coût de la vie.</b> Les travailleurs peuvent estimer qu'il est préférable de rester au chômage dans leur pays d'origine plutôt que d'accepter un emploi dans un autre pays.

### 8.8. Politiques visant à réduire le chômage

Pour réduire le chômage cyclique, un gouvernement utilisera des outils de politique fiscale ou monétaire expansionnistes afin d'augmenter la demande globale. Parmi les outils de politique budgétaire expansionniste qu'il peut utiliser, on peut citer :

- une réduction de la fiscalité indirecte ou directe pour augmenter les dépenses de consommation
- une réduction de l'impôt sur les sociétés pour stimuler l'investissement
- une augmentation des dépenses publiques.

Les outils monétaires possibles sont les suivants :

- Réduire le taux d'intérêt pour augmenter les dépenses de consommation et les investissements.
- Augmenter à nouveau la masse monétaire pour augmenter les dépenses de consommation et les investissements.
- Abaisser le taux de change, soit par une dévaluation formelle, soit par une intervention sur le marché des changes dans le cas d'un flottement dirigé, afin d'augmenter les exportations nettes.


La mesure dans laquelle les instruments de politique budgétaire ou monétaire expansionnistes peuvent accroître l'emploi dépendra d'un certain nombre de facteurs. L'augmentation de la demande globale peut ne pas être très importante si les consommateurs et les entreprises sont inquiets pour l'avenir ou si les entreprises augmentent leur capacité de production en achetant des biens d'équipement dont le fonctionnement ne nécessite que peu de travailleurs supplémentaires. Les politiques budgétaires et monétaires ont également un décalage dans le temps. Le temps que le chômage soit reconnu comme un problème, que les instruments politiques soient décidés et mis en œuvre, d'autres composantes de la demande globale peuvent avoir commencé à augmenter.

Pour réduire le chômage frictionnel et structurel, un gouvernement est susceptible d'introduire des outils de politique de l'offre. Il peut accroître l'information sur le marché du travail. Il pourrait réduire les allocations de chômage et les taux d'imposition sur le revenu afin d'augmenter la récompense du travail et d'encourager les gens à passer moins de temps entre deux emplois. L'amélioration de l'éducation et de la formation peut accroître la mobilité professionnelle des travailleurs et réduire ainsi le chômage structurel. La réforme des syndicats peut empêcher les syndicats de pousser les taux de salaire de leurs membres au-delà des niveaux d'équilibre et de mener des actions syndicales. Cela peut accroître l'efficacité des marchés du travail et des produits du pays.

Certains outils politiques axés sur l'offre peuvent prendre beaucoup de temps avant de produire un effet. Il y a aussi le risque que les travailleurs ne réagissent pas comme prévu, et si la cause du chômage est en fait un manque de demande globale, l'outil politique peut aggraver la situation. Par exemple, une réduction de l'impôt sur le revenu peut réduire non seulement le chômage volontaire, frictionnel et structurel, mais aussi le chômage cyclique. Toutefois, rien ne garantit qu'elle encouragera les chômeurs à rechercher plus activement un emploi. S'il n'y a pas assez d'emplois disponibles, les chômeurs ne seront de toute façon pas en mesure d'en trouver. S'il n'y a pas d'offres d'emploi appropriées, une réduction des allocations de chômage diminuera les dépenses de consommation, ce qui pourrait entraîner un chômage cyclique.

## 9. La relation entre l'inflation et le chômage

Les économistes ont consacré beaucoup d'attention à la relation entre l'inflation et le chômage. Ils ont étudié comment les variations du taux d'inflation peuvent affecter le chômage et comment les variations du taux de chômage peuvent affecter le taux d'inflation. Ils ont également examiné comment la politique gouvernementale visant à réduire le taux de chômage influence à la fois le taux de chômage et le taux d'inflation.

### 9.1. La courbe de Phillips

L'étude la plus célèbre sur la relation entre l'inflation et le chômage a été publiée en 1958 par Bill Phillips, un économiste néo-zélandais basé à la London School of Economics. Il a analysé des données historiques sur la relation entre les variations du chômage et les salaires monétaires (considérés comme un indicateur de l'inflation) au Royaume-Uni sur la période 1861-1957. Il a constaté une relation inverse, comme le montre la figure ci-dessous.


<img src="../inkscape-files/diagrams/phillips-curve.svg" alt="La courbe de Phillips" width="500" >


Une baisse du chômage peut entraîner une hausse de l'inflation en raison de l'augmentation de la demande globale et de l'éventuelle pression à la hausse sur les salaires. La courbe de Phillips traditionnelle est inclinée vers le bas, de la gauche vers la droite. Elle est plus prononcée lorsque les taux d'inflation sont élevés, ce qui indique qu'un passage d'un taux de chômage très bas à un taux encore plus bas se traduira par une hausse significative du taux d'inflation. La courbe devient beaucoup plus plate à mesure que le chômage augmente. Cela indique que la réduction du chômage à partir d'un taux très élevé peut ne pas avoir beaucoup d'impact sur le niveau des prix et qu'un taux de chômage très élevé peut s'accompagner d'une déflation.

La courbe de Phillips traditionnelle suggère également qu'un gouvernement peut choisir la meilleure combinaison de chômage et d'inflation et peut faire un compromis entre les deux. Par exemple, si le taux de chômage actuel est de 8 % et le taux d'inflation de 4 %, un gouvernement peut chercher à abaisser le taux de chômage à 5 % en mettant en œuvre des instruments de politique budgétaire ou monétaire expansionnistes, tout en acceptant que cette amélioration puisse être achetée au prix d'une inflation plus élevée.

La courbe de Phillips traditionnelle se déplace vers la droite si le taux d'inflation associé à un taux de chômage donné augmente. Par exemple, si les travailleurs attendent des salaires plus élevés parce qu'ils s'attendent à ce que l'inflation soit plus élevée ou parce que les travailleurs deviennent moins qualifiés, la courbe se déplacera vers l'extérieur. En revanche, les améliorations du côté de l'offre de l'économie déplaceront la courbe vers la gauche.

### 9.2. La courbe de Phillips augmentée des anticipations

La courbe de Phillips traditionnelle est soutenue par les keynésiens mais remise en question par les monétaristes. Les monétaristes affirment que, bien qu'il puisse y avoir un compromis à court terme, à long terme, les outils de politique gouvernementale visant à augmenter la demande globale n'auront aucun impact sur le chômage, mais ne réussiront qu'à augmenter le taux d'inflation. Pour étayer ce point de vue, l'économiste américain Milton Friedman a mis au point la courbe de Phillips augmentée des anticipations, comme le montre la ligne verticale de la figure 46.6. La position de cette ligne est déterminée par le taux de chômage naturel.

[compléter avec un graphique]

Une augmentation de la demande globale parvient à réduire le chômage de 8 % à 4 %, mais crée une inflation de 5 % et fait passer l'économie à une courbe de Phillips à court terme plus élevée. Les entreprises augmentent leur production et les salaires plus élevés attirent davantage de personnes sur le marché du travail. Toutefois, lorsque les entreprises se rendent compte que leurs coûts ont augmenté et que leurs bénéfices réels restent inchangés, elles réduisent leur production et certains travailleurs, constatant que les salaires réels n'ont pas augmenté, quittent le marché du travail. Le chômage revient à 8 % à long terme, mais une inflation de 5 % a été intégrée au système. Les entreprises et les travailleurs présumeront que l'inflation se maintiendra à 5 % lorsqu'ils fixeront leurs prix et soumettront leurs revendications salariales. Une nouvelle tentative pour ramener le chômage à 4 % fera finalement passer l'économie à la CPS2 et portera le taux d'inflation à 12 %.

<img src="https://core-econ.org/the-economy/v1/book/fr/images/web/figure-15-06-g.jpg" alt="Depuis la fin des années 1990 jusqu’à aujourd’hui, la courbe de Phillips est basse et plate." width="500" >


## 10. La lutte contre le chômage et l'inflation

[Dessine-moi l'éco : Y a-t-il un remède au chômage ?](https://www.youtube.com/watch?v=s3cM2Kx-jUY)

### 10.1. Politique keynésienne de lutte contre le chômage et l'inflation

La macroéconomie keynésienne affirme que la solution à une récession est une politique fiscale expansionniste, telle que des réductions d'impôts pour stimuler la consommation et l'investissement, ou des augmentations directes des dépenses publiques qui déplaceraient la courbe de la demande globale vers la droite. Par exemple, si la demande globale se situait initialement à AD dans la figure ci-dessous, de sorte que l'économie était en récession, la politique appropriée consisterait pour le gouvernement à déplacer la demande globale vers la droite, de AD à AD1, où l'économie se situerait au niveau du PIB potentiel et du plein emploi.

<img src="../inkscape-files/diagrams/aggregate-demand-aggregate-supply-shift-of-demand-a-FR.svg" alt="Lutter contre la récession et l'inflation avec une politique keynésienne" width="500" >

<img src="../inkscape-files/diagrams/aggregate-demand-aggregate-supply-shift-of-demand-b-FR.svg" alt="Lutter contre la récession et l'inflation avec une politique keynésienne" width="500" >

Keynes a fait remarquer qu'il serait bien que le gouvernement puisse dépenser de l'argent supplémentaire pour le logement, les routes et d'autres équipements, mais il a également soutenu que si le gouvernement ne pouvait pas se mettre d'accord sur la façon de dépenser l'argent de manière pratique, il pourrait alors dépenser de manière peu pratique. Par exemple, Keynes a suggéré de construire des monuments, comme un équivalent moderne des pyramides égyptiennes. Il a proposé que le gouvernement enterre l'argent sous terre et laisse les compagnies minières le déterrer à nouveau. Ces suggestions étaient légèrement ironique, mais leur but était de souligner qu'une Grande Dépression n'est pas le moment d'ergoter sur les spécificités des programmes de dépenses publiques et des réductions d'impôts, alors que l'objectif devrait être d'augmenter la demande globale suffisamment pour que l'économie atteigne son PIB potentiel.


### 10.2. Politique neoclassique de lutte contre le chômage et l'inflation

Pour comprendre les recommandations politiques des économistes néoclassiques, il est utile de commencer par la perspective keynésienne. Supposons qu'une baisse de la demande globale entraîne une récession de l'économie avec un taux de chômage élevé. La réponse keynésienne consisterait à utiliser la politique gouvernementale pour stimuler la demande globale et éliminer l'écart de récession. Les économistes néoclassiques estiment que la réponse keynésienne, même si elle part d'une bonne intention, n'aura pas de bons résultats pour les raisons que nous évoquerons prochainement. Puisque les économistes néoclassiques pensent que l'économie se corrigera d'elle-même avec le temps, le seul avantage d'une politique de stabilisation keynésienne serait d'accélérer le processus et de réduire au minimum la période pendant laquelle les chômeurs sont sans emploi. Est-ce là le résultat probable ?

La politique macroéconomique keynésienne exige un certain optimisme quant à la capacité du gouvernement à reconnaître une situation de demande globale trop faible ou trop forte, et à ajuster la demande globale en conséquence avec le niveau adéquat de changements dans les impôts ou les dépenses, le tout promulgué en temps opportun. Après tout, affirment les économistes néoclassiques, il faut des mois aux statisticiens du gouvernement pour produire ne serait-ce que des estimations préliminaires du PIB afin que les responsables politiques sachent si une récession est en cours, et ces estimations préliminaires peuvent être révisées de manière substantielle par la suite. De plus, la question de la rapidité d'action se pose. Le processus politique peut prendre plus de mois pour promulguer une réduction d'impôt ou une augmentation des dépenses. Des considérations politiques ou économiques peuvent déterminer le montant des modifications d'impôts ou de dépenses. Il faudra ensuite encore des mois à l'économie pour mettre en œuvre les changements de la demande globale par le biais des dépenses et de la production. Lorsque les économistes et les décideurs politiques tiennent compte de tous ces décalages et de ces réalités politiques, une politique budgétaire active risque de ne pas résoudre le problème actuel, voire d'aggraver la situation économique future. La récession moyenne aux États-Unis après la Seconde Guerre mondiale n'a duré qu'un an environ. Au moment où la politique gouvernementale s'active, la récession est probablement terminée. Par conséquent, le seul résultat du réglage fin du gouvernement sera de stimuler l'économie lorsqu'elle est déjà en train de se redresser (ou de la contracter lorsqu'elle est déjà en train de chuter). En d'autres termes, une politique macroéconomique active est susceptible d'exacerber les cycles plutôt que de les atténuer. Certains économistes néoclassiques pensent qu'une grande partie des cycles économiques que nous observons sont dus à une politique gouvernementale défectueuse. 


## Source:

- CORE Econ : https://www.core-econ.org/
- CORE Econ : [Econofides](https://www.sciencespo.fr/department-economics/econofides/terminale-ses/text/03.html#34-quelles-sont-les-principales-politiques-quil-est-possible-de-mettre-en-%C5%93uvre-pour-lutter-contre-le-ch%C3%B4mage)
- OpenStax : https://openstax.org/
- http://annotations.blog.free.fr/index.php?post/1989/02/25/Les-th%C3%A9ories-du-ch%C3%B4mage
- Cambridge AS&A Level Economics Coursebook
- Économie du travail: La formation des salaires et les déterminants du chômage. Pierre Cahuc et André Zylberberg. De Boeck.
