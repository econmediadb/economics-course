# Chapitre ??? : Inégalités

- inégalités
- mesurer les inégalités
- comment réduire les inégalités


## Résumé

Ce chapitre sur les inégalités aborde plusieurs aspects clés : l'évolution des inégalités économiques depuis le début du 20e siècle, le caractère multiforme et cumulatif des inégalités, les outils de mesure des inégalités, les différentes formes d'égalité et la notion de justice sociale, ainsi que l'intervention des pouvoirs publics. Il examine la dynamique des inégalités de revenus et de patrimoine, leur impact sociétal, et l'efficacité des politiques publiques dans la réduction de ces inégalités. Le chapitre met également en lumière les diverses approches théoriques de la justice sociale et souligne l'importance des interventions étatiques pour promouvoir l'équité sociale.


```mermaid
graph LR
    CO("Objectifs du Cours d'Économie")
    TEND("Connaître les Tendances des Inégalités")
    OUTILS("Interpréter Outils de Mesure des Inégalités")
    ACTION("Action des Pouvoirs Publics")

    CO --> TEND
    CO --> OUTILS
    CO --> ACTION

    TEND --> TEND1("Évolution depuis le XXe siècle")
    TEND --> TEND2("Nature Multiforme et Cumulative des Inégalités")

    OUTILS --> OUTILS_STAT("Mesure Statique")
    OUTILS --> OUTILS_DYN("Mesure Dynamique")

    OUTILS_STAT --> STAT1("Rapport Inter-Quantiles")
    OUTILS_STAT --> STAT2("Courbe de Lorenz")
    OUTILS_STAT --> STAT3("Coefficient de Gini")
    OUTILS_STAT --> STAT4("Top 1%")

    OUTILS_DYN --> DYN1("Corrélation Revenu Parents-Enfants")

    ACTION --> ACTION1("Fiscalité")
    ACTION --> ACTION2("Protection Sociale")
    ACTION --> ACTION3("Services Collectifs")
    ACTION --> ACTION4("Lutte contre les Discriminations")
    ACTION --> ACTION5("Contraintes de Financement")
    ACTION --> ACTION6("Débats d'Efficacité et de Légitimité")
    ACTION --> ACTION7("Risques d'Effets Pervers")

    linkStyle default interpolate basis

```

## Introduction

La notion de justice sociale, essentielle à la définition d'une société juste, s'ancre dans une diversité de critères, parmi lesquels le type et le niveau d'inégalités prédominent souvent. Historiquement, on a assisté à une baisse des inégalités de revenu depuis le début du XXe siècle, suivie d'une recrudescence à partir des années 1980. Cette dynamique soulève des interrogations cruciales : les pouvoirs publics doivent-ils combattre cette croissance des inégalités pour promouvoir la justice sociale ? Peut-on considérer toutes les inégalités comme intrinsèquement injustes ?

La proposition selon laquelle les politiques publiques peuvent rendre une société plus juste est loin de faire l'unanimité. Elle est notamment contestée par les penseurs marxistes, qui jugent les fondements de la société capitaliste comme injustes, ainsi que par des théoriciens comme Hayek, pour qui la notion de "mirage de la justice sociale" est prédominante. Selon lui, seule une décision individuelle peut être qualifiée de juste ou d'injuste, et l'attribution des ressources ne résulte d'aucune volonté individuelle ou collective.

Dans ce contexte, les conceptions de la justice sociale sont multiples et l'intervention des pouvoirs publics en la matière est complexe, opérant sous diverses contraintes. Cette complexité souligne l'importance d'examiner en profondeur les différentes perspectives et défis associés à la quête d'une société juste et équitable.


## Les évolutions des inégalités économiques depuis le début du 20e siècle

**Figure**: Part du revenu national et des richesses des 1% (et 10%) les plus riches. Cette figure montre les grandes tendances d'évolution des inégalités depuis le début du 20e siècle. On considère généralement que les inégalités économiques regroupent les inégalités de patrimoine et les inégalités de revenu. (Source: [download table](https://wid.world/world/#sptinc_p90p100_z/US;FR;DE;CN;ZA;GB;WO/last/eu/k/p/yearly/s/false/24.722500000000004/80/curve/false/country) and the [CC license](https://creativecommons.org/licenses/by/4.0/) )
- https://data.oecd.org/chart/7ihp
- https://wid.world/share/#0/countriestimeseries/sptinc_p99p100_z/US;FR;DE;CN;ZA;GB;WO;LU/last/eu/k/p/yearly/s/false/3.7/40/curve/false/country

<center>
<img src="../images/20231216-inegalites-long-terme-1percentshare.png" 
     width = 50% 
     height= auto />
<img src="../images/2023121616-inegalites-long-terme-10percentshare.png" 
     width = 50% 
     height= auto />
</center>

Par exemple, si on choisit comme indicateur le « top 1 % », on observe une diminution
des inégalités de revenu depuis le début du 20e siècle jusque dans les années 1980,
puis une augmentation ensuite. Les différents pays développés suivent cette évolution,
même si la remontée des inégalités est plus accentuée dans certains pays comme les
États-Unis.

**Figure**: Concentration du patrimoine.
<center>
<img src="../images/2023121616-inegalites-richesse-long-terme-1percentshare.png" 
     width = 50% 
     height= auto />
<img src="../images/2023121616-inegalites-richesse-long-terme-10percentshare.png" 
     width = 50% 
     height= auto />
</center>

Au Luxembourg, les inégalités de patrimoine, sont restées relativement stable dans les trois dernières décennies. Les 10% les plus riches détiennent autour de 60% du patrimoine. Aux États-Unies on assiste depuis le début des années 1980 à une hausse des inégalités de patrimoine, la part du top 10% ayant atteint 71% à la fin des années 2010. 

[add Gini coefficient for same countries: https://data.oecd.org/chart/7imE and https://data.oecd.org/inequality/income-inequality.htm ]



## Interprétation des principaux outils de mesure des inégalités

La mesure des inégalités est un enjeu important puisqu’il peut y avoir un décalage
entre leur ressenti et leur réalité. La mesure des inégalités économiques peut être
statique (rapport inter-quantile, courbe de Lorenz et coefficient
de Gini, top 1%) ou dynamique (lien entre revenu des enfants et revenu des parents).


### Les mesures des inégalités économiques *en statique*

La courbe de Lorenz permet de représenter la concentration d’une variable dans une
population donnée. Par exemple, pour le revenu, il s’agit de trier par ordre croissant les
revenus de l’ensemble des habitants, puis de tracer la courbe avec en abscisse la part
cumulée de la population et en ordonnée la part cumulée des revenus. Si la répartition
est parfaitement égalitaire, alors la courbe de Lorenz sera une droite à 45 degrés ; cela
signifie que les 10% les plus pauvres du pays gagnent 10% du revenu total, que les 40%
les plus pauvres gagnent 40% du revenu total par exemple.

**Encadré : Le coefficient de Gini** <br>
Le coefficient (ou indice) de Gini est calculé en multipliant par 2 la surface de concentration, c’est-à-dire l’aire située entre la bissectrice et la courbe de Lorenz de répartition de la variable (revenu,
patrimoine). C’est un indicateur compris entre 0 (distribution parfaitement égalitaire) et 1
(inégalité extrême où un ménage possède 100% du revenu ou du patrimoine). L’intérêt de cet indice
est de synthétiser en un chiffre le niveau des inégalités. Mais sa principale limite est qu’il ne prend
pas en compte la répartition des revenus (ou du patrimoine) au sein de la population étudiée. 
Par exemple, un indice de Gini de 0,5 peut correspondre à deux répartitions très différentes du revenu.

Les indicateurs de dispersion que sont les différents quantiles sont également utilisés.
Ainsi, il est possible de calculer différents rapports inter-quantiles comme les rapports
inter-déciles : D9/D1 donne le coefficient multiplicateur entre le plafond des 10%
les plus pauvres et le plancher des 10% les plus riches ou encore le rapport D9/D5
ou D5/D1 qui permettent les comparaisons par rapport à la médiane. Une limite de
l’utilisation des fractiles est que l’on ne connaît pas les dynamiques à l’intérieur des
déciles.

Il est aussi possible d’utiliser le top 1% pour aller au-delà des 10% les plus riches et
analyser plus finement ce qui se passe au niveau des très hauts revenus ou des très
hauts patrimoines (cf. Figure au début de cette section). 
Par exemple, cela permet de remarquer que c’est avant tout la
forte diminution de la part du patrimoine total possédée par les 1% les plus riches
entre 1900 et 1980 qui contribue à la diminution des inégalités de patrimoine sur cette
période et que de 1990 à 2015, l’augmentation des inégalités de patrimoine est due
essentiellement à la forte augmentation de la part du patrimoine possédée par les 1%
les plus riches.



### Un indicateur des inégalités économiques *en dynamique*

La question de la reproduction des inégalités de revenu d’une génération à l’autre
se pose aussi. Celle-ci est due à la transmission du patrimoine mais aussi du capital
culturel et social entre générations.

L’observation des déciles de niveau de vie selon l’origine sociale des individus permet
de rendre compte des inégalités économiques dynamiques. On observe une corrélation 
positive entre les revenus des parents (plus exactement leur position sociale) et
ceux de leurs enfants. Par exemple la part des enfants d’ouvriers décroît de manière
continue à mesure que l’on progresse dans la hiérarchie des revenus. À l’inverse, les
enfants de cadres supérieurs sont, en général, surreprésentés parmi les 10 % les plus aisés.
[!!!trouver des données pour le Luxembourg!!!]

<br>

<br>

**Encadré : La construction d'une société plus inclusive** (Source: [LIS](https://www.lisdatacenter.org/newsletter/nl-2018-8-im-2/)) <br>
Miles Corak, dans son article pour le LIS Cross-National Data Center, explore la construction d'une société plus inclusive. Il définit l'inclusion comme la capacité de tous les enfants à réaliser pleinement leur potentiel, indépendamment de leur milieu familial. L'article souligne quatre messages clés :
1. Une société inclusive permet à chaque enfant de devenir tout ce qu'il peut être : Ceci implique que l'origine familiale ne doit pas déterminer le destin, favorisant ainsi l'égalité des chances.
2. Élimination de la pauvreté infantile : L'accent est mis sur la nécessité de répondre aux besoins fondamentaux et de participer pleinement à la société.
3. L'inégalité des revenus menace l'inclusion : La mobilité sociale est compromise par l'inégalité des revenus, empêchant les enfants de familles à faible revenu de progresser.
4. La politique publique doit aborder de multiples dimensions de l'inégalité : Outre les revenus, l'éducation, la santé, et les expériences des enfants sont cruciales pour leur développement.

Corak insiste sur le rôle du gouvernement, des familles et du marché du travail dans le développement de l'enfant. Une politique progressiste, soutenant davantage les plus défavorisés, est essentielle pour l'inclusion sociale. Il conclut qu'une société plus inclusive est celle où les circonstances de naissance importent moins, permettant aux enfants de choisir librement leur avenir.

<br>

<br>

L’indicateur de la corrélation du revenu parents-enfants peut être mesuré par
« l’élasticité intergénérationnelle des revenus », c’est-à-dire la différence en
pourcentage du revenu des enfants qui est associée à une différence de 1% du revenu
des parents.

Une élasticité intergénérationnelle de 0,5 signifie que si l’on considère les parents
dont le patrimoine se situe à 100% au-dessus de la moyenne de leur génération, leurs
enfants seront en moyenne situés à 50% au-dessus de la moyenne de la leur.

Habituellement, l’élasticité prend des valeurs comprises entre 0 et 1. Lorsque
l’élasticité est à 0, les enfants ne tirent aucun avantage à avoir un parent en bonne
position dans la distribution des richesses et la mobilité est parfaite. À l’inverse,
une élasticité égale à 1 montre que la conservation des positions est parfaite et la
hiérarchie des positions se reproduit à l’identique.

À partir de différentes études, Miles Corak (cf. encadré) synthétise les principaux
résultats moyens trouvés pour la valeur de l’élasticité : 0,41 pour la France, 0,15 pour le
Danemark et 0,5 pour le Royaume-Uni.

On observe dans tous les pays une tendance de retour à la moyenne, ce qui signifie que
même si l’on a bien une corrélation positive entre le revenu des parents et celui des
enfants, d’une génération à l’autre, le revenu des enfants se rapproche de la moyenne
des revenus de la génération. Cette corrélation entre revenu des parents et revenu des
enfants et l’effet du retour à la moyenne peuvent donc indiquer le nombre de générations
nécessaires pour qu’un individu atteigne le niveau de vie moyen de sa génération.


<br>

<br>

Le document de l'OCDE intitulé "Transmission intergénérationnelle du désavantage : mobilité ou immobilité entre générations ?" examine la mobilité sociale intergénérationnelle dans les pays de l'OCDE. Il explore comment les caractéristiques et expériences de vie des individus diffèrent ou ressemblent à celles de leurs parents. Le rapport révèle que la mobilité des revenus varie considérablement entre les pays, influencée par des facteurs comme l'inégalité des revenus, l'éducation, et le contexte socio-économique et culturel des parents. Il souligne que l'éducation joue un rôle majeur dans la mobilité intergénérationnelle des revenus et que les disparités éducatives ont tendance à persister au fil des générations. De plus, le document aborde la persistance intergénérationnelle d'autres aspects tels que les métiers, la richesse, et les traits de personnalité, tout en soulignant l'importance des investissements précoces et soutenus dans l'éducation, les soins et la santé des enfants pour briser le cycle des désavantages intergénérationnels.

<br>

<br>



```mermaid
graph LR;
    A[La Mesure des Inégalités] --> B[Importance de la Mesure]
    B --> C[Écart entre Ressenti et Réalité]
    A --> D[Mesures Statiques des Inégalités Économiques]
    A --> E[Mesures Dynamiques des Inégalités Économiques]
    D --> D1[Courbe de Lorenz]
    D --> D2[Coefficient de Gini]
    D --> D3[Rapports Inter-Quantiles]
    D --> D4[Top 1%]
    E --> E1[Corrélation Revenu Parents-Enfants]
    E1 --> E2[Élasticité Intergénérationnelle des Revenus]
    E2 --> E3[Interprétation de l'Élasticité]
    E1 --> E4[Retour à la Moyenne et Mobilité Sociale]
    B --> F[Société Plus Inclusive]
    F --> F1[Approche de Miles Corak]
    F1 --> F2[Inclusion et Potentiel des Enfants]
    F1 --> F3[Impact de la Politique Publique]
```



## Les différentes formes d’égalité et la notion de justice sociale

Il existe une inégalité s’il est possible d’établir l’existence d’un avantage ou d’un
désavantage significatif associé à une ou plusieurs différences dans l’accès aux biens
matériels et symboliques valorisés par la société. Constater et expliquer ces inégalités
(approche positive) doit être distingué du jugement de valeur qu’on pourrait porter
sur elles (approche normative). La possibilité d’un tel jugement suppose de définir des
critères permettant de dire ce qui doit être égal ou pas (quelle(s) égalité(s) ?) et, une
fois certaines formes d’inégalités éventuellement admises, il faut encore définir leur
ampleur légitime. Ces critères sont ceux étudiés par la théorie de la justice sociale.
La justice sociale peut être définie comme « l’ensemble des principes qui régissent la
définition et la répartition équitable des droits et des devoirs entre les membres de la
société » (Arnsperger et Van Parijs, 2003, p.10).


### Les différentes formes d’égalité

L'égalité des droits est un principe fondamental en démocratie, affirmant que tous les individus sont égaux devant la loi, bénéficiant des mêmes droits et soumis aux mêmes devoirs, indépendamment de leur statut social. Ce principe implique que toutes les positions sociales sont accessibles à tous sur la base de règles objectives et uniformes. Cependant, l'égalité des droits ne garantit pas l'égalité des chances, qui se définit par l'indépendance de la situation sociale acquise vis-à-vis de la situation sociale héritée. Bien que l'égalité des chances soit un idéal clé en démocratie, sa réalisation ne suffit pas à assurer une égalité totale des situations, car des inégalités économiques et sociales significatives peuvent persister.

L'égalité des situations, en revanche, concerne une répartition plus uniforme des ressources valorisées (comme le revenu et le patrimoine) entre tous les membres de la société. Elle vise à réduire les inégalités liées aux différentes positions sociales, en termes de revenu, de conditions de vie, d'accès aux services et de sécurité. Cette approche, appelée "égalité des places", cherche à minimiser les écarts entre les positions sociales des individus, promouvant ainsi une société plus équitable.


### Différentes conceptions de la justice sociale

L'utilitarisme, promu par Bentham, Mill, Smith, et des économistes néoclassiques comme Pareto et Pigou, vise une société où le bien-être global est maximisé. La justice sociale, selon cette perspective, ne dépend pas de l'égalité, mais de l'augmentation de la satisfaction collective. Le libertarisme, influencé par Locke et Hayek, considère la justice comme le respect des libertés individuelles et des droits de propriété, avec un rôle limité de l'État à la protection de ces libertés.

L'égalitarisme libéral, développé par John Rawls, envisage une société juste comme une répartition équitable des biens premiers sociaux, incluant les libertés fondamentales, l'accès aux positions sociales, et les avantages socio-économiques. Rawls souligne l'égalité des libertés, des chances et le principe de différence au profit des plus défavorisés.

L'égalitarisme strict, en rupture avec ces conceptions, prône l'égalité totale et la réduction des inégalités comme objectif principal. Cette approche, partiellement alignée avec la pensée marxiste, aspire à une société sans exploitation, où chacun accède à la richesse commune selon ses besoins. Marx envisage une société « juste » où l'exploitation est abolie et les ressources sont partagées en fonction des besoins individuels.



```mermaid
graph LR;
    A[Inégalité et Accès aux Biens] --> B[Approche Positive et Normative]
    B --> C[Constater et Expliquer les Inégalités]
    B --> D[Jugement de Valeur sur les Inégalités]
    D --> E[Établir des Critères de Justice Sociale]
    E --> F[Différentes Formes d'Égalité]
    F --> F1[Égalité des Droits]
    F --> F2[Égalité des Chances]
    F --> F3[Égalité des Situations]
    A --> G[Conceptions de la Justice Sociale]
    G --> G1[Utilitarisme]
    G --> G2[Libertarisme]
    G --> G3[Égalitarisme Libéral]
    G --> G4[Égalitarisme Strict]

    F1 --> F1a[Égalité Devant la Loi]
    F2 --> F2a[Indépendance Sociale Acquise]
    F3 --> F3a[Répartition Uniforme des Ressources]
    G1 --> G1a[Maximisation du Bien-être Global]
    G2 --> G2a[Respect des Libertés Individuelles]
    G3 --> G3a[Répartition Équitable des Biens Premiers Sociaux]
    G4 --> G4a[Égalité Totale et Réduction des Inégalités]
```



## Intervention des pouvoirs publics

En comptabilité nationale, il est d’usage de rassembler sous le terme
de prélèvements obligatoires l’ensemble constitué des impôts, des taxes fiscales et
des cotisations sociales. Ces prélèvements ont en effet en commun d’être versés à
l’autorité publique (administrations publiques et Union européenne) et de ne pas faire
l’objet d’une contrepartie directe (pas de liaison avec un bien ou service identifiable,
contrairement au prix de marché).

La protection sociale englobe divers mécanismes institutionnels, tant publics que privés, qui forment un système de prévoyance collective et de solidarité sociale. Elle vise à couvrir les charges liées à des risques sociaux comme la santé, la vieillesse, le chômage et la pauvreté. Les prestations de protection sociale peuvent être contributives, basées sur une logique d'assurance, ou non contributives, suivant une logique d'assistance. Ce système assure une redistribution des ressources, à la fois horizontale (par exemple, entre personnes malades et bien portantes) et verticale (des individus plus favorisés vers ceux moins favorisés).

Un service collectif financé par les impôts peut réduire les inégalités de revenus en redistribuant les ressources, particulièrement bénéfique pour les ménages à faible revenu qui contribuent peu à son financement mais en profitent autant que les autres. Cette consommation collective augmente les ressources disponibles pour d'autres besoins. En garantissant l'égalité d'accès aux moyens de réussite, ce service contribue à l'égalité des chances et, par extension, à l'égalité des situations.

La lutte contre les discriminations est un moyen pour les pouvoirs publics de promouvoir la justice sociale, principalement à travers des lois et réglementations, comme celles sur la parité. Elle comprend aussi la discrimination positive, qui offre un traitement préférentiel aux membres de minorités traditionnellement désavantagées, pour compenser les inconvénients liés à leur statut. Cette approche représente une exception à l'égalité de traitement.



```mermaid
graph TD;
    A[Prélèvements Obligatoires en Comptabilité Nationale] -->|Constitués de| B[Impôts, Taxes Fiscales, et Cotisations Sociales]
    B --> C[Versés à l'Autorité Publique]
    C -->|Sans Contrepartie Directe| D[Pas de Liaison avec Biens/Services Identifiables]
    A --> E[Protection Sociale]
    E -->|Mécanismes Institutionnels| F[Publics et Privés]
    F --> G[Système de Prévoyance Collective et Solidarité Sociale]
    G -->|Couvre Charges de| H[Risques Sociaux: Santé, Vieillesse, Chômage, Pauvreté]
    H --> I[Prestations Contributives et Non Contributives]
    I -->|Redistribution des Ressources| J[Horizontale et Verticale]
    A --> K[Services Collectifs Financés par Impôts]
    K --> L[Réduction des Inégalités de Revenus]
    L --> M[Bénéfique pour Ménages à Faible Revenu]
    M --> N[Augmente Ressources pour Autres Besoins]
    N --> O[Contribution à l'Égalité des Chances et des Situations]
    A --> P[Lutte contre les Discriminations]
    P --> Q[Promotion de la Justice Sociale par Lois et Réglementations]
    Q --> R[Inclut Discrimination Positive]
    R -->|Traitement Préférentiel pour| S[Minorités Traditionnellement Désavantagées]
    S --> T[Exception à l'Égalité de Traitement]
```




## Applications

1. Rechercher sur internet un graphique comprenant à la fois une courbe de Lorenz du patrimoine et du revenu. Lire les courbes. Comparer les inégalités de revenu avec les inégalités de patrimoine. 
   - Utilisez un moteur de recherche pour trouver un graphique comprenant à la fois une courbe de Lorenz pour le patrimoine et le revenu. Vous pourriez utiliser des termes de recherche comme "courbe de Lorenz revenu et patrimoine" ou "comparaison courbe de Lorenz revenu patrimoine". <br> 
   *Sources fiables* : publications académiques, des rapports d'institutions économiques ou des articles de médias reconnus dans le domaine économique.
   - Identifiez où la courbe s'écarte le plus de la ligne de 45 degrés, ce qui indique un haut niveau d'inégalité.
   - En général, les inégalités de patrimoine sont plus marquées que les inégalités de revenu, donc vous devriez voir la courbe du patrimoine plus éloignée de la diagonale que celle du revenu.
2. **Analyse de Courbes de Lorenz et Coefficients de Gini** : Utiliser des données réelles pour tracer des courbes de Lorenz pour différents pays et calculer les coefficients de Gini correspondants. Comparer ces courbes pour discuter des différences dans les niveaux d'inégalités entre les pays.
3. **Étude de Cas sur les Inégalités Intergénérationnelles** : Rechercher et analyser un rapport de l'OCDE ou un article académique sur la transmission intergénérationnelle des inégalités. Discuter de l'impact de l'éducation et du patrimoine familial sur les opportunités économiques des générations futures.
4. **Débat sur les Politiques Publiques** : Organiser un débat en classe sur les différentes politiques publiques pour réduire les inégalités. Chaque groupe pourrait se concentrer sur un aspect spécifique, comme la fiscalité progressive, la protection sociale, ou la lutte contre les discriminations.
5. **Projet de Recherche sur les Conceptions de la Justice Sociale** : Réaliser un projet de recherche individuel ou en groupe sur les différentes conceptions de la justice sociale (utilitarisme, libertarisme, égalitarisme libéral, etc.). Analyser comment ces théories influencent les politiques publiques en matière d'inégalités.


[ajouter des applications du livrescolaire.fr]


## Sources

- [Quelles inégalités sont compatibles avec les différentes conceptions de la justice sociale?](https://eduscol.education.fr/document/23230/download)

- Barilari, A. (2000). Le consentement à l’impôt. Les Presses de Science Po. 146p.
- Boudon, R., (2003). Raison, bonnes raisons. PUF. 192p.
- Dupuy, J.-P. (1997). Libéralisme et justice sociale : le sacrifice et l’envie. Hachette littératures. 374p.
- Duvoux N. (2017). Les inégalités sociales. PUF. Collection Que sais-je? 128p.
- Fitoussi J-P., Rosanvallon P. (1996). Le nouvel âge des inégalités. Seuil. 232p.
- Galland O., Lemel Y. (2018). Sociologie des inégalités. Armand Colin. Collection U Sociologie, 352p.
- Garbinti B., Goupille-Lebretb J., Piketty T. (2018). Income inequality in France, 1900-2014 : Evidence from Distributional National Accounts. Journal of Public Economics. Volume 162, p63-77.
- Garbinti B. et Goupille-Lebret J. (2019). Inégalités de revenu et de richesse en France : évolutions et liens sur longue période. Économie et statistiques (N° 510-511-512).
- Garbinti B. et Goupille-Lebret J. (2018). Inégalités de patrimoine en France : quelles évolutions de 1800 à 2014 ? Rue de la Banque N°66.
- Piketty T. (2015). L’économie des inégalités. La Découverte. Collection Repères, N°216,
128p.

- [The Impact of Parental Income and Education on the Schooling of Their Children](https://docs.iza.org/dp1496.pdf)
- [Building a more inclusive society ](https://www.lisdatacenter.org/newsletter/nl-2018-8-im-2/) by Miles Corak (Department of Economics and Stone Center on Socio-Economic Inequality. The Graduate Center, City University of New York)

- Arnsperger, C. and Van Parijs, P., 2003. Éthique économique et sociale. La Découverte. 

- [Inégalité et pauvreté (OCDE)](https://www.oecd.org/fr/social/inegalite-et-pauvrete.htm)

- [Intergenerational Transmission of Disadvantage (OECD)](https://read.oecd-ilibrary.org/social-issues-migration-health/intergenerational-transmission-of-disadvantage_217730505550#page1)

- [Les inégalités en sept tableaux (Alternatives Économiques)](https://www.alternatives-economiques.fr/inegalites-sept-tableaux/00102019)

- [Major Streams in the Economics of Inequality: A Qualitative and Quantitative Analysis of the Literature since 1950s](https://www.iza.org/publications/dp/14777/major-streams-in-the-economics-of-inequality-a-qualitative-and-quantitative-analysis-of-the-literature-since-1950s)