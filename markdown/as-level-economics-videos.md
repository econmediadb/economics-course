# Videos for AS-level and IGCSE Economics

## Cité de l'Economie

- [Common property](https://www.youtube.com/watch?v=7EP5PtTT6wg)
- [Positive and negative externalities](https://www.youtube.com/watch?v=YAIIjc9GmVA)
- [The prisoner's dilemma](https://www.youtube.com/watch?v=Xn3In2roJ5U)
- [Emission allowance markets](https://www.youtube.com/watch?v=sAwt2jTiA74)
- [Value chains](https://www.youtube.com/watch?v=MoqFkwG7tUc)
- [The Moral Hazard](https://www.youtube.com/watch?v=TNzqiN-Hm-8)
- [The Social Security](https://www.youtube.com/watch?v=eJPFi-aGkFY)
- [Social and solidarity economy (SSE)](https://www.youtube.com/watch?v=uWTm60o8iF8)
- [The systemic shock](https://www.youtube.com/watch?v=iCTDB3yXRo8)
- [World Trade Organization](https://www.youtube.com/watch?v=zAdYlDm3-EM)


## Draw me the Economy

- [Money Supply](https://www.youtube.com/watch?v=2CStPH8Hq_M)
- [What is comparative advantage?](https://www.youtube.com/watch?v=0hK9p8BSDMM)
- [What is gross domestic product?](https://www.youtube.com/watch?v=-Zhum539cyE)
- [What is the purpose of the EU budget?](https://www.youtube.com/watch?v=a5kF78h-vcg)
- [How bad news affect the economy?](https://www.youtube.com/watch?v=EonWGzkVAYI)
- [How can a country go bankrupt?](https://www.youtube.com/watch?v=hMBf7ipyXgs)
- [The stock market and business financing](https://www.youtube.com/watch?v=-oeWmNJ2Buw)
- [What is the budget wall?](https://www.youtube.com/watch?v=HnjEBP-96K8)
- [No economy without trust](https://www.youtube.com/watch?v=3mp2hTmFGRU)

## IMF Institute Learning Channel

- [The Balance Sheet](https://www.youtube.com/watch?v=8cAEk1H0ZnE)
- [Stocks and Economic Flows: Illustration](https://www.youtube.com/watch?v=vk17spPkNdE)
- [Swan Diagram](https://www.youtube.com/watch?v=iMhJIaYI45w)  to identify imbalances and analyze macroeconomic policies to address those imbalances
- [Terms of Trade Shock](https://www.youtube.com/watch?v=cxRhTuIZ_ok)
- [Classification of Exchange Rate Regimes](https://www.youtube.com/watch?v=4xcEfd35XRU)
- [Valuation of economic flows and stocks in macroeconomic statistics](https://www.youtube.com/watch?v=3tOPD_gXhwE)
- [Monetary and nonmonetary transactions in macroeconomic statistics](https://www.youtube.com/watch?v=2KB9xEwLYAI)
- [The households sector in macroeconomic statistics](https://www.youtube.com/watch?v=NXMqTi9Trg8)

## Khan Academy: Introduction to Economics

- [Supply, demand, and market equilibrium](https://www.youtube.com/watch?v=8JYP_wU1JTU&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=3)
- [Scarcity](https://www.youtube.com/watch?v=iy-fhpbTH9E&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=3)
- [Four factors of production](https://www.youtube.com/watch?v=-IvwoqPh1_I&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=4)
- [Scarcity and rivalry](https://www.youtube.com/watch?v=uVA1-m8SVvA&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=5)
- [Normative and positive statements](https://www.youtube.com/watch?v=YtX6SGw7E3c&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=6)
- [Economic models](https://www.youtube.com/watch?v=7n_Hf_UsW7I&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=7)
- [Command and market economies](https://www.youtube.com/watch?v=Ve6K10-Yx_M&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=8)
- [Production possibilities frontier](https://www.youtube.com/watch?v=_7VHfuWV-Qg&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=9)
- [Opportunity Cost](https://www.youtube.com/watch?v=pkEiHZAtoro&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=10)
- [Increasing opportunity cost](https://www.youtube.com/watch?v=00fgAG6VrRQ&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=11)
- [PPCs for increasing, decreasing and constant opportunity cost](https://www.youtube.com/watch?v=-aVZtzD44vI&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=12)
- [Production Possibilities Curve as a model of a country's economy](https://www.youtube.com/watch?v=9zZY6RfN6iA&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=13)
- [Comparative advantage specialization and gains from trade](https://www.youtube.com/watch?v=xx9xNJlPOJo&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=14)
- [Comparative advantage and absolute advantage](https://www.youtube.com/watch?v=xN3UV5FsBkU&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=15)
- [Opportunity cost and comparative advantage using an output table](https://www.youtube.com/watch?v=K8p0F92gVUM&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=16)
- [Terms of Trade and the Gains from Trade](https://www.youtube.com/watch?v=C-xLUS5JGIM&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=17)
- [Input approach to determining comparative advantage](https://www.youtube.com/watch?v=Uqf5c6Xcjr4&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=18)
- [When there aren't gains from trade](https://www.youtube.com/watch?v=TeAzW_tY6m4&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=19)
- [Law of demand](https://www.youtube.com/watch?v=ShzPtU7IOXs&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=20)
- [Price of related products and demand](https://www.youtube.com/watch?v=-oClpRv7msg&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=21)
- [Change in expected future prices and demand](https://www.youtube.com/watch?v=7siSvYs84fg&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=22)
- [Normal and inferior goods](https://www.youtube.com/watch?v=wYuAwm-5-Bk&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=23)
- [Change in demand versus change in quantity demanded](https://www.youtube.com/watch?v=iC9hkhbIimA&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=24)
- [Law of supply](https://www.youtube.com/watch?v=3xCzhdVtdMI&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=25)
- [Factors affecting supply](https://www.youtube.com/watch?v=0isM0GF-rMI&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=26)
- [Change in supply versus change in quantity supplied](https://www.youtube.com/watch?v=NrlF8mMHfLE&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=27)
- [Market equilibrium](https://www.youtube.com/watch?v=PEMkfgrifDw&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=28)
- [Changes in Market Equilibrium](https://www.youtube.com/watch?v=NgPqyM3I_8o&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=29)
- [Changes in equilibrium price and quantity when supply and demand change](https://www.youtube.com/watch?v=kl4n-EWwPyA&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=30)
- [Changes in income, population, or preferences](https://www.youtube.com/watch?v=do1HDIdfQkU&list=PLSQl0a2vh4HDERCw_ddanXbsDpFWcpL-S&index=31)


## Bank of England - Education

- [Money creation in the modern economy](https://www.youtube.com/watch?v=CvRAqR2pAgw&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=3)
- [Quantitative easing - how it works](https://www.youtube.com/watch?v=J9wRq6C2fgo&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=1)
- [The role of the Bank of England: Money (Episode 1)](https://www.youtube.com/watch?v=dI0MVV5UjKc&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=3)
- [What is inflation?](https://www.youtube.com/watch?v=APqGR9s2Ywg&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=4)
- [Role of the Bank of England: prices & the value of money (episode 2)](https://www.youtube.com/watch?v=-d0-8tr6DGo&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=5)
- [Role of the Bank of England: controlling spending (episode 3)](https://www.youtube.com/watch?v=7I48BA9-iu4&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=6)
- [Role of the Bank of England: the financial system (episode 4)](https://www.youtube.com/watch?v=D--q6OVHJbQ&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=7)
- [Money in the modern economy: an introduction](https://www.youtube.com/watch?v=ziTE32hiWdk&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=8)
- [Role of the Bank of England: how the Bank works (episode 7)](https://www.youtube.com/watch?v=b75qgqHqV98&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=9)
- [History of inflation](https://www.youtube.com/watch?v=7rpvxZphZZc&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=10)
- [What is the Bank of England?](https://www.youtube.com/watch?v=cmzXGgzWH7g&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=11) The Bank of England is unlike banks you see on the high street or use for online banking.
- [Role of the Bank of England: the money go round (episode 5)](https://www.youtube.com/watch?v=x0xzplJOyHg&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=12)
- [What is stress testing?](https://www.youtube.com/watch?v=XtihtTHVXTE&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=13)
- [Role of the Bank of England: keeping confidence (episode 6)](https://www.youtube.com/watch?v=bVZz5g3W5BM&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=14)
- [What causes inflation?](https://www.youtube.com/watch?v=WZ_OyroIQc0&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=15)
- [How does the Bank manage inflation?](https://www.youtube.com/watch?v=ypv8cqxfWic&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=17)
- [What are bank funding costs?](https://www.youtube.com/watch?v=kFtpWnpqqFA&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=21)
- [Why does inflation matter?](https://www.youtube.com/watch?v=ce3U6n13ON4&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=22)
- [How are cryptocurrencies created?](https://www.youtube.com/watch?v=v8IFb1E8-RA&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=23)
- [Why do interest rates matter to me?](https://www.youtube.com/watch?v=L-mT7o-0PkY&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=24)
- [Why do financial markets matter?](https://www.youtube.com/watch?v=X182ULoFrHw&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=25)
- [Why does economic growth matter?](https://www.youtube.com/watch?v=sWznhE2jEUA&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=27)
- [Who sets the exchange rate?](https://www.youtube.com/watch?v=G9Pd6LQGH_s&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=29)
- [What do investment banks do?](https://www.youtube.com/watch?v=eTqT0psuSjk&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=30)
- [Gold and the Bank of England](https://www.youtube.com/watch?v=1mKewn85sY4&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=31)
- [Does the Bank of England print money?](https://www.youtube.com/watch?v=blX1XlMsvgE&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=36)
- [Regulating the banking system](https://www.youtube.com/watch?v=ryOjSsPxvL8&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=37)
- [Lessons from Japan's banking crisis](https://www.youtube.com/watch?v=-9R7LVq3DU0&list=PLslyOrpjJ0z0GLFBEzj8mLcYKPjqB1zY2&index=51)


## CORE Econ

- [Income and substitution effects](https://www.youtube.com/watch?v=mpZKa5bbnUQ)
- [The Phillips curve](https://www.youtube.com/watch?v=7RIoTg2u-vM)
- [Expected inflation and the Phillips curve](https://www.youtube.com/watch?v=j6YXY3zNmsM)
- [The multiplier model](https://www.youtube.com/watch?v=VfKgztLaVg8)
- [The labour market (WS/PS) model for the aggregate economy](https://www.youtube.com/watch?v=adp7UFYkU0I)
- [Efficiency in competitive markets](https://www.youtube.com/watch?v=r-eUWmGD_oY)
- [Price-taking firms](https://www.youtube.com/watch?v=LCjsarda5vw)
- [Invisible hand game](https://www.youtube.com/watch?v=Sfh0Qj78dvA)
- [Cost curves](https://www.youtube.com/watch?v=Bx9c6ItoSR4)
- [Monopoly: price-setting decision](https://www.youtube.com/watch?v=UIeoMZ6Lf9E)
- [Check for more CORE Econ videos on their Youtube Channel](https://www.youtube.com/@COREEcon/videos)
<br>

<br>

<br>

<br>

## Project Syndicate - In Theory

- [All *In Theory* videos](https://www.youtube.com/watch?v=HnCD6rUdv1U&list=PLlArSuF2TLy65cs8R9huQSIJaJL5kJVxA)
- [The Rise and Fall of GDP](https://www.youtube.com/watch?v=zbywKvR3fVU)
- [Trickle-Down Economics](https://www.youtube.com/watch?v=TuSwBDZaEtQ)
- [The Current-Account Tightrope](https://www.youtube.com/watch?v=zXcJZ4ObEGk)
- [Economic Growth](https://www.youtube.com/watch?v=swQY-nIojcc)
