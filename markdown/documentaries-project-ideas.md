# Project Ideas

![QR code or link http://www.edulink.lu/djsq](../qr-codes/djsq.png)


1. [Putting Drivers in the Driver's Seat: Kenya](https://www3.nhk.or.jp/nhkworld/en/ondemand/video/2098015/)
2. [Showcasing Heritage with 3D Animation: Cambodia](https://www3.nhk.or.jp/nhkworld/en/ondemand/video/2098012/)
3. [Wipe Out Malaria with Drones: Sierra Leone](https://www3.nhk.or.jp/nhkworld/en/ondemand/video/2098014/)
4. [Matchmaking Social Entrepreneurs: Cambodia](https://www3.nhk.or.jp/nhkworld/en/ondemand/video/2098016/)
5. [Fashion Makes Dreams a Reality: The Philippines](https://www3.nhk.or.jp/nhkworld/en/ondemand/video/2098013/)
6. [Putting Drivers in the Driver's Seat: Kenya](https://www3.nhk.or.jp/nhkworld/en/ondemand/video/2098015/)
7. [Showcasing Heritage with 3D Animation: Cambodia](https://www3.nhk.or.jp/nhkworld/en/ondemand/video/2098012/)
8. [Empowering Syrians through IT](https://www3.nhk.or.jp/nhkworld/en/ondemand/video/2098010/)
9. [A New Route for Quality Cacao: Ghana](https://www3.nhk.or.jp/nhkworld/en/ondemand/video/2098008/)
10. [Tackling Social Issues through Business: Japan](https://www3.nhk.or.jp/nhkworld/en/ondemand/video/2098007/)

## Source

[Sharing the Future (NHK)](https://www3.nhk.or.jp/nhkworld/en/tv/sharing/)