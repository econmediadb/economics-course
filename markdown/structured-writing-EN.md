# Explanatory note on how to use the structured economic essay model

The structured economic essay model consists of three major parts: knowledge and application, analysis, and evaluation. This model is designed to help you write a well-organized and comprehensive essay on an economic topic.


## Part 1: Knowledge and application

In this section, you should demonstrate your knowledge of the economic concepts related to your topic. Start by introducing the key concepts and explaining them in your own words. Then, apply these concepts to the specific case or scenario you are analyzing. Use relevant examples and data to support your arguments.


## Part 2: Analysis


In this section, you should analyze the economic factors that are relevant to your topic. Identify the main issues and explain how they are related to each other. Use economic theories and models to support your analysis. Be sure to consider both the positive and negative effects of the economic factors you are discussing.


## Part 3: Evaluation


In this section, you should evaluate the economic factors you have analyzed and provide your own perspective on the topic. Consider the strengths and weaknesses of the economic factors and how they could be improved. Provide your own recommendations for how the economic factors could be better managed or addressed.

Remember to use clear and concise language throughout your essay. Use appropriate economic terminology and provide relevant examples and data to support your arguments.

By following this structured economic essay model (see diagram below), you can ensure that your essay is well-organized, comprehensive, and demonstrates a clear understanding of the economic concepts related to your topic. Good luck!

## Extending the simple model 

In its simplest form, learners need to use chains of reasoning to demonstrate their understanding of basic economic mechanisms. 

<img src="https://gitlab.com/econmediadb/economics-course/-/raw/main/mindmap/018-Structured-Writing-Simple-Model-EN.png" alt="Alternative Decision Mindmap" width="50%" />

This can be extended to a second model, where two sets of chains are used to describe the impact of two different economic policies on inflation or the economy as a whole.

<img src="https://gitlab.com/econmediadb/economics-course/-/raw/main/mindmap/018-Structured-Writing-Model-Alternative-Decision-EN.png" alt="Alternative Decision Mindmap" width="50%" />


A third and more complex model involves explaining the weaknesses or strengths of the mechanisms described in the analysis part. This model requires a deeper understanding of economic concepts and the ability to critically evaluate the effectiveness of different policies.

<img src="https://gitlab.com/econmediadb/economics-course/-/raw/main/mindmap/018-Structured-Writing-Model-Alternative-Decision-With-AssessmentEN.png" alt="Alternative Decision Mindmap with Assessment" width="50%" />

In summary, these models provide a framework for learners to demonstrate their understanding of economic mechanisms and how they can be applied to real-world situations. By using chains of reasoning and analyzing the strengths and weaknesses of different policies, learners can develop their critical thinking skills and become more knowledgeable about economics.



## Types of writing assignments

1. Response paper (1-2 paragraphs)
    - summarize an extract
    - answer question about text
    - helps to focus attention on important topics in the text
    - helps to develop the themes and vocabulary
2. Short essay (3-4 paragraphs)
    - discuss or compare policy implications
    - explain a model
    - criticize an argument
    - present a case study
    - asks the learner to have a thesis or central argument
    - present an analysis to make your case
3. Empirical exercise (5-6 paragraphs)
    - analyze economic data
    - use data to answer an economic question
    - draw conclusion from evidence
4. Term paper (10-15 paragraphs)

## Source

Neugeboren, Robert H. The Student’s Guide to Writing Economics. New York ;: Routledge, 2005.





