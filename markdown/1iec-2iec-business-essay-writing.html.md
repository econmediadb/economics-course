# Business Essay Writing

## 2iec and 1iec Business





### <em>Marketing mix</em> (1iec, Question 1c Business Paper 2022-08-18)

#### a. Question

Assess the likely importance of distribution (place) in Rolex’s marketing mix.


#### b. Sample Answers

[Sample answer](https://qualifications.pearson.com/content/dam/pdf/A-Level/Business/2015/Exam-materials/9bs0-01-pef-20220818.pdf#page=13)


#### c. Explanation

- This question clearly analyses the importance of creativity, doing so with strong links to the context. 
- Chains of reasoning are well-developed to explain how and why Hans Wilsdorf was creative. 
- The risk-taking characteristic, although implied in being an entrepreneur, is exemplified with reference to Wilsdorf selling watches to men, where previously they were viewed as women's jewellery.


#### d. Tips


---




### <em>Public limited company</em> (1iec, Question 1e Business Paper 2022-08-18)

#### a. Question

In order to remain competitive in the luxury watch market, Rolex could become a public limited company or remain a private limited company.

Evaluate these two options and recommend which one is more likely to support
Rolex remaining competitive in the luxury watch market.


#### b. Explanation

This question is the first of two 20 mark questions on the paper. In this case it was focused
on the decision as to whether Rolex should remain a Private Limited Company or become a
Public Limited Company (PLC), in order to maintain its competitiveness. This point about
competitiveness is key to success and higher marks. The question itself is accessible and
many candidates were able to give a strong answer, comparing the two legal types of
ownership.

#### c. Sample Answers

[Sample answer](https://qualifications.pearson.com/content/dam/pdf/A-Level/Business/2015/Exam-materials/9bs0-01-pef-20220818.pdf#page=17)

#### d. Tips

- it is not always necessary to write lots and fill all lines available. 
- keep your focus clearly on the question
- link each sentence and paragraph back to the question, and 
- make extensive use of the extract and context. 
- give particular care and detail to the recommendation, which should be (as is the case here) a
significant proportion of your answer
- this response links the capital raising potential of becoming a PLC, to the maturity phase of the
product life cycle of their most popular watches.

---





### <em>Multinational</em> (1iec, Question 2d Business Paper 2022-08-18)

#### a. Question

Assess the possible impact of multinationals, such as Nike, on the economy of Indonesia.

#### b. Explanation


#### c. Sample Answers

[Sample answer](https://qualifications.pearson.com/content/dam/pdf/A-Level/Business/2015/Exam-materials/9bs0-01-pef-20220818.pdf#page=26)

#### d. Tips


---






### <em>Competitiveness</em> (1iec, Question 2e Business Paper 2022-08-18)

#### a. Question

Nike is aiming to achieve competitive advantage in the global sports footwear market. In order to do this Nike could focus on cost competitiveness or differentiation.

Evaluate these two options and recommend which one is most suitable for Nike to maintain its global competitiveness.


#### b. Explanation

- This question requires candidates to take Porter's generic strategies, and apply it to global competitiveness
- There is a range of information in the extracts that candidates could use to address this question. 
- Some of this is specific context, such as Nike's partnership with Flex supports the option of differentiation to achieve competitive advantage, while cost competitiveness is more generally supported by information about Indonesia and Vietnam and business costs of operations. 
- Candidates can choose either option and fully support this, however the best answers are those that are clear about the focus on the global competitiveness of Nike.

- this response is very clear about the difference between cost and price. 
- Porter's theory of competitive advantage states that businesses gain from being low cost, or through differentiating. 
- Low cost gives businesses the option of reducing prices, or increasing profit margins.
- Many candidates used cost competitiveness as the same as price competitiveness, which is inaccurate. 
- This response also applies Porters' framework to a global context and links competitiveness to locating in economies such as Indonesia – exactly what the question requires. 
- The differentiation option is made possible by Nike Flex and the potential for customisation of footwear. 
- The judgement brings developing markets and market growth as the other factors that support the option of cost competitiveness that is supported and argued for.

#### c. Sample Answers

[Sample answer](https://qualifications.pearson.com/content/dam/pdf/A-Level/Business/2015/Exam-materials/9bs0-01-pef-20220818.pdf#page=30)

#### d. Tips

- Simple, but important: costs are what businesses pay, prices are what businesses charge. 
- This distinction is particularly important when discussing competitive advantage because price is not a term that Porter used in this model. 
- To write the best answers possible, link theoretical knowledge to the global context, in this case international footwear sales, to maximise your marks.

---




### <em>Porter’s five force</em> (1iec, Question 1c Business Paper 2022-06-10)

#### a. Question

Assess, with reference to Porter’s five forces, whether the ‘bargaining power of buyers’ represents the most significant external influence on easyJet plc.

#### b. Explanation


#### c. Sample Answers

[Sample answer (40%)](https://qualifications.pearson.com/content/dam/pdf/A-Level/Business/2015/Exam-materials/9bs0-02-pef-20220818.pdf#page=10)

[Sample answer (90%)](https://qualifications.pearson.com/content/dam/pdf/A-Level/Business/2015/Exam-materials/9bs0-02-pef-20220818.pdf#page=12)

#### d. Tips

- Fewer arguments that are well developed and consistently supported by the context will enable you to access the higher levels of the mark scheme

---







### <em>Working capital</em> (1iec, Question 1d Business Paper 2022-06-10)

#### a. Question

Assess whether the change in price of jet fuel between November 2015 and July 2018 may have affected easyJet plc’s management of its working capital.

#### b. Explanation


#### c. Sample Answers

[Sample answer (92%)](https://qualifications.pearson.com/content/dam/pdf/A-Level/Business/2015/Exam-materials/9bs0-02-pef-20220818.pdf#page=16)

[Sample answer (34%)](https://qualifications.pearson.com/content/dam/pdf/A-Level/Business/2015/Exam-materials/9bs0-02-pef-20220818.pdf#page=18)

#### d. Tips

- Working capital is a concept that many candidates struggle with. 
- It is important that candidates prepare well by thoroughly revising each topic in the specification.

---







### <em>Profitability</em> (1iec, Question X Business Paper YYYY-MM-DD)

#### a. Question

EasyJet plc wishes to improve its profitability. To achieve this, easyJet plc is considering either purchasing a new fleet of electric and hydrogen powered aircraft or focusing on increasing its market share of the package holiday market.

Evaluate these two options and recommend which one is more suitable for easyJet plc to improve its profitability.

#### b. Explanation

- Stronger responses made selective, well developed arguments for both options consistently supported by
material from the extracts. 
- This led to a well-supported recommendation with insightful use of 'MOPS'. 
- Weaker responses tended to offer a 'scatter gun' approach, consisting of too many arguments which often lacked analysis and application. Recommendations were often based upon assertion or simply repeated previous arguments. 
- A common mistake was to write lengthy answers which lacked focus and as a consequence many candidates had insufficient time to fully attempt later questions.

#### c. Sample Answers

[Sample answer (40%)](https://qualifications.pearson.com/content/dam/pdf/A-Level/Business/2015/Exam-materials/9bs0-02-pef-20220818.pdf#page=21)

[Sample answer (85%)](https://qualifications.pearson.com/content/dam/pdf/A-Level/Business/2015/Exam-materials/9bs0-02-pef-20220818.pdf#page=24)


#### d. Tips

- arguments need to be well developed with coherent chains of reasoning and consistently supported by context
- candidates should be encouraged to make fewer arguments but to ensure that they are well developed and contextualised.

---







### <em>Topic</em> (1iec, Question X Business Paper YYYY-MM-DD)

#### a. Question



#### b. Explanation


#### c. Sample Answers


#### d. Tips



---

