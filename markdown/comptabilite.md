# Comptabilité Générale

## Cours de 2DG

### Tutorial

Le code suivant permet de construire des comptes en T:

```tex
\newcommand\Taccount[3][5.5cm]%
   {{\renewcommand\arraystretch{1.3}%
    \begin{tabular}[t]{@{}p{#1}|p{#1}@{}}
    \multicolumn{2}{@{}c@{}}{#2}\\
    \hline
    #3
    \end{tabular}%
   }}
```

```tex
\Taccount{ 
            \textbf{D} \emph{\small{Comptes d'actif}}  \textbf{C}
         }
         {
            v.i.                                            &  \\ 
            \textcolor{ao(english)}{$\uparrow$ de la v.i.}  &  \textcolor{bostonuniversityred}{$\downarrow$ de la v.i.}
         }
```

where the colors are defined as follows:

```tex
% Define colors (taken from https://latexcolor.com/)
\definecolor{bostonuniversityred}{rgb}{0.8, 0.0, 0.0}
\definecolor{ao(english)}{rgb}{0.0, 0.5, 0.0}
```

![compte-en-T](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/T-compte.png "compte-en-T")


### Les comptes de bilan et les comptes de gestion

[Introduction aux comptes de bilan et aux comptes de gestion](pdf/comptes-de-bilan-et-comptes-de-gestion.pdf) et le [code source LaTeX](https://www.overleaf.com/read/gpfxqrdmrwgv)

### La taxe sur la valeur ajoutée

[Introduction à la taxe sur la valeur ajoutée](pdf/taxe-sur-la-valeur-ajoutee.pdf) et le [code source LaTeX](https://www.overleaf.com/read/gpfxqrdmrwgv)

### Les réductions de prix

[Introduction à la facturation et aux réductions de prix](pdf/facuration-et-reduction-de-prix.pdf) et le [code source LaTeX](https://www.overleaf.com/read/gpfxqrdmrwgv)

### Les stocks


### Les amortissements


### La cession d'immobilisations corporelles et incorporelles


### Le portefeuille de titres


### Les dépréciations des comptes clients


### Les provisions pour risque et charges


### La régularisation des charges et des produits


