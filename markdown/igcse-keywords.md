# IGCSE Economics Keywords

## Chapter 1: The nature of the economic problem

- basic economic problem
- economic agents
- economic goods
- free goods
- goods
- needs
- private sector
- public sector
- services
- wants

## Chapter 2: The factors of production

- capital
- ceteris paribus
- enterprise
- factors of production
- geographical mobility
- labour
- land
- occupational mobility

## Chapter 3: Opportunity cost

- consumers
- decision makers
- firms
- government
- opportunity cost

## Chapter 4: Production possibility curve

- efficiency
- inefficiency
- movement
- production possibility curve (PPC)
- productive capacity
- shift
- the PPC diagram

## Chapter 5: Microeconomics and macroeconomics

- consumers
- households
- microeconomics
- macroeconomics

## Chapter 6: Th erole of markets in allocating resources

- market disequilibrium
- market equilibrium
- market system
- price mechanism

## Chapter 7: Demand

- complements
- contraction in demand
- decrease in demand
- demand
- extension in demand
- increase in demand
- law of demand
- market demand
- quantity demanded
- substitutes

## Chapter 8: Supply

- contraction in supply
- decrease in supply
- extension in supply
- increase in supply
- law of supply
- market supply
- quantity supplied
- supply

## Chapter 9: Price determination

- equilibrium price
- excess demand
- excess supply
- market disequilibrium
- market equilibrium
- shortages
- surpluses

## Chapter 10: Price changes

- decrease in demand
- decrease in supply
- increase in demand
- increase in supply
- non-price factors
- sales tax
- subsidy

## Chapter 11: Price elasticity of demand

- perfectly price elastic
- perfectly price inelastic
- price discrimination
- price elastic demand
- price elasticity of demand (PED)
- price inelastic demand
- profit
- sales revenue
- unitary price elasticity of demand

## Chapter 12: Price elasticity of supply

- price elastic supply
- price elasticity of supply (PES)
- price inelastic supply
- stocks
- unitary price elasticity of supply

## Chapter 13: Market economic system

- economic system
- market economy
- mixed economy
- planned economy
- private sector
- public sector

## Chapter 14: Market failure

- demerit goods
- external benefits
- external costs
- externalities (spillover effects)
- free riders
- market failure
- merit goods
- private benefits
- private costs
- public goods
- social benefits
- social costs

## Chapter 15: Mixed economic system

- direct provision
- maximum price
- minimum price
- mixed economy
- nationalisation
- privatisation
- rules and regulations
- subsidy
- taxation

## Chapter 16: Money and banking

- bank deposits
- bartering
- cash
- central bank
- commercial banks
- functions of money
- money
- stock exchange

## Chapter 17: Household

- bad debts
- borrowing
- capital expenditure
- collateral
- conspicuous consumption
- consumer spending
- current expenditure
- disposable income
- dissaving
- income
- mortgage
- saving
- savings ratio
- wealth

## Chapter 18: Workers

- backward-bending supply of labour curve
- demand for labour
- derived demand
- division of labour
- equilibrium wage rate
- fringe benefits (perks)
- geographical mobility of labour
- labour force participation rate
- national minimum wage
- non-wage factors
- occupational mobility of labour
- piece rate
- productivity of labour
- salaries
- specialisation
- supply of labour
- wage determination
- wage factors
- wages

## Chapter 19: Trade unions

- collective bargaining
- craft union
- general union
- go-slow
- industrial action
- industrial union
- sit-in
- strike
- trade union
- white-collar union
- works-to-rule

## Chapter 20: Firms

- average costs
- conglomerate integration
- demerger
- diseconomies of scale
- economies of scale
- external economies of scale
- external growth
- franchise
- horizontal integration
- interdependence
- internal economies of scale
internal growth
- market share
- merger
- primary sector
- private sector
- public sector
- secondary sector

## Chapter 21: Firms and production

- capital-intensive
- derived demand
- innovation
- labour productivity
- labour-intensive
- production
- productivity

## Chapter 22: Firms' costs, revenue and objectives

- average fixed cost
- average revenue
- average total cost
- average variable cost
- costs of production
- fixed costs
- objectives
- profit
- profit maximisation
- sales revenue
- total cost
- total revenue
- variable costs

## Chapter 23: Market structure

- barriers to entry
- competitive markets
- market structure
- monopoly
- price maker [price setter]
- price takers
