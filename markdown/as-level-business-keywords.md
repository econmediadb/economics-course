# AS-level Economics

Five Parts of the AS-level Business course:
1. Business and its environment
2. Human resource management
3. Marketing
4. Operations management
5. Finance and accounting

## Part 1: Business and its environment

1. Enterprise
2. Business structure
3. Size of business
4. Business objectives
5. Stakeholders in a business

### Chapter 1: Enterprise

- business objectives
- transformation process
- factors of production
- primary sector
- adding value
- brand
- market forces
- opportunity cost
- entrepreneur
- enterprise
- intrapreneur

### Chapter 2: Business structure

- nationalisation
- privatisation
- merit goods
- unlimited liability
- company
- shareholders
- limited liability
- franchise
- co-operatives
- joint ventures
- social enterprises

### Chapter 3: Size of business

- niche
- type of integration (horizontal, vertical, conglomerate diversification)
- internal and external growth


### Chapter 4: Business objectives

- objective
- labour productivity
- corporate objective
- market share
- cash flow
- ethics
- social responsibility
- mission statement
- aim
- strategy
- tactics
- target
- budgets
- ethical behaviour

### Chapter 5: Stakeholders in a business

- stakeholders
- authority
- internal stakeholders
- external stakeholders
- dividends

## Part 2: Human resource management

1. Human resource management
2. Motivation
3. Management

### Chapter 10: Human resource management

- human resource management (HRM)
- delayering
- teamworking
- workforce plan (or human resource plan)
- labour turnover
- recruitment and selection
- job descriptions
- person specifications (or job specifications)
- employment contract
- business culture
- dismissal
- redundancies
- employee welfare
- employee morale
- work-life balance
- diversity
- equality
- training
- development
- delegation
- intrapreneurship
- multi-skilling
- trade union
- collective bargaining

$$
\begin{aligned}
\text{turnover} & = \frac{ \text{number of staff leaving during the year} }{ \text{average number of staff} } \times 100
\end{aligned} 
$$


### Chapter 11: Motivation

- motivation
- absenteeism
- human needs
- schools of thought
- piece-rate
- division of labour
- hierarchy of needs
- hygiene factors ( or maintenance factors)
- motivators
- performance-related pay (PRP)
- variable pay
- fringe benefits (or perks)
- job redesign
- job enrichment (or vertical loading)
- job enlargement (or horizontal loading)
- job rotation
- empowerment
- job design
- employee participation


### Chapter 12: Management

- leadership
- management
- autocratic management (or authoritarian management)
- paternalistic management
- democratic management (or participative management)
- laissez-faire management


## Part 3: Marketing

1. The nature of marketing
2. Market research
3. The marketing mix - product and price
4. The marketing mix - promotion and place

### Chapter 17: The nature of marketing

- marketing
- marketing objective
- corporate objective
- marketing strategy
- business-to-consumer marketing (B2C)
- business-to-business marketing (B2B)
- market size
- market growth
- unique selling point (USP)
- niche marketing
- market segment
- mass marketing
- customer-relationship marketing (CRM)
- customer retention

$$
\begin{aligned}
\text{market share} & = \frac{ \text{sales of a business (or product)} }{ \text{total market sales} } \times 100 \\
\\
\text{market growth} & = \frac{ \text{(market sales)}_{ \text{this year} } - \text{(market sales)}_{ \text{last year} } }{ \text{market sales}_{ \text{last year} } } \times 100 
\end{aligned} 
$$


### Chapter 18: Market research

- market research
- primary market research
- focus group
- secondary market research
- sample
- validity of market research
- reliability of market research

$$
\begin{aligned}
\text{market growth} & = \frac{ \text{change in market size} }{ \text{original market size} } \times 100 \\
\\
\text{market share of business} & = \frac{ \text{sales by the business} }{ \text{total market sales} } \times 100
\end{aligned} 
$$

### Chapter 19: The marketing mix - product and price

- marketing mix
- products
- tangible attributes
- intangible aspects
- product differentiation
- product portfolio analysis
- product life cycle
- extension strategy
- product portfolio analysis (PPA)
- Boston Matrix
- competitive pricing
- penetration pricing
- price skimming
- price discrimination
- dynamic pricing
- cost-based pricing
- psychological pricing
- promotional mix

### Chapter 20: The marketing mix - promotion and place

- digital promotion
- click-through rate (CTR) 
- marketing expenditure budget
- distribution channel
- distribution outlet

## Part 4: Operations management

1. The nature of operations
2. Inventory management
3. Capacity utilisation and outsourcing

### Chapter 23: The nature of operations

- output
- inventory
- operations mangement
- productivity
- sustainable
- capital-intensive
- labour-intensive

$$
\begin{aligned}
\text{labour productivity} & = \frac{ \text{total output} }{ \text{numbner of employees} } \times 100 
\end{aligned} 
$$

### Chapter 24: Inventory management

- supply chain
- supply chain management
- lean production


### Chapter 25: Capacity utilisation and outsourcing

- capacity
- factors of production
- capacity utilisation
- capacity under-utilisation
- rationalisation
- subcontracting
- outsourcing

$$
\begin{aligned}
\text{per cent capacity} & = \frac{ \text{existing output}_{ \text{over a given time period} } }{ \text{maximum possible output}_{ \text{over a given time period} } } \times 100 
\end{aligned} 
$$

## Part 5: Finance and accounting

1. Business finance
2. Forecasting and managing cash flows
3. Costs
4. Budgets

### Chapter 29: Business finance

- asset
- capital
- non-current assets
- short-term sources of finance
- long-term sources of finance
- insolvency
- liabilities
- bankruptcy
- liquidation
- administration
- working capital
- current assets
- trade payables
- trade receivables
- revenue expenditure
- capital expenditure
- statement of financial position
- income statement

$$
\begin{aligned}
\text{working capital} & = \text{current assets} - \text{current liabilities} 
\end{aligned} 
$$

### Chapter XX: Sources of finance

- internal source of finance
- external source of finance
- trade credit
- bank loan
- venture capital
- debt factoring
- microfinance
- crowdfunding
- government grant

### Chapter 30: Forecasting and managing cash flows

- cash
- cash-flow forecast


### Chapter 31: Costs

- costs
- revenue
- direct costs
- indirect costs
- full costing
- contribution
- break-even
- profits
- contributioon costing
- average costs
- marginal costs
- cost-plus pricing
- contribution pricing
- special-order decisions
-  margin of safety

$$
\begin{aligned}
\text{profit (or loss)} & = \text{total revenue} - \text{total costs} \\
\\
\text{total cost of production} & = \text{direct costs} + \text{indirect costs} \\
\\
\text{contribution per unit} & = \text{selling price of one unit of output} + \text{variable costs of producing that unit} \\
\text{or} \\
\text{contribution} & = \text{revenue} + \text{variable costs} \\
\\
\text{break-even output} & = \frac{ \text{fixed costs} }{ \text{selling price per unit} - \text{variable cost per unit} } \\
\text{or} \\
\text{break-even output} & = \frac{ \text{fixed costs} }{ \text{contribution per unit} } \\
\\
\text{margin of safety} & = \frac{ \text{current level of sales} - \text{break-even output} }{ \text{current level of sales} } \times 100
\end{aligned} 


$$

### Chapter 32: Budgets

- incrfemental budgeting
- flexible budget
- budget holder
- zero budgets



