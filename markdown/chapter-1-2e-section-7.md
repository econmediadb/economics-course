## 1.7. Exercices

### 1.7.X. Coût d'opportunité

[lien](extrait-biofuel.md)

### 1.7.X. Elasticité-prix demande 

1. Calculez les élasticités-prix demande dans chacun des cas présentés dans le Tableau ci-dessous et expliquez si la demande serait considérée comme élastique ou inélastique par rapport au prix.

| prix <br> initiale | nouveau <br> prix | quantité initiale <br> demandée | nouvelle quantité <br> demandée |
|:-------------:|:------------:|:--------------------------:|:--------------------------:|
| 100 €         | 102 €        | 2.000 unités par semaine   | 1.950 unités par semaine   |
| 55,50 €       | 54,95 €      | 5.000 unités par semaine   | 6.000 unités par semaine   |

2. A l'aide d'un exemple numérique de votre choix, expliquez la signification de ces valeurs de l'élasticité : <br>
    a. $\varepsilon = 1.5$ <br>
    b. $\varepsilon = 0.6$

**Solution**
1.  a.  -2.5%/2% = −1.25 élastique <br>
    b.  20%/−0.99% = −20.20 élastique

    $ \varepsilon = \frac{ \text{variation en \% de la quantité demandée} }{ \text{variation en \% du prix} } $
2. a. Une élasticité-prix demande de (-) 1,5 signifie que la demande est élastique. Par exemple, une augmentation de 10 % du prix peut entraîner une diminution de 15 % de la quantité demandée. <br>
   b. Une élasticité-prix demande de (-) 0,6 signifie que la demande est inélastique. Dans ce cas, une augmentation de 10 % du prix entraînerait une baisse de la demande de 6 %.

### 1.7.X. Elasticité-prix demande

[lien](exercice-elasticite-prix-demande-interpretation.md)
