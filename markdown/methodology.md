# Methodologie

![Structured writing](https://www.overleaf.com/read/pfhfzwvxwgff)


## 1. Conception du matériel de cours

La conception du matériel se fait selon les étapes suivantes :

1. Le **programme d'études** pour les classes de l'enseignement secondaire classique : 
   - Présentation des résultats et les objectifs d'apprentissage pour chaque unité 
   - Réfléchir à la manière dont il faut aider les élèves à atteindre ces objectifs.
2. Recherche des **ressources actuelles** : 
   - Examiner les ressources actuellement disponibles pour l'enseignement de l'économie dans l'enseignement secondaire. Il peut s'agir 
     - de manuels, 
     - de ressources en ligne et 
     - d'autres matériels pédagogiques. 
   - Réfléchir à ce qui est déjà disponible et aux lacunes qui peuvent être combler avec le nouveau matériel.
3. Définir le **public** : Comprendre le public cible aidera à adapter les ressources à leurs besoins et à leurs styles d'apprentissage.
   - Penser à ceux qui utiliseront le matériel de cours. 
     - S'agira-t-il d'étudiants, d'enseignants ou des deux ? 
     - S'assurer que le matériel est rédigé de manière à convenir au public cible.
4. Déterminez les **objectifs** : 
   - Que veut-on atteindre avec le matériel de cours ? 
   - Faut-il se concentrer sur un sujet ou un concept particulier de l'économie ? 
   - Veut-on donner aux apprenants une vue d'ensemble de l'économie ? 
5. Phase de **rédaction** : 
   - Diviser le matériel en unités ou chapitres plus petits afin de le rendre plus facile à assimiler pour les apprenants.
   - Revoyer et réviser : Une fois que le matériel de cours est rédigé, il faut prendre le temps de le revoir et de le réviser. 
   - S'assurer que le matériel est exact et à jour, et demander l'avis d'autres éducateurs ou d'experts en la matière.
6. Partage des ressources : 
   - Une fois que l'on est satisfait des ressources de cours, le partage en ligne par le biais de plateformes telles que GitLab est possible. 
   - Il faut veiller à inclure des instructions claires sur la manière dont les apprenants peuvent accéder à cos ressources et les utiliser.


## 2. La différenciation pédagogique

Différencier l'enseignement signifie adapter les stratégies et le matériel pédagogique pour répondre aux divers besoins des élèves. S'il y a des apprenants talentueux et des apprenants qui ont besoin de plus de soutien dans la classe, nous pouvons faire appel aux stratégies suivantes pour différencier l'enseignement (see Tomlinson, 2014; Sprenger, 2008; Heacox, 2012):
1. Utilisez des devoirs à plusieurs niveaux : Créez des devoirs à plusieurs niveaux de difficulté, afin que les élèves doués puissent se dépasser et que les élèves qui ont besoin d'un soutien plus important puissent travailler sur des tâches plus appropriées à leur niveau.
2. Proposez des regroupements flexibles : Envisagez de regrouper les élèves de différentes manières en fonction de la tâche. Par exemple, vous pouvez regrouper les apprenants doués pour des activités plus difficiles ou les associer à des apprenants qui ont besoin de plus de soutien pour des projets collaboratifs.
3. Utilisez plusieurs approches pédagogiques : Des élèves différents peuvent mieux répondre à des approches pédagogiques différentes. Par exemple, certains étudiants peuvent bénéficier d'un apprentissage visuel ou pratique, tandis que d'autres peuvent préférer des méthodes plus traditionnelles telles que les cours magistraux ou les travaux de lecture.
4. Offrez le choix : Donnez aux élèves la possibilité de choisir parmi une variété de devoirs ou d'activités, afin qu'ils puissent sélectionner les tâches qui correspondent à leurs intérêts et à leurs styles d'apprentissage.
5. Utiliser la technologie : La technologie peut être un outil puissant pour différencier l'enseignement. Envisagez d'utiliser des logiciels ou des applications éducatives qui permettent aux élèves de travailler à leur propre rythme ou qui offrent plusieurs niveaux de difficulté.
6. Fournir un soutien individualisé : Envisagez d'offrir une aide supplémentaire aux apprenants qui ont besoin de plus de soutien. Il peut s'agir de fournir un tutorat individuel, de mettre en place une ligne d'assistance téléphonique pour les devoirs ou de créer un centre de ressources où les élèves peuvent obtenir une aide supplémentaire.
7. Collaborez avec d'autres éducateurs : Envisagez de travailler avec d'autres éducateurs pour partager des idées et des stratégies de différenciation de l'enseignement. Vous pouvez également envisager de rechercher des opportunités de développement professionnel axées sur la différenciation de l'enseignement.

## 3. Un exemple de devoirs à trois différents niveaux de difficultés en économie pour l'enseignement secondaire

### 3.1. Exemple de sujet : L'offre et la demande

Dans cet exemple, les devoirs à plusieurs niveaux fournissent une gamme d'activités qui conviennent aux apprenants à différents niveaux de compréhension. <br>
Le niveau débutant est plus basique et se concentre sur l'introduction de concepts clés, tandis que le niveau intermédiaire s'appuie sur cette base en introduisant des idées et des compétences plus complexes. Le niveau avancé est plus difficile et demande aux élèves d'appliquer leur compréhension à des situations du monde réel et d'analyser l'impact des politiques gouvernementales.

- Niveau 1 : **Débutant**
  - Lire un passage sur les concepts de base de l'offre et de la demande.
  - Regardez une vidéo expliquant la relation entre l'offre et la demande.
  - Effectuer une activité d'appariement pour vérifier la compréhension des termes clés liés à l'offre et à la demande.
- Niveau 2 : **Intermédiaire**
  - Lire un passage sur la façon dont les changements de l'offre et de la demande peuvent affecter l'équilibre du marché.
  - Compléter un tableau montrant comment les changements de l'offre et de la demande peuvent affecter l'équilibre du marché.
  - Analyser un exemple concret de la façon dont les variations de l'offre et de la demande ont affecté un marché spécifique.
- Niveau 3 : **Avancé**
  - Lire un passage sur la façon dont les politiques gouvernementales peuvent influencer l'offre et la demande.
  - Analyser un exemple concret de la façon dont une politique gouvernementale a affecté l'offre et la demande d'un marché spécifique.
  - Créer une présentation expliquant comment l'offre et la demande interagissent sur un marché spécifique et comment les politiques gouvernementales peuvent influencer cette interaction.

### 3.2. Exemple de sujet : La structure du marché 

Dans cet exemple, les devoirs à plusieurs niveaux fournissent une gamme d'activités qui conviennent aux apprenants à différents niveaux de compréhension. <br>
Le niveau débutant est plus basique et se concentre sur l'introduction de concepts clés, tandis que le niveau intermédiaire s'appuie sur cette base en introduisant des idées et des compétences plus complexes. Le niveau avancé est plus difficile et demande aux étudiants d'appliquer leur compréhension à des situations réelles et d'analyser l'impact de la structure du marché sur l'efficacité économique et le bien-être des consommateurs.


- Niveau 1 : **Débutant**
  - Lire un passage sur les différents types de structures de marché (concurrence parfaite, concurrence monopolistique, oligopole et monopole).
  - Regardez une vidéo expliquant les caractéristiques de chaque structure de marché.
  - Effectuer une activité d'appariement pour tester la compréhension des termes clés liés à la structure du marché.
- Niveau 2 : **Intermédiaire**
  - Lire un passage sur la façon dont les entreprises de différentes structures de marché prennent des décisions en matière de prix et de production.
  - Compléter un tableau montrant comment les entreprises de différentes structures de marché peuvent prendre des décisions en matière de prix et de production.
  - Analyser un exemple concret d'une entreprise opérant dans une structure de marché spécifique et expliquer comment ses décisions de prix et de production sont influencées par la structure de marché.
- Niveau 3 : **Avancé**
  - Lire un passage sur l'impact de la structure du marché sur l'efficacité économique et le bien-être des consommateurs.
  - Analyser un exemple concret de l'impact de la structure du marché sur l'efficacité économique et le bien-être des consommateurs.
  - Créer une présentation expliquant comment la structure du marché peut affecter l'efficacité économique et le bien-être des consommateurs, et comment les politiques gouvernementales peuvent influencer la structure du marché.


## Sources

Benjamin, Amy. Differentiated instruction using technology: A guide for middle & HS teachers. Routledge, 2014.

Chang, Ha-Joon. Economics: the user's guide. Bloomsbury Publishing USA, 2015.

Clayton, Gary E., James E. Brown, and Marsha Hafkin Greenberg. Economics: Principles and practices. Glencoe/McGraw-Hill, 2003.

Cleaver, Tony. Economics: the basics. Routledge, 2011.

Cox, Janelle. Differentiated Instruction Strategies: Tiered Assignments. https://www.teachhub.com/teaching-strategies/2014/09/differentiated-instruction-strategies-tiered-assignments/

Heacox, Diane. Differentiating instruction in the regular classroom: How to reach and teach all learners (Updated anniversary edition). Free Spirit Publishing, 2012.

McEachern, William A. Economics: A contemporary introduction. Cengage Learning, 2016.

Mishkin, Frederic S. The economics of money, banking, and financial markets. Pearson education, 2007.

Sprenger, Marilee. Differentiation through learning styles and memory. Corwin Press, 2008.

Tomlinson, Carol Ann. The differentiated classroom: Responding to the needs of all learners. Ascd, 2014.

https://oraprdnt.uqtr.uquebec.ca/pls/public/docs/GSC1041/O0002435057_M5_Differenciation_Texte_syntese.pdf

