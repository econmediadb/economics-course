## 1.1. La prise de décision rationnelle

**Mots-clés** de la section 1.1 : *préférence, choix économique, modèle, ceteris paribus, choix entre temps libre et travail, utilité, coûts d’opportunité, contrainte budgétaire, coûts de production, raisonnement à la marge, choix de la quantité à produire, ...*

### 1.1.1. Les préférences et choix économiques

<!-- mots-clés de la section 1.1 :  -->

Le choix quotidien d'une élève consiste dans le choix du nombre d'heures que vous passez à étudier. Les facteurs qui influencent ce choix sont :
  - l'intérêt pour la matière
  - la difficulté de la matière
  - les nombres d'heures que vos amis passent à étudier
  - la conviction que plus vous passez de temps à étudier, plus vos notes seront élevées
  - ...    

*Construction d'un modèle simple de choix de nombre d’heures travaillées par un élève.*

Le modèle est fondé sur l’hypothèse que la note finale sera d’autant plus élevée que le temps consacré à étudier sera important. 

Nous supposons qu'il existe une relation positive entre le nombre d’heures travaillées et la note finale. Il faut donc vérifier si cette hypothèse est vérifiée dans les faits.

Un groupe de psychologues de l’éducation a étudié le comportement de 84 étudiants de l’université d’État de Floride afin d’identifier les facteurs ayant affecté leurs performances.

Après avoir pris en compte l’environnement et d’autres facteurs les psychologues ont estimé qu’une heure supplémentaire passée à étudier chaque semaine augmentait la moyenne générale à la fin du semestre de 0,24 point en moyenne.


Un élève peut faire varier le nombre d’heures qu’il passe à étudier. Dans ce modèle, le temps passé à étudier fait référence à la totalité du temps que l’élève consacre à son apprentissage, que ce soit en classe ou individuellement, quotidiennement (et non par semaine, comme c’était le cas pour les étudiants en Floride). La relation entre le nombre d’heures passées à étudier et la note finale est représentée dans les colonnes du Tableau 1.1.

<!--Tableau 2.2.-->

**Tableau 1.1** Comment le temps passé à étudier affecte-t-il la note de l'élève ? 
| Heures passées à étudier | 0 | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9  | 10 | 11 | 12 | 13 | 14 | 15 ou plus |
|--------------------------|---|----|----|----|----|----|----|----|----|----|----|----|----|----|----|------------|
| Note                     | 0 | 22 | 33 | 42 | 50 | 57 | 63 | 69 | 74 | 78 | 81 | 84 | 86 | 88 | 89 | 90         |

En réalité, la note finale peut aussi être affectée par des éléments imprévisibles. Dans la vie de tous les jours, nous rassemblons ces événements sous le terme de « chance ». Pour ce faire, nous utilisons l’expression « en gardant les autres choses constantes », ou, plus souvent, l’expression latine, ceteris paribus. La signification littérale de l’expression est « toutes choses égales par ailleurs ». Dans un modèle économique, elle signifie que l’analyse « garde les autres facteurs inchangés ».

La décision dépend des préférences de l'élève – c’est-à-dire de ce qui lui importe. Si l'élève en question ne se préoccupait que de ses notes, il étudierait 15 heures par jour. Mais, l'élève souhaite aussi avoir du temps libre – il aime dormir, sortir et aussi regarder la télévision.

---

**APPLICATION**

Imaginons maintenant une fermière autosuffisante, qui produit des céréales qu’elle mange et ne vend à personne. Son choix est cependant contraint : pour produire des céréales, il faut travailler. Le Tableau 1.2 indique comment le montant de céréales produit dépend du nombre d’heures travaillées chaque jour.

<!-- Tableau 2.3 -->

**Tableau 1.2** Comment le temps passé à travailler affecte-t-il la production de la fermière ?
| Heures travaillées | 0 | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9  | 10 | 11 | 12 | 13 | ... | 18 | 24 |
|--------------------------|---|----|----|----|----|----|----|----|----|----|----|----|----|----|----|------------| ---- |
| Céréales                     | 0 | 9 | 18 | 26 | 33 | 40 | 46 | 51 | 55 | 58 | 60 | 62 | 64 | 66 | ... | 69         | 71 |

La fermière choisit son nombre d’heures de travail. Par exemple, si elle travaille 12 heures par jour, elle produira 64 unités de céréales. Nous faisons l’hypothèse que si elle produit trop peu de céréales, elle mourra de faim. 

*Pour quelle raison ne peut-elle pas produire le plus de céréales possible ?*

Tout comme l'élève, la fermière accorde de la valeur au temps libre : elle doit faire un choix entre consommer des céréales et consommer du temps libre.

---

---
**EXERCICE** :  Hypothèses ceteris paribus

On vous a demandé de conduire dans votre lycée une étude similaire à celle menée à l’université d’État de Floride.

 1. Hormis l’environnement de travail, quels facteurs selon vous devraient être maintenus constants dans un modèle portant sur la relation entre les heures passées à étudier et la note finale ?
 2. Outre les conditions pour étudier (environnement de travail), quelles autres informations voudriez-vous collecter sur les lycéens ?

---

### 1.1.2. La rationalité et l'utilité individuelle

Une journée compte 24 heures. L'élève doit diviser ce temps entre ses études (le temps qu’il passe à apprendre) et son temps libre (c’est-à-dire le reste de son temps). Nous illustrons ses préférences dans le Graphique 2.2, représentant le temps libre en abscisse et la note finale en ordonnée.


<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/images/web/graphique-02-02.jpg" />
    <br>
    Graphique 2.2. Préférences de l'élève.

Le temps libre fait référence au temps qui n’est pas passé à étudier. Chaque point du graphique représente une combinaison différente de temps libre et d’une note finale.

Pour décrire les préférences, il n’y a pas besoin de connaître l’utilité exacte de chaque option ; nous avons seulement besoin de connaître quelles combinaisons procurent une utilité supérieure ou inférieure à celle apportée par les autres. Dans notre modèle des préférences d’un lycéen, les biens sont « la note finale » et « le temps libre ». Dans d’autres modèles, il s’agira souvent de biens de consommation, comme la nourriture ou les vêtements, et nous appelons l’individu un consommateur.

Pour comprendre le comportement économique, nous avons besoin de connaître les préférences des gens. En économie, nous nous figurons les personnes comme prenant des décisions selon leurs préférences, ce par quoi nous désignons les goûts, les dégoûts, les attitudes, les sentiments et les croyances qui les animent et les motivent.


---

**APPLICATION**

Supposons que :

- L'élève préfère plus de temps libre à moins de temps libre : les combinaisons A et B donnent toutes les deux une note de 84, mais elle préférera A car la combinaison donne plus de temps libre.
- L'élève préfère une note élevée à une note faible : avec les combinaisons C et D, elle a 20 heures de temps libre par jour, mais elle préfère D car cela lui donne une note plus élevée.

Comparons toutefois les points A et D dans **Graphique 2.2**. 

*L'élève préférerait-elle D (note faible et beaucoup de temps libre) ou A (note élevée et moins de temps libre) ?*
Une manière de le découvrir serait de lui demander.    

Supposons qu’elle se déclare indifférente entre A et D, ce qui implique qu’elle tirerait la même satisfaction des deux résultats. Nous disons alors que ces deux résultats lui *procurent la même utilité*. Et nous savons qu’il préfère A à B, donc que B lui procure une utilité moindre que A ou D.

<!-- Tableau 2.4 -->
Tableau 1.3 Représentation des préférences de l'élève. 
|                       | A  | E  | F  | G  | H  | D  |
|-----------------------|----|----|----|----|----|----|
| Heures de temps libre | 15 | 16 | 17 | 18 | 19 | 20 |
| Note finale           | 84 | 75 | 67 | 60 | 54 | 50 |

---

---
**VIDEO** : Economie expérimentale

[Dans notre vidéo Économiste en action «  Invisible hands working together », Juan Camilo Cárdenas parle de son usage innovant de l’économie expérimentale dans des situations de la vie réelle.](https://tinyurl.com/se37rca)

---



---
**(option) EXERCICE** : Quelles sont vos préférences ?

[lien](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/text/02.html#24-la-rationalit%C3%A9-et-lutilit%C3%A9-individuelle)

---

<br>

---

**(option) EXERCICE** : Préférences

[lien](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/text/02.html#24-la-rationalit%C3%A9-et-lutilit%C3%A9-individuelle)

---

### 1.1.3. Les coûts d’opportunité

Revenons maintenant au problème de l'élève, qui doit arbitrer entre ses notes et son temps libre. Cette fois-ci nous montrerons comment la note finale dépend de la quantité de temps libre plutôt que du temps passé à étudier. Le Tableau 1.4 montre la relation entre sa note finale et le nombre d’heures de temps libre par jour — soit l’image inversée du Tableau 1.1.

<!-- Tableau 2.5 -->
Tableau 1.4 Comment le temps passé à étudier affecte-t-il la note de l'élève ? 
| Heures de temps libre par jour | 24 | 23 | 22 | 21 | 20 | 19 | 18 | 17 | 16 | 15 | 14 | 13 | 12 | 11 | 10 | 9 ou moins |
|--------------------------------|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|------------|
| Note                           | 0  | 20 | 33 | 42 | 50 | 57 | 63 | 69 | 74 | 78 | 81 | 84 | 86 | 88 | 89 | 90         |

Si l'élève travaille pendant 24 heures, cela signifie qu’il n’a pas de temps libre et que sa note finale est de 90. S’il choisit d’avoir 24 heures de temps libre par jour, il aura une note de zéro. Une autre façon d’exprimer cela est de dire que le temps libre a un **coût d’opportunité** : pour avoir plus de temps libre, l'élève doit renoncer à l’opportunité d’avoir une note plus élevée.

Le temps libre a un coût d’opportunité, sous la forme d’une perte de points de sa note. Cela représente l’arbitrage qu’il doit faire entre la note et le temps libre.

````
coût d’opportunité du capital : La quantité de revenu qu’un investisseur aurait pu obtenir en investissant l’unité de capital ailleurs
````


### 1.1.4. La contrainte budgétaire

Comme pour la fermière, nous allons réfléchir en termes de consommation et de temps libre, par jour et en moyenne, pour introduire la notion de contrainte budgétaire. Nous supposons que les dépense de la fermière – c’est-à-dire sa consommation moyenne de nourriture, de logement et d’autres biens et services – ne peuvent pas excéder ses revenus (donc, par exemple, elle ne peut pas emprunter pour augmenter sa consommation) :

    Consommation = salaire x (24−temps libre)

Nous appellerons cette équation la *contrainte budgétaire*, parce qu’elle permet de calculer ce que la fermière peut se permettre d’acheter.

<!-- Tableau 2.6a -->
La fermière s'attend à gagner en moyenne un salaire de 15 euros par heure. Dans les colonnes du Tableau 1.5a nous avons calculé votre temps libre pour des heures de travail variant entre 0 et 16 heures par jour, et votre consommation maximale, quand votre salaire horaire est de 15 euros. L’équation de la contrainte budgétaire est :

    Consommation = 15 x (24−temps libre)

<!-- Tableau 2.6a -->
Tableau 1.5a Comment le temps passé à travailler affecte-t-il la consommation quotidienne ? 
| Heures de travail | Temps libre | Consommation |
|:-----------------:|------------:|-------------:|
| 0                 | 24          | 0 €          |
| 2                 | 22          | 30 €         |
| 4                 | 20          | 60 €         |
| 6                 | 18          | 90 €         |
| 8                 | 16          | 120 €        |
| 10                | 14          | 150 €        |
| 12                | 12          | 180 €        |
| 14                | 10          | 210 €        |
| 16                | 8           | 240 €        |



Les emplois diffèrent par le nombre d’heures qu'on passe à travailler — *quel serait donc le nombre d’heures travaillées idéal ?*

Tout comme l'exemple de l'élève, la fermière cherche un équilibre entre deux arbitrages :

- Le montant de consommation qu'elle est désireuse d’échanger contre une heure de temps libre.
- Le montant de consommation qu'elle peut obtenir en renonçant à une heure de temps libre, qui est égal au salaire.

Prenons maintenant la situation suivante. La fermière obtient une aide correspondant à un revenu quotidien de 50 euros. La fermière réalise immédiatement que cela modifiera son choix d’emploi.

<!-- Tableau 2.6b -->
La nouvelle situation est décrite dans le Tableau 1.5b : pour chaque niveau de temps libre, le revenu total est supérieur de 50 euros au niveau précédent. La contrainte budgétaire de la fermière est maintenant :

    Consommation = 15x(24−temps libre) + 50

Tableau 1.5b Comment le temps passé à travailler affecte-t-il la consommation quotidienne ? 
| Heures de travail | Temps libre | Consommation |
|:-----------------:|------------:|-------------:|
| 0                 | 24          | 50 €         |
| 2                 | 22          | 80 €         |
| 4                 | 20          | 110 €        |
| 6                 | 18          | 140 €        |
| 8                 | 16          | 170 €        |
| 10                | 14          | 200 €        |
| 12                | 12          | 230 €        |
| 14                | 10          | 260 €        |
| 16                | 8           | 290 €        |

Remarquez que le revenu additionnel de 50 euros ne change pas le coût d’opportunité du temps : chaque heure de temps libre supplémentaire réduit toujours la consommation de 15 euros (le salaire).

Une année plus tard, la chance tourne pour la fermière : les céréales qu'elle produit sont achetées à un prix supérieur sur le marché. Ceci augmente son salaire de 10 euros par heure, et ceci laisse la possibilité de repenser les heures qu'elle travaille. La contrainte budgétaire de la fermière est désormais égale à :

    Consommation = 25x(24−temps libre)

<!-- Tableau 2.6c -->
Dans le Tableau 1.5c, vous pouvez voir comment la contrainte budgétaire évolue lorsque le salaire augmente.

Tableau 1.5c Comment le temps passé à travailler affecte-t-il la consommation quotidienne ? 
| Heures de travail | Temps libre | Consommation |
|:-----------------:|------------:|-------------:|
| 0                 | 24          | 0 €          |
| 2                 | 22          | 50 €         |
| 4                 | 20          | 100 €        |
| 6                 | 18          | 150 €        |
| 8                 | 16          | 200 €        |
| 10                | 14          | 250 €        |
| 12                | 12          | 300 €        |
| 14                | 10          | 350 €        |
| 16                | 8           | 400 €        |

--- 
**APPLICATION**

*Comparez les résultats dans le Tableau 1.5a et le Tableau 1.5c.*

Avec 24 heures de temps libre (et pas de travail), la  consommation serait de 0, quel que soit le salaire. Mais pour chaque heure de temps libre à laquelle elle renonce, la consommation augmente désormais de 25 euros, au lieu de 15 euros précédemment.

---


### 1.1.5. La maximisation et le raisonnement à la marge

Dans cette section, nous allons étudier la manière dont les entreprises qui fabriquent des produits différenciés fixent un prix et choisissent la quantité à produire afin de maximiser leurs profits. Cela dépend de la demande qui lui est adressée, c’est-à-dire de la disposition des clients potentiels à payer pour le produit, et des coûts de production.

Nous allons prendre l’exemple d’une petite entreprise qui fabrique des voitures et que nous appellerons *Beautiful Cars*.

#### 1.1.5.1. Les coûts de production

Si l'on prend les coûts de fabrication et de vente des voitures, nous pouvons citer :
- locaux (une usine) équipés de machines pour le moulage, 
- le forgeage, le montage et le soudage des carrosseries
- achat des matières premières et des composants 
- rémunération des ouvriers qui font fonctionner les équipements
- rémunération des salariés pour gérer les processus d’achat, de production et pour vendre les voitures

L’une des composantes du coût de production des voitures est le montant à verser aux propriétaires de l’entreprise (les actionnaires) afin de couvrir le coût d’opportunité du capital.

En général, les actionnaires n’investiront pas s’ils peuvent faire un meilleur usage de leur argent en investissant et en réalisant des profits ailleurs. Ce qu’ils pourraient recevoir ailleurs, pour chaque euro investi, est un autre exemple de coût d’opportunité, appelé ici coût d’opportunité du capital.

**Les coûts fixes**

Certains coûts ne varient pas avec le nombre de voitures. On les appelle *coûts fixes*. Suivez les étapes de l’analyse du Graphique 2.3 pour voir que plus il y a de voitures produites, plus les coûts totaux (les coûts fixes et les coûts variables, comme le nombre d’heures travaillées à rémunérer aux employés) sont élevés.

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/images/web/graphique-02-03-b.jpg" />
    <br>
    Graphique 2.3 Coût total de production.

**Les coûts totaux (croissants)**

Les coûts totaux augmentent avec la quantité d’unités produites, en partie parce que l’entreprise a besoin d’employer plus d’ouvriers. 

Suivez les étapes de l’analyse du Graphique 2.4 pour voir que, à partir du coût total de production, nous avons déterminé le coût moyen d’une voiture, et la manière dont il varie avec la production. La partie supérieure du Graphique 2.4 montre la manière dont les coûts totaux dépendent de la quantité de voitures produites chaque jour. La variation du coût moyen est présentée sur la partie inférieure.

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/images/web/graphique-02-04-d.jpg" />
    <br>
    Graphique 2.4 Beautiful Cars : variation du coût total et moyen.

**Les coûts moyens**

Nous pouvons calculer le coût moyen pour chaque nombre de voitures pour représenter la variation du coût moyen d’une voiture produite du graphique du bas.

Nous pouvons voir sur le Graphique 2.4 que les coûts moyens de Beautiful Cars sont décroissants pour de faibles niveaux de production (jusqu’à 40 voitures produites). Pour des niveaux de production élevés (plus de 40 voitures produites), les coûts moyens augmentent. Cela peut être dû au fait que l’entreprise doit augmenter le nombre de roulements quotidiens des équipes sur la chaîne de montage. Elle doit peut-être aussi payer des heures supplémentaires et l’équipement peut tomber en panne plus fréquemment lorsque la chaîne de montage fonctionne plus longtemps.

**Le coût marginal**

Le coût marginal est le coût de production additionnel dû à une unité supplémentaire. Suivez les étapes de l’analyse du Graphique 2.5 pour voir que la production des Beautiful Cars présente des coûts marginaux croissants.

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/images/web/graphique-02-05-c.jpg" alt="Coût total de production"/>
    <br>
    Graphique 2.5 Coût marginal d’une voiture.


Si nous calculons le coût marginal à chaque niveau de production de Beautiful Cars, nous pouvons représenter la variation du coût marginal.

#### 1.1.5.2. La courbe de demande

Afin de fixer un prix, une entreprise a besoin d’informations concernant la demande, notamment le nombre de clients potentiels prêts à payer pour son produit. Pour un modèle simple de la demande de Beautiful Cars, imaginez qu’il y ait 100 clients potentiels qui achèteraient chacun une Beautiful Cars aujourd’hui, si le prix était assez bas. Supposez que nous ordonnons sur une droite l’ensemble des clients potentiels par ordre décroissant de leur disposition à payer, et que nous représentons graphiquement comment la disposition à payer varie le long de cette droite (Graphique 2.6). Alors, pour tout niveau de prix, disons égal à 3.200 euros, le graphique montre le nombre de clients dont la disposition à payer serait supérieure ou égale au prix. Dans ce cas, 60 clients sont prêts à payer 3.200 euros ou plus, donc la demande de voitures pour un prix de 3 200 euros est 60.

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/images/web/graphique-02-06.jpg" />
    <br>
    Graphique 2.6 Demande de voitures (par jour).

Les courbes de demande sont souvent dessinées sous la forme de lignes droites, comme dans cet exemple, bien qu’il n’y ait aucune raison de penser qu’elles soient droites en réalité : nous avons bien vu que la courbe de demande pour les Cheerios pomme-cannelle n’était pas droite. En revanche, nous pouvons nous attendre à ce que les courbes de demande soient descendantes : lorsque le prix augmente, la quantité demandée par les consommateurs diminue. Réciproquement, quand la quantité disponible est faible, le produit peut être vendu à un prix élevé.

Si le prix d’une voiture Beautiful Cars est élevé, la demande sera faible, car seuls les consommateurs qui préfèrent Beautiful Cars aux autres marques l’achèteront. Si le prix baisse, davantage de consommateurs seront attirés par Beautiful Cars, ceux-là mêmes qui auraient sûrement acheté une Ford ou une Volvo sinon.


### 1.1.6. Choix de la quantité à produire ? 

<!-- L’égalisation entre coût marginal et recette marginale -->

Quel est le meilleur choix de prix et de quantité pour le producteur ? Rappelez-vous que les recettes sont données par le prix de vente multiplié par la quantité vendue (Graphique 2.6). La recette marginale correspond à l’augmentation des recettes induite par l’augmentation de la quantité produite. Le Tableau 1.6 montre comment calculer la recette marginale avec 20 voitures produites, c’est-à-dire l’augmentation des recettes lorsque la quantité passe de 20 à 21 voitures produites.

Tableau 1.6 Calcul de la recette marginale. 
| Quantité                     | Prix                     | Recette                                                                           |
|------------------------------|--------------------------|-----------------------------------------------------------------------------------|
| Quantité = 20                | Prix = 6.400 €           | Recette = 128.000 €                                                               |
| Quantité = 21                | Prix = 6.320 €           | Recette = 132.720 €                                                               |
| Variation de la quantité = 1 | Variation du prix = 80 € | Recette marginale = 4.720 € |

<!-- = variation de la recette / variation de la quantité -->

*Gain de recettes* (21e voiture) : 	6 320 € <br>
*Perte de recettes* (80 € sur chacune des 20 autres voitures) :	-1 600 € <br>
*Recette marginale* 	4 720 €

Quand la production passe de 20 à 21 voitures, les recettes varient pour deux raisons. Une voiture supplémentaire est vendue, mais, étant donné que le nouveau prix de vente est plus faible avec 21 voitures produites, il y a aussi une perte de 80 euros sur chacune des 20 autres voitures vendues au nouveau prix. Les recettes marginales correspondent à l’effet net de ces deux changements.

Sur le Tableau 1.7, nous trouvons la variation des recettes marginales, et l’utilisons pour trouver le point correspondant à un profit maximal. Les deux premières colonnes décrivent la courbe de demande et la troisième colonne décrit la variation du coût marginal. Vous pouvez voir la manière dont le profit varie en fonction de la production dans la dernière colonne du Tableau 1.7.

Tableau 1.7 Recette marginale, coût marginal et profit. 
| Prix  | Quantité | Coût <br> marginal | Recette <br> marginale | Profit  |
|-------|---------:|:-------------:|------------------:|--------:|
| 8.000 | 0        | 1.000         | 8.000             | -48.000 |
| 7.200 | 10       | 1.600         | 6.413             | 11.063  |
| 6.400 | 20       | 2.200         | 4.825             | 48.250  |
| 5.600 | 30       | 2.800         | 3.238             | 63.563  |
| 4.800 | 40       | 3.400         | 1.650             | 57.000  |
| 4.000 | 50       | 4.000         | 63                | 28.563  |
| 3.200 | 60       | 4.600         | -1.525            | -21.750 |

Les trois dernières colonnes dans le Tableau 2.8 suggèrent que le point maximisant les profits se trouve là où la recette marginale égalise le coût marginal. Pour comprendre pourquoi, souvenez-vous que le profit est la différence entre les recettes et les coûts, de sorte que, pour n’importe quel niveau de production, la variation des profits lorsque la production augmente d’une unité (le profit marginal) est égale à la différence entre la variation des recettes et la variation des coûts :

    profit = recette totale − coût total

    profit marginal = recette marginale − coût marginal

Ainsi :

- Si la recette marginale est supérieure au coût marginal, l’entreprise pourrait augmenter son profit en augmentant la production.
- Si la recette marginale est inférieure au coût marginal, le profit marginal est négatif. Il serait préférable de diminuer la production.

---
**APPLICATION**

i) *Construire un graphique avec quantité produite à l'abscisse et le montant (coût marginal et recette marginale) à l'ordonnée.*

INSERT TEMPLATE FOR GRAPH HERE

ii) *Quel est la quantité optimale à produire dans l'exemple du Tableau 1.7?*


 - Quand la production est inférieure à 32, la recette marginale est supérieure au coût marginal : le profit marginal est positif, donc le profit augmente avec la production.
 - Quand la production est supérieure à 32, la recette marginale est inférieure au coût marginal : le profit marginal est négatif, donc le profit diminue avec la production.
 - Quand la production est égale à 32, la recette marginale est égale au coût marginal : le niveau de profit atteint son maximum.

iii) Quel est le prix et la quantité qui maximise le profit?

Le prix et la quantité maximisant les profits sont 5 440 euros et 32 voitures, correspondant à un profit de 63.360 euros. 

iv) Calculez le profit de l'entreprise.

Rappelez-vous que le profit de l’entreprise est la différence entre ses recettes et ses coûts totaux :

    profit = recette totale − coût total 
           = prix x quantité − coût total

De manière équivalente, le profit est le nombre d’unités produites multiplié par le profit par unité, qui est la différence entre le prix et le coût moyen.

    profit = quantité(prix−coût total quantité)
           = quantité(prix−coût moyen)

Cette formule correspond au profit économique. Le profit économique est le profit supplémentaire par rapport au rendement minimal exigé par les actionnaires, qui est appelé le profit normal. Rappelez-vous que les coûts de production prennent en compte le coût d’opportunité du capital (les paiements faits aux actionnaires pour les inciter à prendre des parts). Si le prix est égal au coût moyen, le profit économique de l’entreprise est zéro.

---


Le succès d’une entreprise ne dépend pas seulement de la détermination du bon prix. Le choix du produit et sa capacité à attirer les clients, à produire à moindres coûts en proposant une qualité supérieure à celle de la concurrence importent tout autant. Elle doit aussi être en mesure de recruter et de garder les salariés qui rendent possibles toutes ces choses.
