# Notions et concepts utilisés dans le cours de 3e

## Introduction

| **Chapitre : Introduction**                                     | Indispensable | Agréable à avoir | Peut être évité |
|-----------------------------------------------------------------|---------------|------------------|-----------------|
| acteur économique                                               |               |                  |                 |
| besoins économiques primaires et secondaires                    |               |                  |                 |
| utilité                                                         |               |                  |                 |
| 3 problèmes économiques fondamentaux                            |               |                  |                 |
| système économique: *économie de marché* et *économie planifié* |               |                  |                 |
| économie normative et économie positive                         |               |                  |                 |


## Chapitre 1: Comment crée-t-on des richesses et comment les mesure-t-on?

| **Chapitre 1**                                                            | Indispensable | Agréable à avoir | Peut être évité |
|---------------------------------------------------------------------------|---------------|------------------|-----------------|
| revenu national                                                           |               |                  |                 |
| production au sens économique                                             |               |                  |                 |
| biens de consommation                                                     |               |                  |                 |
| biens intermédiaires                                                      |               |                  |                 |
| services                                                                  |               |                  |                 |
| production *marchande* et *non-marchande*                                 |               |                  |                 |
| facteurs de production: *travail*, *capital*  et *ressources naturelles*  |               |                  |                 |
| facteurs de production *complémentaires* et *substituables*               |               |                  |                 |
| taux de croissance                                                        |               |                  |                 |
| PIB réel annuel                                                           |               |                  |                 |
| PIB par habitant                                                          |               |                  |                 |
| évolution du PIB par habitant à *moyen terme* et à *long terme*           |               |                  |                 |
| indicateurs complémentaires                                               |               |                  |                 |

## Chapitre 2: La répartition de la richesse

| **Chapitre 2**                                                              | Indispensable | Agréable à avoir | Peut être évité |
|-----------------------------------------------------------------------------|---------------|------------------|-----------------|
| partage de la richesse                                                      |               |                  |                 |
| valeur ajoutée brute                                                        |               |                  |                 |
| revenu primaire: *revenus du travail* et *revenus du capital*               |               |                  |                 |
| inégalités de revenu: part de la richesse des 1% les plus riches            |               |                  |                 |
| répartition initiale des revenus                                            |               |                  |                 |
| processus de redistribution                                                 |               |                  |                 |
| raison de l'existence des inégalités                                        |               |                  |                 |
| conséquences des inégalités                                                 |               |                  |                 |
| mesure des inégalités                                                       |               |                  |                 |
| lutte contre les inégalités                                                 |               |                  |                 |
| indice de Gini                                                              |               |                  |                 |
| part du revenu national avant impôt détenue par une partie de la population |               |                  |                 |
| évolution de l'inégalité                                                    |               |                  |                 |

## Chapitre 3: Comment se forment les prix sur un marché?

| **Chapitre 3**                                      | Indispensable | Agréable à avoir | Peut être évité |
|-----------------------------------------------------|---------------|------------------|-----------------|
| circuit économique                                  |               |                  |                 |
| rôle des institutions                               |               |                  |                 |
| 4 grandes catégories des marchés                    |               |                  |                 |
| marché des biens et services                        |               |                  |                 |
| marché du travail                                   |               |                  |                 |
| marché financier                                    |               |                  |                 |
| marché des changes                                  |               |                  |                 |
| structure de marché                                 |               |                  |                 |
| concurrence parfaite                                |               |                  |                 |
| monopole                                            |               |                  |                 |
| oligopole                                           |               |                  |                 |
| degré de concurrence                                |               |                  |                 |
| nombre de producteurs sur le marché                 |               |                  |                 |
| différentiation ou non-différentiation des produits |               |                  |                 |
| concurrence parfaite                                |               |                  |                 |
| preneur de prix                                     |               |                  |                 |
| atomicité                                           |               |                  |                 |
| homogéneité                                         |               |                  |                 |
| transparence de l'information                       |               |                  |                 |
| pouvoir de négociation                              |               |                  |                 |
| fluidité du marché                                  |               |                  |                 |
| concurrence imparfaite                              |               |                  |                 |
| oligopole                                           |               |                  |                 |
| monopole                                            |               |                  |                 |
| fonction de demande                                 |               |                  |                 |
| disposition à payer                                 |               |                  |                 |
| fonction d'offre                                    |               |                  |                 |
| disposition à vendre                                |               |                  |                 |
| déplacement de la demande                           |               |                  |                 |
| déplacement de l'offre                              |               |                  |                 |

