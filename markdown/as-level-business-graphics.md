# AS-level Economics

Five Parts of the AS-level Business course:
1. Business and its environment
2. Human resource management
3. Marketing
4. Operations management
5. Finance and accounting

## Part 1: Business and its environment

1. Enterprise
2. Business structure
3. Size of business
4. Business objectives
5. Stakeholders in a business

<!-- The Dynamic Business Environment   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/bcfd360c919d155bcd2a08d229f338b32a3e93ce" alt="The Dynamic Business Environment" style="width:50%">
  <figcaption>Fig.X - The Dynamic Business Environment (Attribution: Copyright Rice University, OpenStax, under CC-BY 4.0 license)</figcaption>
</figure>

<!-- Economics as a Circular Flow -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/cbda59a08064cfd5f55a5910fe19eba2e6f5a215" alt="Economics as a Circular Flow" style="width:50%">
  <figcaption>Fig.X -  Economics as a Circular Flow (Attribution: Copyright Rice University, OpenStax, under CC-BY 4.0 license)</figcaption>
</figure>

<!-- Demand Curve for Jackets for Snowboarders -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/ea25b143fc0b609beffc293f7d57456021215af3" alt="Demand Curve for Jackets for Snowboarders" style="width:50%">
  <figcaption>Fig.X - Demand Curve for Jackets for Snowboarders (Attribution: Copyright Rice University, OpenStax, under CC-BY 4.0 license)</figcaption>
</figure>

<!-- Supply Curve for Jackets for Snowboarders -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/884083f356cab0fec756af6eec661d522d8a09d8" alt="Supply Curve for Jackets for Snowboarders" style="width:50%">
  <figcaption>Fig.X - Supply Curve for Jackets for Snowboarders (Attribution: Copyright Rice University, OpenStax, under CC-BY 4.0 license)</figcaption>
</figure>

<!--  Equilibrium Price and Quantity for Jackets for Snowboarders -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/79ddde4b92d19c306c1322fb7491d1878913bffc" alt=" Equilibrium Price and Quantity for Jackets for Snowboarders" style="width:50%">
  <figcaption>Fig.X - Equilibrium Price and Quantity for Jackets for Snowboarders (Attribution: Copyright Rice University, OpenStax, under CC-BY 4.0 license)</figcaption>
</figure>

<!--  Shifts in Demand for Jackets for Snowboarders -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/69d69ad3487322dc07bf6e0c64a706d8fb3cd4de" alt="Shifts in Demand for Jackets for Snowboarders" style="width:50%">
  <figcaption>Fig.X - Shifts in Demand for Jackets for Snowboarders (Attribution: Copyright Rice University, OpenStax, under CC-BY 4.0 license)</figcaption>
</figure>

<!--  The Pyramid of Corporate Social Responsibility -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/ba7e4305d8cf7b0faf95bdade3f96485f67a7d59" alt="The Pyramid of Corporate Social Responsibility" style="width:50%">
  <figcaption>Fig.X - The Pyramid of Corporate Social Responsibility (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!--  Hybrid and electric cars -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/b4896d14f10f727dbd6b6d9385a91a06dd4cd12c" alt="Hybrid and electric cars" style="width:50%">
  <figcaption>Fig.X - Hybrid cars and all-electric vehicles such as Tesla models are turning heads and changing the way the world drives. Electric vehicles are more eco-friendly, but they are also more expensive to own. Analysts project that after charging, insurance, and maintenance costs, electric cars cost thousands of dollars more than conventional vehicles. Do the environmental benefits associated with electric cars justify the higher cost of ownership? (Credit: Steve Jurvetson/ Flickr/ Attribution 2.0 Generic (CC BY 2.0))</figcaption>
</figure>

<!-- Organizational Structure of Corporations -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/062c3b0b1b36a5e6f7f6966da7df466740e3ba2f" alt="Organizational Structure of Corporations" style="width:50%">
  <figcaption>Fig.X - Organizational Structure of Corporations Attribution: Copyright Rice University, OpenStax, under CC BY-NC-SA 4.0 license</figcaption>
</figure>

<!-- Franchise -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/2a55a4e60b44e8f1f6250b671c932a2ccccc99a3" alt="Franchise" style="width:50%">
  <figcaption>Fig.X - Countless franchise opportunities exist for entrepreneurs with access to start-up capital. Despite the broad range of franchise opportunities available, lists of the fastest-growing franchises are heavily weighted with restaurant chains and cleaning services. Start-up costs for a Quiznos franchise can be pricey; expenses associated with opening a Club Pilates franchise or a Visiting Angels adult care service are significantly lower. How do entrepreneurs evaluate which franchising opportunity is right for them? (Credit: Mr. Blue Mau Mau/ Flickr/ Attribution 2.0 Generic (CC BY 2.0))</figcaption>
</figure>

<!-- Entrepreneur: Elon Musk -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/1c14b9a570a6b8dee0bd213e5c4e7ec0fb40a27e" alt="Entrepreneur: Elon Musk" style="width:50%">
  <figcaption>Fig.X -  If there is one person responsible for the mainstream success of solar energy and electric vehicles in the past 10 years, it’s Elon Musk, founder and CEO of Tesla. Since the 2000s when he founded Tesla, launching innovation in solar technology, and commercial space exploration with SpaceX, Musk has pioneered countless innovations and has challenged traditional automobile, trucking, and energy companies to challenge and rethink their businesses. What entrepreneurial type best describes Elon Musk? (Credit: Steve Jurvetson/ Flickr/ Attribution 2.0 Generic (CC BY 2.0))</figcaption>
</figure>

<!-- Entrepreneur: Ashton Kutcher -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/a9a256b7f7660107b1661c13fc263da587c3929a" alt="Entrepreneur: Ashton Kutcher" style="width:50%">
  <figcaption>Fig.X - Celebrity Ashton Kutcher is more than just a pretty face. The actor-mogul is an active investor in technology-based start-ups such as Airbnb, Skype, and Foursquare with an empire estimated at $200 million dollars. What personality traits are common to successful young entrepreneurs such as Kutcher? (Credit: TechCrunch/ Flickr/ Attribution 2.0 Generic (CC BY 2.0))</figcaption>
</figure>






### Chapter 1: Enterprise

- business objectives
- transformation process
- factors of production
- primary sector
- adding value
- brand
- market forces
- opportunity cost
- entrepreneur
- enterprise
- intrapreneur

### Chapter 2: Business structure

- nationalisation
- privatisation
- merit goods
- unlimited liability
- company
- shareholders
- limited liability
- franchise
- co-operatives
- joint ventures
- social enterprises

### Chapter 3: Size of business

- niche
- type of integration (horizontal, vertical, conglomerate diversification)
- internal and external growth


### Chapter 4: Business objectives

- objective
- labour productivity
- corporate objective
- market share
- cash flow
- ethics
- social responsibility
- mission statement
- aim
- strategy
- tactics
- target
- budgets
- ethical behaviour

### Chapter 5: Stakeholders in a business

- stakeholders
- authority
- internal stakeholders
- external stakeholders
- dividends

## Part 2: Human resource management

1. Human resource management
2. Motivation
3. Management

<!-- Management: Apple -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/b6d33dee9fe70b0deaf0a05db1576d8b7fbc75c0" alt="Management: Apple" style="width:50%">
  <figcaption>Fig.X - To encourage greater collaboration between employees, Apple is investing $5 billion in the construction of its new Cupertino, CA, headquarters, which is replacing several buildings the company had outgrown. Most headquarters-based employees of Apple now share not only the same office space, but also the same technology tools and corporate culture. How do Apple’s planning and organizing decisions increase organizational efficiency and effectiveness? (Credit: Tom Pavel / flickr/ Attribution 2.0 Generic (CC BY 2.0))</figcaption>
</figure>

<!--  The Managerial Pyramid  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/ccbceb8d85228d84e3b1b667bfcf9a340ec2dbe6" alt=" The Managerial Pyramid " style="width:50%">
  <figcaption>Fig.X - The Managerial Pyramid (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!--  The Control Process  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/26302d4a9f24fcb13c5461a933462d9cd4ca563d" alt="The Control Process" style="width:50%">
  <figcaption>Fig.X - The Control Process (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- The Decision-Making Process -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/7b805cd40297dbfa0404f15a790096da3bb8101d" alt="The Decision-Making Process" style="width:50%">
  <figcaption>Fig.X - The Decision-Making Process (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- The Importance of Managerial Skills at Different Management Levels -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/89bb789a74d96ff2e44090bec8f17aa3d74821aa" alt="The Importance of Managerial Skills at Different Management Levels" style="width:50%">
  <figcaption>Fig.X - The Importance of Managerial Skills at Different Management Levels (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- Software: "Dashboards" -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/89bb789a74d96ff2e44090bec8f17aa3d74821aa" alt="Software: 'Dashboards'" style="width:50%">
  <figcaption>Fig.X - Marketing and sales professionals are increasingly turning to advanced software programs called “dashboards” to monitor business and evaluate performance. These computer tools use analytics and big data to help managers identify valuable customers, track sales, and align plans with company objectives—all in real time. A typical dashboard might include sales and bookings forecasts, monthly close data, customer satisfaction data, and employee training schedules. This example tracks customers attending the Consumer Electronics Show so that the buzz created by influencers can be measured. How does information technology affect managerial decision-making? (Credit: Intel Free Press/ flickr/ Attribution 2.0 Generic (CC BY 2.0))</figcaption>
</figure>

<!-- Organisational structure: Ikea -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/7e3a8f02ec541e523cf8aec16633979ca0bc2c82" alt="Organisational structure: Ikea" style="width:50%">
  <figcaption>Fig.X - Founded in 1943, Sweden retailer IKEA has grown from a small mail-order operation to a global force in home furnishings with more than 390 stores throughout Europe, North America, Africa, Australia, and Asia. Best known for its contemporary furniture designs, highly trafficked store openings, and quirky advertising, the IKEA Group consists of multiple divisions corresponding to the company’s retail, supply chain, sales, and design and manufacturing functions. What factors likely influenced the development of IKEA’s organizational structure as the company expanded over the years? (Credit: JJBers/ flickr/ Attribution 2.0 Generic (CC BY 2.0))</figcaption>
</figure>

<!--  Organization Chart for a Typical Appliance Manufacturer -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/3b09984a869e66e7367fdf67fd2e305ef27ad640" alt=" Organization Chart for a Typical Appliance Manufacturer" style="width:50%">
  <figcaption>Fig.X -  Organization Chart for a Typical Appliance Manufacturer Attribution: Copyright Rice University, OpenStax, under CC BY-NC-SA 4.0 license</figcaption>
</figure>

<!-- Five Traditional Ways to Organize -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/70c4e62f1246c06bb51f2a222daea577f3097c62" alt="Five Traditional Ways to Organize" style="width:50%">
  <figcaption>Fig.X - Five Traditional Ways to Organize (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- Line-and-Staff Organization -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/8ab08b831dee7fb98800235539784ad561c8f2ed" alt="Line-and-Staff Organization" style="width:50%">
  <figcaption>Fig.X - Line-and-Staff Organization (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- Matrix Organization -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/43d6412a978a42ebaf37eeb9d5397ed63b6ee0d9" alt="Matrix Organization" style="width:50%">
  <figcaption>Fig.X -  Matrix Organization (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- Flat versus Tall Organizational Structures -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/0f6c1d1ed2ed8944f95e4b8af335b73191437f71" alt="Flat versus Tall Organizational Structures" style="width:50%">
  <figcaption>Fig.X -  Flat versus Tall Organizational Structures (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- Informal, personal connexions -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/431d809724af24bcc1542dc80b2b3d573483996a" alt="Informal, personal connexions" style="width:50%">
  <figcaption>Fig.X -  Smart managers understand that not all of a company’s influential relationships appear as part of the organization chart. A web of informal, personal connections exists between workers, and vital information and knowledge pass through this web constantly. Using social media analysis software and other tracking tools, managers can map and quantify the normally invisible relationships that form between employees at all levels of an organization. How might identifying a company’s informal organization help managers foster teamwork, motivate employees, and boost productivity? (Credit: University of Exeter /flickr / Attribution 2.0 Generic (CC BY 2.0))</figcaption>
</figure>

<!-- Global teams -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/319aede2e7a07b256cd1ab38e25e91b9bac1a1a0" alt="Global teams" style="width:50%">
  <figcaption>Fig.X -  In today’s high-tech world, teams can exist any place where there is access to the internet. With globalization and outsourcing being common strategies in business operations today, companies of all shapes and sizes utilize virtual teams to coordinate people and projects halfway around the world. Unlike coworkers in traditional teams, virtual team members rarely meet in person, working from different locations and continents. What practical benefits do virtual teams offer to businesses, employees, and other members? (Credit: ThoroughlyReviewed/ Flickr/ Attribution 2.0 Generic (CC BY 2.0))</figcaption>
</figure>

<!-- Human Resource Management Process -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/21e5ccf449d90ac9f6c116461557e560f48d4f6a" alt="Human Resource Management Process" style="width:50%">
  <figcaption>Fig.X -  Human Resource Management Process (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- Human Resource Planning Process -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/1c905fee941b885fb41ca0c8061c40b9e5665dcf" alt="Human Resource Planning Process" style="width:50%">
  <figcaption>Fig.X - Human Resource Planning Process (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- Steps of the Employee Selection Process -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/2a9b7fbdcef8979ac6f3d8f2d1d1b320f9057e5b" alt="Steps of the Employee Selection Process" style="width:50%">
  <figcaption>Fig.X - Steps of the Employee Selection Process (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- Employee Training and Development Process -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/957687a60d53ea02381cca4e77a3d9ab345fa9cd" alt="Employee Training and Development Process" style="width:50%">
  <figcaption>Fig.X - Employee Training and Development Process (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- Performance Planning and Evaluation -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/5a896abea265add0cecbdb170d59d6019bc04fc6" alt="Performance Planning and Evaluation" style="width:50%">
  <figcaption>Fig.X - Performance Planning and Evaluation</figcaption>
</figure>

<!-- Union Organizing Process and Election -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/c31b8c481d32d83ac889d675dd08f0b123915922" alt="Union Organizing Process and Election" style="width:50%">
  <figcaption>Fig.X - Union Organizing Process and Election (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- The Process of Negotiating Labor Agreements -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/1df8b725fbcf05724ff0de945d41fc670a3e03eb" alt="The Process of Negotiating Labor Agreements" style="width:50%">
  <figcaption>Fig.X - The Process of Negotiating Labor Agreements (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- Typical Grievance Procedure -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/d6ef45a15f3b9ccec85fdb12525f720c20022a86" alt="Typical Grievance Procedure" style="width:50%">
  <figcaption>Fig.X - Typical Grievance Procedure (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- Model of Motivation -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/66916190abe189da0c330097cda0725cd73e99ed" alt="Model of Motivation" style="width:50%">
  <figcaption>Fig.X - Model of Motivation (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- Maslow’s Hierarchy of Needs -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/c100178797991ee9900eb35312099263599a8893" alt="Maslow’s Hierarchy of Needs" style="width:50%">
  <figcaption>Fig.X - Maslow’s Hierarchy of Needs (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- How Expectations Can Lead to Motivation -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/6cee7e2b468fb8696e92581489fa202e79d4be7b" alt="How Expectations Can Lead to Motivation" style="width:50%">
  <figcaption>Fig.X - How Expectations Can Lead to Motivation (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- Idealism: Ben and Jerrys -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/64fbc4b8c57ecf731bef1e4793ae05cd5328af59" alt="Idealism: Ben and Jerrys" style="width:50%">
  <figcaption>Fig.X - Ben & Jerry’s founders Ben Cohen and Jerry Greenfield firmly believe the maxim that companies “do well by doing good.” This idealism led the founders to once famously swear that no Ben & Jerry’s executive would ever make more than seven times the lowliest worker’s wage. But when growth required attracting exceptional top-level management, the company eventually abandoned its self-imposed ratio between its lowest and highest compensation rates. How might perceived inequities in pay affect worker satisfaction and motivation? (Credit: Mike Mozart/ flickr/ Attribution 2.0 Generic (CC BY 2.0))</figcaption>
</figure>

<!-- Perks -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/3b2dcd0632eb3498d33cb5a3bb4a11908c265aa1" alt="Perks" style="width:50%">
  <figcaption>Fig.X - Companies sometimes create unusual perks to help attract and retain talented workers. Timberland employees receive a $3,000 subsidy to buy a hybrid automobile. Worthington Industries offers workers on-site haircuts for just $4. And at SC Johnson, retirees receive a lifetime membership to the company fitness center. One company even has a beer tap that it offers after 3 p.m. every Friday to get workers off to a relaxing weekend. What trends are emerging in the ways companies seek to motivate workers and keep them happy on the job? (Credit: nyuhuhuu/ flickr/ Attribution 2.0 Generic (CC BY 2.0))</figcaption>
</figure>

<!-- Absenteeism -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/295274ca568f101778e71c8755fc4c3a5792d60e" alt="Absenteeism" style="width:50%">
  <figcaption>Fig.X - Employers seeking to stem the rising tide of absenteeism are developing innovative, flexible benefits for their employees. SC Johnson offers workers on-site childcare, an in-house doctor, and paternity leave. Prudential allows employees to take time off to care for sick children and elderly parents. Hewlett-Packard boasts a range of flexible work options to fit employees’ hectic lives. Do flexible options and benefits adequately address the root causes of absenteeism? (Credit: MarylandGovPics/ flickr/ Attribution 2.0 Generic (CC BY 2.0))</figcaption>
</figure>



### Chapter 10: Human resource management

- human resource management (HRM)
- delayering
- teamworking
- workforce plan (or human resource plan)
- labour turnover
- recruitment and selection
- job descriptions
- person specifications (or job specifications)
- employment contract
- business culture
- dismissal
- redundancies
- employee welfare
- employee morale
- work-life balance
- diversity
- equality
- training
- development
- delegation
- intrapreneurship
- multi-skilling
- trade union
- collective bargaining

$$
\begin{aligned}
\text{turnover} & = \frac{ \text{number of staff leaving during the year} }{ \text{average number of staff} } \times 100
\end{aligned} 
$$


### Chapter 11: Motivation

- motivation
- absenteeism
- human needs
- schools of thought
- piece-rate
- division of labour
- hierarchy of needs
- hygiene factors ( or maintenance factors)
- motivators
- performance-related pay (PRP)
- variable pay
- fringe benefits (or perks)
- job redesign
- job enrichment (or vertical loading)
- job enlargement (or horizontal loading)
- job rotation
- empowerment
- job design
- employee participation


### Chapter 12: Management

- leadership
- management
- autocratic management (or authoritarian management)
- paternalistic management
- democratic management (or participative management)
- laissez-faire management


## Part 3: Marketing

1. The nature of marketing
2. Market research
3. The marketing mix - product and price
4. The marketing mix - promotion and place


<!-- Marketing: Geico -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/ed705a88309385a636c35b9b919a7a6abfb0f309" alt="Marketing: Geico" style="width:50%">
  <figcaption>Fig.X - Geico—the major auto insurer with the scaly mascot—famously boasts a 97 percent customer-satisfaction rating. Although the firm’s claim may be exaggerated a bit, consumers get the message that Geico delivers quality insurance coverage at low prices. In what way does the company’s quirky and ubiquitous advertising—in which customers claim to have saved a bunch of money on car insurance by switching to Geico—influence customers’ service expectations? (Credit: Mike Mozart/ Flickr/ Attribution 2.0 Generic (CC BY 2.0))</figcaption>
</figure>

<!-- Consumer Purchase Decision-Making Process -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/0a67a5375ab6bcfcddbeabe11b06941b4ce7d4dd" alt="Consumer Purchase Decision-Making Process" style="width:50%">
  <figcaption>Fig.X - Consumer Purchase Decision-Making Process (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- Tangible and Intangible Attributes of a Product Create Value -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/114f1a88e609c3aeefe09a04dd691ea851a48ef1" alt="Tangible and Intangible Attributes of a Product Create Value" style="width:50%">
  <figcaption>Fig.X - Tangible and Intangible Attributes of a Product Create Value (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- Steps to Develop New Products That Satisfy Customers -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/1a7e50c57836239abbbb6e266bf0ea964db6ba22" alt="Steps to Develop New Products That Satisfy Customers" style="width:50%">
  <figcaption>Fig.X - Steps to Develop New Products That Satisfy Customers (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- Sales and Profits during the Product Life Cycle -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/0d3fc19823fa16250c9eba00a3f765397751bcf1" alt="Sales and Profits during the Product Life Cycle" style="width:50%">
  <figcaption>Fig.X - Sales and Profits during the Product Life Cycle (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>

<!-- Product portfolio: Coca-Cola -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/c7a2318ff2db63a6969e464db3079cca06131458" alt="Product portfolio: Coca-Cola" style="width:50%">
  <figcaption>Fig.X -  Each year Coca-Cola adds new drinks to its product portfolio. While some of these new beverages are close relatives of the original Coca-Cola Classic, others, such as Vitaminwater, constitute entirely new categories of soft drink. What challenges do new products such as Vitaminwater face during the introduction phase of the product life cycle? (Credit: kobakou/ Flickr/ Attribution 2.0 Generic (CC BY 2.0))</figcaption>
</figure>

<!--  Traditional Advertising and Targeted Marketing Using Big Data -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/0441aa4c4df00f922e76c7f058be58630b3d0231" alt=" Traditional Advertising and Targeted Marketing Using Big Data" style="width:50%">
  <figcaption>Fig.X -  Traditional Advertising and Targeted Marketing Using Big Data (Attribution: Copyright Rice University, OpenStax, under CC BY 4.0 license.)</figcaption>
</figure>



### Chapter 17: The nature of marketing

- marketing
- marketing objective
- corporate objective
- marketing strategy
- business-to-consumer marketing (B2C)
- business-to-business marketing (B2B)
- market size
- market growth
- unique selling point (USP)
- niche marketing
- market segment
- mass marketing
- customer-relationship marketing (CRM)
- customer retention

$$
\begin{aligned}
\text{market share} & = \frac{ \text{sales of a business (or product)} }{ \text{total market sales} } \times 100 \\
\\
\text{market growth} & = \frac{ \text{(market sales)}_{ \text{this year} } - \text{(market sales)}_{ \text{last year} } }{ \text{market sales}_{ \text{last year} } } \times 100 
\end{aligned} 
$$


### Chapter 18: Market research

- market research
- primary market research
- focus group
- secondary market research
- sample
- validity of market research
- reliability of market research

$$
\begin{aligned}
\text{market growth} & = \frac{ \text{change in market size} }{ \text{original market size} } \times 100 \\
\\
\text{market share of business} & = \frac{ \text{sales by the business} }{ \text{total market sales} } \times 100
\end{aligned} 
$$

### Chapter 19: The marketing mix - product and price

- marketing mix
- products
- tangible attributes
- intangible aspects
- product differentiation
- product portfolio analysis
- product life cycle
- extension strategy
- product portfolio analysis (PPA)
- Boston Matrix
- competitive pricing
- penetration pricing
- price skimming
- price discrimination
- dynamic pricing
- cost-based pricing
- psychological pricing
- promotional mix

### Chapter 20: The marketing mix - promotion and place

- digital promotion
- click-through rate (CTR) 
- marketing expenditure budget
- distribution channel
- distribution outlet

## Part 4: Operations management

1. The nature of operations
2. Inventory management
3. Capacity utilisation and outsourcing

### Chapter 23: The nature of operations

- output
- inventory
- operations mangement
- productivity
- sustainable
- capital-intensive
- labour-intensive

$$
\begin{aligned}
\text{labour productivity} & = \frac{ \text{total output} }{ \text{numbner of employees} } \times 100 
\end{aligned} 
$$

### Chapter 24: Inventory management

- supply chain
- supply chain management
- lean production


### Chapter 25: Capacity utilisation and outsourcing

- capacity
- factors of production
- capacity utilisation
- capacity under-utilisation
- rationalisation
- subcontracting
- outsourcing

$$
\begin{aligned}
\text{per cent capacity} & = \frac{ \text{existing output}_{ \text{over a given time period} } }{ \text{maximum possible output}_{ \text{over a given time period} } } \times 100 
\end{aligned} 
$$

## Part 5: Finance and accounting

1. Business finance
2. Forecasting and managing cash flows
3. Costs
4. Budgets

### Chapter 29: Business finance

- asset
- capital
- non-current assets
- short-term sources of finance
- long-term sources of finance
- insolvency
- liabilities
- bankruptcy
- liquidation
- administration
- working capital
- current assets
- trade payables
- trade receivables
- revenue expenditure
- capital expenditure
- statement of financial position
- income statement

$$
\begin{aligned}
\text{working capital} & = \text{current assets} - \text{current liabilities} 
\end{aligned} 
$$

### Chapter XX: Sources of finance

- internal source of finance
- external source of finance
- trade credit
- bank loan
- venture capital
- debt factoring
- microfinance
- crowdfunding
- government grant

### Chapter 30: Forecasting and managing cash flows

- cash
- cash-flow forecast


### Chapter 31: Costs

- costs
- revenue
- direct costs
- indirect costs
- full costing
- contribution
- break-even
- profits
- contributioon costing
- average costs
- marginal costs
- cost-plus pricing
- contribution pricing
- special-order decisions
-  margin of safety

$$
\begin{aligned}
\text{profit (or loss)} & = \text{total revenue} - \text{total costs} \\
\\
\text{total cost of production} & = \text{direct costs} + \text{indirect costs} \\
\\
\text{contribution per unit} & = \text{selling price of one unit of output} + \text{variable costs of producing that unit} \\
\text{or} \\
\text{contribution} & = \text{revenue} + \text{variable costs} \\
\\
\text{break-even output} & = \frac{ \text{fixed costs} }{ \text{selling price per unit} - \text{variable cost per unit} } \\
\text{or} \\
\text{break-even output} & = \frac{ \text{fixed costs} }{ \text{contribution per unit} } \\
\\
\text{margin of safety} & = \frac{ \text{current level of sales} - \text{break-even output} }{ \text{current level of sales} } \times 100
\end{aligned} 


$$

### Chapter 32: Budgets

- incrfemental budgeting
- flexible budget
- budget holder
- zero budgets


