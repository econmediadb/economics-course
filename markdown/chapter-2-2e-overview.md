# 2. Financement et marchés financiers


```mermaid
flowchart TD
    A[Financement des acteurs économiques] --> A1[marchés <br> financiers]
    A1 --> A2[rôle des <br> marchés <br> financiers]
    A1 --> A3[type <br> d'institutions <br> financières]
    A1 --> A4[type de <br> marchés]
    A1 --> A5[défaillance <br> du marché]
    A1 --> A6[réglementation <br> des marchés <br> financiers et <br> banques centrales]
    A2 --> A21[- prêts <br> - épargne <br> - faciliter l'échange <br> de biens et <br> de services <br> - ...]
    A3 --> A31[- banques de détail <br> - banques commerciales <br> - banques d'investissement <br> - spéculateurs]
    A4 --> A41[-  marché monétaire <br> - marché des capitaux <br> - marchés des changes <br> - marchés des commodités <br> - marchés des assurances <br> - marchés des dérivés]
    A5 --> A51[- information <br> asymétrique <br> - aléa moral <br> - bulle spéculative <br> - manipulation <br> du marché] 
    A1 --> A7[secteur public]
    A7 --> A71[dépense <br> publique]
    A7 --> A72[imposition]
    A7 --> A73[finances <br> publiques]
```

## Objectifs d'apprentissage

- Comprendre que le financement consiste à couvrir des
besoins de financement par des capacités de financement.
- Comprendre que le taux d’intérêt – à la fois la rémunération du
prêteur et le coût du crédit pour l’emprunteur – est le prix sur
le marché des fonds prêtables.
- Savoir que le revenu disponible des ménages se répartit entre
consommation et épargne et qu’ils peuvent dégager des
besoins ou des capacités de financement.
- Savoir ce qu’est l’excédent brut d’exploitation et comprendre
que les entreprises se financent par autofinancement et
financement externe (emprunts bancaires et recours au
marché financier, en particulier actions et obligations).
- Savoir que le solde budgétaire résulte de la différence entre
les recettes (fiscales et non fiscales) et les dépenses de
l’État ; comprendre que le déficit budgétaire est financé par
l’emprunt et savoir qu’une politique de dépenses publiques
peut avoir des effets contradictoires sur l’activité (relance de la
demande / effet d’éviction).

## Aperçu général

[Source 1](https://www.core-econ.org/the-economy/book/fr/text/10.html)

- Les individus peuvent réorganiser leurs dépenses dans le temps en empruntant, en prêtant, en investissant et en épargnant.
- Bien que les transactions sur le marché du crédit soient motivées par des bénéfices réciproques, il existe un conflit d’intérêts entre les emprunteurs et les prêteurs concernant le taux d’intérêt, l’usage prudent des fonds prêtés et leur remboursement.
- L’emprunt et le prêt constituent une relation de type principal-agent, dans laquelle le prêteur (le principal) ne peut pas garantir le remboursement du prêt par l’emprunteur (l’agent) par les moyens d’un contrat exécutoire, car les informations nécessaires pour le faire sont soit indisponibles, soit invérifiables.
- Pour répondre à ce problème, les prêteurs exigent souvent des emprunteurs qu’ils contribuent au projet avec une partie de leur propre argent.
- Par conséquent, les personnes dont la richesse est limitée ne sont pas toujours en mesure d’obtenir des prêts, ou les obtiennent seulement à des taux d’intérêt plus élevés.
- La monnaie est un intermédiaire des échanges composé des billets de banque, des dépôts bancaires ou de toute autre chose pouvant être utilisée pour acheter des biens et des services. Elle est acceptée comme moyen de paiement, car des tiers peuvent l’utiliser aux mêmes fins.
- Les banques sont des entreprises qui maximisent leur profit et créent de la monnaie sous la forme des dépôts bancaires à travers l’offre de crédit.
- La banque centrale d’un pays crée un type particulier de monnaie ayant cours légal et fournit des prêts aux banques, à un taux d’intérêt directeur qu’elle fixe.
- Le taux d’intérêt facturé par les banques aux emprunteurs (entreprises et ménages) est déterminé en grande partie par le taux d’intérêt directeur fixé par la banque centrale.


## 2.1. Le rôle des marchés financiers

TBD

## 2.2. Type d'institutions financières

TBD

## 2.3. Type de marchés

TBD

## 2.4. Défaillance du marché  (2e ou 1e ?)

TBD

## Sources

- [Spécialité SES de première générale](https://eduscol.education.fr/document/23152/download)
