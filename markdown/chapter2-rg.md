# Chapitre 2: La répartition de la richesse

Après avoir étudié ce chapitre, vous devriez être en mesure d'expliquer les concepts suivants:

    inégalités, valeur ajoutée brute, revenu primaire, 
    révolution industrielle, répartition des revenus,
    courbe de Lorenz, coefficient de Gini, hasard de la naissance.


![Components of chapter 2](https://gitlab.com/econmediadb/economics-course/-/raw/main/concept-maps/20220524-year1-overview-chapter2.png "Concepts - Chapter 2")


## 1. Le partage de la valeur ajoutée entre les différents agents économiques

### 1.a. Le partage de la richesse

```mermaid
graph TD;
A[Partage de la richesse créée] --> B[Répartition inégale de la richesse];
B --> C[Problèmes liés à ces inégalités];
```


## 2. Le partage de la valeur ajoutée entre les différents agents économiques


### 2.a. La valeur ajoutée brute

```mermaid
graph TD;
A[Acteurs qui contribuent à la VAB]-->B[salariés];
A --> C[entreprises];
A --> D[banques]
A --> E[Etat]
```

```mermaid
graph TD;
A[Acteurs qui bénificient du partage de la valeur ajoutée]-->B[entreprises];
B --> B1[autofinancement];
B --> B2[bénéfice mis en réserve];
A --> C[actionnaires];
C --> C1[dividendes];
A --> D[prêteurs];
D --> D1[intérêts sur les crédits accordés];
A --> E[salariés];
E --> E1[salaires];
E --> E2[primes];
A --> F[protection sociale];
F --> F1[cotisations sociales];
A --> G[État];
G --> G1[impôts];
G --> G2[taxes];
```

### 2.b. Le revenu primaire

```mermaid
graph TD;
A[revenu primaire]-->B[revenus du travail];
A --> C[revenus du capital]
```


## 3. Les inégalités de revenu


### 3.a. Part du revenu total reçue par le centile le mieux payé (1913–2015)

![Part de la richesse](https://www.core-econ.org/the-economy/book/fr/images/web/figure-19-03.jpg "Part de la richesse")

### 3.b. La répartition initiale des revenus

```mermaid
graph TD
    A[répartition initiale des revenus] --> B[processus de redistribution]
    A --> C[raison de l'existence des inégalités]
    C --> D[hasard de la naissance]
    A --> E[conséquence des inégalités]
    E --> F[situation cyclique]
    E --> G[cercle vicieux]
    A --> H[mesure des inégalités]
    H --> I[découpage]
    I --> J[décile]
    I --> K[centile]
    I --> L[quintile]
    I --> M[quartile]
    H --> N[courbe de Lorenz]
    H --> O[coefficient de Gini]
    A --> P[lutte contre les inégalités]
    P --> Q[redistribution]
    Q --> R[verticale]
    Q --> S[horizontale]
    P --> T[objectif]
```

### 3.c. Indice de Gini pour le Luxembourg

[code source LaTeX](https://www.overleaf.com/read/qgyfjnnvtwpy)

![Indice de Gini Luxembourg](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220713-gini-coefficient-luxembourg.png "Indice de Gini Luxembourg")

<details>
  <summary markdown="span">Source: World Bank, GINI Index for Luxembourg [SIPOVGINILUX], retrieved from FRED, Federal Reserve Bank of St. Louis; https://fred.stlouisfed.org/series/SIPOVGINILUX, June 23, 2022.</summary>

  Unités :  Indice, non corrigé des variations saisonnières

  Fréquence :  Annuelle

  L'indice de Gini mesure dans quelle mesure la répartition des revenus ou des dépenses de consommation entre les individus ou les ménages d'une économie s'écarte d'une répartition parfaitement égale. Une courbe de Lorenz représente les pourcentages cumulés du revenu total reçu par rapport au nombre cumulé de bénéficiaires, en commençant par l'individu ou le ménage le plus pauvre. L'indice de Gini mesure la surface entre la courbe de Lorenz et une ligne hypothétique d'égalité absolue, exprimée en pourcentage de la surface maximale sous la ligne. Ainsi, un indice de Gini de 0 représente une égalité parfaite, tandis qu'un indice de 100 implique une inégalité parfaite.

  Indicateur source : SI.POV.GINI. 
</details>


### 3.d.  Part du revenu national avant impôt détenue par le groupe p0p50 (pour le Luxembourg)

[code source LaTeX](https://www.overleaf.com/read/qgyfjnnvtwpy)

![p0-p50](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220715-share-p0-p50.png "p0-050")

<details>
  <summary markdown="span">Source: http://wordpress.wid.world </summary>

  **Variable name**: Pre-tax national income. Share Adults. equal-split adults

  **Intuitive description**: Pre-tax national income share held by the p0p50 group. Pre-tax national income  is the sum of all pre-tax personal income flows accruing to the owners of the production factors, labor and capital, before taking into account the operation of the tax/transfer system, but after taking into account the operation of pension system. The central difference between personal factor income and pre-tax income is the treatment of pensions, which are counted on a contribution basis by factor income and on a distribution basis by pre-tax income. The population is comprised of individuals over age 20. The base unit is the individual (rather than the household) but resources are split equally within couples.

  **Technical description**: Pre-tax national income =Pre-tax labor income [total pre-tax income ranking]+Pre-tax capital income [total pre-tax income ranking]

  **Methodological Notes**: Summary of data construction by year (see source for details): 1985: survey + concept correction + extrapolated nonresponse, 1986-1990: interpolated survey + concept correction + extrapolated nonresponse, 1991: survey + concept correction + extrapolated nonresponse, 1992-1993: interpolated survey + concept correction + extrapolated nonresponse, 1994: survey + concept correction + extrapolated nonresponse, 1995-1996: interpolated survey + concept correction + extrapolated nonresponse, 1997: survey + concept correction + extrapolated nonresponse, 1998-1999: interpolated survey + concept correction + extrapolated nonresponse, 2000: survey + concept correction + extrapolated nonresponse, 2001-2002: interpolated survey + concept correction + extrapolated nonresponse, 2003-2009: survey + extrapolated nonresponse, 2010: survey + tax data, 2011: survey + extrapolated nonresponse, 2012: survey + tax data, 2013-2018: survey + extrapolated nonresponse, 2019: extrapolated distribution + extrapolated nonresponse, 2020: extrapolated distribution

  **Sources**: ; Before 1985, [URL][URL_LINK]http://wordpress.wid.world/document/countries-with-regional-income-imputations-on-wid-world-world-inequality-lab-technical-note-2021-15/[/URL_LINK][URL_TEXT]Chancel and Piketty (2021). â€œCountries with Regional Income Imputations on WID.worldâ€[/URL_TEXT][/URL][URL][URL_LINK]https://wid.world/document/why-is-europe-more-equal-than-the-united-states-world-inequality-lab-wp-2020-19/[/URL_LINK][URL_TEXT]Blanchet, Chancel and Gethin (2020), â€œWhy is Europe less Unequal than the US?â€; [/URL_TEXT][/URL][URL][URL_LINK]http://wordpress.wid.world/document/2020-dina-update-for-europe-world-inequality-lab-technical-note-2020-04/[/URL_LINK][URL_TEXT]Updated by Morgan and Neef (2020), â€œRegional DINA update for Europeâ€; [/URL_TEXT][/URL][URL][URL_LINK]http://wordpress.wid.world/document/2021-dina-regional-update-for-europe-world-inequality-lab-technical-note-2021-8/[/URL_LINK][URL_TEXT]Updated by Morgan and Neef (2021), â€œ2021 DINA Regional Update for Europeâ€[/URL_TEXT][/URL]

  **Extrapolations**: [[1980, 2003], [2018, 2021]] 
</details>


### 3.e.  Part du revenu national avant impôt détenue par le groupe p99p100 (pour le Luxembourg)

[code source LaTeX](https://www.overleaf.com/read/qgyfjnnvtwpy)

![p99-p100](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220715-share-p99-p100.png "p99-100")





## 4. Lutter contre les inégalités

### 4.a. Politiques de réduction de la pauvreté et de redistribution des revenus

La pauvreté n'est pas seulement source de division sociale, elle peut aussi freiner le développement économique d'un pays. Les gouvernements interviendront donc souvent dans leurs économies pour tenter de réduire la pauvreté de plusieurs manières. 

- Promouvoir la *croissance économique*
- Améliorer la quantité et la qualité de l'*éducation*
- Trouver des mesures pour réduire le *chômage*
- Augmentation des *impôts progressifs*
- Réduction des *impôts indirects*
- L'argent provenant des impôts peut financer les *services sociaux* et fournir une *aide au revenu* aux personnes à très faible revenu.
- Les recettes fiscales peuvent également être utilisées pour *subventionner la construction de logements* gratuits ou à faible coût pour les familles pauvres, et pour leur fournir des *soins de santé* et des *déplacements gratuits*.
- Introduire des lois sur le *salaire minimum* pour augmenter les salaires des employés les moins bien payés.

## 5. L'évolution des inégalités économiques depuis le début du 20e siècle

...


MOVE THE REST OF THE MATERIAL TO DIFFERENT CHAPTER


## L‘avant et l‘après de la révolution industrielle

```mermaid
graph TD
    A[révolution industrielle]-->B{avant}
    A --> C{après}
    B --> D[production a lieu dans un cadre familial]
    C --> E[valeur ajoutée brute]
    E --> F[rémunération des facteurs de production]
    F --> |40%| G[capital]
    F --> |60%| H[travail]
    G --> I[dynamique du partage de VAB entre capital et travail]
    H --> I
    I --> |stable à court et moyen terme| L[les salariés évoluent en ligne avec les gains de productivité]
    I --> |volatile à long terme| M[pouvoir de négociation entre salariés et employeurs]
```








