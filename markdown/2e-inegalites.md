# Pauvreté et inégalités


## Objectifs d'apprentissage

- la nature et les causes des inégalités dans la répartition des revenus et des richesses
- la différence entre l'égalité et l'équité en ce qui concerne la répartition des revenus et des richesses
- comment les politiques gouvernementales telles que la fiscalité progressive, les transferts et le salaire minimum national tentent de rendre la répartition des revenus plus équitable
- la différence entre la pauvreté absolue et la pauvreté relative

## 1. Revenu et richesse

Vous devez connaître la différence entre le revenu et la richesse. Le revenu est la rémunération des services d'un facteur de production. Pour le travail, le revenu est versé sous forme de salaires et de primes. Pour les autres facteurs de production, le revenu prend la forme de loyers, d'intérêts et de bénéfices.

Le revenu est un concept de flux. En effet, les rendements des différents facteurs de production sont variables au cours d'une période donnée.

La richesse décrit le stock d'actifs qu'une personne a accumulé au fil du temps, par exemple des biens immobiliers, des actions, et de l'or. Ces actifs sont là pour assurer la sécurité et, dans certains cas, un flux de revenus pour l'avenir.


## 2. Mesurer l'inégalité des revenus et des richesses

## 2.1. Coefficient de Gini

Le coefficient de Gini est une mesure numérique de l'ampleur des inégalités de revenus dans une économie. Si la répartition des revenus dans une économie est égale, le coefficient de Gini aura une valeur de 0. À l'autre extrême, si tous les revenus reviennent à une seule personne, e coefficient de Gini sera de 1. Ces deux extrêmes ne se produisent pas dans le monde réel. La norme veut que les coefficients de Gini se situent entre les valeurs 0 et 1. Un coefficient de Gini de 0,3 indique donc une répartition plus égale des revenus qu'un coefficient de 0,5. Le tableau suivant contient quelques exemples de différents types d'économie. (Parfois les données des tableaux sont présentées sous forme de pourcentages : 100% représentant une inégalité totale).

[Gini coefficient, 2019](https://ourworldindata.org/explorers/inequality?time=2019&facet=none&Data=World+Inequality+Database+%28Incomes+before+tax%29&Indicator=Gini+coefficient&country=CHL~ZAF~USA~FRA~CHN~BRA)

<iframe src="https://ourworldindata.org/explorers/inequality?time=2019&facet=none&country=CHL~ZAF~USA~FRA~CHN~BRA~LUX&Data=World+Inequality+Database+%28Incomes+before+tax%29&Indicator=Gini+coefficient&hideControls=true" loading="lazy" style="width: 100%; height: 600px; border: 0px none;"></iframe>

### Application

    Certains économistes affirment que d'autres types d'économie ont une répartition des revenus plus inégale que les économies développées, et utilisent 0,45 (ou 45 %) comme référence. Les données du graphique ci-dessus confirment-elles ce point de vue ?

## 2.2. Courbe de Lorenz

Un autre outil utile permettant de représenter et comparer les distributions de revenus ou de richesse et de montrer l’ampleur des inégalités est la **courbe de Lorenz**⁠ (inventée en 1905 par Max Lorenz (1876–1959), un économiste américain, lorsqu’il était encore étudiant). Elle mesure la dispersion des revenus, ou de tout autre variable, dans une population.

La courbe de Lorenz représente l’ensemble de la population ordonnée sur l’axe des abscisses du plus pauvre au plus riche. La hauteur de la courbe à chaque point de l’axe des abscisses indique la fraction du revenu total reçue par la fraction de population indiquée à ce point sur l’axe des abscisses.

![Taux de salaire minimum](../inkscape-files/diagrams/courbe-de-lorenz.svg "taux de salaire minimum")

## 2.3. Relation entre la courbe de Lorenz et le coefficient de Gini

![Taux de salaire minimum](../inkscape-files/diagrams/courbe-de-lorenz-coefficient-de-gini.svg "gini et lorenz")

Nous pouvons calculer le coefficient de Gini en utilisant la courbe de Lorenz. Le coefficient de Gini peut être défini, comme étant l’aire A, entre la courbe de Lorenz et la droite d’égalité parfaite, exprimée en proportion de l’aire (A + B), le triangle sous la droite à 45 degrés :

$$
\text{Gini} = \frac{A}{A+B}
$$

## 3. Les raisons économiques de l'inégalité des revenus et des richesses

Les économistes s'accordent à dire que l'inégalité des revenus et des richesses constitue un obstacle à la croissance économique et au développement. Les raisons de cette inégalité sont économiques, d'autres sociales, culturelles et politiques. Les raisons économiques de l'inégalité sont les suivantes:

- le manque d'opportunités d'emploi, en particulier pour les jeunes, mais aussi pour les personnes ayant des compétences professionnelles.
- une formation professionnelle insuffisante, ce qui signifie que les industries locales ne peuvent pas obtenir la main-d'œuvre nécessaire pour maintenir une activité viable sur les marchés nationaux et internationaux.
- un manque d'investissement dans les secteurs de l'éducation et de la santé, ce qui réduit le capital humain nécessaire à la promotion de l'économie.
- des infrastructures médiocres telles que les routes, les chemins de fer, l'approvisionnement en eau et en électricité ainsi que l'approvisionnement en eau
- un faible taux d'épargne, qui freine les investissements des secteurs privé et public 
- l'incapacité de nombreuses personnes à obtenir des crédits pour financer les petites entreprises et améliorer l'éducation personnelle.

## 4. Politiques de redistribution des revenus et des richesses

Les gouvernements peuvent utiliser une série de politiques pour réduire l'inégalité dans la distribution des revenus et des richesses. La plupart des gouvernements cherchent à réduire l'inégalité des revenus mais il peut également y avoir des politiques de redistribution des richesses. De nombreuses politiques dépendent des fonds générés par les recettes fiscales pour leur mise en œuvre et leur réglementation.

La collecte des impôts pose de sérieuses difficultés dans la plupart des pays à faible revenu et dans de nombreux pays à revenu intermédiaire, où l'économie informelle est énorme et où seul un très faible pourcentage de la population paie des impôts directs plutôt qu'indirects. La corruption et l'évasion fiscale (qui consiste à ne pas payer d'impôts de manière délibérée) sont également monnaie courante. 

Cela empêche les gouvernements de mettre en œuvre avec succès des politiques de redistribution des revenus et des richesses.

### 4.1. Salaire minimum

Un taux de salaire minimum est une exigence légale de ce que l'employeur doit verser à un salarié par heure. Il s'agit d'un taux avant impôt et toute déduction de sécurité sociale. Il est aujourd'hui largement appliqué dans de nombreuses économies. Les employeurs qui ne versent pas le salaire minimum légal peuvent être condamnés à une amende ou à d'autres formes de sanctions.

<img src="https://ec.europa.eu/eurostat/statistics-explained/images/thumb/c/c1/Minimum_wages%2C_January_2024_and_January_2014_%28levels%2C_in_%E2%82%AC_per_month_and_average_annual_growth%2C_in_%25%29_.png/1050px-Minimum_wages%2C_January_2024_and_January_2014_%28levels%2C_in_%E2%82%AC_per_month_and_average_annual_growth%2C_in_%25%29_.png" alt="minimum wage" style="width:400px;"/>

[Source: Eurostat](https://ec.europa.eu/eurostat/statistics-explained/index.php?title=Minimum_wage_statistics)

L'introduction d'un salaire minimum peut réduire la pauvreté dans toutes les économies. Le problème pour la plupart des pays à faible revenu et à revenu moyen inférieur est que la législation ne s'applique qu'à une minorité de travailleurs mal payés. En effet, la législation n'aurait pas d'impact sur les les vastes secteurs informels qui prévalent dans ces économies. De même, un taux de salaire minimum n'a aucune pertinence lorsque les travailleurs sont indépendants ou dirigent de petites entreprises où travaillent des membres de leur famille.

Les détracteurs d'une politique de salaire minimum affirment que son introduction conduit au chômage. Cet argument est illustré dans la figure ci-dessous. Le taux de salaire d'équilibre est W. L'introduction d'un salaire minimum fait passer le taux de salaire à W1. À ce taux de salaire plus élevé, les employeurs n'emploieront que des travailleurs QD, alors que les travailleurs QO sont prêts à travailler. Ceux qui sont employés recevront un salaire plus élevé, mais au prix d'une diminution du nombre d'emplois disponibles. Ce chômage est illustré par le passage de QD à QO dans la figure.

![Taux de salaire minimum](../inkscape-files/diagrams/taux-de-salaire-salaire-minimum.svg "taux de salaire minimum")

### 4.2. Transferts en nature

Un transfert en nature est un paiement provenant des recettes fiscales que reçoivent certains membres de la communauté. Ces paiements ne sont pas effectués par le biais du marché, puisqu'il n'y a pas de production. Leur fonction est d'assurer une répartition plus équitable des revenus. Les principaux bénéficiaires sont les groupes vulnérables tels que les personnes âgées, les personnes à mobilités réduites, les chômeurs et les personnes aux revenus les plus faibles. Les paiements tendent à transférer les revenus des personnes capables de travailler et de payer des impôts à celles qui ne peuvent pas travailler ou qui ont besoin d'aide. Voici quelques exemples de paiements de transfert :

- les pensions de vieillesse
- les allocations de chômage
- les allocations de logement
- les coupons alimentaires
- les allocations familiales.

L'effet des transferts en nature sur le marché est une question controversée. Il est clair que dans la plupart des cas, ils sont nécessaires pour protéger les groupes les plus vulnérables de la communauté. Les paiements de transfert réduisent la pauvreté et permettent une répartition plus équitable des revenus. L'argument opposé est que les allocations de chômage et les prestations versées aux personnes ayant les revenus les plus faibles peuvent dissuader les gens d'accepter un emploi, ce qui augmente le taux de chômage. En conséquence, la production de l'économie est inférieure à ce qu'elle pourrait être et il y a une forme d'inefficacité.

Un autre moyen de réduire les inégalités dans la société est que le gouvernement peut fournir certains biens et services importants, souvent gratuitement à l'utilisateur. Ces services sont financés par le système fiscal. Si les biens et les services sont utilisés de la même manière par tous les citoyens, ce sont les personnes aux revenus les plus faibles qui gagnent le plus en pourcentage de leur revenu L'inégalité est ainsi réduite.

Les deux principaux exemples de gratuité dans de nombreuses économies sont les soins de santé et l'enseignement primaire et secondaire. 

### 4.3. Imposition

Le système fiscal peut être utilisé pour réduire les inégalités de revenus et de richesses. Cela passe notamment par l'utilisation d'impôts progressifs. Le meilleur exemple est celui de l'impôt progressif sur le revenu, qui permet d'imposer aux personnes ayant des revenus élevés un taux ou un pourcentage plus élevé de leur revenu que ceux qui ont des revenus plus faibles. Dans certaines économies, le taux supérieur de l'impôt sur le revenu peut atteindre 80 %, contre 40 % et 20 % pour les taux inférieurs. De cette manière, les écarts de revenus sont réduits. La plupart des systèmes d'impôt sur le revenu sont similaires à cet exemple et sont donc progressifs par nature. Le taux moyen d'imposition augmente au fur et à mesure que les personnes gagnent de l'argent. Le taux moyen d'imposition augmente au fur et à mesure que les revenus augmentent. Cependant, le principal problème est que pour certaines personnes, il en résulte une désincitation à travailler et peut-être même à vivre dans un régime à forte imposition. Pour des raisons fiscales, elles peuvent chercher à s'installer dans un pays qui a un régime fiscal plus favorable. 

Des taxes peuvent également être imposées pour réduire les inégalités de richesse. Les droits de succession en sont un exemple. Les personnes qui héritent d'un patrimoine supérieur à un certain montant doivent payer une partie de la valeur de ce patrimoine sous forme d'impôt à l'État. Un autre exemple similaire est celui de l'impôt sur le capital. sur le gain financier qu'une personne a pu réaliser pendant la période où un actif, tel qu'un bien immobilier ou un bien de consommation, a été acquis. qu'un actif, tel qu'un bien immobilier ou un portefeuille financier, a été détenu.

![Revenu marchand et revenu disponible](../inkscape-files/diagrams/revenu-marchand-et-revenu-disponible.svg "revenu marchand et revenu disponible")

### Application

    Pour votre propre pays ou pour un pays que vous connaissez, dressez une liste des politiques qui sont actuellement utilisées pour redistribuer les revenus et les richesses


## 5. Équité et égalité

Il y a équité lorsqu'une société distribue ses ressources de manière équitable entre ses membres. La distribution peut concerner les revenus, les prestations publiques et même la richesse. L'équité a deux aspects :
- l'équité horizontale selon laquelle les consommateurs et les autres personnes devraient payer le même niveau d'imposition
- l'équité verticale, où les impôts doivent être répartis équitablement entre les riches et les pauvres d'une société

L'égalité n'est pas la même chose que l'équité. L'égalité vise à promouvoir l'équité, mais elle ne peut fonctionner que si tout le monde part de la même position et a besoin de la même aide. C'est
l'idéal de l'égalité réelle de tous.

## 6. Pauvreté absolue et relative

La pauvreté existe lorsque les familles n'ont pas assez d'argent ou d'accès aux ressources pour avoir un niveau de vie raisonnable. Cela inclut non seulement la nourriture et le logement, mais aussi l'accès à une éducation décente, aux soins de santé, à l'approvisionnement en eau et à l'assainissement. Bien que la pauvreté mondiale diminue, il n'en reste pas moins que les personnes nées dans la pauvreté sont beaucoup plus susceptibles de rester pauvres. Quelques-unes peuvent échapper à ce que l'on appelle le "cycle de la pauvreté", mais elles sont l'exception.

Il existe plusieurs définitions de la pauvreté. L'extrême pauvreté est définie par la Banque mondiale comme "le seuil international de pauvreté qui consiste à vivre avec moins de 2,15 dollar par jour". L'année 2030 est l'échéance fixée par les Nations unies pour l'objectif de développement durable (ODD 1) des Nations unies visant à l'éradication de l'extrême pauvreté.

<img src="https://www.fdlux.lu//sites/fdlux.lu/files/images/E_SDG_poster_UN_emblem_WEB.png" alt="odd1 onu" style="width:400px;"/>

La pauvreté absolue est définie par les économistes comme le fait que le revenu du ménage est inférieur à un certain niveau qui empêche une personne ou une famille de satisfaire les besoins fondamentaux de la vie tels que la nourriture, le logement, l'eau, les soins de santé et l'éducation. Dans cet état de pauvreté, les personnes touchées ne peuvent pas bénéficier de la croissance économique et de l'augmentation du niveau de vie. Elles sont piégées dans un cycle de pauvreté. Il n'est pas possible de donner une valeur globale au niveau de revenu de la pauvreté absolue, car il varie d'un pays à l'autre. 

<img src="https://ourworldindata.org/images/published/Five-income-distributions-national-poverty-and-IPL-2_850.webp" alt="distribution des revenus et ligne de pauvreté" style="width:400px;"/>

La pauvreté relative, comme le terme l'indique, compare le revenu d'un ménage au revenu moyen de son pays. En règle générale, lorsque ce revenu est inférieur ou égal à la moyenne, le ménage est considéré comme étant en situation de pauvreté relative. Les ménages ont de l'argent, mais seulement assez pour répondre aux besoins de base. Leurs membres ne sont pas en mesure de profiter du même niveau et de la même qualité de vie que le reste de la population dont les revenus sont supérieurs à la moyenne. L'accès aux biens de consommation, au logement et à l'éducation est limité par un faible revenu. La croissance économique est un facteur important qui peut déterminer si un ménage sort de la pauvreté relative.


### Application 1

1. Expliquez la différence entre la pauvreté relative et la pauvreté absolue.
2. Décrire comment les inégalités dans la distribution des revenus et des richesses ont évolué ces dernières années.
3. Discutez le rôle de la fiscalité progressive et les transferts pour réduire les inégalités de revenus et la pauvreté.
4. Êtes-vous d'accord avec le fait qu'une croissance économique plus rapide, nécessaire pour réduire la pauvreté, implique nécessairement que la répartition des revenus devienne moins égale ? Justifiez votre réponse.

### Application 2



_Un monde inégalitaire (par Aude Martin)_ <br>
[Source: Alternatives Economiques](https://www.alternatives-economiques.fr/un-monde-inegalitaire/00090152)

(...)

Après avoir reflué des années 1920 jusqu’à la fin des Trente Glorieuses (1945-1975), les inégalités sont reparties à la hausse. En flèche en Chine et en Russie après leur transition du communisme au capitalisme, plus modérément en Europe de l’Ouest, où la casse a été limitée par rapport aux Etats-Unis. La croissance européenne n’en reste pas moins inégalitaire. _« Dans la quasi-­totalité des pays pour lesquels des données sont disponibles, on observe une hausse de la part du revenu national captée par les 10 % des citoyens les plus aisés »_, résument les chercheurs Thomas Blanchet, Lucas Chancel et Amory Gethin, auteurs d’une récente étude sur le modèle social européen. (...)

Les inégalités de revenus entre Etats ont beau s’être réduites depuis 1980 sous l’effet de la forte croissance asiatique, le constat est sans appel : le 1 % du haut de la population mondiale a capté deux fois plus de richesses que la moitié du bas depuis cette date (respectivement 27 % et 12 %). Une répartition (...) marquée la moindre croissance du revenu des classes moyennes et populaires mondiales par rapport aux ménages des pays pauvres et émergents ; et surtout par rapport aux plus riches des pays riches.

L’explication principale à cette progression continue des inégalités nous est fournie par Thomas Piketty, et tient à la supériorité du rendement du capital (ce qu’il rapporte en dividendes, inté­rêts, etc.) sur la croissance des autres revenus. _« Cela implique mécaniquement que les patrimoines issus du passé se recapitalisent plus vite que le rythme de progression de la production et des revenus »_, note-t-il dans son ouvrage _Le capital au XXIe siècle_. En clair, la valeur du capital s’accroît plus vite que le reste de l’économie. Quand bien même les revenus du travail sont inégalement répartis, les revenus du capital le sont toujours plus en raison de cet effet boule de neige.

Dans ces conditions, la dynamique inégalitaire du capitalisme semble inexorable. Mais la bonne nouvelle, c’est que la forte disparité du degré d’inégalité d’un pays à l’autre nous apprend que « les institutions et les politiques publiques jouent un rôle dans leur évolution » (...). Instaurer une fiscalité plus progressive, améliorer l’offre de services publics et accentuer la lutte contre les paradis fiscaux sont autant d’outils permettant d’éviter la concentration des richesses dans les mains de quelques-uns. Car si le capitalisme est la cause principale des inégalités, la finance y contribue par son opacité.

_Questions_

1. Analysez comment les inégalités ont évolué au fil du temps.
2. Quelle est l’explication avancée par Thomas Piketty concernant l’évolution des inégalités ?
3. Analysez le rôle des institutions et des politiques publiques dans l’évolution des inégalités, en vous appuyant sur les éléments présentés dans le chapitre en cours. Discutez le passage suivant de l'extrait _« les institutions et les politiques publiques jouent un rôle dans leur évolution »_ .
