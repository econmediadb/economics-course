# Questions

## Contrôle des connaissances: QCM

- Politique économique : cours et QCM corrigés
  <details>
  <summary markdown="span">Source:</summary>

    [Politique économique : cours et QCM corrigés
](https://a-z.lu/primo-explore/fulldisplay?docid=ALEPH_LUX01001421467&context=L&vid=BIBNET&search_scope=All_content&tab=all_content&lang=fr_FR)
  </details>

1. La **croissance** est un phénomène de :
   - Court terme
   - Long terme
   <details>
   <summary markdown="span">Réponse:</summary>
     Long terme 
   </details>
1. Lorsque le **taux de croissance** devient **négatif**, on parle alors de :
   - Récession
   - Dépression
   <details>
   <summary markdown="span">Réponse:</summary>
     Dépression 
   </details>      

1. 


## Chapitre 2: Inégalités

- Dans quelle mesure le système capitaliste produit-il des inégalités ?
  <details>
  <summary markdown="span">Source:</summary>

    [Dictionnaire d'économie politique : capitalisme, institutions, pouvoir
 (p.255-261)](https://a-z.lu/primo-explore/fulldisplay?docid=ALEPH_LUX01001558964&context=L&vid=BIBNET&search_scope=All_content&tab=all_content&lang=fr_FR)
  </details>

