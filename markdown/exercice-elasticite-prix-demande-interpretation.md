
**Interprétation élasticité prix-demande**

Un fabricant a reçu une étude de marché estimant les valeurs de l'élasticité prix de la demande pour ses chemises actuellement vendues sur trois marchés : les commerçants indépendants, les magasins de mode de prestige et la vente par correspondance. Expliquez et commentez les valeurs des élasticités prix de la demande présentées dans le tableau ci-dessous :

| Marché                   | Prix actuel | Ventes actuelles     | Valeur élasticité prix-demande |
|--------------------------|-------------|----------------------|--------------------------------|
| Commerçants indépendants | 8 €         | 40.000 unités par an | 1.0                            |
| Magasins de mode         | 15 €        | 10.000 unités par an | 0.2                            |
| Vente par correspondance | 10 €        | 3.000 unités par an  | 3.0                            |

**Solution**

Toutes les estimations des élasticitéS prix-demande sont négatives, ce qui indique qu'une augmentation du prix entraîne une baisse de la demande et vice versa. La demande de chemises vendues par correspondance est élastique, elle est unitaire pour les ventes par l'intermédiaire de détaillants indépendants et inélastique pour les ventes par l'intermédiaire de magasins de mode.

