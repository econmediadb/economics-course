# AS-level Economics

## Main parts of the economics course

1. Basic economic ideas and resource allocation
2. The price system and the microeconomy
3. Government microeconomic intervention
4. The macroeconomy
5. Government macroeconomic intervention
6. International economic issues

[more or less 5 weeks/part]

## Part 1: Basic economic ideas and resource allocation

### Main chapters of part 1

1. Scarcity, choice and opportunity cost
2. Economic methodology
3. Factors of production
4. Resource allocation in different economic systems
5. Production possibility curves
6. Classification of goods and services

<!-- People experiencing homelessness are a stark reminder that scarcity of resources is real. (Credit: "Pittsburgh Homeless" by "daveyinn"/Flickr Creative Commons, CC BY 2.0) -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/fad0e7644528235aaebe7f8ad5ec7e3602706328" alt="Basic Economic Problem - Scarcity of Resources" style="width:50%">
  <figcaption>Fig.X - Basic Economic Problem - Scarcity of Resources. (Credit: "Pittsburgh Homeless" by "daveyinn"/Flickr Creative Commons, CC BY 2.0)</figcaption>
</figure> 

<!-- Adam Smith -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/abc76451c77edc0569aa2af879956b25fa17c129" alt="Adam Smith" style="width:50%">
  <figcaption>Fig.X - Adam Smith introduced the idea of dividing labor into discrete tasks. (Credit: "Adam Smith" by Cadell and Davies (1811), John Horsburgh (1828), or R.C. Bell (1872)/Wikimedia Commons, Public Domain)</figcaption>
</figure>

[The Wealth of Nations by Adam Smith (audio and transcript)](https://www.britannica.com/topic/the-Wealth-of-Nations)

<!-- La Richesse des Nations par Adam Smith (Gallica) -->
<div style="display: block; "><iframe style="width:500px; height: 554.8919753086419px; border: 0;" src="https://gallica.bnf.fr/ark:/12148/bpt6k75319v/f9.double.mini"></iframe></div>

<!--  Division of Labor -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/64ca456471b6d936cb0f53a84006cf29997e84bb" alt="Division of Labor" style="width:50%">
  <figcaption>Fig.X - Division of Labor: Workers on an assembly line are an example of the divisions of labor. (Credit: "Red Wing Shoe Factory Tour" by Nina Hale/Flickr Creative Commons, CC BY 2.0)</figcaption>
</figure>


<!-- John Maynard Keynes -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/89ad7d87595dd610af33d1aef44f896e46587e52" alt="John Maynard Keynes" style="width:50%">
  <figcaption>Fig.X - John Maynard Keynes One of the most influential economists in modern times was John Maynard Keynes. (Credit: “John Maynard Keynes” by IMF/Wikimedia Commons, Public Domain)</figcaption>
</figure>

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZRvaxUNDTKY?si=RXq5nzFIpuEr09OE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>



### Chapter 1: Scarcity, choice and opportunity cost

- wants
- needs
- resources
- economic problem
- opportunity cost
- scarcity and choice

<!-- Choices and Tradeoffs -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/5a1c044deac5acfdd49ba024da1c17ba361ac9e2" alt="Choices and Tradeoffs" style="width:50%">
  <figcaption>Fig.X - Choices and Tradeoffs In general, the higher the degree, the higher the salary, so why aren’t more people pursuing higher degrees? The short answer: choices and tradeoffs. (Credit: modification of "College of DuPage Commencement 2018 107" by COD Newsroom/Flickr, CC BY 2.0)</figcaption>
</figure>

<!-- The Budget Constraint -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/1e6acbb4ab3f597a104c28cc3ba208dc5debdaf3" alt="The Budget Constraint" style="width:50%">
  <figcaption>Fig.X - The Budget Constraint: Alphonso’s Consumption Choice Opportunity Frontier Each point on the budget constraint represents a combination of burgers and bus tickets whose total cost adds up to Alphonso’s budget of $10. The relative price of burgers and bus tickets determines the slope of the budget constraint. All along the budget set, giving up one burger means gaining four bus tickets.</figcaption>
</figure>


### Chapter 2: Economic methodology

- positive statement
- normative statement
- value judgement
- ceteris paribus
- economic law
- microeconomics
- macroeconomics
- short run
- long run
- very long run

Economics seeks to describe economic behavior as it actually exists. Philosophers draw a distinction between positive statements, which describe the world as it is, and normative statements, which describe how the world should be. 
Positive statements are factual. They may be true or false, but we can test them, at least in principle. 
Normative statements are subjective questions of opinion.

### Chapter 3: Factors of production

- primary sector
- secondary sector
- tertiary sector
- land
- labour
- capital
- enterprise
- entrepreneur
- human capital
- physical capital
- rent
- wage
- salary
- interest
- profit
- specialisation
- division of labour
- Adam Smith
- enterprise culture

<!-- The Circular Flow Diagram -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/626d1c7cf155ffff8f72475d68ac15fe4da4eb75" alt="The Circular Flow Diagram" style="width:50%">
  <figcaption>Fig.X - The Circular Flow Diagram The circular flow diagram shows how households and firms interact in the goods and services market, and in the labor market. The direction of the arrows shows that in the goods and services market, households receive goods and services and pay firms for them. In the labor market, households provide labor and receive payment from firms through wages, salaries, and benefits.</figcaption>
</figure>


### Chapter 4: Resource allocation in different economic systems

- allocative mechanism
- market economy or market system
- market
- planned (or command) economy
- mixed economy
- transitional economy

<!-- A Command Economy -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/2a6af7aa6dce6a6e2f047d2d2835a506618adea4" alt="A Command Economy" style="width:50%">
  <figcaption>Fig.X - A Command Economy Ancient Egypt was an example of a command economy. (Credit: "Pyramids at Giza" by Jay Bergesen/Flickr Creative Commons, CC BY 2.0)</figcaption>
</figure>


<!-- A Market Economy -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/156ff3bfad3dafb431ef8afb9c61a2248e7df9b1" alt="A Market Economy" style="width:50%">
  <figcaption>Fig.X - A Market Economy Nothing says “market” more than The New York Stock Exchange. (Credit: work by Erik Drost/Flickr Creative Commons, CC BY 2.0)</figcaption>
</figure>

<!-- Index of Economic Freedom -->
[2023 Index of Economic Freedom - Interactive Heat Map](https://www.heritage.org/index/heatmap)

 <figure>
  <img src="../images/index-of-economic-freedom-heatmap-2023.png" alt="Index of Economic Freedom" style="width:50%">
  <figcaption>Fig.X - 2023 Index of Economic Freedom - Heat Map</figcaption>
</figure>


### Chapter 5: Production possibility curves

- production possibility curve (or frontier)
- scarcity and choice
- economic growth

<!-- Production possibilities frontier (PPF) -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/b5a33337ada51f6d64ae40d7e3f97fec2659e108" alt="Production possibilities frontier" style="width:50%">
  <figcaption>Fig.X - A Healthcare vs. Education Production Possibilities Frontier This production possibilities frontier shows a tradeoff between devoting social resources to healthcare and devoting them to education. At A all resources go to healthcare and at B, most go to healthcare. At D most resources go to education, and at F, all go to education.</figcaption>
</figure>

<!-- Productive and Allocative Efficiency -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/79dd80a2684689ad1140ba93aa07462b2b857cb5" alt="Productive and Allocative Efficiency" style="width:50%">
  <figcaption>Fig.X - Productive and Allocative Efficiency Productive efficiency means it is impossible to produce more of one good without decreasing the quantity that is produced of another good. Thus, all choices along a given PPF like B, C, and D display productive efficiency, but R does not. Allocative efficiency means that the particular mix of goods being produced—that is, the specific choice along the production possibilities frontier—represents the allocation that society most desires.</figcaption>
</figure>

<!-- Production Possibility Frontier for the U.S. and Brazil -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/32b5df814819fcabd18b2a8203f7a7aef51c8fd0" alt="Production Possibility Frontier for the U.S. and Brazil " style="width:50%">
  <figcaption>Fig.X -  Production Possibility Frontier for the U.S. and Brazil The U.S. PPF is flatter than the Brazil PPF implying that the opportunity cost of wheat in terms of sugar cane is lower in the U.S. than in Brazil. Conversely, the opportunity cost of sugar cane is lower in Brazil. The U.S. has comparative advantage in wheat and Brazil has comparative advantage in sugar cane.</figcaption>
</figure>

<!-- The Tradeoff Diagram -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/e1c35f7fad20ff4499ff70c7a1c1e202ed7ce268" alt="The Tradeoff Diagram" style="width:50%">
  <figcaption>Fig.X -   The Tradeoff Diagram Both the individual opportunity set (or budget constraint) and the social production possibilities frontier show the constraints under which individual consumers and society as a whole operate. Both diagrams show the tradeoff in choosing more of one good at the cost of less of the other.</figcaption>
</figure>



### Chapter 6: Classification of goods and services

- free good
- private good
- rivalry
- excludability
- public good
- non-excludability
- non-rivalry
- free rider
- government expenditure
- non-rejectability
- merit good
- information failure
- market imperfection
- market failure
- demerit good

[Introduction to public good (Voyager and NASA)](https://openstax.org/books/principles-economics-3e/pages/13-introduction-to-positive-externalities-and-public-goods?query=public%20good&target=%7B%22index%22%3A0%2C%22type%22%3A%22search%22%7D#fs-idm25531472)

## Part 2: The price system and the microeconomy

### Main chapters of part 2

7. Demand and supply curves
8. Price elasticity, income elasticity and cross elasticity of demand
9. Price elasticity of supply
10. The interaction of demand and supply
11. Consumer and producer surplus

### Chapter 7: Demand and supply curves

- effective demand
- demand
- law of demand
- demand curve
- demand schedule
- derived demand
- supply curve
- supply schedule
- change in quantity demanded
- extension in demand
- contraction in demand
- change in quantity supplied
- extension in supply
- contraction in supply
- change in demand
- composite demand
- normal good
- inferior good
- indirect taxes
- subsidy

<!-- A Choice between Consumption Goods -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/3108eb94a783d4e9fb3415caa8ff83a3c8d61aac" alt="A Choice between Consumption Goods" style="width:50%">
  <figcaption>Fig.X - A Choice between Consumption Goods José has income of $56. Movies cost $7 and T-shirts cost $14. The points on the budget constraint line show the combinations of affordable movies and T-shirts.</figcaption>
</figure>

<!-- How a Change in Income Affects Consumption Choices -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/894492c1efc968fec9cf4314a5873c6458259c0a" alt="How a Change in Income Affects Consumption Choices" style="width:50%">
  <figcaption>Fig.X - How a Change in Income Affects Consumption Choices The utility-maximizing choice on the original budget constraint is M. The dashed horizontal and vertical lines extending through point M allow you to see at a glance whether the quantity consumed of goods on the new budget constraint is higher or lower than on the original budget constraint. On the new budget constraint, Kimberly will make a choice like N if both goods are normal goods. If overnight stays is an inferior good, Kimberly will make a choice like P. If concert tickets are an inferior good, Kimberly will make a choice like Q.</figcaption>
</figure>

<!-- How a Change in Price Affects Consumption Choices -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/c1e0ab3e0970ee44ffd6a79e1918a5aec62f1636" alt="How a Change in Price Affects Consumption Choices" style="width:50%">
  <figcaption>Fig.X - How a Change in Price Affects Consumption Choices The original utility-maximizing choice is M. When the price rises, the budget constraint rotates clockwise. The dashed lines make it possible to see at a glance whether the new consumption choice involves less of both goods, or less of one good and more of the other. The new possible choices would be fewer baseball bats and more cameras, like point H, or less of both goods, as at point J. Choice K would mean that the higher price of bats led to exactly the same quantity of bat consumption, but fewer cameras. Theoretically possible, but unlikely in the real world, we rule out choices like L because they would mean that a higher price for baseball bats means a greater consumption of baseball bats.</figcaption>
</figure>

<!-- The Foundations of a Demand Curve: An Example of Housing -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/b6923db15fef4b4ba3daab72b3db124583bc108a" alt="The Foundations of a Demand Curve: An Example of Housing" style="width:50%">
  <figcaption>Fig.X - The Foundations of a Demand Curve: An Example of Housing (a) As the price increases from P0 to P1 to P2 to P3, the budget constraint on the upper part of the diagram rotates clockwise. The utility-maximizing choice changes from M0 to M1 to M2 to M3. As a result, the quantity demanded of housing shifts from Q0 to Q1 to Q2 to Q3, ceteris paribus. (b) The demand curve graphs each combination of the price of housing and the quantity of housing demanded, ceteris paribus. The quantities of housing are the same at the points on both (a) and (b). Thus, the original price of housing (P0) and the original quantity of housing (Q0) appear on the demand curve as point E0. The higher price of housing (P1) and the corresponding lower quantity demanded of housing (Q1) appear on the demand curve as point E1.</figcaption>
</figure>

<!-- Farmer’s Market  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/768a247caf0478bbf20c8020fac1c5cf6e0cee89" alt="Farmer’s Market " style="width:50%">
  <figcaption>Fig.X -   Farmer’s Market Organic vegetables and fruits that are grown and sold within a specific geographical region should, in theory, cost less than conventional produce because the transportation costs are less. That is not, however, usually the case. (Credit: modification of "Old Farmers' Market" by NatalieMaynor/Flickr, CC BY 2.0)</figcaption>
</figure>


[Weirdest Celebrity Items Sold At Auction: Britney Spears' Gum, Brad Pitt's Breath And More (Huffpost)](https://www.huffpost.com/entry/weirdest-celebrity-items-sold-at-auction_n_1791850)


<!-- A Demand Curve for Gasoline  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/a591b2927eda14cd9c8692084ac24c9074441bbb" alt="A Demand Curve for Gasoline" style="width:50%">
  <figcaption>Fig.X -   A Demand Curve for Gasoline The demand schedule shows that as price rises, quantity demanded decreases, and vice versa. We graph these points, and the line connecting them is the demand curve (D). The downward slope of the demand curve again illustrates the law of demand—the inverse relationship between prices and quantity demanded.</figcaption>
</figure>

<!-- A Supply Curve for Gasoline   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/dc5552e05117d2233339d960336aa4c2f8d25251" alt="A Supply Curve for Gasoline " style="width:50%">
  <figcaption>Fig.X - A Supply Curve for Gasoline The supply schedule is the table that shows quantity supplied of gasoline at each price. As price rises, quantity supplied also increases, and vice versa. The supply curve (S) is created by graphing the points from the supply schedule and then connecting them. The upward slope of the supply curve illustrates the law of supply—that a higher price leads to a higher quantity supplied, and vice versa.</figcaption>
</figure>

<!-- Shifts in Demand: A Car Example   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/a2858fa4eb32f68eb064c1650c9ba5af639fe314" alt="Shifts in Demand: A Car Example" style="width:50%">
  <figcaption>Fig.X -  Shifts in Demand: A Car Example Increased demand means that at every given price, the quantity demanded is higher, so that the demand curve shifts to the right from D0 to D1. Decreased demand means that at every given price, the quantity demanded is lower, so that the demand curve shifts to the left from D0 to D2.</figcaption>
</figure>


<!-- Demand Curve   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/7a1f445af14b01a553c20783d7631baead44e57a" alt="Demand Curve" style="width:50%">
  <figcaption>Fig.X -  Demand Curve We can use the demand curve to identify how much consumers would buy at any given price.</figcaption>
</figure>


<!-- Demand Curve with Income Increase  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/e03ad357b0f3dac3c54940c216906b99fec36263" alt="Demand Curve with Income Increase " style="width:50%">
  <figcaption>Fig.X -  Demand Curve with Income Increase With an increase in income, consumers will purchase larger quantities, pushing demand to the right.</figcaption>
</figure>


<!-- Demand Curve Shifted Right   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/a585d748d13ffb41a35c44fb859412e4f6c1a816" alt="Demand Curve Shifted Right " style="width:50%">
  <figcaption>Fig.X -  Demand Curve Shifted Right With an increase in income, consumers will purchase larger quantities, pushing demand to the right, and causing the demand curve to shift right.</figcaption>
</figure>


<!-- Factors That Shift Demand Curves   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/65c17584fc369f62c1977f893fd35d1abd563538" alt="Factors That Shift Demand Curves" style="width:50%">
  <figcaption>Fig.X -  Factors That Shift Demand Curves (a) A list of factors that can cause an increase in demand from D0 to D1. (b) The same factors, if their direction is reversed, can cause a decrease in demand from D0 to D1.</figcaption>
</figure>

<!-- Shifts in Supply: A Car Example   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/11a4586c6228af210af8f3a8bbdcebca25d7ea4f" alt="Shifts in Supply: A Car Example" style="width:50%">
  <figcaption>Fig.X -  Shifts in Supply: A Car Example Decreased supply means that at every given price, the quantity supplied is lower, so that the supply curve shifts to the left, from S0 to S1. Increased supply means that at every given price, the quantity supplied is higher, so that the supply curve shifts to the right, from S0 to S2.</figcaption>
</figure>


<!-- Supply Curve   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/1899990f90f0ad5efc0f580b649a618ae9b02af5" alt="Supply Curve" style="width:50%">
  <figcaption>Fig.X -  Supply Curve You can use a supply curve to show the minimum price a firm will accept to produce a given quantity of output.</figcaption>
</figure>


<!-- Setting Prices    -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/7e9369217e462de874c03fc34677917b564933d7" alt="Setting Prices " style="width:50%">
  <figcaption>Fig.X - Setting Prices The cost of production and the desired profit equal the price a firm will set for a product.</figcaption>
</figure>


<!--  Increasing Costs Leads to Increasing Price    -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/472e2358a4cfe78a26e23af812d1470445c13e1f" alt=" Increasing Costs Leads to Increasing Price" style="width:50%">
  <figcaption>Fig.X -  Increasing Costs Leads to Increasing Price Because the cost of production and the desired profit equal the price a firm will set for a product, if the cost of production increases, the price for the product will also need to increase.</figcaption>
</figure>


<!--  Supply Curve Shifts  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/e0f2ca47dbc3ca8f0943d4a3edf33e985f2a33ed" alt="Supply Curve Shifts" style="width:50%">
  <figcaption>Fig.X - Supply Curve Shifts When the cost of production increases, the supply curve shifts upwardly to a new price level.</figcaption>
</figure>


<!-- Factors That Shift Supply Curves   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/498e7d7cbc00a885faee08544c39cc295ac72c07" alt="Factors That Shift Supply Curves " style="width:50%">
  <figcaption>Fig.X - Factors That Shift Supply Curves (a) A list of factors that can cause an increase in supply from S0 to S1. (b) The same factors, if their direction is reversed, can cause a decrease in supply from S0 to S1. </figcaption>
</figure>




### Chapter 8: Price elasticity, income elasticity and cross elasticity of demand

- price elasticity of demand
- income elasticity of demand
- cross elasticity of demand (or cross-price elasticity of demand)
- substitute goods
- complementary goods
- perfectly inelastic
- inelastic
- unitary elasticity
- elastic
- perfectly elastic
- total revenue

<!--   On-Demand Media Pricing  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/d12176907a8d98b800db4a2317e8cd9d1e108bde" alt="On-Demand Media Pricing" style="width:50%">
  <figcaption>Fig.X - On-Demand Media Pricing Many on-demand Internet streaming media providers, such as Netflix, have introduced tiered pricing for levels of access to services, begging the question, how will these prices affect buyer’s purchasing choices? (Credit: modification of “160906_FF_CreditCardAgreements” by kdiwavvou/Flickr, Public Domain)</figcaption>
</figure>

<!--  Calculating the Price Elasticity of Demand -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/b5d88d1502a1f5326699d34bd79701f0ea6878e9" alt="Calculating the Price Elasticity of Demand" style="width:50%">
  <figcaption>Fig.X - Calculating the Price Elasticity of Demand We calculate the price elasticity of demand as the percentage change in quantity divided by the percentage change in price.</figcaption>
</figure>


### Chapter 9: Price elasticity of supply

- price elasticity of supply
- stocks
- perishability

<!-- Price Elasticity of Supply -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/9e2781c68f0707c6f7fce8bdae829f3965b904e3" alt="Price Elasticity of Supply" style="width:50%">
  <figcaption>Fig.X - Price Elasticity of Supply We calculate the price elasticity of supply as the percentage change in quantity divided by the percentage change in price.</figcaption>
</figure>

<!--  Infinite Elasticity -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/75e75c9367579225bb31ff1719c693add22895ca" alt=" Infinite Elasticity" style="width:50%">
  <figcaption>Fig.X -  Infinite Elasticity The horizontal lines show that an infinite quantity will be demanded or supplied at a specific price. This illustrates the cases of a perfectly (or infinitely) elastic demand curve and supply curve. The quantity supplied or demanded is extremely responsive to price changes, moving from zero for prices close to P to infinite when prices reach P.</figcaption>
</figure>

<!-- Zero Elasticity -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/ff50743942da09db765fc483531fcfdbc0313613" alt="Zero Elasticity" style="width:50%">
  <figcaption>Fig.X - Zero Elasticity The vertical supply curve and vertical demand curve show that there will be zero percentage change in quantity (a) demanded or (b) supplied, regardless of the price.</figcaption>
</figure>

<!-- A Constant Unitary Elasticity Demand Curve -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/9e571c5725473c6f41e69ab78ebe5cba674d1707" alt="A Constant Unitary Elasticity Demand Curve" style="width:50%">
  <figcaption>Fig.X -  A Constant Unitary Elasticity Demand Curve A demand curve with constant unitary elasticity will be a curved line. Notice how price and quantity demanded change by an identical percentage amount between each pair of points on the demand curve.</figcaption>
</figure>

<!--  A Constant Unitary Elasticity Supply Curve -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/88a69ad2b37ade499271f0d4c3f886f27b0301cb" alt=" A Constant Unitary Elasticity Supply Curve" style="width:50%">
  <figcaption>Fig.X - A Constant Unitary Elasticity Supply Curve A constant unitary elasticity supply curve is a straight line reaching up from the origin. Between each pair of points, the percentage increase in quantity supplied is the same as the percentage increase in price.</figcaption>
</figure>

<!--  Passing along Cost Savings to Consumers -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/275f0f84b10779198a2cbf3e4dbcf8c303242404" alt=" Passing along Cost Savings to Consumers" style="width:50%">
  <figcaption>Fig.X -  Passing along Cost Savings to Consumers Cost-saving gains cause supply to shift out to the right from S0 to S1; that is, at any given price, firms will be willing to supply a greater quantity. If demand is inelastic, as in (a), the result of this cost-saving technological improvement will be substantially lower prices. If demand is elastic, as in (b), the result will be only slightly lower prices. Consumers benefit in either case, from a greater quantity at a lower price, but the benefit is greater when demand is inelastic, as in (a).</figcaption>
</figure>

<!-- Passing along Higher Costs to Consumers -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/02911944317a54d9546fc07f565f147f1af7462f" alt="Passing along Higher Costs to Consumers" style="width:50%">
  <figcaption>Fig.X - Passing along Higher Costs to Consumers Higher costs, like a higher tax on cigarette companies for the example we gave in the text, lead supply to shift to the left. This shift is identical in (a) and (b). However, in (a), where demand is inelastic, companies largely can pass the cost increase along to consumers in the form of higher prices, without much of a decline in equilibrium quantity. In (b), demand is elastic, so the shift in supply results primarily in a lower equilibrium quantity. Consumers do not benefit in either case, but in (a), they pay a higher price for the same quantity, while in (b), they must buy a lower quantity (and presumably needing to shift their consumption elsewhere).</figcaption>
</figure>

<!-- Elasticity and Tax Incidence -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/b08cfd88b4611466a9bc7e9b82e0990e7d985304" alt="Elasticity and Tax Incidence" style="width:50%">
  <figcaption>Fig.X -  Elasticity and Tax Incidence An excise tax introduces a wedge between the price paid by consumers (Pc) and the price received by producers (Pp). The vertical distance between Pc and Pp is the amount of the tax per unit. Pe is the equilibrium price prior to introduction of the tax. (a) When the demand is more elastic than supply, the tax incidence on consumers Pc – Pe is lower than the tax incidence on producers Pe – Pp. (b) When the supply is more elastic than demand, the tax incidence on consumers Pc – Pe is larger than the tax incidence on producers Pe – Pp. The more elastic the demand and supply curves, the lower the tax revenue. </figcaption>
</figure>

<!-- How a Shift in Supply Can Affect Price or Quantity -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/992148af109c6342c9339b2572791461d0aa0dd0" alt="How a Shift in Supply Can Affect Price or Quantity" style="width:50%">
  <figcaption>Fig.X - How a Shift in Supply Can Affect Price or Quantity The intersection (E0) between demand curve D and supply curve S0 is the same in both (a) and (b). The shift of supply to the left from S0 to S1 is identical in both (a) and (b). The new equilibrium (E1) has a higher price and a lower quantity than the original equilibrium (E0) in both (a) and (b). However, the shape of the demand curve D is different in (a) and (b), being more elastic in (b) than in (a). As a result, the shift in supply can result either in a new equilibrium with a much higher price and an only slightly smaller quantity, as in (a), with more inelastic demand, or in a new equilibrium with only a small increase in price and a relatively larger reduction in quantity, as in (b), with more elastic demand. </figcaption>
</figure>


### Chapter 10: The interaction of demand and supply

- equilibrium 
- disequilibrium
- equilibrium price
- equilibrium quantity
- joint demand
- alternative demand
- derived demand
- joint supply
- rationing
- signalling
- price mechanism
- transmission of preferences
- incentivisation
- scarcity and choice



<!-- Good Weather for Salmon Fishing: The Four-Step Process   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/60b9a29b260b77e9a91e1ec966d41109f13bfe1f" alt="Good Weather for Salmon Fishing: The Four-Step Process" style="width:50%">
  <figcaption>Fig.X - Good Weather for Salmon Fishing: The Four-Step Process Unusually good weather leads to changes in the price and quantity of salmon.</figcaption>
</figure>


<!-- The Print News Market: A Four-Step Analysis   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/9490d677e30ecfd05e5a684e2b14eb022a5e2fc3" alt="The Print News Market: A Four-Step Analysis" style="width:50%">
  <figcaption>Fig.X -  The Print News Market: A Four-Step Analysis A change in tastes from print news sources to digital sources results in a leftward shift in demand for the former. The result is a decrease in both equilibrium price and quantity.</figcaption>
</figure>


<!--  Higher Compensation for Postal Workers: A Four-Step Analysis   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/05073030c65fdcc05e9705f210261b445ec69bbc" alt=" Higher Compensation for Postal Workers: A Four-Step Analysis" style="width:50%">
  <figcaption>Fig.X - Higher Compensation for Postal Workers: A Four-Step Analysis (a) Higher labor compensation causes a leftward shift in the supply curve, a decrease in the equilibrium quantity, and an increase in the equilibrium price. (b) A change in tastes away from Postal Services causes a leftward shift in the demand curve, a decrease in the equilibrium quantity, and a decrease in the equilibrium price.</figcaption>
</figure>


<!--  Combined Effect of Decreased Demand and Decreased Supply    -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/402c09fa371559c3c38d5545bdd136173a0e2f87" alt="Combined Effect of Decreased Demand and Decreased Supply " style="width:50%">
  <figcaption>Fig.X - Combined Effect of Decreased Demand and Decreased Supply Supply and demand shifts cause changes in equilibrium price and quantity.</figcaption>
</figure>


<!--  Shifts of Demand or Supply versus Movements along a Demand or Supply Curve -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/1f4d706301bc3e5b60dc501a6fa1ca5ab9f024eb" alt="Shifts of Demand or Supply versus Movements along a Demand or Supply Curve" style="width:50%">
  <figcaption>Fig.X -  Shifts of Demand or Supply versus Movements along a Demand or Supply Curve A shift in one curve never causes a shift in the other curve. Rather, a shift in one curve causes a movement along the second curve.</figcaption>
</figure>

<!--  Demand and Supply Curves -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/57c1e6c5d8548e4b0d193854c7b78d7b2b61114e" alt="Demand and Supply Curves" style="width:50%">
  <figcaption>Fig.X -   Demand and Supply Curves The figure displays a generic demand and supply curve. The horizontal axis shows the different measures of quantity: a quantity of a good or service, a quantity of labor for a given job, or a quantity of financial capital. The vertical axis shows a measure of price: the price of a good or service, the wage in the labor market, or the rate of return (like the interest rate) in the financial market. We can use the demand and supply curves explain how economic events will cause changes in prices, wages, and rates of return.</figcaption>
</figure>

<!--  Impact of Increasing Demand for Nurses 2020–2030  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/fa00e3d9bba2f37a1561388f37290d3fc9a25e9d" alt="Impact of Increasing Demand for Nurses 2020–2030 " style="width:50%">
  <figcaption>Fig.X - Impact of Increasing Demand for Nurses 2020–2030 In 2020, the median salary for nurses was $75,330. As demand for services increases, the demand curve shifts to the right (from D0 to D1) and the equilibrium quantity of nurses increases from Qe0 to Qe1. The equilibrium salary increases from Pe0 to Pe1.</figcaption>
</figure>

<!--   Impact of Decreasing Supply of Nurses between 2020 and 2030  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/0d30879730d653f4acb8eb7d9aca3fa007ea2fec" alt=" Impact of Decreasing Supply of Nurses between 2020 and 2030" style="width:50%">
  <figcaption>Fig.X -  Impact of Decreasing Supply of Nurses between 2020 and 2030 The increase in demand for nurses shown in Figure 4.10 leads to both higher prices and higher quantities demanded. As nurses retire from the work force, the supply of nurses decreases, causing a leftward shift in the supply curve and higher salaries for nurses at Pe2. The net effect on the equilibrium quantity of nurses is uncertain, which in this representation is less than Qe1, but more than the initial Qe0.</figcaption>
</figure>



### Chapter 11: Consumer and producer surplus

- consumer surplus
- producer surplus

<!-- Demand and Supply for Gasoline   -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/57f3b3c40b3db192b5e93cb8454cceb72f1fc1d9" alt="Demand and Supply for Gasoline" style="width:50%">
  <figcaption>Fig.X - Demand and Supply for Gasoline The demand curve (D) and the supply curve (S) intersect at the equilibrium point E, with a price of $1.40 and a quantity of 600. The equilibrium price is the only price where quantity demanded is equal to quantity supplied. At a price above equilibrium like $1.80, quantity supplied exceeds the quantity demanded, so there is excess supply. At a price below equilibrium such as $1.20, quantity demanded exceeds quantity supplied, so there is excess demand.</figcaption>
</figure>

<!-- Consumer and Producer Surplus -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/07416e1de46955bdc4cc7317fc83fc1d71bb8a9b" alt="Consumer and Producer Surplus" style="width:50%">
  <figcaption>Fig.X - Consumer and Producer Surplus The somewhat triangular area labeled by F shows the area of consumer surplus, which shows that the equilibrium price in the market was less than what many of the consumers were willing to pay. Point J on the demand curve shows that, even at the price of $90, consumers would have been willing to purchase a quantity of 20 million. The somewhat triangular area labeled by G shows the area of producer surplus, which shows that the equilibrium price received in the market was more than what many of the producers were willing to accept for their products. For example, point K on the supply curve shows that at a price of $45, firms would have been willing to supply a quantity of 14 million.</figcaption>
</figure>

<!--  Efficiency and Price Floors and Ceilings  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/6fce0024f9af1cfd314ff1261969037ea3ddb350" alt=" Efficiency and Price Floors and Ceilings " style="width:50%">
  <figcaption>Fig.X -  Efficiency and Price Floors and Ceilings (a) The original equilibrium price is $600 with a quantity of 20,000. Consumer surplus is T + U, and producer surplus is V + W + X. A price ceiling is imposed at $400, so firms in the market now produce only a quantity of 15,000. As a result, the new consumer surplus is T + V, while the new producer surplus is X. (b) The original equilibrium is $8 at a quantity of 1,800. Consumer surplus is G + H + J, and producer surplus is I + K. A price floor is imposed at $12, which means that quantity demanded falls to 1,400. As a result, the new consumer surplus is G, and the new producer surplus is H + I.
</figcaption>
</figure>


## Part 3: Government microeconomic intervention

### Main chapters of part 3

12. Reasons for government intervention in markets
13. Methods and effects of government intervention in markets
14. Addressing income and wealth inequality


### Chapter 12: Reasons for government intervention in markets

- market failure
- equality and equity
- non-provision of public goods
- overconsumption of demerit goods
- under consumption of merit goods
- controlling prices in markets

### Chapter 13: Methods and effects of government intervention in markets

- indirect tax
- excise duty
- impact of tax
- incidence of tax
- specific tax
- subsidy
- impact of a subsidy
- incidence of a subsidy
- direct provision of goods and services
- maximum price
- minimum price
- price stabilisation
- buffer stock

<!-- A Price Ceiling Example—Rent Control  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/3ab9f77c4631ab9876a0da3842ff58365f69257f" alt="A Price Ceiling Example—Rent Control " style="width:50%">
  <figcaption>Fig.X -  A Price Ceiling Example—Rent Control The original intersection of demand and supply occurs at E0. If demand shifts from D0 to D1, the new equilibrium would be at E1—unless a price ceiling prevents the price from rising. If the price is not permitted to rise, the quantity supplied remains at 15,000. However, after the change in demand, the quantity demanded rises to 19,000, resulting in a shortage.</figcaption>
</figure>

<!-- European Wheat Prices: A Price Floor Example  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/88d49e5ca66783c0b7a3543a10b0ec657ce53a80" alt="European Wheat Prices: A Price Floor Example" style="width:50%">
  <figcaption>Fig.X -  European Wheat Prices: A Price Floor Example The intersection of demand (D) and supply (S) would be at the equilibrium point E0. However, a price floor set at Pf holds the price above E0 and prevents it from falling. The result of the price floor is that the quantity supplied Qs exceeds the quantity demanded Qd. There is excess supply, also called a surplus.</figcaption>
</figure>


### Chapter 14: Addressing income and wealth inequality

- income
- wealth
- Gini coefficient
- minimum wage
- transfer payment

[complete this with CORE graphics]

## Part 4: The macroeconomy

### Main chapters of part 4

15. National income statistics
16. Introduction to the circular flow of income
17. Aggregate demand and aggregate supply analysis
18. Economic growth
19. Unemployment
20. Price stability



### Chapter 15: National income statistics

- national income
- gross domestic product (GDP)
- gross national income (GNI)
- net national income (NNI)
- at current market prices
- at constant prices
- GDP deflator
- exports
- imports
- depreciation (of capital)
- net domestic product (NDP)
- net national product (NNP)

### Chapter 16: Introduction to the circular flow of income

- circular flow of income
- closed economy
- open economy
- injection
- leakage (or withdrawal)

### Chapter 17: Aggregate demand and aggregate supply analysis

- aggregate demand (AD)
- aggregate supply (AS)

### Chapter 18: Economic growth

- economic growth
- business cycle (or trade cycle)
- labour-intensive production
- capital-intensive production

<figure>
<iframe src="https://ourworldindata.org/grapher/real-gdp-growth?time=2009" loading="lazy" style="width: 100%; height: 600px; border: 0px none;"></iframe>
</figure>

### Chapter 19: Unemployment

- unemployment
- unemployment rate
- labour force
- working population
- dependency ratio
- participation rate
- claimant count
- labour force survey

<!-- Labor market  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/ef0ab9fad84f8a6dd7dac20cd751abb6498b20a9" alt="Labor market" style="width:50%">
  <figcaption>Fig.X -  People often think of demand and supply in relation to goods, but labor markets, such as the nursing profession, can also apply to this analysis. (Credit: modification of "Hospital do Subúrbio" by Jaques Wagner Governador/Flickr Creative Commons, CC BY 2.0)</figcaption>
</figure>


<!-- Labor Market Example: Demand and Supply for Nurses in Minneapolis-St. Paul-Bloomington  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/c0b696ebdb7d2f68fcccb751c0cdf327af6b7b2b" alt="Labor Market Example: Demand and Supply for Nurses in Minneapolis-St. Paul-Bloomington" style="width:50%">
  <figcaption>Fig.X -   Labor Market Example: Demand and Supply for Nurses in Minneapolis-St. Paul-Bloomington The demand curve (D) of those employers who want to hire nurses intersects with the supply curve (S) of those who are qualified and willing to work as nurses at the equilibrium point (E). The equilibrium salary is $85,000 and the equilibrium quantity is 41,000 nurses. At an above-equilibrium salary of $90,000, quantity supplied increases to 45,000, but the quantity of nurses demanded at the higher pay declines to 40,000. At this above-equilibrium salary, an excess supply or surplus of nurses would exist. At a below-equilibrium salary of $75,000, quantity supplied declines to 34,000, while the quantity demanded at the lower wage increases to 47,000 nurses. At this below-equilibrium salary, excess demand or a shortage exists.</figcaption>
</figure>


<!-- Technology and Wages: Applying Demand and Supply  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/2a8eaafaf4246abd72a0daff0618e9a050ea4ad6" alt="Technology and Wages: Applying Demand and Supply" style="width:50%">
  <figcaption>Fig.X - Technology and Wages: Applying Demand and Supply (a) The demand for low-skill labor shifts to the left when technology can do the job previously done by these workers. (b) New technologies can also increase the demand for high-skill labor in fields such as information technology and network administration.</figcaption>
</figure>


<!-- A Living Wage: Example of a Price Floor -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/502b6ba4c069fdd35983691d21d055bfc1e26496" alt="A Living Wage: Example of a Price Floor" style="width:50%">
  <figcaption>Fig.X - A Living Wage: Example of a Price Floor The original equilibrium in this labor market is a wage of $10/hour and a quantity of 1,200 workers, shown at point E. Imposing a wage floor at $12/hour leads to an excess supply of labor. At that wage, the quantity of labor supplied is 1,600 and the quantity of labor demanded is only 700.</figcaption>
</figure>


### Chapter 20: Price stability

- inflation
- creeping inflation
- accelerating inflation
- hyperinflation
- deflation
- disinflation
- general price level
- cost of living
- consumer price index (CPI)
- sampling
- household expenditure
- weights
- base year
- nominal value
- real value
- demand-pull inflation
- monetary inflation
- cost-push inflation
- anticipated inflation
- unanticipated inflation
- imported inflation
- menu costs
- shoe leather costs
- fiscal drag
- stagflation

## Part 5: Government macroeconomic intervention

### Main chapters of part 5

21. Government macroeconomic policy objectives
22. Fiscal policy
23. Monetary policy
24. Supply-side policy

### Chapter 21: Government macroeconomic policy objectives

- inflation target
- unemployment
- economic growth
- equilibrium and disequilibrium


### Chapter 22: Fiscal policy

- fiscal policy
- government budget
- budget deficit
- budget surplus
- balanced budget
- national debt
- direct tax
- income tax
- indirect tax
- specific tax
- ad valorem tax
- progressive taxation
- regressive taxation
- proportional taxation
- flat-rate tax
- marginal tax rate
- average tax rate
- canons of taxation
- capital spending (or investment spending)
- current spending
- discretionary fiscal policy
- automatic stabilisers
- expansionary fiscal policy
- contractionary fiscal policy

### Chapter 23: Monetary policy

- monetary policy
- hot money
- quantitative easing
- open market operations
- expansionary monetary policy
- contractionary monetary policy

### Chapter 24: Supply-side policy

- supply-side policy
- productivity
- productive capacity
- labour productivity

## Part 6: International economic issues

### Main chapters of part 6

25. The reasons for international trade
26. Protectionism
27. Current account of the balance of payments
28. Exchange rates
29. Policies to correct imbalances in the current account of the balance of payments

### Chapter 25: The reasons for international trade

- absolute advantage
- comparative advantage
- trading possibility curve
- bilateral trade
- multilateral trade
- globalisation
- specialisation
- free trade
- trade liberalisation
- trade creation
- World Trade Organization
- terms of trade

### Chapter 26: Protectionism

- protectionism
- tariff
- import duty
- quota
- export subsidy
- exchange controls
- embargo
- voluntary export restraint (VER)
- infant industry argument
- sunrise industries
- sunset industries
- dumping

### Chapter 27: Current account of the balance of payments

- current account of the balance of payments
- balance of trade in goods account
- exports
- imports
- balance of trade in services account
- primary income
- net investment income
- secondary income
- current transfers
- deficit
- surplus
- external balance
- marginal propensity to import

### Chapter 28: Exchange rates

- exchange rate
- floating exchange rate
- depreciation
- appreciation

### Chapter 29: Policies to correct imbalances in the current account of the balance of payments

- stability of the current account
- fiscal policy and current account
- monetary policy and current account
- supply-side policy and current account
- protectionist policy




## Concept maps

```mermaid
graph LR
    PES[Price Elasticity of Supply]
    PES --> Definition
    PES --> Formula
    PES --> Determinants
    PES --> ElasticityRange[Range of Elasticity]
    PES --> Implications
    PES --> Applications
    PES --> GraphicalRepresentation[Graphical Representation]
    PES --> FactorsAffectingResponsiveness[Factors Affecting Responsiveness]
    
    Definition["Measure of responsiveness of quantity supplied to a change in price"]
    Formula --> FormulaCalc["PES = % Change in Quantity Supplied / % Change in Price"]
    Formula --> InfluenceTimePeriod["Influence of Time Period"]
    Determinants --> TimePeriodForProduction["Time period for production"]
    Determinants --> MobilityFactors["Mobility of Factors of Production"]
    Determinants --> AbilityToStore["Ability to Store Stocks"]
    Determinants --> SpareCapacity["Spare Capacity"]
    Determinants --> SpeedOfProduction["Speed of Production"]

    ElasticityRange --> PerfectlyInelastic["Perfectly Inelastic Supply (PES = 0)"]
    ElasticityRange --> InelasticSupply["Inelastic Supply (0 < PES < 1)"]
    ElasticityRange --> UnitaryElasticity["Unitary Elasticity (PES = 1)"]
    ElasticityRange --> ElasticSupply["Elastic Supply (PES > 1)"]
    ElasticityRange --> PerfectlyElastic["Perfectly Elastic Supply (PES = infinity)"]

    Implications --> ForProducers["For Producers"]
    Implications --> ForGovernment["For Government"]
    Implications --> ForEconomy["For Economy"]

    Applications --> Agriculture["Agriculture: Often inelastic"]
    Applications --> ManufacturedGoods["Manufactured Goods: More elastic"]
    Applications --> Services["Services: Varying elasticity"]

    GraphicalRepresentation --> SupplyCurves["Supply curves with different elasticities"]
    GraphicalRepresentation --> ShiftsVsMovements["Shifts vs. movements along the curve"]

    FactorsAffectingResponsiveness --> InnovationTechnology["Innovation and technology"]
    FactorsAffectingResponsiveness --> FactorSubstitution["Factor substitution possibilities"]
    FactorsAffectingResponsiveness --> MarketStructure["Market structure and competition"]
``````