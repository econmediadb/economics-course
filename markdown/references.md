# Notes ...

## Teaching, Doing, and Sharing Project Management ...

```mermaid
graph TD;
A[social innovation] --> |requires| B1[collaboration];
B1 --> |enhanced through| B2[project-based learning];
B2 --> C1[innovative communities];
C1 --> |strengthened through| C2[peer learning]; 
```

Randall, Daniel L., Jacquelyn C. Johnson, Richard E. West, and David A. Wiley. “Teaching, Doing, and Sharing Project Management in a Studio Environment: The Development of an Instructional Design Open-Source Project Management Textbook.” Educational Technology 53, no. 6 (2013): 24–28. 

**Permanent link:** http://www.jstor.org/stable/44430213.
https://a-z.lu/primo-explore/fulldisplay?docid=TN_cdi_jstor_primary_44430213&context=PC&vid=BIBNET&search_scope=All_content&tab=all_content&lang=fr_FR . 

**Keywords:** collaborative innovation, project-based learning, open-access textbook, creative thinking, free open educational resources (OER), no-cost open digital textbooks

