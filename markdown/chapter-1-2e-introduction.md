# Chapitre 1: Le fonctionnement des marchés

## Situation du chapitre par rapport au cours de l'enseignement secondaire classique

```mermaid
flowchart LR
  subgraph MARCHES-DES-BIENS-ET-SERVICES
    direction TB
    subgraph SCIENCES-ECO-3e
        direction TB
            A[L'économie en tant que <br> science sociale]-->B[Affirmations économiques <br> positives et normatives]
            B --> C[problème économique]
            C --> D[Frontières des <br> possibilités de production]
            D --> E[division du travail]
            E --> F[Économie de marché, <br> économie mixte et <br> économie planifiée]
    end
    direction LR
    subgraph MARCHES-2e
        direction TB
            c1[décision rationnelle]-->c2[demand et offre]
            c2-->c3[élasticité]
            c3-->c4[mécanisme des prix]
            c4 --> c5[surplus du <br> consommateur et <br> du producteur]
            c5 --> c6[impôts et subventions]
    end
    subgraph DEFAILLANCE-INTERVENTION-1e
        direction TB
            b1[défaillance du marché]-->b2[intervention publique]
    end
  end
  SCIENCES-ECO-3e --> MARCHES-2e
  MARCHES-2e --> DEFAILLANCE-INTERVENTION-1e
```

## Sommaire

1. La prise de décision rationnelle
   1. Le consommateur
        1.1. Les préférences et choix économiques
        1.2. La rationalité et l'utilité individuelle
        1.3. Les coûts d’opportunité
        1.4. La contrainte budgétaire
   2. Le producteur     
        2.1. La maximisation (perspective du producteur)
        2.2. Choix de la quantité à produire
2. La demande
    1. Déplacement *de* la courbe de demande
    2. Déplacement *sur* la courbe de demande
3. Elasticité
    1. élasticité-prix de la demande
    2. élasticité-prix croisée de la demande
4. L'offre
5. Détermination des prix  
6. Le surplus du consommateur et du producteur  
7. Exercices
8. Sources

## Objectifs du chapitre

1. Savoir interpréter des courbes d'offre et de demande ainsi que leurs pentes, et comprendre comment leur confrontation détermine l'équilibre sur un marché de type concurrentiel o`les agents sont preneurs de prix.

    1. Comprendre que la courbe de demande est une fonction décroissante du prix et représenter graphiquement une courbe de demande.

    1. Interpréter la pente de la courbe de demande.

    1. Comprendre pourquoi l’offre est une fonction croissante du prix et représenter graphiquement une courbe d’offre.

2. Savoir illustrer et interpréter les déplacements des courbes et sur les courbes, par différents exemples chiffrées.

    1. Montrer qu’une variation de l’offre (de la demande) indépendamment de la variation du prix provoque un déplacement de la courbe d’offre (de demande).

    2. Montrer qu’un déséquilibre se résorbe par la variation du prix et un déplacement sur les courbes d’offre et de demande.

3. Savoir déduire la courbe d'offre de la maximisation du profit par le producteur et comprendre qu'en situation de coût marginal croissant, le producteur produit la quantité qui permet d'égaliser le coût marginal et le prix.

4. Comprendre la notion de surplus du producteur et du consommateur.



