# En quoi le marché est-il une institution?

Beaucoup de grandes métropoles dans le monde se sont développées autour de places de marché et de bazars le long des anciennes routes commerciales, telles que la route de la soie entre la Chine et la Méditerranée. Dans le grand bazar d’Istanbul, l’un des plus grands et plus anciens marchés couverts au monde, les magasins vendant des tapis, de l’or, du cuir et des tissus sont regroupés dans différentes zones. Dans les villes et les villages médiévaux, il était courant que les fabricants et les vendeurs de certains produits installent leurs magasins proches les uns des autres, pour que les clients sachent où les trouver. La City de Londres est aujourd’hui un centre financier, mais des traces du commerce qui y avait cours autrefois perdurent grâce aux noms des rues : Pudding Lane (voie des Abats), Bread Street (rue du Pain), Milk Street (rue du Lait), Threadneedle Street (rue du Fil et des Aiguilles), Ropemaker Street (rue des Cordiers), Poultry Street (rue de la Volaille) et Silk Street (rue de la Soie).

Un marché est un moyen de réunir les agents économiques susceptibles d!avoir un intérêt à l’échange d!un bien ou d!un service (ou d!un facteur de production) en tant qu!acheteurs (demandeurs) ou vendeurs (offreurs). Des institutions marchandes posent les règles de l’échange qui s!imposent aux demandeurs et aux offreurs. L’économiste entend par institutions non seulement des organisations, mais avant tout des règles et des pratiques acceptées par tous. Certaines objections morales à l’échange de biens contre de l!argent, qui peuvent diverger d’un pays à l’autre, expliquent pourquoi certains biens et services ne sont
pas attribués par le biais des marchés (exemple : les produits du corps humain, comme les gamètes, le sang, les organes).

Chaque fois que vous vous engagez à payer une certaine somme d!argent à un vendeur en échange d!un bien, par exemple une paire de chaussures, vous concluez implicitement un contrat avec lui. Lorsque vous rentrez chez vous, si vous vous apercevez qu’elle présente un défaut, vous pouvez la faire remplacer ou être remboursé : des règles juridiques vous protègent. Le droit, un ensemble de règles déterminées par les pouvoirs publics, encadre l’échange sur le marché.


![Modèle simple pour une rédaction structuré.](https://gitlab.com/econmediadb/economics-course/-/raw/main/mindmap/018-Structured-Writing-Simple-Model-Institution-FR.png)


## Sources

- [CORE L'Economie](https://www.core-econ.org/the-economy/book/fr/text/08.html#82-march%C3%A9-et-prix-d%C3%A9quilibre) 
