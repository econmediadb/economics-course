# Chapitre 1: Le fonctionnement des marchés



```
Situer le chapitre 1 (2e) par rapport au cours de l'enseignement secondaire classique
```

```mermaid
flowchart LR
  subgraph MARCHES-DES-BIENS-ET-SERVICES
    direction TB
    subgraph SCIENCES-ECO-3e
        direction TB
            A[L'économie en tant que <br> science sociale]-->B[Affirmations économiques <br> positives et normatives]
            B --> C[problème économique]
            C --> D[Frontières des <br> possibilités de production]
            D --> E[division du travail]
            E --> F[Économie de marché, <br> économie mixte et <br> économie planifiée]
    end
    direction LR
    subgraph MARCHES-2e
        direction TB
            c1[décision rationnelle]-->c2[demand et offre]
            c2-->c3[élasticité]
            c3-->c4[mécanisme des prix]
            c4 --> c5[surplus du <br> consommateur et <br> du producteur]
            c5 --> c6[impôts et subventions]
    end
    subgraph DEFAILLANCE-INTERVENTION-1e
        direction TB
            b1[défaillance du marché]-->b2[intervention publique]
    end
  end
  SCIENCES-ECO-3e --> MARCHES-2e
  MARCHES-2e --> DEFAILLANCE-INTERVENTION-1e
```


```
Sommaire:

1. La prise de décision rationnelle
   1. Le consommateur
        1.1. Les préférences et choix économiques
        1.2. La rationalité et l'utilité individuelle
        1.3. Les coûts d’opportunité
        1.4. La contrainte budgétaire
   2. Le producteur     
        2.1. La maximisation (perspective du producteur)
        2.2. Choix de la quantité à produire
2. La demande
    1. Déplacement *de* la courbe de demande
    2. Déplacement *sur* la courbe de demande
3. Elasticité
    1. élasticité-prix de la demande
    2. élasticité-prix croisée de la demande
4. L'offre
5. Détermination des prix  
6. Le surplus du consommateur et du producteur  
7. Exercices
8. Sources
```

````
Objectifs du chapitre:

1. Savoir interpréter des courbes d'offre et de demande ainsi que leurs pentes, et comprendre comment leur confrontation détermine l'équilibre sur un marché de type concurrentiel o`les agents sont preneurs de prix.

    1.a. Comprendre que la courbe de demande est une fonction décroissante du prix et représenter graphiquement une courbe de demande.

    1.b. Interpréter la pente de la courbe de demande.

    1.c. Comprendre pourquoi l’offre est une fonction croissante du prix et représenter graphiquement une courbe d’offre.

2. Savoir illustrer et interpréter les déplacements des courbes et sur les courbes, par différents exemples chiffrées.

    2.a. Montrer qu’une variation de l’offre (de la demande) indépendamment de la variation du prix provoque un déplacement de la courbe d’offre (de demande).

    2.b. Montrer qu’un déséquilibre se résorbe par la variation du prix et un déplacement sur les courbes d’offre et de demande.

3. Savoir déduire la courbe d'offre de la maximisation du profit par le producteur et comprendre qu'en situation de coût marginal croissant, le producteur produit la quantité qui permet d'égaliser le coût marginal et le prix.

4. Comprendre la notion de surplus du producteur et du consommateur.



````


## 1.1. La prise de décision rationnelle

**Mots-clés** de la section 1.1 : *préférence, choix économique, modèle, ceteris paribus, choix entre temps libre et travail, utilité, coûts d’opportunité, contrainte budgétaire, coûts de production, raisonnement à la marge, choix de la quantité à produire, ...*

### 1.1.1. Les préférences et choix économiques

<!-- mots-clés de la section 1.1 :  -->

Le choix quotidien d'une élève consiste dans le choix du nombre d'heures que vous passez à étudier. Les facteurs qui influencent ce choix sont :
  - l'intérêt pour la matière
  - la difficulté de la matière
  - les nombres d'heures que vos amis passent à étudier
  - la conviction que plus vous passez de temps à étudier, plus vos notes seront élevées
  - ...    

*Construction d'un modèle simple de choix de nombre d’heures travaillées par un élève.*

Le modèle est fondé sur l’hypothèse que la note finale sera d’autant plus élevée que le temps consacré à étudier sera important. 

Nous supposons qu'il existe une relation positive entre le nombre d’heures travaillées et la note finale. Il faut donc vérifier si cette hypothèse est vérifiée dans les faits.

Un groupe de psychologues de l’éducation a étudié le comportement de 84 étudiants de l’université d’État de Floride afin d’identifier les facteurs ayant affecté leurs performances.

Après avoir pris en compte l’environnement et d’autres facteurs les psychologues ont estimé qu’une heure supplémentaire passée à étudier chaque semaine augmentait la moyenne générale à la fin du semestre de 0,24 point en moyenne.


Un élève peut faire varier le nombre d’heures qu’il passe à étudier. Dans ce modèle, le temps passé à étudier fait référence à la totalité du temps que l’élève consacre à son apprentissage, que ce soit en classe ou individuellement, quotidiennement (et non par semaine, comme c’était le cas pour les étudiants en Floride). La relation entre le nombre d’heures passées à étudier et la note finale est représentée dans les colonnes du Tableau 1.1.

<!--Tableau 2.2.-->

**Tableau 1.1** Comment le temps passé à étudier affecte-t-il la note de l'élève ? 
| Heures passées à étudier | 0 | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9  | 10 | 11 | 12 | 13 | 14 | 15 ou plus |
|--------------------------|---|----|----|----|----|----|----|----|----|----|----|----|----|----|----|------------|
| Note                     | 0 | 22 | 33 | 42 | 50 | 57 | 63 | 69 | 74 | 78 | 81 | 84 | 86 | 88 | 89 | 90         |

En réalité, la note finale peut aussi être affectée par des éléments imprévisibles. Dans la vie de tous les jours, nous rassemblons ces événements sous le terme de « chance ». Pour ce faire, nous utilisons l’expression « en gardant les autres choses constantes », ou, plus souvent, l’expression latine, ceteris paribus. La signification littérale de l’expression est « toutes choses égales par ailleurs ». Dans un modèle économique, elle signifie que l’analyse « garde les autres facteurs inchangés ».

La décision dépend des préférences de l'élève – c’est-à-dire de ce qui lui importe. Si l'élève en question ne se préoccupait que de ses notes, il étudierait 15 heures par jour. Mais, l'élève souhaite aussi avoir du temps libre – il aime dormir, sortir et aussi regarder la télévision.

---

#### APPLICATION

Imaginons maintenant une fermière autosuffisante, qui produit des céréales qu’elle mange et ne vend à personne. Son choix est cependant contraint : pour produire des céréales, il faut travailler. Le Tableau 1.2 indique comment le montant de céréales produit dépend du nombre d’heures travaillées chaque jour.

<!-- Tableau 2.3 -->

**Tableau 1.2** Comment le temps passé à travailler affecte-t-il la production de la fermière ?
| Heures travaillées | 0 | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9  | 10 | 11 | 12 | 13 | ... | 18 | 24 |
|--------------------------|---|----|----|----|----|----|----|----|----|----|----|----|----|----|----|------------| ---- |
| Céréales                     | 0 | 9 | 18 | 26 | 33 | 40 | 46 | 51 | 55 | 58 | 60 | 62 | 64 | 66 | ... | 69         | 71 |

La fermière choisit son nombre d’heures de travail. Par exemple, si elle travaille 12 heures par jour, elle produira 64 unités de céréales. Nous faisons l’hypothèse que si elle produit trop peu de céréales, elle mourra de faim. 

*Pour quelle raison ne peut-elle pas produire le plus de céréales possible ?*

Tout comme l'élève, la fermière accorde de la valeur au temps libre : elle doit faire un choix entre consommer des céréales et consommer du temps libre.

---

---
EXERCICE :  Hypothèses ceteris paribus

On vous a demandé de conduire dans votre lycée une étude similaire à celle menée à l’université d’État de Floride.

 1. Hormis l’environnement de travail, quels facteurs selon vous devraient être maintenus constants dans un modèle portant sur la relation entre les heures passées à étudier et la note finale ?
 2. Outre les conditions pour étudier (environnement de travail), quelles autres informations voudriez-vous collecter sur les lycéens ?

---

### 1.1.2. La rationalité et l'utilité individuelle

Une journée compte 24 heures. L'élève doit diviser ce temps entre ses études (le temps qu’il passe à apprendre) et son temps libre (c’est-à-dire le reste de son temps). Nous illustrons ses préférences dans le Graphique 2.2, représentant le temps libre en abscisse et la note finale en ordonnée.


<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/images/web/graphique-02-02.jpg" />
    Graphique 2.2. Préférences de l'élève.

Le temps libre fait référence au temps qui n’est pas passé à étudier. Chaque point du graphique représente une combinaison différente de temps libre et d’une note finale.

Pour décrire les préférences, il n’y a pas besoin de connaître l’utilité exacte de chaque option ; nous avons seulement besoin de connaître quelles combinaisons procurent une utilité supérieure ou inférieure à celle apportée par les autres. Dans notre modèle des préférences d’un lycéen, les biens sont « la note finale » et « le temps libre ». Dans d’autres modèles, il s’agira souvent de biens de consommation, comme la nourriture ou les vêtements, et nous appelons l’individu un consommateur.

Pour comprendre le comportement économique, nous avons besoin de connaître les préférences des gens. En économie, nous nous figurons les personnes comme prenant des décisions selon leurs préférences, ce par quoi nous désignons les goûts, les dégoûts, les attitudes, les sentiments et les croyances qui les animent et les motivent.


---

APPLICATION

Supposons que :

- L'élève préfère plus de temps libre à moins de temps libre : les combinaisons A et B donnent toutes les deux une note de 84, mais elle préférera A car la combinaison donne plus de temps libre.
- L'élève préfère une note élevée à une note faible : avec les combinaisons C et D, elle a 20 heures de temps libre par jour, mais elle préfère D car cela lui donne une note plus élevée.

Comparons toutefois les points A et D dans **Graphique 2.2**. 

*L'élève préférerait-elle D (note faible et beaucoup de temps libre) ou A (note élevée et moins de temps libre) ?*
Une manière de le découvrir serait de lui demander.    

Supposons qu’elle se déclare indifférente entre A et D, ce qui implique qu’elle tirerait la même satisfaction des deux résultats. Nous disons alors que ces deux résultats lui *procurent la même utilité*. Et nous savons qu’il préfère A à B, donc que B lui procure une utilité moindre que A ou D.

<!-- Tableau 2.4 -->
Tableau 1.3 Représentation des préférences de l'élève. 
|                       | A  | E  | F  | G  | H  | D  |
|-----------------------|----|----|----|----|----|----|
| Heures de temps libre | 15 | 16 | 17 | 18 | 19 | 20 |
| Note finale           | 84 | 75 | 67 | 60 | 54 | 50 |

---

---
VIDEO : Economie expérimentale

[Dans notre vidéo Économiste en action «  Invisible hands working together », Juan Camilo Cárdenas parle de son usage innovant de l’économie expérimentale dans des situations de la vie réelle.](https://tinyurl.com/se37rca)

---



---
(option) EXERCICE : Quelles sont vos préférences ?

[lien](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/text/02.html#24-la-rationalit%C3%A9-et-lutilit%C3%A9-individuelle)

---

---

(option) EXERCICE : Préférences

[lien](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/text/02.html#24-la-rationalit%C3%A9-et-lutilit%C3%A9-individuelle)

---

### 1.1.3. Les coûts d’opportunité

Revenons maintenant au problème de l'élève, qui doit arbitrer entre ses notes et son temps libre. Cette fois-ci nous montrerons comment la note finale dépend de la quantité de temps libre plutôt que du temps passé à étudier. Le Tableau 1.4 montre la relation entre sa note finale et le nombre d’heures de temps libre par jour — soit l’image inversée du Tableau 1.1.

<!-- Tableau 2.5 -->
Tableau 1.4 Comment le temps passé à étudier affecte-t-il la note de l'élève ? 
| Heures de temps libre par jour | 24 | 23 | 22 | 21 | 20 | 19 | 18 | 17 | 16 | 15 | 14 | 13 | 12 | 11 | 10 | 9 ou moins |
|--------------------------------|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|------------|
| Note                           | 0  | 20 | 33 | 42 | 50 | 57 | 63 | 69 | 74 | 78 | 81 | 84 | 86 | 88 | 89 | 90         |

Si l'élève travaille pendant 24 heures, cela signifie qu’il n’a pas de temps libre et que sa note finale est de 90. S’il choisit d’avoir 24 heures de temps libre par jour, il aura une note de zéro. Une autre façon d’exprimer cela est de dire que le temps libre a un **coût d’opportunité** : pour avoir plus de temps libre, l'élève doit renoncer à l’opportunité d’avoir une note plus élevée.

Le temps libre a un coût d’opportunité, sous la forme d’une perte de points de sa note. Cela représente l’arbitrage qu’il doit faire entre la note et le temps libre.

````
coût d’opportunité du capital : La quantité de revenu qu’un investisseur aurait pu obtenir en investissant l’unité de capital ailleurs
````


### 1.1.4. La contrainte budgétaire

Comme pour la fermière, nous allons réfléchir en termes de consommation et de temps libre, par jour et en moyenne, pour introduire la notion de contrainte budgétaire. Nous supposons que les dépense de la fermière – c’est-à-dire sa consommation moyenne de nourriture, de logement et d’autres biens et services – ne peuvent pas excéder ses revenus (donc, par exemple, elle ne peut pas emprunter pour augmenter sa consommation) :

    Consommation = salaire x (24−temps libre)

Nous appellerons cette équation la *contrainte budgétaire*, parce qu’elle permet de calculer ce que la fermière peut se permettre d’acheter.

<!-- Tableau 2.6a -->
La fermière s'attend à gagner en moyenne un salaire de 15 euros par heure. Dans les colonnes du Tableau 1.5a nous avons calculé votre temps libre pour des heures de travail variant entre 0 et 16 heures par jour, et votre consommation maximale, quand votre salaire horaire est de 15 euros. L’équation de la contrainte budgétaire est :

    Consommation = 15 x (24−temps libre)

<!-- Tableau 2.6a -->
Tableau 1.5a Comment le temps passé à travailler affecte-t-il la consommation quotidienne ? 
| Heures de travail | Temps libre | Consommation |
|:-----------------:|------------:|-------------:|
| 0                 | 24          | 0 €          |
| 2                 | 22          | 30 €         |
| 4                 | 20          | 60 €         |
| 6                 | 18          | 90 €         |
| 8                 | 16          | 120 €        |
| 10                | 14          | 150 €        |
| 12                | 12          | 180 €        |
| 14                | 10          | 210 €        |
| 16                | 8           | 240 €        |



Les emplois diffèrent par le nombre d’heures qu'on passe à travailler — *quel serait donc le nombre d’heures travaillées idéal ?*

Tout comme l'exemple de l'élève, la fermière cherche un équilibre entre deux arbitrages :

- Le montant de consommation qu'elle est désireuse d’échanger contre une heure de temps libre.
- Le montant de consommation qu'elle peut obtenir en renonçant à une heure de temps libre, qui est égal au salaire.

Prenons maintenant la situation suivante. La fermière obtient une aide correspondant à un revenu quotidien de 50 euros. La fermière réalise immédiatement que cela modifiera son choix d’emploi.

<!-- Tableau 2.6b -->
La nouvelle situation est décrite dans le Tableau 1.5b : pour chaque niveau de temps libre, le revenu total est supérieur de 50 euros au niveau précédent. La contrainte budgétaire de la fermière est maintenant :

    Consommation = 15x(24−temps libre) + 50

Tableau 1.5b Comment le temps passé à travailler affecte-t-il la consommation quotidienne ? 
| Heures de travail | Temps libre | Consommation |
|-------------------|-------------|--------------|
| 0                 | 24          | 50 €         |
| 2                 | 22          | 80 €         |
| 4                 | 20          | 110 €        |
| 6                 | 18          | 140 €        |
| 8                 | 16          | 170 €        |
| 10                | 14          | 200 €        |
| 12                | 12          | 230 €        |
| 14                | 10          | 260 €        |
| 16                | 8           | 290 €        |

Remarquez que le revenu additionnel de 50 euros ne change pas le coût d’opportunité du temps : chaque heure de temps libre supplémentaire réduit toujours la consommation de 15 euros (le salaire).

Une année plus tard, la chance tourne pour la fermière : les céréales qu'elle produit sont achetées à un prix supérieur sur le marché. Ceci augmente son salaire de 10 euros par heure, et ceci laisse la possibilité de repenser les heures qu'elle travaille. La contrainte budgétaire de la fermière est désormais égale à :

    Consommation = 25x(24−temps libre)

<!-- Tableau 2.6c -->
Dans le Tableau 1.5c, vous pouvez voir comment la contrainte budgétaire évolue lorsque le salaire augmente.

Tableau 1.5c Comment le temps passé à travailler affecte-t-il la consommation quotidienne ? 
| Heures de travail | Temps libre | Consommation |
|-------------------|-------------|--------------|
| 0                 | 24          | 0 €          |
| 2                 | 22          | 50 €         |
| 4                 | 20          | 100 €        |
| 6                 | 18          | 150 €        |
| 8                 | 16          | 200 €        |
| 10                | 14          | 250 €        |
| 12                | 12          | 300 €        |
| 14                | 10          | 350 €        |
| 16                | 8           | 400 €        |

--- 
APPLICATION

*Comparez les résultats dans le Tableau 1.5a et le Tableau 1.5c.*

Avec 24 heures de temps libre (et pas de travail), la  consommation serait de 0, quel que soit le salaire. Mais pour chaque heure de temps libre à laquelle elle renonce, la consommation augmente désormais de 25 euros, au lieu de 15 euros précédemment.

---


### 1.1.5. La maximisation et le raisonnement à la marge

Dans cette section, nous allons étudier la manière dont les entreprises qui fabriquent des produits différenciés fixent un prix et choisissent la quantité à produire afin de maximiser leurs profits. Cela dépend de la demande qui lui est adressée, c’est-à-dire de la disposition des clients potentiels à payer pour le produit, et des coûts de production.

Nous allons prendre l’exemple d’une petite entreprise qui fabrique des voitures et que nous appellerons *Beautiful Cars*.

#### 1.1.5.1. Les coûts de production

Si l'on prend les coûts de fabrication et de vente des voitures, nous pouvons citer :
- locaux (une usine) équipés de machines pour le moulage, 
- le forgeage, le montage et le soudage des carrosseries
- achat des matières premières et des composants 
- rémunération des ouvriers qui font fonctionner les équipements
- rémunération des salariés pour gérer les processus d’achat, de production et pour vendre les voitures

L’une des composantes du coût de production des voitures est le montant à verser aux propriétaires de l’entreprise (les actionnaires) afin de couvrir le coût d’opportunité du capital.

En général, les actionnaires n’investiront pas s’ils peuvent faire un meilleur usage de leur argent en investissant et en réalisant des profits ailleurs. Ce qu’ils pourraient recevoir ailleurs, pour chaque euro investi, est un autre exemple de coût d’opportunité, appelé ici coût d’opportunité du capital.

**Les coûts fixes**

Certains coûts ne varient pas avec le nombre de voitures. On les appelle *coûts fixes*. Suivez les étapes de l’analyse du Graphique 2.3 pour voir que plus il y a de voitures produites, plus les coûts totaux (les coûts fixes et les coûts variables, comme le nombre d’heures travaillées à rémunérer aux employés) sont élevés.

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/images/web/graphique-02-03-b.jpg" />
    Graphique 2.3 Coût total de production.

**Les coûts totaux (croissants)**

Les coûts totaux augmentent avec la quantité d’unités produites, en partie parce que l’entreprise a besoin d’employer plus d’ouvriers. 

Suivez les étapes de l’analyse du Graphique 2.4 pour voir que, à partir du coût total de production, nous avons déterminé le coût moyen d’une voiture, et la manière dont il varie avec la production. La partie supérieure du Graphique 2.4 montre la manière dont les coûts totaux dépendent de la quantité de voitures produites chaque jour. La variation du coût moyen est présentée sur la partie inférieure.

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/images/web/graphique-02-04-d.jpg" />
    Graphique 2.4 Beautiful Cars : variation du coût total et moyen.

**Les coûts moyens**

Nous pouvons calculer le coût moyen pour chaque nombre de voitures pour représenter la variation du coût moyen d’une voiture produite du graphique du bas.

Nous pouvons voir sur le Graphique 2.4 que les coûts moyens de Beautiful Cars sont décroissants pour de faibles niveaux de production (jusqu’à 40 voitures produites). Pour des niveaux de production élevés (plus de 40 voitures produites), les coûts moyens augmentent. Cela peut être dû au fait que l’entreprise doit augmenter le nombre de roulements quotidiens des équipes sur la chaîne de montage. Elle doit peut-être aussi payer des heures supplémentaires et l’équipement peut tomber en panne plus fréquemment lorsque la chaîne de montage fonctionne plus longtemps.

**Le coût marginal**

Le coût marginal est le coût de production additionnel dû à une unité supplémentaire. Suivez les étapes de l’analyse du Graphique 2.5 pour voir que la production des Beautiful Cars présente des coûts marginaux croissants.

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/images/web/graphique-02-05-c.jpg" alt="Coût total de production"/>
    Graphique 2.5 Coût marginal d’une voiture.


Si nous calculons le coût marginal à chaque niveau de production de Beautiful Cars, nous pouvons représenter la variation du coût marginal.

#### 1.1.5.2. La courbe de demande

Afin de fixer un prix, une entreprise a besoin d’informations concernant la demande, notamment le nombre de clients potentiels prêts à payer pour son produit. Pour un modèle simple de la demande de Beautiful Cars, imaginez qu’il y ait 100 clients potentiels qui achèteraient chacun une Beautiful Cars aujourd’hui, si le prix était assez bas. Supposez que nous ordonnons sur une droite l’ensemble des clients potentiels par ordre décroissant de leur disposition à payer, et que nous représentons graphiquement comment la disposition à payer varie le long de cette droite (Graphique 2.6). Alors, pour tout niveau de prix, disons égal à 3 200 euros, le graphique montre le nombre de clients dont la disposition à payer serait supérieure ou égale au prix. Dans ce cas, 60 clients sont prêts à payer 3 200 euros ou plus, donc la demande de voitures pour un prix de 3 200 euros est 60.

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/images/web/graphique-02-06.jpg" />
    Graphique 2.6 Demande de voitures (par jour).

Les courbes de demande sont souvent dessinées sous la forme de lignes droites, comme dans cet exemple, bien qu’il n’y ait aucune raison de penser qu’elles soient droites en réalité : nous avons bien vu que la courbe de demande pour les Cheerios pomme-cannelle n’était pas droite. En revanche, nous pouvons nous attendre à ce que les courbes de demande soient descendantes : lorsque le prix augmente, la quantité demandée par les consommateurs diminue. Réciproquement, quand la quantité disponible est faible, le produit peut être vendu à un prix élevé.

Si le prix d’une voiture Beautiful Cars est élevé, la demande sera faible, car seuls les consommateurs qui préfèrent Beautiful Cars aux autres marques l’achèteront. Si le prix baisse, davantage de consommateurs seront attirés par Beautiful Cars, ceux-là mêmes qui auraient sûrement acheté une Ford ou une Volvo sinon.


### 1.1.6. Choix de la quantité à produire ? 

<!-- L’égalisation entre coût marginal et recette marginale -->

Quel est le meilleur choix de prix et de quantité pour le producteur ? Rappelez-vous que les recettes sont données par le prix de vente multiplié par la quantité vendue (Graphique 2.6). La recette marginale correspond à l’augmentation des recettes induite par l’augmentation de la quantité produite. Le Tableau 1.6 montre comment calculer la recette marginale avec 20 voitures produites, c’est-à-dire l’augmentation des recettes lorsque la quantité passe de 20 à 21 voitures produites.

Tableau 1.6 Calcul de la recette marginale. 
| Quantité                     | Prix                     | Recette                                                                           |
|------------------------------|--------------------------|-----------------------------------------------------------------------------------|
| Quantité = 20                | Prix = 6.400 €           | Recette = 128.000 €                                                               |
| Quantité = 21                | Prix = 6.320 €           | Recette = 132.720 €                                                               |
| Variation de la quantité = 1 | Variation du prix = 80 € | Recette marginale = 4.720 € |

<!-- = variation de la recette / variation de la quantité -->

*Gain de recettes* (21e voiture) : 	6 320 € <br>
*Perte de recettes* (80 € sur chacune des 20 autres voitures) :	-1 600 € <br>
*Recette marginale* 	4 720 €

Quand la production passe de 20 à 21 voitures, les recettes varient pour deux raisons. Une voiture supplémentaire est vendue, mais, étant donné que le nouveau prix de vente est plus faible avec 21 voitures produites, il y a aussi une perte de 80 euros sur chacune des 20 autres voitures vendues au nouveau prix. Les recettes marginales correspondent à l’effet net de ces deux changements.

Sur le Tableau 1.7, nous trouvons la variation des recettes marginales, et l’utilisons pour trouver le point correspondant à un profit maximal. Les deux premières colonnes décrivent la courbe de demande et la troisième colonne décrit la variation du coût marginal. Vous pouvez voir la manière dont le profit varie en fonction de la production dans la dernière colonne du Tableau 1.7.

Tableau 1.7 Recette marginale, coût marginal et profit. 
| Prix  | Quantité | Coût <br> marginal | Recette <br> marginale | Profit  |
|-------|---------:|:-------------:|------------------:|--------:|
| 8.000 | 0        | 1.000         | 8.000             | -48.000 |
| 7.200 | 10       | 1.600         | 6.413             | 11.063  |
| 6.400 | 20       | 2.200         | 4.825             | 48.250  |
| 5.600 | 30       | 2.800         | 3.238             | 63.563  |
| 4.800 | 40       | 3.400         | 1.650             | 57.000  |
| 4.000 | 50       | 4.000         | 63                | 28.563  |
| 3.200 | 60       | 4.600         | -1.525            | -21.750 |

Les trois dernières colonnes dans le Tableau 2.8 suggèrent que le point maximisant les profits se trouve là où la recette marginale égalise le coût marginal. Pour comprendre pourquoi, souvenez-vous que le profit est la différence entre les recettes et les coûts, de sorte que, pour n’importe quel niveau de production, la variation des profits lorsque la production augmente d’une unité (le profit marginal) est égale à la différence entre la variation des recettes et la variation des coûts :

    profit = recette totale − coût total

    profit marginal = recette marginale − coût marginal

Ainsi :

- Si la recette marginale est supérieure au coût marginal, l’entreprise pourrait augmenter son profit en augmentant la production.
- Si la recette marginale est inférieure au coût marginal, le profit marginal est négatif. Il serait préférable de diminuer la production.

---
APPLICATION

i) *Construire un graphique avec quantité produite à l'abscisse et le montant (coût marginal et recette marginale) à l'ordonnée.*

INSERT TEMPLATE FOR GRAPH HERE

ii) *Quel est la quantité optimale à produire dans l'exemple du Tableau 1.7?*


 - Quand la production est inférieure à 32, la recette marginale est supérieure au coût marginal : le profit marginal est positif, donc le profit augmente avec la production.
 - Quand la production est supérieure à 32, la recette marginale est inférieure au coût marginal : le profit marginal est négatif, donc le profit diminue avec la production.
 - Quand la production est égale à 32, la recette marginale est égale au coût marginal : le niveau de profit atteint son maximum.

iii) Quel est le prix et la quantité qui maximise le profit?

Le prix et la quantité maximisant les profits sont 5 440 euros et 32 voitures, correspondant à un profit de 63 360 euros. 

iv) Calculez le profit de l'entreprise.

Rappelez-vous que le profit de l’entreprise est la différence entre ses recettes et ses coûts totaux :

    profit = recette totale − coût total 
           = prix x quantité − coût total

De manière équivalente, le profit est le nombre d’unités produites multiplié par le profit par unité, qui est la différence entre le prix et le coût moyen.

    profit = quantité(prix−coût total quantité)
           = quantité(prix−coût moyen)

Cette formule correspond au profit économique. Le profit économique est le profit supplémentaire par rapport au rendement minimal exigé par les actionnaires, qui est appelé le profit normal. Rappelez-vous que les coûts de production prennent en compte le coût d’opportunité du capital (les paiements faits aux actionnaires pour les inciter à prendre des parts). Si le prix est égal au coût moyen, le profit économique de l’entreprise est zéro.

---


Le succès d’une entreprise ne dépend pas seulement de la détermination du bon prix. Le choix du produit et sa capacité à attirer les clients, à produire à moindres coûts en proposant une qualité supérieure à celle de la concurrence importent tout autant. Elle doit aussi être en mesure de recruter et de garder les salariés qui rendent possibles toutes ces choses.


## 1.2. La demande

**Mots-clés** de la section 1.2 : *courbe de demande, déplacement de la courbe de demande, déplacement sur la courbe de demande, (option) taxe forfaitaire ...*

*La demande est une fonction décroissante du prix. Plus le prix est élevé, moins les consommateurs (demandeurs) sont disposés à acheter du pain. La demande de marché, qui correspond à la somme des demandes individuelles, baisse donc. Un prix plus élevé réduit le pouvoir d’achat des consommateurs et diminue la quantité demandée. En effet, un prix plus élevé annule ou reporte la demande de certains consommateurs et réduit la quantité consommée des autres.*

De même, ce phénomène est plus marqué pour certains biens ou services qui ont des proches substituts : lorsque le prix du bien ou du service augmente alors que celui d’un proche substitut reste inchangé, les consommateurs peuvent consommer une quantité moins importante du bien ou du service dont le prix relatif s’est accru (par exemple, les fraises et les framboises, la viande blanche et la viande rouge, une place de cinéma et une vidéo à la demande via une box).

Les déterminants de la demande sont les suivants : le prix, le nombre d’acheteurs, les anticipations de ces acheteurs, la variation du prix d’un bien ou d’un service substituable ou complémentaire, le revenu, les goûts des consommateurs.

<!-- Tableau 1.3 -->
Tableau 1.8 Prix et quantité de pains demandée. 
| Prix en euro | Quantité de pains <br> demandée |
|:------------:|---------------------------:|
| 0,85         | 9 000                      |
| 1,7          | 6 000                      |
| 2            | 5 000                      |
| 2.5          | 4 000                      |
| 3,25         | 2 000                      |
| 4,25         | 0                          |

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-02.jpg" />
    Graphique 1.2 La courbe de demande sur le marché du pain.

On peut interpréter la pente de la courbe de demande.


### 1.2.1. Interprétation de la courbe de demande

La pente d’une courbe de demande nous renseigne sur sa sensibilité par rapport aux variations des prix. 

Quand une variation des prix entraîne une variation plus que proportionnelle des quantités demandées, on dit que la demande est très sensible à la variation des prix (pente de la courbe peu importante). 

Quand une variation des prix entraîne une variation moins que proportionnelle des quantités demandées, on dit que la demande est peu sensible à la variation des prix (pente de la courbe très importante). 

Enfin, si une variation des prix n’entraîne pas de modification des quantités demandées, on dit que la demande est insensible à la variation des prix.

### 1.2.2. Interprétation des déplacements *des* courbes ?

*L’offre et la demande augmentent ou baissent, indépendamment de la variation du prix de marché, sous l’effet de chocs positifs ou négatifs (modifications des conditions d’offre et de demande). Ces variations se traduisent graphiquement par un déplacement des courbes d’offre et de demande vers la droite (en cas de choc positif) ou vers la gauche (en cas de choc négatif). Ce déplacement des courbes se traduit lui-même par un déséquilibre (une surproduction ou une pénurie) à l’ancien prix d’équilibre.*

Appuyons-nous de nouveau sur l’exemple du marché du pain. Supposons que le coût de la farine qui entre dans la composition du pain baisse en raison d’une situation climatique favorable à la production de blé : cela incite les boulangers à produire davantage, à tous niveaux de prix du pain, parce que leurs perspectives de profit augmentent. On parle alors de *choc d’offre* (modification des conditions de l’offre, ici du coût de production) positif (puisque l’offre augmente alors que le niveau général des prix reste constant). Graphiquement, cette hausse de l’offre se traduit par le déplacement de la courbe d’offre vers la droite, parallèlement à la courbe d’offre initiale.

--- 
EXERCICE

- Exercice 1 : Effet d’un choc d’offre positif sur le marché du pain
  - À la suite du choc d’offre positif, quelles sont la quantité demandée et la quantité offerte de pains à l’ancien prix d’équilibre ?
  - Comment qualifie-t-on ce déséquilibre ?
- Exercice 2 : Effet d’un choc de demande positif sur le marché du pain
  - Appuyez-vous sur les acquis de la Section 1.3 sur les déterminants de la demande pour proposer une explication d’un choc de demande positif sur le marché du pain.
  - Représentez graphiquement le déplacement de la courbe pour tous niveaux de prix.
  - Quel déséquilibre apparaît sur le marché du pain en cas de choc de demande positif ?
- Exercice 3 : Effet d’un choc négatif sur le marché du pain
  - Comment les courbes d’offre et de demande se déplacent-elles en cas de chocs négatifs ?
  - Quel déséquilibre apparaît en cas de choc d’offre négatif ? En cas de choc de demande négatif ?

---




<!--
---
APPLICATION

Montrer qu’une variation de l’offre (de la demande) indépendamment de la variation du prix provoque un déplacement de la courbe d’offre (de demande).
TBC
---
-->


### 1.2.3. Interprétation des déplacements *sur les* courbes ?

Lorsque le marché est à l’équilibre, un changement du prix et de la quantité d’équilibre ne peut être dû qu’à un choc : la variation (baisse dans le cas d’un choc négatif, hausse dans le cas d’un choc positif) de l’offre ou de la demande provoque le déplacement des courbes à tous niveaux de prix. Cette variation de l’offre ou de la demande induit l’apparition d’un déséquilibre (surproduction ou pénurie). Celui-ci se traduit par une variation du prix (à la baisse en cas de surproduction, à la hausse en cas de pénurie), qui donne elle-même lieu à un ajustement des quantités offertes et demandées (déplacement sur les courbes), permettant le retour à l’équilibre (nouveau prix et nouvelle quantité d’équilibre). Grâce à la flexibilité des prix, des quantités offertes et demandées, le marché est ainsi autorégulateur : il revient spontanément à l’équilibre.

Puisque les chocs se traduisent par l’apparition de déséquilibres, la question qui se pose dès lors concerne le retour à l’équilibre.

Dans le cas du choc d’offre positif sur le marché du pain étudié précédemment, une surproduction apparaît, car l’offre (6 800 pains) est supérieure à la demande (5 000 pains) à l’ancien prix d’équilibre de 2 euros. Le prix baisse sur le marché du fait de la concurrence entre les offreurs souhaitant tous écouler leur production. En raison de cette baisse du prix du pain, l’offre de pain baisse et la demande de pain augmente. Le Graphique 1.4b montre que cette situation se traduit par un déplacement sur les courbes d’offre et de demande.

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-04-b.jpg" />
    Graphique 1.4b Déplacements sur les courbes d’offre et de demande à la suite d’un choc d’offre positif

---
APPLICATION

- Exercice : L’effet d’une baisse des prix sur l’offre et la demande
  - Appuyez-vous sur les acquis de cette section pour expliquer pourquoi, lorsque le prix baisse, l’offre de pain baisse et la demande de pain augmente.
  - Décrivez les variations de l’offre et de la demande à la suite de la baisse du prix du pain.
  - Quels sont le nouveau prix et la nouvelle quantité d’équilibre ?

---

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-05.jpg" />
    Graphique 1.5 Déplacements sur les courbes d’offre et de demande à la suite d’un choc de demande positif.


### 1.2.4. (Option) Etude de cas  : Taxe forfaitaire

TBC


## 1.3. Elasticités de la demande par rapport au prix

**Mots-clés** de la section 1.3 : *élasticité-prix de la demande, élasticité-prix croisée de la demande, fonction de demande inverse, fonction de demande directe, Loi de la Demande, ...*

### 1.3.1. Élasticité de la demande (présentation simple)

L’**élasticité prix de la demande** est une mesure de la sensibilité des consommateurs à un changement de prix. Elle est définie comme la variation (en pourcentage) de la demande en réponse à une augmentation du prix de 1 %. Par exemple, supposez que lorsque le prix d’un produit augmente de 10 %, nous observions une baisse de 5 % de la quantité vendue. Nous pouvons alors calculer l’élasticité, ε, de la manière suivante :

ε = − variation de la demande (en %) ÷ variation du prix (en %)

ε est la lettre grecque epsilon, souvent utilisée pour représenter l’élasticité. Lorsque le changement du prix est positif le changement de demande est négatif et vice versa. Le signe moins (-) dans la formule de l’élasticité garantit donc que la mesure de la sensibilité au prix soit un nombre positif. Dans cet exemple, nous obtenons donc :

ε = −5 ÷ 10 = 0,5

On dit que la demande est *élastique* lorsque l’élasticité est supérieure à 1 et *inélastique* lorsqu’elle est inférieure à 1.

L’**élasticité-prix croisée de la demande** fait référence à la variation en pourcentage de la quantité demandée d’un produit donné en raison de la variation en pourcentage du prix d’un autre produit « proche ». Si tous les prix peuvent varier, la quantité demandée du produit A dépend non seulement de son propre prix (voir élasticité de la demande) mais aussi des prix des autres produits.




### 1.3.2. Élasticité de la demande (avancé: présentation mathématique)

Il y a deux manières d’écrire une fonction de demande. Nous pouvons décrire la demande de *Beautiful Cars* en utilisant la fonction de demande *inverse* suivante:

𝑃=𝑓(𝑄)

où 𝑓(𝑄) est le prix auquel l’entreprise peut vendre exactement 𝑄 voitures. Pour définir l’élasticité, il est plus commode d’écrire la fonction de demande sous sa forme directe :

𝑄=𝑔(𝑃)

𝑔(𝑃) est la quantité de Beautiful Cars demandée quand le prix est 𝑃. (La fonction 𝑔 est la fonction inverse de 𝑓 ; mathématiquement, on peut écrire 𝑔(𝑃)=𝑓−1(𝑃) .)

La dérivée de la fonction de demande est 𝑑𝑄/𝑑𝑃=𝑔′(𝑃)
. C’est une manière de mesurer à quel point la demande du consommateur (𝑄) change en réponse à un changement du prix. Cependant, ce n’est pas une mesure très utile parce qu’elle dépend des unités dans lesquelles sont exprimés 𝑃 et 𝑄. 

À la place, nous définissons l’élasticité-prix de la demande dans le texte comme :

$-\frac{ \text{variation en \% de la quantité demandée} }{ \text{variation en \% du prix} }$

$-\frac{ variation en \% de la quantité demandée }{ variation en \% du prix }$

C’est une mesure plus utile de la sensibilité de la demande au prix. Vous pouvez déduire de la définition qu’elle est indépendante des unités de mesure. Cependant elle est étroitement liée à la dérivée 𝑔′(𝑃) — pour le voir, supposez que le prix change de 𝑃 à 𝑃+Δ𝑃, entraînant une variation de la quantité demandée de 𝑄=𝑔(𝑃) à 𝑄+Δ𝑄. Le changement en pourcentage du prix est 100Δ𝑃/𝑃, et le changement en pourcentage de la quantité est 100Δ𝑄/𝑄. En substituant cela dans l’expression de l’élasticité, l’on obtient :

$−\frac{\Delta 𝑄}{𝑄}/\frac{\Delta 𝑃}{𝑃} = − \frac{𝑃}{𝑄} \frac{\Delta Q}{\Delta P}$

En prenant la limite de cette expression quand $\Delta P \longrightarrow 0$, on obtient la définition algébrique de l’élasticité-prix de la demande, que nous dénotons par 𝜀 comme dans le texte :

$\varepsilon = - \frac{P}{Q} \frac{dQ}{dP}$

Et puisque 𝑄=𝑔(𝑃), l’élasticité peut aussi être écrite comme :

$\varepsilon = \frac{P g'(P)}{g(P)}$

Remarquez que la valeur de l’élasticité est normalement positive, puisque selon la *Loi de la Demande*, la dérivée de la fonction de demande sera négative.

Lorsqu’elle est définie de manière algébrique, 𝜀
est seulement approximativement identique à notre définition originale de l’élasticité selon laquelle il s’agit de la variation en pourcentage de la quantité lorsque le prix augmente de 1 %. Cependant, si l’on suppose que l’augmentation de 1 % correspond à une faible augmentation (ce qui est une hypothèse réaliste), c’est une bonne approximation, et nous l’interprétons souvent de cette manière.

Prenez la fonction de demande suivante :

$𝑄=100𝑃^{−0,8}$

Ici,
$\varepsilon = - \frac{P}{Q} \frac{dQ}{dP}=-\frac{P}{100𝑃^{−0,8}} \times -80P^{-1,8} = 0,8 $

Dans ce cas particulier, l’élasticité de la demande est constante – elle est égale à 0,8

en tous points de la courbe de demande.

En général les élasticités ne sont pas constantes. Elles varient lorsque l’on se déplace le long de la courbe de demande. L’exemple ci-dessus illustre un cas spécial. Si la forme de la fonction de demande est $Q=aP^{-c}$, où 𝑎 et 𝑐 sont des constantes positives, l’élasticité de la demande est 𝑐. C’est la seule classe de fonctions de demande pour laquelle l’élasticité est constante.

## 1.4. L'offre

**Mots-clés** de la section 1.4 : *offre, ...*

*L’offre détermine les quantités que les vendeurs sont prêts à offrir sur le marché pour chaque niveau de prix donné. L’offre de marché s’obtient en additionnant les offres individuelles. L’offre est une fonction croissante des prix, car plus le prix augmente et plus les perspectives de profit sont importantes pour les vendeurs. La pente d’une courbe d’offre nous renseigne sur la sensibilité de l’offre à la suite d’une variation des prix.*

Pour un modèle simple d’un marché comptant de nombreux acheteurs et vendeurs (on parle ici d’*atomicité*), prenons l’exemple d’une ville où un grand nombre de petites boulangeries fabriquent du pain et le vendent directement aux consommateurs. La courbe d’offre de marché indique la quantité totale produite par l’ensemble des boulangeries à chaque niveau de prix donné.

L’offre (dans notre exemple, l’offre quotidienne totale de pains de la part de l’ensemble des boulangers de la ville) est une fonction croissante du prix : plus le prix est élevé, plus les boulangers (offreurs) sont disposés à produire et à vendre, car cela constitue une perspective de gain plus élevé, augmentant l’offre de marché. L’offre de marché est donc la somme des offres de tous les producteurs. De plus, ces prix plus élevés incitent de nouveaux individus à devenir boulangers, augmentant le nombre d’offreurs et l’offre totale.

Voici les déterminants qui influencent les quantités offertes : les coûts de production (par exemple, l’évolution du prix des matières premières ou bien le changement de prix des facteurs de production), le nombre d’offreurs, les évolutions technologiques, un changement des anticipations des producteurs.

---
EXERCICE

- Exercice : L’offre et le prix
  - Qu’est-ce que l’offre ? 
  - Qu’est-ce que l’offre de marché ?
  - Pourquoi l’offre est-elle une fonction croissante du prix ? 

---


Le Tableau 1.9 et le Graphique 1.1 montrent les quantités offertes en fonction du prix du pain qui vont permettre de construire la courbe d’offre.

Tableau 1.9 Prix et quantité de pains offerte. 
| Prix en euro | Quantité de <br> pains offerte |
|:------------:|:-------------------------:|
| 1,10          | 1.000                     |
| 1,50          | 3.000                     |
| 2,00            | 5.000                     |
| 2.35         | 6.000                     |
| 3,77         | 9.000                     |


<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-01.jpg" />
    Graphique 1.1 La courbe d’offre sur le marché du pain.


---
APPLICATION

*Interpréter la pente de la courbe d’offre.*

La pente d’une courbe d’offre nous renseigne sur la sensibilité de l’offre par rapport aux variations des prix.

Quand une variation des prix entraîne une variation plus que proportionnelle des quantités offertes, on dit que l’offre est très sensible à la variation des prix (pente de la courbe peu importante). Quand une variation des prix entraîne une variation moins que proportionnelle des quantités offertes, on dit que l’offre est peu sensible à la variation des prix (pente de la courbe très importante). Enfin, si une variation des prix n’entraîne pas de modification des quantités offertes, on dit que l’offre est insensible à la variation des prix.

---


## 1.5. Détermination des prix

*L’interaction entre l’offre et la demande détermine un équilibre de marché tel que les acheteurs et les vendeurs sont preneurs de prix et réalisent l’échange à la quantité qui correspond au prix du marché ; on parle d’équilibre concurrentiel. Si la demande est supérieure à l’offre (situation de pénurie) le prix va augmenter et si l’offre est supérieure à la demande (situation de surproduction) le prix va baisser pour que l’équilibre soit de nouveau atteint.*

Nous connaissons maintenant la courbe d’offre et la courbe de demande (voir Graphique 1.1 et Graphique 1.2) sur le marché du pain. Le Graphique 1.3 montre que le prix d’équilibre est exactement de 2 euros. À ce prix, le marché est à l’équilibre : les consommateurs demandent 5 000 pains par jour et les entreprises en fournissent 5 000 par jour.

Si de nombreuses entreprises (offreurs) fabriquent des produits identiques et si les clients (demandeurs) peuvent choisir n’importe quelle entreprise pour effectuer leurs achats, alors les entreprises sont *preneuses de prix* à l’équilibre. Elles n’auraient aucun avantage à vendre à un prix différent de celui en vigueur.

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-03.jpg" />
    Graphique 1.3 Équilibre sur le marché du pain.


## 1.6. Le surplus du consommateur et du producteur

### 1.6.1. La notion de coût de production

Prenons l’exemple du marché des avions privés et imaginons une entreprise fictive qui produit des avions similaires à ceux des grandes marques, comme Cessna, Bombardier, Gulfstream ou encore Dassault. Nous appellerons cette entreprise Supers Jets. Si cette entreprise produit des avions identiques à ses concurrents, rappelez-vous qu’elle est preneuse de prix : elle ne peut pas vendre un avion, de la même gamme, à un prix supérieur à celui de ses concurrents.

Pensez aux coûts de production et de vente des avions. Tout d’abord, l’entreprise a besoin de mettre en œuvre un processus de recherche et développement pour concevoir son appareil. Elle a également besoin de locaux (une usine) équipés de machines pour la confection des appareils. Elle peut les louer à une autre entreprise ou rassembler le capital financier nécessaire pour investir dans ses propres locaux et équipements. Ensuite, elle doit acheter les matières premières et les composants, et rémunérer les salariés (des ouvriers notamment) qui font fonctionner les équipements. Elle a aussi besoin d’autres salariés (des cadres, des professions intermédiaires ou des employés) pour gérer le processus d’achat et de production ainsi que pour vendre les avions.

Pour produire, l’entreprise fait donc face d’une part à des coûts fixes qui ne dépendent pas de son activité ou encore des quantités produites, d’autre part à des coûts variables qui dépendent des quantités produites. Les coûts fixes sont donc à acquitter même si aucune quantité n’a été produite et aucun chiffre d’affaires n’a été réalisé.

Le coût total noté $CT$ se compose de l’ensemble des coûts supportés par l’entreprise.

Le coût moyen ($CM$) de production de Supers Jets correspond au coût total pour une quantité d’avions produits $CT$ divisé par cette quantité ($Q$).

$CM(Q) = \frac{CT(Q)}{Q}$

Le coût marginal ($Cm$) correspond au coût de production d’une unité supplémentaire (ici l’unité est l’avion) ; la quantité produite passe alors de $Q$ à $Q+1$.

$Cm(Q+1)=CT(Q + 1)−CT(Q)$




### 1.6.2. Quelle quantité le producteur décide-t-il de produire en situation de « preneur de prix » ?

Le profit total augmente tant que le coût marginal est inférieur au prix de marché, car le producteur réalise un profit marginal (le solde « prix de marché – coût marginal » est positif). À la quantité pour laquelle le coût marginal égalise le prix de marché, le profit total est à son maximum.

Une entreprise preneuse de prix produit donc une quantité telle que $Cm = P^*$ (ou $P^*$ est le prix de marché). En effet, si l’entreprise augmentait sa production jusqu’à un niveau où $Cm > P^*$, le coût de fabrication de la dernière unité produite serait supérieur à $P^*$, de telle sorte que l’entreprise réaliserait une perte sur cette unité. Si l’entreprise produisait à un niveau où $Cm < P^*$, elle pourrait produire davantage, tant qu’elle réalise un profit marginal positif. À la quantité pour laquelle $Cm = P^*$, le profit total est maximal.

Supposez maintenant que vous soyez le chef d’entreprise de Supers Jets et que vous deviez déterminer la quantité d’avions à produire. Comme tout chef d’entreprise, vous avez comme objectif non seulement de faire des profits, mais également de maximiser votre profit. Le profit d’une entreprise est la différence entre ses recettes et ses coûts totaux.

$profit=recettes−coûts =𝑃𝑄−𝐶𝑇(𝑄)$

De manière équivalente, le profit est le nombre d’unités produites multiplié par le profit unitaire, qui est la différence entre le prix et le coût moyen.

$profit=Q \left( P - \frac{CT(Q)}{Q} \right)  $

On suppose que compte tenu de l’offre et de la demande globale d’avions privés le prix d’équilibre du marché est de 340 000 euros. On rappelle que vos principaux concurrents produisent des avions strictement identiques aux vôtres. Si vous choisissiez un prix plus élevé, les clients potentiels se tourneraient vers d’autres concurrents. Vous êtes donc preneur de prix.

---
APPLICATION

*Pour un prix de marché de 340 000 euros, quelle quantité allez-vous accepter de produire ? Déterminez la quantité à produire pour laquelle votre profit est maximal.*

| Prix du <br> marché <br> (€) | Quantité <br> produite | Coût moyen <br> (€) | Coût marginal <br> (€) | Chiffres <br> d'affaires <br> (€) | Coût total <br> (€) | Profit <br> (€) |
|:------------------:|:-----------------:|---------------:|------------------:|------------------------:|---------------:|-----------:|
| 340.000            | 1                 | 750.000        | 300.000           | 340.000                 | 750.000        | −410.000   |
| 340.000            | 2                 | 475.000        | 200.000           | 680.000                 | 950.000        | −270.000   |
| 340.000            | 3                 | 367.000        | 151.000           | 1.020.000               | 1.101.000      | -81.000    |
| 340.000            | 4                 | 305.500        | 121.000           | 1.360.000               | 1.222.000      | 138.000    |
| 340.000            | 5                 | 273.000        | 143.000           | 1.700.000               | 1.365.000      | 335.000    |
| 340.000            | 6                 | 256.500        | 174.000           | 2.040.000               | 1.539.000      | 501.000    |
| 340.000            | 7                 | 250.000        | 211.000           | 2.380.000               | 1.750.000      | 630.000    |
| 340.000            | 8                 | 250.000        | 250.000           | 2.720.000               | 2.000.000      | 720.000    |
| 340.000            | 9                 | 255.000        | 295.000           | 3.060.000                | 2.295.000      | 765.000    |
| 340.000            | 10                | 263.500        | 340.000           | 3.400.000               | 2.635.000      | 765.000    |
| 340.000            | 11                | 275.000        | 390.000           | 3.740.000               | 3.025.000      | 715.000    |
| 340.000            | 12                | 288.750        | 440.000           | 4.080.000               | 3.465.000      | 615.000    |
| 340.000            | 13                | 305.000        | 500.000           | 4.420.000               | 3.965.000      | 455.000    |
| 340.000            | 14                | 324.000        | 571.000           | 4.760.000               | 4.536.000      | 224.000    |
| 340.000            | 15                | 346.000        | 654.000           | 5.100.000               | 5.190.000      | −90.000    |
| 340.000            | 16                | 370.500        | 738.000           | 5.440.000               | 5.928.000      | −488.000   |
| 340.000            | 17                | 398.000        | 838.000           | 5.780.000               | 6.766.000      | −986.000   |
| 340.000            | 18                | 428.000        | 938.000           | 6.120.000               | 7.704.000      | −1.584.000 |
| 340.000            | 19                | 461.000        | 1.055.000         | 6.460.000               | 8.759.000      | −2.299.000 |
| 340.000            | 20                | 497.000        | 1.181.000         | 6.800.000               | 9.940.000      | −3.140.000 |

---

<br>
<br>

---
APPLICATION

*On suppose désormais que compte tenu de l’offre et de la demande globale le prix d’équilibre du marché soit de 500.000 euros.*

| Prix du <br> marché <br> (€) | Quantité <br> produite | Coût moyen <br> (€) | Coût marginal <br> (€) | Chiffres <br> d’affaires <br> (€) | Coût total <br> (€) | Profit <br> (€) |
|:--------------------:|:-----------------:|-----------------:|--------------------:|--------------------------:|-----------------:|-------------:|
| 500.000              | 1                 | 750.000           | 300.000              | 500.000                   | 750.000          | −250 000     |
| 500.000              | 2                 | 475.000           | 200.000              | 1.000.000                 | 950.000          | 50 000       |
| 500.000              | 3                 | 367.000           | 151.000              | 1.500.000                 | 1.101.000        | 499 000      |
| 500.000              | 4                 | 305.500          | 121.000             | 2.000.000                  | 1.222.000        | 778 000      |
| 500.000              | 5                 | 273.000          | 143.000             | 2.500.000                 | 1.365.000        | 1.135 000    |
| 500.000              | 6                 | 256.500          | 174.000             | 3.000.000                 | 1.539.000        | 1.461 000    |
| 500.000              | 7                 | 250.000          | 211.000             | 3.500.000                 | 1.750.000        | 1.750 000    |
| 500.000              | 8                 | 250.000          | 250.000             | 4.000.000                 | 2.000.000        | 2 000000     |
| 500.000              | 9                 | 255.000          | 295.000             | 4.500.000                 | 2.295.000        | 2.205 000    |
| 500.000              | 10                | 263.500          | 340.000             | 5.000.000                 | 2.635.000        | 2.365 000    |
| 500.000              | 11                | 275.000          | 390.000             | 5.500.000                 | 3.025.000        | 2.475 000    |
| 500.000              | 12                | 288.750          | 440.000             | 6.000.000                 | 3.465.000        | 2.535 000    |
| 500.000              | 13                | 305.000          | 500.000             | 6.500.000                 | 3.965.000        | 2.535 000    |
| 500.000              | 14                | 324.000          | 571.000             | 7.000.000                 | 4.536.000        | 2.464 000    |
| 500.000              | 15                | 346.000          | 654.000             | 7.500.000                 | 5.190.000        | 2.310 000    |
| 500.000              | 16                | 370.500          | 738.000             | 8.000.000                 | 5.928.000        | 2.072 000    |
| 500.000              | 17                | 398.000          | 838.000             | 8.500.000                 | 6.766.000        | 1.734 000    |
| 500.000              | 18                | 428.000          | 938.000             | 9.000.000                 | 7.704.000        | 1.296 000    |
| 500.000              | 19                | 461.000          | 1.055.000           | 9.500.000                 | 8.759.000        | 741 000      |
| 500.000              | 20                | 497.000          | 1.181.000           | 10.000.000                | 9.940.000        | 60 000       |

---


Tableau 1.4 La maximisation du profit pour un niveau de prix donné (P* = 340 000 euros) 
| Prix du <br> marché <br> (€) | Quantité <br> produite | Coût moyen <br> (CM, €) | Coût marginal <br> (Cm, €) | Chiffres d’affaires <br> (€) | Coût total <br> (CT, €) | Profit <br> (€) |
|----------------------|-------------------|----------------------|-------------------------|---------------------------|----------------------|--------------|
| 340.000              | 8                 | 250.000              | 250.000                 | 2.720.000                 | 2.000.000            | 720.000      |
| 340.000              | 9                 | 255.000              | 295.000                 | 3.060.000                  | 2.295.000            | 765.000      |
| 340.000              | 10                | 263.500              | 340.000                 | 3.400.000                 | 2.635.000            | 765.000      |
| 340.000              | 11                | 275.000              | 390.000                 | 3.740.000                 | 3.025.000            | 715.000      |
| 340.000              | 12                | 288.750              | 440.000                 | 4.080.000                 | 3.465.000            | 615.000      |



Tableau 1.5 La maximisation du profit pour un niveau de prix donné (P* = 500 000 euros) 
| Prix du <br> marché <br> (€) | Quantité <br> produite | Coût moyen <br> (CM, €) | Coût marginal <br> (Cm, €) | Chiffres d’affaires <br> (€) | Coût total <br> (CT, €) | Profit <br> (€) |
|----------------------|-------------------|----------------------|-------------------------|---------------------------|----------------------|--------------|
| 500.000              | 10                | 263.500              | 340.000                 | 5.000.000                 | 2.635.000            | 2.365.000    |
| 500.000              | 11                | 275.000              | 390.000                 | 5.500.000                 | 3.025.000            | 2.475.000    |
| 500.000              | 12                | 288.750              | 440.000                 | 6.000.000                 | 3.465.000            | 2.535.000    |
| 500.000              | 13                | 305.000              | 500.000                 | 6.500.000                 | 3.965.000            | 2.535.000    |
| 500.000              | 14                | 324.000              | 571.000                 | 7.000.000                 | 4.536.000            | 2.464.000    |
| 500.000              | 15                | 346.000              | 654.000                 | 7.500.000                 | 5.190.000            | 2.310.000    |


### 1.6.3. Comment déduire la courbe d’offre du producteur de la maximisation du profit ?

*Pour une entreprise en situation de preneuse de prix, la courbe d’offre débute au minimum du coût moyen et se confond avec la courbe de coût marginal. La courbe d’offre détermine donc à partir du coût moyen minimal la quantité à produire pour laquelle le profit est maximal.*

Rappel : le producteur cherche à faire du profit et à le maximiser. Le profit est maximal lorsque la dernière unité produite rapporte autant que ce qu’elle coûte.

Lorsque les prix baissent (du fait de la concurrence que se livrent les entreprises présentes sur le marché), vous choisissez de produire des quantités différentes. Vous choisissez la quantité pour laquelle $Cm = \text{prix de marché}$ ; en effet, nous avons vu que pour un prix de marché donné ($P^*$) le profit est maximal pour $P^* = Cm$.

On remarque néanmoins que si le prix baissait jusqu’à devenir inférieur à 250.000 euros, votre entreprise Supers Jets accuserait des pertes. En effet, le coût moyen minimal est de 250.000 euros, ce qui suppose qu’en deçà de ce prix votre entreprise ferait forcément des pertes. Vous refuseriez alors de produire au-dessous de ce prix. D’autres entreprises, plus compétitives, pourraient continuer à produire. Le Graphique 1.8 représente les courbes de coût marginal et de coût moyen de l’entreprise Supers Jets.

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-08.jpg" />
    Graphique 1.8 La représentation graphique de la courbe d’offre de Supers Jets.


### 1.6.4. Que sont le surplus du producteur et le surplus du consommateur ?

Quand le marché du pain est à l’équilibre avec la quantité de pains offerte sur le marché égalisant la quantité demandée, le surplus total est représenté par l’aire sous la courbe de demande et au-dessus de la courbe d’offre.

La quantité d’équilibre de 5.000 pains en situation de concurrence est telle que le surplus total est maximisé. Si une quantité inférieure à 5 000 pains était produite, les gains totaux issus de l’échange seraient plus faibles.

À l’équilibre, tous les gains potentiels à l’échange sont exploités. Cette propriété – le fait que le surplus combiné des consommateurs et des producteurs soit maximisé au point où l’offre égale la demande – est vraie de manière générale dans ce modèle : si les acheteurs comme les vendeurs sont preneurs de prix, la quantité d’équilibre maximise la somme des gains issus de l’échange sur le marché.

Les participants à ce marché sont preneurs de prix, les acheteurs et vendeurs sont bien sûr libres de choisir un autre prix, mais ils n’en tireraient aucun avantage.

Cependant, dans l’exemple du marché du pain, même si tous les agents acceptent le prix de 2 euros par pain, certains vont tirer plus de gains de cet échange. En effet, certains consommateurs (la demande) étaient prêts à payer plus de 2 euros et certains producteurs (l’offre) étaient prêts à vendre moins cher que 2 euros.

#### 1.6.4.1. Le surplus du consommateur

Souvenez-vous que la courbe de demande indique la disposition à payer de chaque client potentiel. Un client disposé à payer plus que le prix de vente achètera le pain et gagnera à cet échange un surplus, car la valeur qu’il attribue au pain est supérieure au prix qu’il doit payer pour l’acquérir. Le Graphique 1.9a illustre le surplus du consommateur.

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-09-a-d.jpg" />
    Graphique 1.9a Le surplus du consommateur.

Le surplus total des consommateurs est l’aire colorée qui représente le gain à l’échange des consommateurs au prix de marché donné, c’est-à-dire la différence entre ce que ces consommateurs étaient disposés à payer et le prix (ce qu’ils payent vraiment).

#### 1.6.4.2. Le surplus du producteur

Souvenez-vous que la courbe d’offre correspond à la courbe de coût marginal, c’est-à-dire le coût de la dernière unité produite. Un producteur supportant un coût marginal inférieur au prix continue à produire pour vendre à un prix qui lui permet de gagner un profit, le surplus du producteur.

Comme le prix d’équilibre est unique et que le coût marginal est croissant, le surplus du producteur va être de plus en plus réduit. Il est même nul lorsque le coût marginal a rejoint le niveau du prix. Le Graphique 1.9b illustre le surplus du producteur.

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-09-b-c.jpg" />
    Graphique 1.9b Le surplus du producteur


Le surplus total du producteur est l’aire colorée qui représente le gain à l’échange des producteurs au prix de marché donné, c’est-à-dire la différence entre le prix payé pour chaque pain et le coût marginal de chacun de ces pains.


#### 1.6.4.3. Pourquoi le surplus est-il maximisé à l’équilibre ?

Les consommateurs (demandeurs) et les producteurs (offreurs) de pain qui décident volontairement d’échanger tirent tous des gains de cet échange. Les consommateurs qui sont prêts à payer un prix supérieur ou égal au prix du marché gagnent le surplus du consommateur. Les producteurs qui ont des coûts marginaux inférieurs ou égaux au prix du marché gagnent le surplus du producteur. Le surplus total mesure les gains à l’échange ou gains générés par le commerce pour tous les agents économiques qui y participent.

Le Graphique 1.9c montre comment calculer le surplus total (les gains tirés de l’échange) à l’équilibre concurrentiel du marché du pain.

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-09-d-b.jpg" />
    Graphique 1.9d La perte sèche.

Ces deux exemples de perte sèche montrent que le surplus total est réduit s’il s’éloigne du prix d’équilibre de 2 euros et de la quantité d’équilibre de 5 000 pains. Pour un prix supérieur (prix plancher), les producteurs sont prêts à vendre plus de pains, mais il y a moins de demandes. Pour un prix inférieur (prix plafond), les consommateurs sont disposés à acheter plus de pains, mais les boulangers en offrent moins.


## 1.7. Exercices

### 1.7.X. Coût d'opportunité

[lien](extrait-biofuel.md)

### 1.7.X. Elasticité-prix demande 

1. Calculez les élasticités-prix demande dans chacun des cas présentés dans le Tableau ci-dessous et expliquez si la demande serait considérée comme élastique ou inélastique par rapport au prix.

| prix <br> initiale | nouveau <br> prix | quantité initiale <br> demandée | nouvelle quantité <br> demandée |
|:-------------:|:------------:|:--------------------------:|:--------------------------:|
| 100 €         | 102 €        | 2.000 unités par semaine   | 1.950 unités par semaine   |
| 55,50 €       | 54,95 €      | 5.000 unités par semaine   | 6.000 unités par semaine   |

2. A l'aide d'un exemple numérique de votre choix, expliquez la signification de ces valeurs de l'élasticité : <br>
    a. $\varepsilon = 1.5$ <br>
    b. $\varepsilon = 0.6$

**Solution**
1.  a.  -2.5%/2% = −1.25 élastique <br>
    b.  20%/−0.99% = −20.20 élastique

    $ \varepsilon = \frac{ \text{variation en \% de la quantité demandée} }{ \text{variation en \% du prix} } $
2. a. Une élasticité-prix demande de (-) 1,5 signifie que la demande est élastique. Par exemple, une augmentation de 10 % du prix peut entraîner une diminution de 15 % de la quantité demandée. <br>
   b. Une élasticité-prix demande de (-) 0,6 signifie que la demande est inélastique. Dans ce cas, une augmentation de 10 % du prix entraînerait une baisse de la demande de 6 %.

### 1.7.X. Elasticité-prix demande

[lien](exercice-elasticite-prix-demande-interpretation.md)

## 1.8. Sources

- [A/AS Level Syllabus](https://qualifications.pearson.com/en/qualifications/edexcel-a-levels/economics-a-2015.html#%2Ftab-ASlevel)
- [Econofides: L'économie pour le Lycée](https://www.sciencespo.fr/department-economics/econofides/index.html)
- [Core-Econ - Econofides](https://www.sciencespo.fr/department-economics/econofides/)
- [Core-Econ - L'Economie](https://www.core-econ.org/the-economy/fr/)
