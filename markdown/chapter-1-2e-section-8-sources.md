## 1.8. Sources

- [A/AS Level Syllabus](https://qualifications.pearson.com/en/qualifications/edexcel-a-levels/economics-a-2015.html#%2Ftab-ASlevel)
- [Econofides: L'économie pour le Lycée](https://www.sciencespo.fr/department-economics/econofides/index.html)
- [Core-Econ - Econofides](https://www.sciencespo.fr/department-economics/econofides/)
- [Core-Econ - L'Economie](https://www.core-econ.org/the-economy/fr/)
