# Economics Essay Writing

## 2iec and 1iec Economics






### <em>Monopoly Power</em> (1iec, Question 7 Economics Paper 2022-05-24)

#### a. Question

Tesla held an 82% market share of the electric vehicle market in the United States
during the first half of 2020.

Evaluate whether a monopoly is likely to operate efficiently. Refer to at least one
monopoly of your choice.

#### b. Explanation

The stem pointed students in the direction of Tesla and application of this to the question
was popular with good insights into how Tesla may focus on dynamic efficiency but then, due
to its dominant position, act in an inefficient manner.

Unlike previous essay-based questions students were allowed to discuss other examples of
monopolies and this approach was taken up as they moved on to discuss a second efficiency
point, often looking at the behaviour of a natural monopoly, such as in the water industry.

Students were not required to draw a diagram, but the better analytical responses often did
make effective use of a precise cost and revenue diagram. The diagram in the mark scheme
is for illustrative purposes and other suitable diagrams were allowed.

In terms of evaluation, students did approach the question in different directions, some
arguing that monopolies were inefficient and then countering this, and others vice-versa.
Either approach was valid.

Lastly, there were attempts at a conclusion, but this was often used to summarise points
already made rather than making a clear judgement. When the latter was the focus of the
conclusion, it was often done well and helped to raise the overall score.

#### c. Sample Answers

[Student answer (44%)](https://qualifications.pearson.com/content/dam/pdf/A-Level/Economics/2015/Exam-materials/9ec0-01-pef-20220818.pdf#page=49)

[Student answer (76%)](https://qualifications.pearson.com/content/dam/pdf/A-Level/Economics/2015/Exam-materials/9ec0-01-pef-20220818.pdf#page=53)

#### d. Tips

- STRUCTURE: spend time planning your essay response in terms of the key points and evaluation that you want to make to avoid the risk of repetition 
- KNOWLEDGE: start with the key concepts and see how they are applied to the case study you are looking at 
- APPLICATION: try to ensure that examples given link tightly to the economic concept you are trying to address 
- EXPLAIN: focus should be on showing an ability to link knowledge and understanding in context using appropriate examples 
- EVALUATION: remember a judgement is expected to get the very top marks for evaluation 



---


### <em>Labour</em> (1iec, Question 8 Economics Paper 2022-05-24)

#### a. Question

Some occupations in the UK are facing large labour shortages. Hard-to-fill
vacancies include qualified chefs, software engineers, construction workers and
qualified nurses.

Evaluate the factors that might influence the supply of labour in an occupation of
your choice.

#### b. Explanation

This question drew on candidates’ knowledge from the topic area of Supply of Labour, as
indicated on the Advance Information for Paper 1.

An accurate drawing of the labour market diagram was not required, but some candidates
did identify suitable labour market diagrams to aid their explanation. Candidates did not
have to focus on one occupation and answers to any occupation were allowed, as well as to
more than one occupation to make both questions equally accessible.

This question was very topical, but it was less popular than question 7. On the whole,
responses were not as strong for this question.

Despite the topic being current, many responses were very context rich, but candidates
needed to ensure they spent time on the theory. However, there were some particularly good
theoretical answers that were well applied.

In some cases, there was evidence of candidates only briefly covering a wide range of factors
that influence supply, but with limited evaluation. Candidates do not have to offer three
points or more, and typically the best performing responses covered in-depth two points
with coherent chains of reasoning that were well evaluated.

#### c. Sample Answers

[Student answer (36%)](https://qualifications.pearson.com/content/dam/pdf/A-Level/Economics/2015/Exam-materials/9ec0-01-pef-20220818.pdf#page=59)

[Student answer (64%)](https://qualifications.pearson.com/content/dam/pdf/A-Level/Economics/2015/Exam-materials/9ec0-01-pef-20220818.pdf#page=64)


#### d. Tips

- UNDERSTANDING: make effective use of a knowledge organiser so that your knowledge and understanding is precise
- ANALYSIS: avoid narrow responses or superficial answers by practising going beyond two stage chains of reasoning 
- ANALYSIS: draw upon your wide-ranging knowledge of economic concepts and terms to provide a coherent chain of reasoning.
- EVALUATION: ensure you are evaluating what you have written in the context of the question set
- EVALUATION: ensure that you offer a clear judgement that answers the question


---


### <em>Protectionism</em> (1iec, Question 7 Economics Paper 2022-06-07)

#### a. Question

Both China and the United States have been gradually increasing the tariffs imposed
on each other’s exports as their trade dispute continues. The IMF estimated that the
trade dispute between China and the United States would reduce global GDP growth
by 0.8 percentage points by the end of 2020.

Evaluate the likely impact of an increase in protectionism on the global economy.

#### b. Explanation

This question focused on the impact of protectionism on the global economy. Overall,
candidates coped well with this question. They were able to correctly draw and explain a
tariff diagram and discuss the impact of tariffs, as well as evaluate those impacts. Some of
the better responses related this to real world tariffs, such as the trade dispute between the
USA and China. However, the strongest responses were clearly able to effectively relate it to
the global economy. Other methods such as quotas or subsidies were also often discussed
with a range of efficiency.

#### c. Sample Answers

[Student answer (64%)](https://qualifications.pearson.com/content/dam/pdf/A-Level/Economics/2015/Exam-materials/9ec0-02-pef-20220818.pdf#page=47)

[Student answer (88%)](https://qualifications.pearson.com/content/dam/pdf/A-Level/Economics/2015/Exam-materials/9ec0-02-pef-20220818.pdf#page=50)



#### d. Tips

- ANALYSIS: take note of the depth of the chains of argument given in this answer as an example of what is needed to meet the criteria for the top levels


---


### <em>Globalisation</em> (1iec, Question 8 Economics Paper 2022-06-07)

#### a. Question

With global economic growth slowing and inequality rising, the case against
globalisation is strengthening. But there are also many benefits to globalisation.
Global trade and international trade agreements allow countries to focus on their
strengths and share expertise, as well as reducing risk through diversification.

Evaluate the likely impact of globalisation on the global economy.


#### b. Explanation

The focus in this question was on the impact of globalisation on the global economy.
Comparative advantage was frequently discussed by candidates, although many struggled to
properly explain how it worked or provided plausible real-world examples.

In terms of evaluation of comparative advantage, some of the better responses clearly
critiqued it whilst still keeping their focus on globalisation. Weaker responses tended to lose
focus and discussed the impact of protectionism on comparative advantage. Other benefits
included higher economic growth, more jobs, and more competition between firms leading
to lower prices and/or better quality.

#### c. Sample Answers

[Student answer (92%)](https://qualifications.pearson.com/content/dam/pdf/A-Level/Economics/2015/Exam-materials/9ec0-02-pef-20220818.pdf#page=57)



#### d. Tips

- ANALYSIS: provide high quality arguments in your reasoning



---


### <em>Depreciation of a currency</em> (1iec, Question 8 Economics Paper 2022-06-07)

#### a. Question

Evaluate the microeconomic and macroeconomic effects of a depreciation of the pound. Refer to restaurants or other food delivery services in your answer.


#### b. Explanation

There were some very good essays which showed good understanding of the
microeconomics of an exchange rate depreciation on the price of imported ingredients, with
many showing a shift in MC as well as AC and a new profit area highlighted. In the macro
context some argued effectively that when there is a depreciation, it is likely that the SRAS
curve will shift left to reflect the rising cost of imported inputs. A few candidates made
explicit links along the lines that a depreciation might increase the popularity of the UK as a
tourist destination and so the demand for restaurant meals might increase. Restaurant
meals are not typically internationally traded and so it was surprising to see the number of
candidates who argued that UK restaurants will sell more meals abroad following a
depreciation, or other reasons for a rise in AD. Nevertheless there were some very well
informed candidates who showed excellent knowledge of recent developments in the UK
economy with regard to the difficulty of attracting staff to this sector.

Micro effects of exchange rate depreciation on the food delivery and/or restaurant sector
were well explained and were often supported by an accurately drawn cost and revenue
diagram showing a rise in AC and MC. The stronger candidates used type of restaurant to
really develop their answers where ingredients would need to be sourced from abroad e.g.
Japanese and Asian restaurants. Macro effects were more challenging in terms of application
to food delivery and/or restaurant sector. Quite a few candidates wrote about how exchange
rate depreciation could lead to food delivery companies increasing their export which was
unconvincing. Candidates who developed the macroeconomics effects using the AD/AS
model, typically with a well explained diagram, were able to offer more developed responses,
which scored higher marks. A reminder to centres that at least one good detailed application
to the context provided (i.e. the restaurant or food delivery services) was required to score
highly. Generic response to the effects of depreciation, tended to score Level 3 KAA. There
was good understanding shown of the Marshall-Lerner condition and J Curve in many
responses.

The micro side of depreciation was best linked to costs, as here. The candidate then links the
costs of food in restaurants to the diagram by making it clear that variable costs shift, and
therefore both MC and AC shift.


#### c. Sample Answers

[Student answer (88%)](https://qualifications.pearson.com/content/dam/pdf/A-Level/Economics/2015/Exam-materials/9ec0-03-pef-20220818.pdf#page=22)

[Student answer (72%)](https://qualifications.pearson.com/content/dam/pdf/A-Level/Economics/2015/Exam-materials/9ec0-03-pef-20220818.pdf#page=27)

#### d. Tips

- STRUCTURE: do not aim for perfection either in terms of wording or presentation
- ANALYSIS: aim instead for purity of logic and strength in the links of the analysis and evaluation
- do not force the examiner to do the work for you. If your costs shift, say whether they are fixed or variable then link your application to the diagram that you draw




---


### <em>Depreciation of a currency</em> (1iec, Question 8 Economics Paper 2022-06-07)

#### a. Question

Evaluate the microeconomic and macroeconomic effects of decreasing interest rates in Kenya, or another developing country of your choice.


#### b. Explanation

This was the more popular of the essays where, overall, all students demonstrated the basic
knowledge on the impact of low interest rates on AD and costs of borrowing. Only a few of
them, though, managed to bring in good economics and to make their answers more
relevant to the context with real life examples drawn not only from Kenya but also from other
Sub-Saharan countries or BRIC economies. Some mentioned the lengthy process of the
transmission mechanism.

Most candidates who answered it managed two effects.

The main problem was one of timing: some were only able to put down the briefest of
answers. Many candidates are selecting too many essay points – they need to use less points
and to write in more depth on those specific points.

The examiners' advice: that it is better to make one point well rather than a number with
brief or incorrect chains of reasoning.

- Accessible essay which most students responded well to 
- More able students were able to apply a traditional interest rates questions to the LEDC e.g – use of savings gap
- Good answers used AD/AS diagrams within their macro analysis and cost/revenue diagrams within their micro analysis. This enabled them to analyse effectively and look at the wider impacts.
- Evaluation was most effective when linked to the data extract and the rates actually available in Kenya
- Accessible essay which most students responded well to
- More able students were able to apply a traditional interest rates questions to the LEDC e.g – use of savings gap



#### c. Sample Answers

[Student answer](https://qualifications.pearson.com/content/dam/pdf/A-Level/Economics/2015/Exam-materials/9ec0-03-pef-20220818.pdf#page=48)

#### d. Tips

- STRUCTURE: make at least one micro and one macro point. 
- ANALYSIS: if you have more time then develop the side you think is the weakest with another more convincing point
- ANALYSIS: it is better to make fewer points really well argued than using a scattered approach with several relevant thoughts



---


### <em>Depreciation of a currency</em> (1iec, Question 8 Economics Paper 2022-06-07)

#### a. Question

Evaluate the microeconomic and macroeconomic strategies that could be used to promote development in Kenya, or another developing country of your choice.


#### b. Explanation

This question was less popular, with the large majority of students unable to make clear links between growth and development, but many candidates embedded in their answers excellent economics, theories/models (Harrod-Domar; Lewis Model from the extract; Prebisch Singer Hypthesis) and even diagrams not in the specifications such as the MEC diagram, plus a wide
range of models from development economics. The weakest candidates could not go beyond corruption or time lag when evaluating (see comments at the end of this report).

Some candidates did extremely well. The key point was to choose development strategies rather than to rehearse any macro policy (e.g., monetary policy is not a development policy and could not gain high KAA marks). It was also important to deal with development rather than merely growth.

- This was a very accessible essay with a huge variety of different strategies chosen 
- Students seemed well prepared for this (very typical style of question used in the past in other contexts, e.g. Mozambique)
- The best answers not only had depth to their analysis with model/diagrams, but also had good application to LEDCs (rather than simply writing about general supply-side policies)
- Again lots of candidates writing too many points in insufficient depth.
- Many candidates ran out of time on the last essay



#### c. Sample Answers

[Student answer](https://qualifications.pearson.com/content/dam/pdf/A-Level/Economics/2015/Exam-materials/9ec0-03-pef-20220818.pdf#page=55)


#### d. Tips

- EVALUATION: remember to give an overall judgement


