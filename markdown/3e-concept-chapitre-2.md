# Chapitre 2 : Comment crée-t-on des richesses et comment les mesure-t-on ?

## 1. La richesse d’une économie

```mermaid
graph TD;
A[bien-être] --> |lié à la| B[richesse d'un pays];
B --> |accroissement de la| C[production des biens & services];
C --> D[The Wealth of Nations Adam Smith, 1776];
D --> |analyse l'| E[origine de la prospérité récente];
E --> F[revenu];
F --> |fruit du capital| F1[intérêt/dividende];
F --> |rémunération d'une activité| F2[profit];
F --> |rémunération du travail| F3[salaire];
F2 --> G[revenu national];
F1 --> |provient de la contribution des acteurs économiques| G;
F3 --> G;
G --> |provient de la| H[production nationale];
G --> |sera distribué aux| I[acteurs qui ont participé à la création]
```

<br>

<br>

<br>

**Sources**
- [An Inquiry into the Nature and Causes of the Wealth of Nations by Adam Smith](https://www.gutenberg.org/ebooks/3300)
- [La richesse des nations par Adam Smith](https://gallica.bnf.fr/ark:/12148/bpt6k75319v.pdf)

## 2. Le circuit économique

```mermaid
graph TD;
A[modèle économique] --> |décrit sous  <br> forme d'un | B[circuit économique];
B --> |représente l'|C[activité économique]
C --> |entre les|D[acteurs économiques]
D --> C1[ménages]
C1 --> C1a[consommation]
D --> C2[entreprises]
C2 --> C2a[production]
D --> C3[banques]
C3 --> C3a[financement de <br> l'économie ]
D --> C4[Etat]
C4 --> |fournit des|C4a[services gratuits]
C4 --> |s'occupe de la|C4b[redistribution <br> des ressources]
D --> C5[reste du monde]
C5 --> C5a[échanges <br> internationaux]
style D fill:#C0C0C0
```

## 3. La diversité de la production

```mermaid
graph TD;
A[propriété privée] --> |est essentielle au fonctionnement des| B[marchés];
B --> |rend possible la| C[production au sens économique];
C --> D1[biens de consommation];
C --> D2[biens intermédiaires];
C --> D3[biens de production]
C --> D4[services];
```

<br>

<br>

<br>

```mermaid
graph TD;
A[production] --> B1[marchande];
B1 --> C[biens];
C --> |vendus à des| C1[prix économiquement <br> significatifs];
A --> B2[non-marchande];
B2 --> D1[biens];
B2 --> D2[services];
D1 --> |vendus à des| E[prix bas ou gratuit];
D2 --> |vendus à des| E;
style B1 fill:#C0C0C0
style B2 fill:#C0C0C0
```

<br>

<br>

<br>

```mermaid
graph TD;
A[classification des entreprises] -->|en fonction de| B[nature de production];
B --> |répartie en|C[3 secteurs]
C --> C1[secteur primaire]
C1 --> |liées à l'|C1a[exploitation des <br> ressources naturelles]
C --> C2[secteur secondaire]
C2 --> C2a[transformation des <br> matières premières]
C --> C3[secteur tertiaire]
C3 --> C3a[services]
C3a --> C3a1[marchand]
C3a --> C3a2[non-marchand]
style C1 fill:#C0C0C0
style C2 fill:#C0C0C0
style C3 fill:#C0C0C0
```

## 4. Comment produire et mesurer la production

```mermaid
graph TD;
A[critères de performance <br> de l'entreprise] --> A1[bilan];
A1 --> |document qui <br> établit <br> ce que l'|A1a[entreprise possède]
A --> A2[chiffre d'affaire]
A2 --> |représente le|A2a[total de la valeur <br> des ventes réalisées]
A --> A3[profit]
A3 --> |est la|A3a[différence entre <br> produits et charges]
style A1 fill:#C0C0C0
style A2 fill:#C0C0C0
style A3 fill:#C0C0C0
```

<br>

<br>

<br>

```mermaid
graph TD;
A[facteurs de production] --> A1[travail]
A --> A2[capital]
A2 --> |sont nécessaires|B[ressources naturelles];
A1 --> |pour transformer les|B
B --> |en| C[produits semi-finis et finis]
style A1 fill:#C0C0C0
style A2 fill:#C0C0C0
```

<br>

<br>

<br>

```mermaid
graph TD;
A[combiner les facteurs de production] --> B1[complémentaires];
B1 --> |augmenter simultanément| B11[capital & travail]; 
A  --> B2[substituables];
B2 --> |remplacer un facteur par l'autre| B21[capital ou travail];
B11 --> |choix d'une| C[combinaison optimale];
B21 --> |choix d'une| C;
C --> |produire afin de| D1[maximiser les profits];
C --> |produire afin de| D2[minimiser les coûts];
```

<br>

<br>

<br>

<br>

<br>

<br>


$$
 \text{productivité du travail} = \frac{\text{quantité produite}}{\text{quantité de travail utilisée}}
$$

<br>

<br>

<br>


$$
 \begin{aligned} 
 \text{chiffres d'affaires} & = \text{prix de vente} \times \text{quantités vendues} \\
 \text{coûts de production} & = \text{salaires, matières premières, énergie ...} \\
 \text{bénéfice/perte} & = \text{montant qui reste à l'entreprise pour rémunérer les propriétaires et investir}
\end{aligned}
$$

<br>

<br>

<br>


$$
 \begin{aligned} 
 \text{consommation intermédiaire} & = \text{biens ou services détruits ou transformés lors de la production}  \\
 \text{valeur ajoutée} & = \text{richesse nouvelle créée par l'activité productive} 
\end{aligned}
$$

## 5. Comment mesurer la création de richesses d'une nation?

```mermaid
graph TD;
A[mesurer la création de richesse] --> A1[Produit intérieur brut <br> PIB]
A --> A2[croissance économique]
A --> A3[cycles économiques]
A --> A4[PIB par habitant]
```

<br>

<br>

<br>

$$
 \begin{aligned} 
 \text{PIB} & = \text{somme des valeurs ajoutées}  \\
 \text{taux de croissance} & = \frac{\text{variation du PIB}}{\text{valeur initiale du PIB}}   \\
 \text{PIB par habitant} & = \frac{\text{valeur du PIB}}{\text{population du pays}}  
\end{aligned}
$$

<br>

<br>

<br>

### PIB aux prix du marché  (monnaies locales courantes) (série reliée) - Luxembourg 

<img src="images/20230525-annual-gdp-annualsum.png" alt="GDP Total Luxembourg" width="50%">

**Source** : 
- [European Central Bank - Statistical Data Warehouse](https://sdw.ecb.europa.eu/browseTable.do?org.apache.struts.taglib.html.TOKEN=f7d3d70e73c6c384e55d433bb9c9e84f&df=true&ec=&dc=&oc=&pb=&rc=&DATASET=0&removeItem=&removedItemList=&mergeFilter=&activeTab=IDCM&showHide=&MAX_DOWNLOAD_SERIES=500&SERIES_MAX_NUM=50&node=9691186&legendRef=reference&legendPub=published&legendNor=&SERIES_KEY=320.MNA.Q.N.LU.W2.S1.S1.B.B1GQ._Z._Z._Z.EUR.V.N&SERIES_KEY=320.MNA.Q.Y.LU.W2.S1.S1.B.B1GQ._Z._Z._Z.XDC.LR.GY&SERIES_KEY=320.MNA.A.N.LU.W2.S1.S1.B.B1GQ._Z._Z._Z.EUR.V.N&SERIES_KEY=320.MNA.A.N.LU.W2.S1.S1.B.B1GQ._Z._Z._Z.XDC.V.N&trans=N)
- [LaTeX source code (`main-lux-gdp-total-ecbdata.tex`)](https://www.overleaf.com/read/bbgdjfqzrqkp)
- [Banque Mondiale](https://donnees.banquemondiale.org/indicator/NY.GDP.MKTP.CN.AD?end=2021&locations=LU&start=1996&view=chart)
- [Principaux agrégats: trois approches (prix courants) (en millions EUR) - Statec](https://lustat.statec.lu/vis?tm=PIB%20annuel%20aux%20prix%20courants&pg=0&hc[Fr%C3%A9quence]=Annuelle&df[ds]=ds-release&df[id]=DF_E2100&df[ag]=LU1&df[vs]=1.0&pd=%2C2022&dq=.A)

<br>

<br>

<br>

### Taux de croissance du PIB (% de variation aux prix de l'année précédente chaînés; année de référence = 2015)

_Attention: la dernière valeur pour l'année 2022 est à vérifier!!!_

<img src="images/20230525-pib-growth-rate-luxembourg.png" alt="GDP Growth Luxembourg" width="50%">

**Source** :
- [Statec Data Explorer](https://lustat.statec.lu/vis?tm=PIB%20croissance%20annuelle&pg=0&hc[Fr%C3%A9quence]=Annuelle&df[ds]=ds-release&df[id]=DF_E2102&df[ag]=LU1&df[vs]=1.0&pd=2015%2C2022&dq=A.)
- [LaTeX source code (`main-lux-annual-growth-banquemondiale-alternative-design.tex`)](https://www.overleaf.com/read/bbgdjfqzrqkp)

## 6. Comment répartir la valeur

$$
 \begin{aligned} 
 \text{PIB} & = \sum\text{valeurs ajoutées} 
\end{aligned}
$$

<br>

<br>

<br>


```mermaid
graph TD;
A[valeur ajoutée] --> A1[salariés]
A1 --- A1a[salaires et <br> cotisations sociales]
A --> A2[Etat]
A2 --- A2a[impôts et taxes]
A --> A3[banques]
A3 --- A3a[intérêts]
A --> A4[propriétaires]
A4 --- A4a[partie du bénéfice]
A --> A5[entreprise]
A5 --- A5a[autofinancement]
style A fill:#C0C0C0
```

## 7. Comment le PIB a-t-il évolué dans le monde à long terme?

```mermaid
graph TD;
A[évolution du PIB <br> à long terme] --> A1[crosse de hockey]
A --> A2[progrès technique]
style A fill:#C0C0C0
```

## 8. Les indicateurs complémentaires

```mermaid
graph TD;
A[indicateurs complémentaires] --> A1[inégalités de revenus]
A --> A2[externalités négatives]
A --> A3[indice de <br> développement humain]
style A fill:#C0C0C0
```
