# Chapitre 3: Comment se forment les prix sur un marché?

Après avoir étudié ce chapitre, vous devriez être en mesure d'expliquer les concepts suivants:

    rôle des institutions, marché, structure de marché, 
    concurrence parfaite, concurrence imparfaite, monopole,
    oligopole, différentiation des produits, preneur de prix,
    atomicité, homogéneité, transparence de l'information, 
    pouvoir de négociation, fluidité du marché.

![Components of chapter 3](https://gitlab.com/econmediadb/economics-course/-/raw/main/concept-maps/20220524-year1-overview-chapter3.png "Concepts - Chapter 3")

## 1. Qu'est-ce qu'un marché

### 1.a. Circuit économique: économie monétaire avec marchés

[code source LaTeX](https://www.overleaf.com/read/hwbqncyqjgbk)

![Circuit economique](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220713-circuit-economique.png "Circuit économique")

### 1.b. Le rôle des institutions


```mermaid
graph TD
    A[Institutions] --> |déterminent| B[règles et pratique du marché]
    B --> |affectent| C[contrats]
    B --> |affectent| D[fonctionnement du marché]
```


### 1.c. Les marchés

```mermaid
graph TD
    A[4 grandes catégories] --> B[marché des biens et services]
    A --> C[marché du travail]
    A --> D[marché financier]
    A --> E[marché des changes]
    B --> |production| B1[entreprises]
    B --> |consommation| B2[ménages]
    C --> |offre de travail| C1[particulier]
    C --> |demande de travail| C2[entreprise]
    D --> |cours boursier| D1[marché boursier]
    D --> |taux d'intérêt| D2[marché des capitaux]
    E --> |taux de change| E1[monnaie]
```

### 1.d. Structure de marché


```mermaid
graph TD
    A[Structure de marché] --> B[concurrence parfaite]
    A --> C[monopole]
    A --> D[oligopole]
```

```mermaid
graph TD
    A[Structure de marché] --> |différentiation selon| B[3 critères]
    B --> C[degré de concurrence] 
    B --> D[nombre de producteurs sur le marché]
    B --> E[différentiation ou non-différentiation des produits]
```

### 1.e. La concurrence parfaite

```mermaid
graph TD
    A[concurrence parfaite - preneur de prix] --> |atomicité| A1[nombreux agents]
    A --> |homogéneité| A2[bien et services identiques]
    A --> |transparence de l'information| A3[connaissance de l'information]
    A --> A4[pouvoir de négociation]
    A --> |fluidité du marché| A5[entrée et sortie libre du marché]
```

### 1.f. La concurrence imparfaite

```mermaid
graph TD
    A[concurrence imparfaite] --> A1[nombre de concurrents faibles]
    A --> A2[biens et services hétérogènes]
    A --> A3[barrière à l'entrée et sortie du marché]
```

```mermaid
graph TD
    A[concurrence imparfaite] --> A1[oligopole]
    A --> A2[monopole]
```



## 2. Comment se forme l'équilibre sur un marché?

### 2.a. Grille pour les applications

[code source LaTeX](https://www.overleaf.com/read/nzsvtfdbppyk)

![Grid](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220619-grid-demand-supply.png "Grid")

![Grid 2](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220619-grid-demand-supply-2.png "Grid 2")

![Grid 3](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220619-grid-demand-supply-3.png "Grid 3")

### 2.b. Fonction de demande



```mermaid
graph TD
    A[demande] --> A1[DAP]
    A1 --> A2[seuil]
    A2 --> A3[prix du bien]
    A2 --> A4[importance accordé <br> au bien]
    A2 --> A5[ressource disponible]
```
**Note**: DAV = disposition à payer

![Demand](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220524-demand-curve.png "Fonction de demande")

### 2.c. Fonction d'offre

```mermaid
graph TD
    A[demande] --> A1[DAV]
    A1 --> A2[seuil]
    A2 --> A3[prix du bien]
    A2 --> A4[importance accordé <br> au bien]
    A2 --> A5[ressource disponible]
```
**Note**: DAV = disposition à vendre

![Supply](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220524-supply-curve.png "Fonction d'e demande'offre")

### 2.d. Prix et quantité d'équilibre

```mermaid
graph TD
    A[convention pour le graphique] --> A1[axe des ordonnées: <br> prix]
    A --> A2[axe des abscisses: <br> quantités]
```

![Market equilibrium](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220524-market-equilibrium.png "Equilibre du marché")

([code source LaTeX[(https://www.overleaf.com/read/rcpmvdbkszjk))


## 3. Changements d'offre et de demande

### 3.a. Déplacement de la demande

```mermaid
graph TD
    A[déplacement de la demande] --> A1[déplacement vers <br> la droit]
    A1 --> A2[demande excédentaire]
    A2 --> A3[augmentation du <br> prix d'équilibre]
    A3 --> A4[nouveau prix <br> et quantité d'équilibre <br> en B]
```

[code source LaTeX](https://www.overleaf.com/read/rcpmvdbkszjk)

![Deplacement demande](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220524-demande-deplacement.png "Déplacement de la demande")

### 3.b. Déplacement de l'offre : Baisse de l'offre

```mermaid
graph TD
    A[déplacement de l'offre] --> A1[déplacement vers <br> la gauche]
    A1 --> A2[demande excédentaire]
    A2 --> A3[augmentation du <br> prix d'équilibre]
    A3 --> A4[nouveau prix <br> et quantité d'équilibre <br> en B]
```
**Note**: Expliquer le raisonnement économique de l'augmentation des prix. 

[code source LaTeX](https://www.overleaf.com/read/rcpmvdbkszjk)

![Deplacement offre](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220525-offre-deplacement.png "Déplacement de l'offre")


### 3.c. Déplacement de l'offre : Hausse de l'offre

```mermaid
graph TD
    A[déplacement de l'offre] --> A1[déplacement vers <br> la droite]
    A1 --> A2[offre excédentaire]
    A2 --> A3[diminution du <br> prix d'équilibre]
```

[code source LaTeX](https://www.overleaf.com/read/vmjtnwddwqdm)

![Deplacement offre: équilibre initial](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220713-supply-shift-01.png "Déplacement de l'offre")

![Deplacement offre: augmentation de l'offre](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220713-supply-shift-02.png "Déplacement de l'offre")

![Deplacement offre: offre excédentaire et nouvel équilibre](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220713-supply-shift-03.png "Déplacement de l'offre")


## 4.A. APPLICATION: La guerre de Sécession et le blocage des ports

![civilwarblockade](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Scott-anaconda.jpg/2560px-Scott-anaconda.jpg "civilwarblockade")


Lors du déclenchement de la guerre, le 12 avril 1861, le président Abraham Lincoln a ordonné à la marine américaine de bloquer les ports des États confédérés. Ces États avaient déclaré unilatéralement leur indépendance par rapport aux États-Unis, de manière à préserver l’institution qu’était l’esclavage.

Conséquence du blocus naval, les exportations de coton brut américain à destination des filatures de textile du Lancashire en Angleterre, qui représentaient trois quarts de leurs approvisionnements de cette matière première essentielle, se sont pratiquement arrêtées. Naviguant de nuit, quelques navires sont parvenus à échapper aux patrouilles de Lincoln, mais 1 500 navires ont été détruits ou capturés.

Nous allons voir dans cette unité que le prix de marché d’un bien tel que le coton est déterminé par l’interaction entre l’offre et la demande. Dans le cas du coton brut, les faibles quantités atteignant l’Angleterre lors du blocus correspondaient à une réduction dramatique de l’offre. Il y avait un large excès de demandeexcès de demande Une situation dans laquelle la quantité d’un bien demandée est supérieure à la quantité offerte au prix en vigueur. Voir également : excès d’offre.close – c’est-à-dire qu’au prix en vigueur, la quantité de coton brut demandée dépassait l’offre disponible. Par conséquent, certains vendeurs ont réalisé qu’ils pouvaient tirer profit de la situation en augmentant leurs prix. Finalement, le coton fut vendu à des prix six fois plus élevés qu’avant la guerre, ce qui permit aux marchands chanceux qui parvenaient à contourner le blocus de poursuivre leurs affaires. La consommation de coton chuta de moitié par rapport aux niveaux d’avant la guerre, faisant perdre leur emploi à des centaines de milliers de personnes qui travaillaient dans les filatures de coton.

Les propriétaires des filatures réagirent. La hausse des prix représentait en effet pour eux une augmentation de leurs coûts. Certaines entreprises firent faillite et quittèrent le secteur du fait de la réduction de leur profits. Les propriétaires des filatures se tournèrent vers l’Inde pour trouver une alternative au coton américain, augmentant ainsi fortement la demande pour le coton indien. Certains vendeurs sur le marché du coton indien profitèrent de la demande excédentaire pour augmenter leurs prix, ce qui entraîna une hausse des prix du coton indien, qui crurent rapidement, atteignant un niveau proche de celui du coton américain.

Du fait des revenus plus élevés qu’ils pouvaient désormais obtenir en cultivant le coton, les agriculteurs indiens abandonnèrent les autres cultures au profit de celui-ci. Le même phénomène se produisit partout où le coton pouvait être cultivé, notamment au Brésil. En Égypte, les agriculteurs qui s’étaient empressés d’accroître leur production de coton en réponse à la hausse des prix commencèrent à employer des esclaves, capturés (à l’instar des esclaves américains pour la libération desquels Lincoln se battait) en Afrique subsaharienne.

Il y avait toutefois un problème. Le seul pays producteur de coton pouvant combler le déficit laissé par les États-Unis était l’Inde, mais le coton indien différait du coton américain et nécessitait un tout autre traitement. Quelques mois après le passage au coton indien, de nouvelles machines furent développées pour le traiter.

Comme la demande pour ce nouvel équipement augmentait fortement, des entreprises comme Dobson et Barlow qui fabriquaient de la machinerie pour textile, virent leurs profits décoller. Nous le savons pour cette entreprise car les registres détaillés de ses ventes ont été conservés. Elle a réagi, à l’époque, en augmentant la production de ces nouvelles machines et d’autres équipements. Aucune filature ne pouvait se permettre de prendre du retard dans la course au rééquipement, car sinon il leur était impossible d’utiliser la nouvelle matière première. Il en résulta, selon les mots de Douglas Farnie, un historien spécialisé dans l’histoire de la production de coton, « un tel investissement en capital que cela était quasiment équivalent à la création d’une nouvelle industrie ».

La leçon pour les économistes est la suivante : Lincoln ordonna le blocus, mais les agriculteurs et vendeurs qui augmentèrent le prix du coton par la suite ne répondirent pas, eux, à des ordres. Pas plus que les propriétaires des filatures qui réduirent leur production de textile et licencièrent les ouvriers des filatures, ni que les propriétaires des filatures qui cherchaient désespérément de nouvelles sources d’approvisionnement en matière première. En commandant de nouvelles machines, les propriétaires des filatures ont déclenché un essor pour l’investissement et de nouveaux emplois.

Toutes ces décisions ont été prises en quelques mois, par des millions de personnes qui, pour la plupart, ne se connaissaient pas, chacune cherchant à tirer le meilleur parti d’une toute nouvelle situation économique. Le coton américain était devenu plus rare et les individus ont alors réagi, des champs de coton du Maharashtra en Inde jusqu’au delta du Nil, en passant par le Brésil et les filatures du Lancashire.

Pour comprendre la manière dont le changement du prix du coton a transformé le système mondial de production de textile et de coton, vous pouvez penser aux prix déterminés sur les marchés comme des messages. L’augmentation du prix du coton américain véhiculait le message suivant : « Trouvez d’autres sources d’approvisionnement et les nouvelles technologies appropriées pour leur utilisation. » De même, lorsque le prix de l’essence augmente, le message envoyé à l’automobiliste est : « Prenez le train. » Il est aussi transmis à l’opérateur de voies ferrées : « Il y a des profits à réaliser en augmentant le nombre de trains qui circulent. » Quand le prix de l’électricité monte, l’entreprise ou le ménage entend : « Pensez à l’installation de cellules photovoltaïques sur votre toit. »

Dans de nombreux cas – comme la chaîne d’événements ayant suivi la décision prise par Lincoln dans son bureau le 12 avril 1861 –, les messages ont un sens non seulement pour les entreprises individuelles et les familles, mais aussi pour la société en général : si quelque chose devient plus cher, il est probable que cela soit dû à une augmentation du nombre de demandeurs, ou à une augmentation du coût de sa production, ou aux deux. En lui trouvant une alternative, l’individu économise de l’argent et préserve les ressources de la société. En effet, sous certaines conditions, les prix reflètent avec précision la rareté d’un bien ou service.

### 4.A.1 Représentation schématique du texte

```mermaid
graph TD
    A[Guerre de Sécession] --> |entraîne| B[blocus naval]
    B --> |limite| C[exportations du coton]
    C --> |génère| D(excès de demande du coton en GB)
    D --> |génère| E[augmentation des prix du coton en GB]
    E --> |diminue| F[consommation de coton en GB]
    F --> |génère| G[perte d'emploi en GB]
```

### 4.A.2 Modèle utilisé pour comprendre le méchanisme

Afin de comprendre l'augmentation du prix de coton qui provient de l'excès de demande du coton, nous pouvons utiliser un modèle simple d'offre et de demande.

Lorsque le blocus empêche les navires de transport de coton de sortir des ports américains, l'offre se déplace vers la gauche, ce qui entraîne une hausse du prix d'équilibre et une baisse de la quantité d'équilibre.

**Figure 4.A.1.a** ([code source LaTeX](https://www.overleaf.com/read/vmjtnwddwqdm))

![Deplacement offre gauche 1](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220704-SupplyShiftToLeft-01.png  "Deplacement offre gauche 1")

**Figure 4.A.1.b** ([code source LaTeX](https://www.overleaf.com/read/vmjtnwddwqdm))

![Deplacement offre gauche 2](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220704-SupplyShiftToLeft-02.png  "Deplacement offre gauche 2")

**Figure 4.A.1.c** ([code source LaTeX](https://www.overleaf.com/read/vmjtnwddwqdm))

![Deplacement offre gauche 3](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220704-SupplyShiftToLeft-03.png  "Deplacement offre gauche 3")

**Figure 4.A.1.d** ([code source LaTeX](https://www.overleaf.com/read/vmjtnwddwqdm))

![Deplacement offre gauche 4](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220704-SupplyShiftToLeft-04.png  "Deplacement offre gauche 4")
