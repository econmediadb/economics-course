# Concept maps

## 1. Definiton and use of concept maps 

### 1.1. What is the meaning of *concept mapping*?

Concept mapping is the strategy employed to develop a concept map. A concept map consists of nodes or cells that contain a concept, item, or question, and links. The links are labeled and denote direction with an arrow symbol. The labeled links explain the relationship between the nodes.
The arrow describes the direction of the relationship and reads like a sentence.

An example of a general concept map can be found below ([source](https://www.a-z.lu/discovery/fulldisplay?docid=cdi_proquest_miscellaneous_1642318663&context=PC&vid=352LUX_BIBNET_NETWORK:BIBNET_UNION&search_scope=DN_and_CI&tab=DiscoveryNetwork&lang=fr)):

```mermaid
graph TD
    A[concept map] --> |is a|B[graph]
    B --> |consists of| B1[links]
    B --> |consists of| B2[nodes]
    B1 --> |relate pairs of|B2
    B1 --> |have|C1[labels]
    B2 --> |represent|C2[concepts]
    C1 --> |explain <br> relationships|B2
```

### 1.2. Why should we use *concept mapping*?

Concept mapping facilitates meaningful learning and, as a result, higher scores can be achieved in assessments when concept maps are used in the classroom.

Concept maps also perform a social function. The construction of a concept map brings people together if the instructor structures concept mapping as a cooperative group activity

Concept mapping can be used to enhance learners' understanding of a subject and as an assessment tool for instructors to test student mastery of the material.

Concept maps allow to:
1. think abstractly about the material and be able to apply theoretical concepts to their everyday lives, 
2. retain economic concepts long term, and 
3. experience improved attitudes about the usefulness and accessibility of economics

Concept maps allow us to illustrate the complex thinking of a learner and provide insight how learners structure their thinking and understanding of a certain content.

### 1.3. How to introduce concept maps to learners

Once parts of the theoretical material has been introduced, a set of homework questions will be provided. 

These weekly homework questions can involve the construction of a concept map. The learner will construct their concept map from the concepts provided by the question before the class in a presentation and their fellow students will comment, agree or disagree with the linkages, or provide their own alternative concept map.

Through this process, the class constructs a concept map, making the process of learning and understanding a productive and interesting exercise as a result of discussing the links between concepts and finalizing the construction of a “class” concept map.

### 1.4. Key components to a concept map

- Concepts (associated with a topic and essential to understanding)
- Linking words (outlining relationships between concepts to form propositions).
- Cross-links (showing connections between disciplinary areas)
- Hierarchy (general and encompassing concepts to specifics)
- Focus question: which may or may not be used to guide the process of creating the concept map.

## 2. Examples

### 2.1. Example 1: Factors that influence the supply curve

```mermaid
graph TD
    A[supply curve] --> |movement along <br> the supply curve|B1[own <br> price]
    B1 --> B11[change in quantity <br> supplied and price]
    A --> |shift of the <br> supply curve| B2[factors causing <br> the shift]
    B2 --> B21[price of <br> related <br> outputs]
    B2 --> B22[number of <br> suppliers]
    B2 --> B23[price of <br> inputs]
    B2 --> B24[expectations]
    B2 --> B25[technology]
    B21 --> B3[a different amount is supplied at the same price]
    B22 --> B3
    B23 --> B3
    B24 --> B3
    B25 --> B3
```

### 2.2. Example 2: Concepts associated with perfect competition as a market structure

```mermaid
graph TD
    A[market characteristics] --> B1[many buyers <br> and sellers]
    A --> B2[no barriers <br> to entry]
    A --> B3[price taking]
    A --> B4[perfect <br> information]
    A --> B5[homogeneous <br> products]
    B1 --> C[perfect competition]
    B2 --> C
    B3 --> C 
    B4 --> C 
    B5 --> C 
    C --- D[market structure]
    D --- D1[monopoly]
    D --- D2[imperfect <br> competition]
    C --> E1[short run]
    E1 --> E11[positive <br> economic <br> profit]
    E1 --> E12[zero <br> economic <br> profit]
    E1 --> E13[negative <br> economic <br> profit]
    C --> E2[long run]
    E2 --> E21[zero <br> economic <br> profit]
    style D fill:#0080FF
    style C fill:#CCFFFF
    style E1 fill:#CCFFFF
    style E11 fill:#CCFFFF
    style E12 fill:#CCFFFF
    style E13 fill:#CCFFFF
    style E2 fill:#CCFFFF
    style E21 fill:#CCFFFF
    style D1 fill:#CCE5FF
    style D2 fill:#CCCCFF    

```

## Sources

- [The Effectiveness of collaborative problem-solving tutorials in introductory microeconomics](https://www.a-z.lu/discovery/fulldisplay?docid=cdi_proquest_miscellaneous_38977969&context=PC&vid=352LUX_BIBNET_NETWORK:BIBNET_UNION&search_scope=DN_and_CI&tab=DiscoveryNetwork&lang=fr)
- [Effectiveness of concept maps in economics: Evidence from Australia and USA](https://www.a-z.lu/discovery/fulldisplay?docid=cdi_crossref_primary_10_1016_j_lindif_2007_03_003&context=PC&vid=352LUX_BNL:BIBNET_UNION&search_scope=DN_and_CI_UCV&tab=DiscoveryNetwork_UCV&lang=fr)
- [ Enseigner autrement avec le mind mapping : cartes mentales et conceptuelles
](https://www.a-z.lu/discovery/fulldisplay?docid=alma990016038310107255&context=L&vid=352LUX_EDU:BIBNET_UNION&search_scope=DN_and_CI_UCV&tab=DiscoveryNetwork_UCV&lang=fr)
- [Concept Mapping Tutorial](https://wiki.ubc.ca/Documentation:Concept_Mapping_Tutorial)
- [The Theory Underlying Concept Maps and How to Construct and Use Them](https://cmap.ihmc.us/docs/theory-of-concept-maps)
- [Concept maps: A useful and usable tool for computer-based knowledge assessment? A literature review with a focus on usability.](https://www.a-z.lu/discovery/fulldisplay?docid=cdi_proquest_miscellaneous_1642318663&context=PC&vid=352LUX_BIBNET_NETWORK:BIBNET_UNION&search_scope=DN_and_CI&tab=DiscoveryNetwork&lang=fr)
- [Concept Maps (SCRIPT)](https://script.lu/fr/activites/innovation/concept-maps)
- [OASYS: un outil graphique numérique développé de l'uni.lu](https://www.oasys4schools.lu/conceptmaps/)
