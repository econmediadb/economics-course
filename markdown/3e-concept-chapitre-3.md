# Chapitre 3 : Comment se forment les prix sur un marché ?

## 2. Qu'est-ce qu'un marché ?

```mermaid
graph TD;
A[marché] --> |est une| B1[forme d'organisation ordonnée];
B1 --> |des|B2[échanges économiques]
B2 --> |dont les règles <br> sont assurées <br> par les| B3[institutions]
B3 --> |à travers les|B4[contrats]
style A fill:#C0C0C0
```

<br>

<br>

<br>

```mermaid
graph TD;
A[4 grandes catégories de marchés] --> B1[marché des biens et services];
A --> B2[marché du travail]
A --> B3[marché financier]
B3 --> B31[marché boursier]
B3 --> B32[marché des capitaux]
A --> B4[marché des changes]
style A fill:#C0C0C0
```

<br>

<br>

<br>

```mermaid
graph TD;
AA1[nombre de producteurs <br> sur le marché] --> |détermine|A
AA2[différenciation ou <br> non-différentiation <br> des produits] --> |détermine|A
A[structures de marché] --> B1[concurrence parfaite];
A --> B2[concurrence imparfaite]
B2 --> B21[monopole]
B2 --> B22[oligopole]
B1 --> |les vendeurs <br> sont|B11[preneurs de prix]
B1 --> B12[homogénéité <br> du produit]
B1 --> B13[atomicité de <br> l'offre et <br> de la demande]
B1 --> B14[transparence <br> de l'information] 
B1 --> B15[fluidité <br> du marché]
B22 --> B22a[petit nombre <br> de producteurs]
B22 --> B22b[infinité de <br> demandeurs]
B21 --> B21a[un seul <br> vendeur]
B21 --> B21b[multitude <br> d'acheteur]
style A fill:#C0C0C0
```

<br>

<br>

<br>

```mermaid
graph TD;
A[formation de l'équilibre sur le marché] --> B1[demande du marché];
A --> B2[offre du marché]
B1 --- C[équilibre du marché <br> demande=offre ]
B2 --- C
C --> D[prix d'équilibre]
style A fill:#C0C0C0
```

<br>

<br>

<br>

