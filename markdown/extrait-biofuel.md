
**Energie Les agrocarburants, une fausse bonne idée ?**
(Alternatives Economiques - 01 Juillet 2012 - Gabriel Hassan)

L’idée de transformer des végétaux pour produire de l’énergie est ancienne. Elle a été mise en oeuvre à grande échelle dès les années 1970 au Brésil, après le premier choc pétrolier. En 1975, le géant mondial du sucre lançait ainsi le plan Proalcool pour remplacer une partie de l’essence par de l’alcool (ou éthanol) de canne. (...)

Aujourd’hui, les États-Unis dominent la production mondiale grâce à l’éthanol de maïs et ont dépassé le Brésil en termes de volumes produits. (...)

Aujourd’hui, 90 % des véhicules neufs vendus au Brésil sont flex-fuel et plus de la moitié des besoins en carburant sont couverts par l’éthanol. Le pays dispose d’un très gros potentiel grâce à la canne à sucre, par ailleurs beaucoup plus compétitive que le maïs ou la betterave pour produire de l’éthanol. Cependant, à cause du niveau élevé de sa demande intérieure, toujours croissante, le Brésil est parfois amené à couvrir une partie de ses besoins en se fournissant aux États-Unis.

(...)

Mais à mesure que la production augmente, la contestation des agrocarburants s’intensifie. Leurs effets pervers sont dénoncés depuis longtemps. Dès 2008, au moment de la crise alimentaire, il est apparu qu’ils participaient à la flambée des prix agricoles et à leur volatilité. L’autre grand problème est l’expansion des terres dédiées à leur culture. Pour atteindre son objectif d’incorporation en 2020, il faudrait que l’Union européenne importe au moins 40 % de sa consommation, alors que les agrocarburants sont censés contribuer à réduire sa dépendance énergétique. Par ailleurs, ces agrocarburants importés proviendront essentiellement de pays du Sud : Amérique latine, Asie et Afrique subsaharienne. Or dans ces régions, les créations de grands domaines agricoles se font fréquemment aux dépens des populations locales dont les droits fonciers sont peu ou pas protégés par les gouvernements. 

Outre ces impacts sociaux, l’extension des agrocarburants pose une redoutable question environnementale. En Indonésie par exemple, le palmier à huile cause une déforestation massive, et son usage énergétique aggrave le phénomène. Quand les agrocarburants ne s’implantent pas dans des espaces boisés ou des savanes, ils peuvent se substituer à d’autres cultures ou à des pâturages, lesquels vont être déplacés, ce qui va entraîner le défrichage de forêts et des brûlis, et donc des émissions de CO2. (...)

Les émissions de gaz à effet de serre des agrocarburants ont longtemps été jugées bien inférieures à celles de leurs homologues fossiles en raison d’analyses qui ne prenaient pas en compte les potentielles destructions de forêts, de prairies ou de tourbières. En particulier, les effets indirects, liés au déplacement par les cultures à fins énergétiques d’autres cultures ou de pâturages, étaient jugés difficiles à estimer. Ces dernières années, les études se sont multipliées sur ce sujet. Si les résultats sont variables d’une analyse à l’autre, la plupart concluent que l’effet du changement d’affectation des sols est important. Il peut même rendre les agrocarburants plus polluants qu’un carburant fossile. 

(...)

Les défenseurs des agrocarburants annoncent de nouvelles filières qui permettraient de dépasser ces obstacles. Depuis des années, des agrocarburants cellulosiques, à base de sous-produits et de déchets végétaux (pailles de blé, tiges de canne à sucre, broussailles...) sont à l’étude. Les microalgues offrent également des perspectives.


**Questions**

1. Utilisez les informations ci-dessus pour montrer comment le concept de coût d'opportunité pourrait être utilisé pour expliquer les compromis tels qu'ils affectent :
    - les gouvernements
    - les agriculteurs
    - les consommateurs.
2. Décrire les coûts et les avantages probables de la demande mondiale accrue de biocarburants.


**Solution**

1. Les compromis peuvent être les suivants : 
    - *gouvernements* - en interdisant les exportations de denrées alimentaires, le gouvernement renonce aux bénéfices des recettes d'exportation ; 
    - *agriculteurs* - en choisissant de cultiver des plantes pour la production de biocarburants, les agriculteurs réduisent la quantité de terres disponibles pour d'autres cultures et pour la production de bétail ; 
    - *consommateurs* - avec moins de terres consacrées à la production de denrées alimentaires, il peut y avoir une réduction de l'offre de denrées alimentaires et les consommateurs renoncent à l'avantage d'une nourriture moins chère.

