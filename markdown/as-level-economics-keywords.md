# AS-level Economics

## Part 1: Basic economic ideas and resource allocation

### Chapter 1: Scarcity, choice and opportunity cost

- wants
- needs
- resources
- economic problem
- opportunity cost
- scarcity and choice

### Chapter 2: Economic methodology

- positive statement
- normative statement
- value judgement
- ceteris paribus
- economic law
- microeconomics
- macroeconomics
- short run
- long run
- very long run

### Chapter 3: Factors of production

- primary sector
- secondary sector
- tertiary sector
- land
- labour
- capital
- enterprise
- entrepreneur
- human capital
- physical capital
- rent
- wage
- salary
- interest
- profit
- specialisation
- division of labour
- Adam Smith
- enterprise culture

### Chapter 4: Resource allocation in different economic systems

- allocative mechanism
- market economy or market system
- market
- planned (or command) economy
- mixed economy
- transitional economy

### Chapter 5: Production possibility curves

- production possibility curve (or frontier)
- scarcity and choice
- economic growth

### Chapter 6: Classification of goods and services

- free good
- private good
- rivalry
- excludability
- public good
- non-excludability
- non-rivalry
- free rider
- government expenditure
- non-rejectability
- merit good
- information failure
- market imperfection
- market failure
- demerit good

## Part 2: The price system and the microeconomy

### Chapter 7: Demand and supply curves

- effective demand
- demand
- law of demand
- demand curve
- demand schedule
- derived demand
- supply curve
- supply schedule
- change in quantity demanded
- extension in demand
- contraction in demand
- change in quantity supplied
- extension in supply
- contraction in supply
- change in demand
- composite demand
- normal good
- inferior good
- indirect taxes
- subsidy

### Chapter 8: Price elasticity, income elasticity and cross elasticity of demand

- price elasticity of demand
- income elasticity of demand
- cross elasticity of demand (or cross-price elasticity of demand)
- substitute goods
- complementary goods
- perfectly inelastic
- inelastic
- unitary elasticity
- elastic
- perfectly elastic
- total revenue

### Chapter 9: Price elasticity of supply

- price elasticity of supply
- stocks
- perishability

### Chapter 10: The interaction of demand and supply

- equilibrium 
- disequilibrium
- equilibrium price
- equilibrium quantity
- joint demand
- alternative demand
- derived demand
- joint supply
- rationing
- signalling
- price mechanism
- transmission of preferences
- incentivisation
- scarcity and choice

### Chapter 11: Consumer and producer surplus

- consumer surplus
- producer surplus

## Part 3: Government microeconomic intervention

### Chapter 12: Reasons for government intervention in markets

### Chapter 13: Methods and effects of government intervention in markets

- indirect tax
- excise duty
- impact of tax
- incidence of tax
- specific tax
- subsidy
- impact of a subsidy
- incidence of a subsidy
- direct provision of goods and services
- maximum price
- minimum price
- price stabilisation
- buffer stock

### Chapter 14: Addressing income and wealth inequality

- income
- wealth
- Gini coefficient
- minimum wage
- transfer payment

## Part 4: The macroeconomy

### Chapter 15: National income statistics

- national income
- gross domestic product (GDP)
- gross national income (GNI)
- net national income (NNI)
- at current market prices
- at constant prices
- GDP deflator
- exports
- imports
- depreciation (of capital)
- net domestic product (NDP)
- net national product (NNP)

### Chapter 16: Introduction to the circular flow of income

- circular flow of income
- closed economy
- open economy
- injection
- leakage (or withdrawal)

### Chapter 17: Aggregate demand and aggregate supply analysis

- aggregate demand (AD)
- aggregate supply (AS)

### Chapter 18: Economic growth

- economic growth
- business cycle (or trade cycle)
- labour-intensive production
- capital-intensive production

### Chapter 19: Unemployment

- unemployment
- unemployment rate
- labour force
- working population
- dependency ratio
- participation rate
- claimant count
- labour force survey

### Chapter 20: Price stability

- inflation
- creeping inflation
- accelerating inflation
- hyperinflation
- deflation
- disinflation
- general price level
- cost of living
- consumer price index (CPI)
- sampling
- household expenditure
- weights
- base year
- nominal value
- real value
- demand-pull inflation
- monetary inflation
- cost-push inflation
- anticipated inflation
- unanticipated inflation
- imported inflation
- menu costs
- shoe lether costs
- fiscal drag
- stagflation

## Part 5: Government macroeconomic intervention

### Chapter 21: Government macroeconomic policy objectives

### Chapter 22: Fiscal policy

- fiscal policy
- government budget
- budget deficit
- budget surplus
- balanced budget
- national debt
- direct tax
- income tax
- indirect tax
- specific tax
- ad valorem tax
- progressive taxation
- regressive taxation
- proportional taxation
- flat-rate tax
- marginal tax rate
- average tax rate
- canons of taxation
- capital spending (or investment spending)
- current spending
- discretionary fiscal policy
- automatic stabilisers
- expansionary fiscal policy
- contractionary fiscal policy

### Chapter 23: Monetary policy

- monetary policy
- hot money
- quantitative easing
- open market operations
- expansionary monetary policy
- contractionary monetary policy

### Chapter 24: Supply-side policy

- supply-side policy
- productivity
- productive capacity
- labour productivity

## Part 6: International economic issues

### Chapter 25: The reasons for international trade

- absolute advantage
- comparative advantage
- trading possibility curve
- bilateral trade
- multilateral trade
- globalisation
- specialisation
- free trade
- trade liberalisation
- trade creation
- World Trade Organization
- terms of trade

### Chapter 26: Protectionism

- protectionism
- tariff
- import duty
- quota
- export subsidy
- exchange controls
- embargo
- voluntary export restraint (VER)
- infant industry argument
- sunrise industries
- sunset industries
- dumping

### Chapter 27: Current account of the balance of payments

- current account of the balance of payments
- balance of trade in goods account
- exports
- imports
- balance of trade in services account
- primary income
- net investment income
- secondary income
- current transfers
- deficit
- surplus
- external balance
- marginal propensity to import

### Chapter 28: Exchange rates

- exchange rate
- floating exchange rate
- depreciation
- appreciation

### Chapter 29: Policies to correct imbalances in the current account of the balance of payments



