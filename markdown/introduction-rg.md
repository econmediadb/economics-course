# Introduction

Après avoir étudié ce chapitre, vous devriez être en mesure d'expliquer les concepts suivants: 

    acteur économique, rareté, besoin économique, utilité, 
    choix et prise de décision, problèmes économiques fondamentaux, 
    système économique, économie normative et économie positive

![Components of introduction](https://gitlab.com/econmediadb/economics-course/-/raw/main/concept-maps/20220524-year1-overview-introduction.png "Concepts - Introduction")

## 1. Concept fondamental: la rareté

### 1.a. Acteur économique

```mermaid
graph TD;
A[acteur économique] --> |effectue des| B[actes/faits économiques];
B --> C[acheter des biens];
B --> D[travailler à plein temps];
B --> E[épargner];
B --> F[investir];
```

### 1.b. Notion d'acteur économique

```mermaid
graph TD;
A[acteur économique] --> |a de nombreux| B[besoins: innombrables et illimités];
B --> |diffèrent selon| C[types d'acteurs];
B --> |varient selon| D[caractéristiques];
D --> E[temps];
D --> F[espace];
D --> G[valeurs]
```

### 1.c. Notion de besoin économique

```mermaid
graph TD;
A[besoins] -->  B[primaires: besoins vitaux];
A -->  C[secondaires: désirs non-nécessaire];
B --> D[manger]
B --> E[dormir]
B --> F[se loger]
C --> G[voiture climatisée]
```

### 1.d. Notion d'utilité

```mermaid
graph TD;
A[satisfaction des besoins] -->  B[utilité]
B --> C[mesure du bien-être]
```

### 1.e. Ressources rares et choix

```mermaid
graph TD;
A[besoins] -->  B[satisfaction des besoins]
B --> C[ressources disponibles]
C --> D[rareté]
D --> E[prise de décision]
E --> F[choix]
E--> G[arbitrages]
```




## 2. Les trois problèmes économiques fondamentaux

### 2.a. Les problèmes fondamentaux des sciences économique (selon Samuelson)

```mermaid
graph TD;
A[3 problèmes économiques fondamentaux] --> B1[que produire?]
A --> B2[comment produire?]
A --> B3[pour qui produire?]
```

### 2.b. Définition de l'économie

```mermaid
graph TD;
A[définition de l'économie] -->  B1[allocation des ressources]
A --> B2[fonctionnement de la société]
A --> B3[revenu des entreprises]
A --> B4[décisions politiques]
```

### 2.c. Les trois problèmes fondamentaux

```mermaid
graph TD;
A[3 problèmes économiques fondamentaux] --> B1[que produire?]
A --> B2[comment produire?]
A --> B3[pour qui produire?]
B1 --> B11[coût d'opportunité]
B1 --> B12[coût de production]
B2 --> B21[combinaison productive]
B21 --> B211[efficacité productive]
B211 --> B2111[frontière des possibilités de production]
B2111 --> B21111[modèle économique]
B3 --> B31[biens et services produits]
B31 --> B311[facteurs de production]
B311 --> B3111[répartition des revenus]
B3111 --> B31111[circuit économique]
```



## 3. Les systèmes économiques

### 3.a. Le système économique

```mermaid
graph TD;
A[système économique] -->  B1[économie de marché]
A --> B2[économie planifiée]
B1 --> B11[marché & prix = régulateur]
B11 --> B111[capitalisme]
B111 --> B1111[fluctuations des prix]
B1111 --> |sont fonction de| B11111[offre & demande]
B2 --> B21[plan de l'Etat = régulateur]
B21 --> B211[exemple URSS]
B211 --> B2111[investissement]
B211 --> B2112[production]
B211 --> B2113[prix]
B2111 --> B212[régulé par l'Etat]
B2112 --> B212
B2113 --> B212
```

### 3.b. Système économique et pays

[code source LaTeX](https://www.overleaf.com/read/jcdtgbxbkyxh)

![Systeme Economique](https://gitlab.com/econmediadb/economics-course/-/raw/main/images/20220531-systeme-economique-pays.png "Systeme Economique")

A l'extrémité _économie de marché_ du spectre nous retrouvons des pays comme les États-Unis, le Royaume-Uni et le Chili. Hong Kong, bien que faisant maintenant partie de la Chine, a une longue histoire en tant qu'économie capitaliste de marché et est généralement considérée comme opérant à l'extrémité capitaliste du marché. Les pays à l'extrémité _économie planifiée_ du spectre comprennent la Corée du Nord et Cuba.

Certaines économies européennes, telles que la France, l'Allemagne et la Suède, ont un degré de réglementation suffisamment élevé pour que nous les considérions comme opérant davantage vers le centre du spectre.

Source: [Principles of economics](https://open.lib.umn.edu/principleseconomics/chapter/2-3-applications-of-the-production-possibilities-model/)

- **libéralisme économique**: les _prix_, les _quantités produites_ et l'_allocation des facteurs de production_ sont déterminés en fonction de la recherche du profit des entreprises et des individus en concurrence



## 4. Economie normative et économie positive

### 4.a. Economie normative et économie positive

```mermaid
graph TD;
A[économie] --> B[économie positive]
B --> |à travers| B1[modèles économiques]
B1 --> |tente d'expliquer| B11[évènements économiques]
A --> C[économie normative]
C --> |faire des| C1[recommandations]
C1 --> C11[politique économique]
```



