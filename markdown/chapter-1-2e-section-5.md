## 1.5. Détermination des prix

*L’interaction entre l’offre et la demande détermine un équilibre de marché tel que les acheteurs et les vendeurs sont preneurs de prix et réalisent l’échange à la quantité qui correspond au prix du marché ; on parle d’équilibre concurrentiel. Si la demande est supérieure à l’offre (situation de pénurie) le prix va augmenter et si l’offre est supérieure à la demande (situation de surproduction) le prix va baisser pour que l’équilibre soit de nouveau atteint.*

Nous connaissons maintenant la courbe d’offre et la courbe de demande (voir Graphique 1.1 et Graphique 1.2) sur le marché du pain. Le Graphique 1.3 montre que le prix d’équilibre est exactement de 2 euros. À ce prix, le marché est à l’équilibre : les consommateurs demandent 5 000 pains par jour et les entreprises en fournissent 5.000 par jour.

Si de nombreuses entreprises (offreurs) fabriquent des produits identiques et si les clients (demandeurs) peuvent choisir n’importe quelle entreprise pour effectuer leurs achats, alors les entreprises sont *preneuses de prix* à l’équilibre. Elles n’auraient aucun avantage à vendre à un prix différent de celui en vigueur.

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-03.jpg" />
    <br>
    Graphique 1.3 Équilibre sur le marché du pain.
