# Notions et concepts utilisés dans le cours de 2e

## Emploi, travail et chômage

### 1. Les différentes notions de l'emploi

```mermaid
graph TD;
A[population totale] -->  B1[population active];
A -->  B2[population inactive];
B1 --> C1[population active <br> occupée]
B1 --> C2[population active <br> inoccupée]
```

<br>

<br>

```mermaid
graph TD;
A[emploi] -->  B1[emploi intérieur];
A -->  B2[emploi national];
```

<br>

<br>

```mermaid
graph TD;
A[statistiques] -->  B1[taux d'activité];
A -->  B2[taux d'emploi];
```

Cette section  aborde la notion courante de l'emploi et du marché du travail, soulignant la nécessité de définir des concepts liés tels que la population active, la population inactive, et les différentes tranches d'âge. Il mentionne également les définitions du Bureau international du travail.

La population active est divisée en deux sous-groupes : les actifs occupés et les actifs inoccupés (chômeurs). Les inactifs comprennent ceux qui ne cherchent pas activement un emploi, comme les étudiants, les personnes au foyer et les retraités.

L'emploi intérieur inclut toutes les personnes travaillant sur le territoire national, y compris les travailleurs frontaliers étrangers. Cependant, il ne prend pas en compte les frontaliers luxembourgeois travaillant à l'étranger ni les fonctionnaires des institutions internationales extraterritoriales.

L'emploi national englobe les résidents du Grand-Duché de Luxembourg, y compris les frontaliers luxembourgeois travaillant à l'étranger et les agents des institutions internationales.

La population active internationale au Luxembourg est multiculturelle et multilingue, avec de nombreux travailleurs frontaliers attirés par les perspectives de carrière et la qualité de vie.

Le texte explique également comment calculer le taux d'activité et le taux d'emploi, soulignant les principales différences entre les deux indicateurs. Enfin, il indique que le taux d'emploi prend en compte l'ensemble de la population active, tandis que le taux d'activité inclut les chômeurs.

### 2. Mutations structurelles de l'emploi


#### 2.1. L'évolution du taux d'emploi

```mermaid
graph TD;
A[volume de l'emploi] -->   B[3 facteurs];
B --> B1[population totale];
B --> B2[taux d'activité];
B --> B3[proportion de chômeurs];
```

<br>

<br>

```mermaid
graph TD;
A[progression du taux d'emploi] -->   B[progression de l'emploi féminin];
B --> C1[diminution <br> du nombre <br> d'enfants];
B --> C2[structures <br> familiales]
B --> C3[changement des <br> mentalités];
B --> C4[scolarité <br> élevé des <br> femmes];
B --> C5[mise en place <br> de structures];
```

Le taux d'emploi, qui mesure la proportion de la population active occupée par rapport à celle en âge de travailler, a connu une nette progression récemment, principalement grâce à l'augmentation de l'emploi des femmes. Ce phénomène s'explique par la diminution du nombre d'enfants à charge, des changements dans les structures familiales (comme les familles monoparentales), des évolutions des mentalités, un niveau de scolarité plus élevé chez les femmes, et la mise en place de structures facilitant la conciliation entre travail et vie familiale, comme les crèches. Cette hausse du taux d'emploi reflète l'importante contribution des femmes sur le marché du travail, résultant de divers facteurs sociétaux et familiaux.

#### 2.2. La diminution de la durée de travail

```mermaid
graph TD;
A[diminution de la durée de travail] -->   B1[diminution de <br> la durée de <br> la vie active];
A --> B2[réduction du <br> nombre de <br> jours travaillés <br> par an];
A --> B3[réduction du nombre <br> d'heures travaillées <br> par semaine]
```

Depuis plus d'un siècle, la durée de travail des employés dans les économies occidentales a constamment diminué à plusieurs niveaux. Tout d'abord, la période de vie active a rétréci en raison de l'extension de la scolarité et de l'abaissement de l'âge de la retraite, notamment par des formules de préretraite. De plus, la mise en place des congés payés a réduit le nombre de jours travaillés par an. La semaine de travail a également connu des réductions significatives, passant, par exemple, de 48 heures au Luxembourg après la Seconde Guerre mondiale à 40 heures en 1974. Bien que la plupart des pays développés aient stabilisé les durées de travail hebdomadaires, la baisse annuelle est aujourd'hui plus modérée et résulte du développement du temps partiel.

#### 2.3. La tertiarisation de l'emploi

[Cette partie pourrait être un encadré ou une application! Ceci pour éventuellement illustrer les notions dans 2.1. et 2.2.]

L'économie luxembourgeoise a subi des mutations profondes, passant du secteur secondaire au secteur tertiaire, modifiant la structure de l'emploi. Le secteur tertiaire est désormais le principal pourvoyeur d'emplois, en particulier dans les banques, les assurances, les transports, ainsi que des domaines connexes tels que la comptabilité et le droit. La société de l'information requiert une main-d'œuvre hautement spécialisée, principalement concentrée à Luxembourg-Ville et sa périphérie, avec une diminution en s'éloignant de la capitale.

#### 2.4. La féminisation de l'emploi

[Cette partie pourrait être un encadré ou une application! Ceci pour éventuellement illustrer les notions dans 2.1. et 2.2.]

L'emploi des femmes a augmenté au Luxembourg au fil des décennies, passant de 26 % en 1970 à 38 % en 1999, puis à 49 % en 2010. Toutefois, un écart important subsiste par rapport aux hommes, avec un taux d'emploi de 62 % pour les femmes en 2010, contre 79 % pour les hommes. Les pays scandinaves présentent des écarts bien moindres (10 %). L'analyse montre que les jeunes femmes sont plus actives, les étrangères ont un taux d'activité plus élevé que les Luxembourgeoises, le mariage diminue l'activité, et le niveau de scolarité augmente l'emploi féminin. Les mesures pour concilier travail et vie familiale ont favorisé cette progression. Des politiques sociales, familiales, fiscales cohérentes et l'aménagement du temps de travail sont nécessaires pour soutenir davantage l'emploi féminin.

#### 2.5. L'immigration et les frontaliers

```mermaid
graph TD;
A[début 19e siècle: terre d'émigration] -->  |courant <br> migratoire <br> s'inverse| B[fin 19e siècle: terre d'immigration];
B --> |assure la| C[croissance économique au Luxembourg];
C --> |à cause du taux de chômage <br> élevé dans la Grande Région| D[fin 20e siècle: frontaliers des 3 pays limitrophes]
```

Au 19e siècle, le Luxembourg était une terre d'émigration, principalement tournée vers l'agriculture. Entre 1841 et 1891, plus de 72 000 personnes ont quitté le pays, en particulier pour le Brésil, le Guatemala et les États-Unis. À partir de 1870, le Luxembourg a attiré des travailleurs étrangers grâce à l'industrie du fer. Cependant, ce n'est qu'à la fin du 19e siècle que le flux migratoire s'est inversé. Au 20e siècle, il y a eu des vagues successives d'immigration, avec des Allemands, des Italiens, puis des Portugais prenant la relève. Cette immigration centenaire est essentielle pour la croissance économique du Luxembourg en raison de la réduction de la natalité, du vieillissement de la population et du besoin de main-d'œuvre pour des emplois dévalorisés par les Luxembourgeois. Le développement de l'emploi frontalier est également notable, avec 185 000 frontaliers en 2017, principalement en provenance de France, de Belgique et d'Allemagne. Les Luxembourgeois ne représentent plus qu'une petite partie de la population active du pays.

### 3. La notion de travail

Le travail est un élément fondamental de la production humaine. Le marché du travail réunit l'offre de travail des travailleurs et la demande d'emploi des entreprises. Adam Smith, en 1776, a souligné les avantages de la division du travail pour accroître l'efficacité de la production. La spécialisation permet d'améliorer les compétences et d'économiser du temps lors du passage d'une tâche à une autre. L'organisation du travail, la répartition des tâches, des responsabilités et des ressources, est essentielle pour atteindre les objectifs de manière efficace et efficiente au sein d'une organisation.

```mermaid
graph TD;
A[Adam Smith] -->  |démontre les avantages de la| B[division du travail];
B --> |qui a été appliquée par| C[Winslow Taylor];
C --> |qui est à l'origine du| D[taylorisme et de l'organisation scientifique du travail];
D -->  |qui a été utilisé par Henry Ford à l'origine du| E[fordisme];
E --> |qui est à la source de la| F[consommation de masse]
```

Frederick Winslow Taylor, à l'origine du taylorisme en 1911, reprend la théorie d'Adam Smith pour répondre à la nécessité de produire davantage et en masse avec une main-d'œuvre peu qualifiée. Il formule l'Organisation Scientifique du Travail (OST), basée sur la division verticale des tâches entre exécution et conception, et une division horizontale parcellisée. Henry Ford développe le fordisme en ajoutant une chaîne de montage, propulsant la production à grande échelle et la consommation de masse à l'échelle mondiale.

Dans les années 30, la sociologie s'intéresse davantage aux aspects sociaux des entreprises, remettant en question le taylorisme et le fordisme. Ces méthodes sont accusées de déshumaniser les ouvriers, les traitant comme des outils remplaçables. Des approches coopératives émergent, valorisant les relations interpersonnelles et la personnalité des salariés. Les tâches sont enrichies et élargies pour susciter l'intérêt des employés dans le bon fonctionnement de l'organisation.

### 4. Le fonctionnement du marché du travail

```mermaid
graph TD;
A[marché de travail] -->  B[offre de travail];
A --> C[demande de travail]
```

Le marché du travail repose sur l'offre de travail, venant des ménages, et la demande de travail des entreprises. L'offre est déterminée par la population active, influencée par la démographie, les comportements sociologiques, et l'arrivée de travailleurs frontaliers. La demande dépend de la production, avec une augmentation à court terme en cas de besoin accru. À moyen et long terme, la productivité influence la demande, car une hausse de celle-ci diminue le besoin de travail.

#### 4.1. La demande de travail

1. La loi des rendements non proportionnels
2. La théorie de la productivité marginale du travail
3. La fonction de demande de travail

```mermaid
graph TD;
A[demande de travail] --> |est dérivée de <br> la demande de <br> biens et services| B[demande dérivée];
```

<br>

<br>

```mermaid
graph TD;
A[facteurs qui influencent la demande de travail] -->  B1[prix du travail et <br> des autres facteurs <br> de production];
A --> B2[productivité du travail et <br> des autres facteurs <br> de production]
```

<br>

<br>

```mermaid
graph TD;
A["fonction de production <br> Q=f  (N,L,K)  "] -->  B1[3 facteurs de productions];
B1 --> C1[nature];
B1 --> C2[travail];
B1 --> C3[capital];
C1 --> D[substituabilité d'un facteur de production];
C2 --> D;
C3 --> D;
A --> B2[productivité moyenne <br> Q_M=Q/L ];
A --> B3[productivité marginale <br> Q_m ];
B1 --> D[rendements non proportionnels];
B2 --> D;
B3 --> D;
```

<br>

<br>

```mermaid
graph TD;
A[Quelle quantité de travail l'employeur <br> doit-il acheter sur le marché du travail?] -->  |en situation de <br> concurrence pure et parfaite| B[L'employeur doit accepter <br> les conditions du marché];
B --> |le salaire est donc déterminé selon| C[ coût marginal = salaire <br> Cm = w];
A --> |en ajoutant à la production <br> des heures supplémentaires| D[rendement de l'heure <br> supplémentaire diminue];
D --> E[productivité marginal décroissante];
E --> |sachant que l'entreprise <br> est price taker| F[revenu marginal = productivité marginal x prix];
C --> G[décision d'embauche : <br> embaucher tant que le Rm > w  <br> c-à-d Pm > Cm ] 
F --> G
G --> |l'employeur arrête <br> d'embaucher dès que| H[Pm = w]
```


La demande de travail est subordonnée à la demande de biens et services, car les travailleurs contribuent à leur production. Plusieurs facteurs influent sur cette demande, notamment le prix du travail par rapport aux autres facteurs de production, comme le capital. En général, la demande de travail est inversement liée au salaire. La productivité du travail et des autres facteurs de production joue également un rôle, car la demande dépend du produit marginal, soit la production supplémentaire résultant de l'ajout d'une unité de travail.

La production d'une entreprise dépend de trois facteurs : les ressources naturelles, les ressources humaines et les ressources en capital. La demande de travail est influencée par le coût du travail par rapport à d'autres facteurs de production, ainsi que par la productivité du travail. Lorsqu'on augmente la quantité de travail tout en maintenant les autres facteurs constants, la quantité produite augmente d'abord plus que proportionnellement, atteint un maximum, puis diminue. Cela s'explique par la loi des rendements non proportionnels, qui décrit cette relation complexe entre les facteurs de production et la production totale.

L'employeur se soucie principalement de la quantité de travail qu'il achètera sur le marché. En situation de concurrence pure et parfaite, l'employeur doit accepter les conditions du marché, notamment le salaire horaire. Ce salaire est une donnée du marché qui s'impose à lui. Les rendements du travail sont décroissants, ce qui signifie que la productivité marginale du travail diminue à partir d'un certain seuil. Dans une économie de concurrence parfaite, l'entreprise n'a aucune influence sur les prix, et le revenu marginal est calculé en multipliant la productivité marginale par le prix.

Sous cette hypothèse, la décision d'embauche est simple. L'employeur embauche tant que le revenu marginal est supérieur au salaire du marché, c'est-à-dire tant que la productivité marginale dépasse le coût marginal. À mesure que l'embauche augmente, la productivité marginale diminue. La demande de travail est donc décroissante par rapport au salaire, équivalant à la courbe de productivité marginale du travail. Les changements de salaire ou de productivité marginale déplacent cette courbe de demande.

#### 4.2. L’offre de travail


1. Facteurs explicatifs de l'offre de travail

```mermaid
graph TD;
A[facteurs qui influencent l'offre de travail] --> B1[facteurs liés au salaire];
A --> B2[facteurs non- lié au salaire];
B1 --> C1[changement lelong de la courbe d'offre];
B2 --> C2[déplacement de la courbe];
C2 --> D21[variation <br> de la population <br> active];
C2 --> D22[variation <br> de l'imposition des <br> rémunérations  <br> du travail];
C2 --> D23[variation <br> de l'indemnisation <br> du chômage];
C2 --> D24[transparence <br> du marché <br> du travail];
C2 --> D25[immigration];
C2 --> D26[niveau de salaire <br> dans les régions <br> limitrophes];
```


L'offre de travail correspond au nombre de travailleurs disponibles multiplié par le nombre d'heures qu'ils sont prêts à travailler. Elle dépend principalement du salaire, car des salaires plus élevés incitent davantage de personnes à travailler, lié au coût d'opportunité. D'autres facteurs, tels que les avantages en nature, le statut social, et les conditions de travail, influencent également l'attrait d'un emploi. Les variations du salaire, de la population active, de l'imposition, des allocations de chômage, de l'immigration, ou des salaires dans d'autres secteurs, modifient l'offre de travail. La courbe de l'offre totale est la somme des offres individuelles pour chaque niveau de salaire, et elle peut être influencée par des effets de substitution et de revenu.

#### 4.3. Équilibre sur un marché en concurrence parfaite.

Un marché du travail en concurrence parfaite nécessite de nombreuses entreprises prêtes à embaucher et des travailleurs qualifiés et mobiles. La transparence totale est essentielle, offrant aux travailleurs et aux employeurs les informations nécessaires. Le salaire serait déterminé par l'offre et la demande. Un excès de main-d'œuvre ferait chuter les salaires, tandis qu'une pénurie les ferait augmenter. Tout changement dans l'offre ou la demande entraînerait un ajustement du salaire et de la quantité de travail d'équilibre.

#### 4.4. Le marché du travail – un marché hétérogène

Le marché du travail est complexe et fragmenté, en raison de la diversité des compétences et des professions. Les salaires sont souvent rigides à la baisse, et les syndicats et les fédérations patronales jouent un rôle crucial dans la négociation des salaires. De plus, l'État intervient à travers des lois sur le travail, ce qui ajoute des complexités au marché du travail.


### 6. Le chômage

Le chômage est l'inactivité d'une personne désirant travailler. Il est apparu avec l'avènement des sociétés capitalistes et la révolution industrielle. Le chômage de masse a surgi lors de la Grande Dépression des années 1930, atteignant des sommets dramatiques. Les Trente Glorieuses ont vu une baisse du chômage, mais le premier choc pétrolier en 1973 a marqué son retour. Depuis les années 90, les taux de chômage varient, avec des baisses significatives dans certains pays anglo-saxons et des taux élevés persistants, comme en France.

#### 6.1. Le chômage - Notions

Le taux de chômage, exprimé en pourcentage, se calcule en multipliant le nombre de demandeurs d'emploi par 100, divisé par la population active. Au Luxembourg, un demandeur d'emploi est un résident sans emploi, disponible sur le marché du travail, à la recherche d'un emploi approprié, sans obligation d'aide à l'emploi et qu'il soit indemnisé ou non, respectant les obligations de l'Agence pour le Développement de l'Emploi (ADEM).

#### 6.2. Approches théoriques: fonctionnement du marché du travail et chômage

1. Le chômage classique
2. Le chômage keynésien

Pour les économistes classiques, le chômage est considéré comme temporaire et associé à des rigidités structurelles telles que le salaire minimum et les allocations chômage. Le marché du travail est caractérisé par une offre croissante de travail en fonction du salaire et une demande décroissante des entreprises en fonction du salaire réel. Le retour à l'équilibre du marché du travail s'effectue par une baisse des salaires. Si les salaires sont flexibles à la baisse, les chômeurs acceptent des salaires réduits pour retrouver un emploi. Cependant, si l'État fixe un salaire social minimum au-dessus du salaire d'équilibre, le chômage devient durable. Pour les classiques, l'emploi dépend du marché du travail, et cela influencera l'équilibre sur le marché des biens et services en vertu de la loi des débouchés ("l'offre crée sa propre demande").

John Maynard Keynes s'oppose au modèle néoclassique en remettant en question les comportements de l'offre de travail. Il estime que les ménages n'ajustent pas leur offre de travail en fonction du salaire et que le concept de chômage "volontaire" est invalide. Pour Keynes, la cause du chômage réside dans une insuffisance de la demande globale, pas dans le coût du travail. Les pouvoirs publics doivent intervenir pour réduire le chômage en stimulant la demande, ce qui diffère des néoclassiques.


#### 6.3. Types de chômage

Les économistes classiques considèrent le chômage comme principalement temporaire et volontaire. Cependant, compte tenu de la diversité du marché du travail et des particularités précédemment exposées, il est courant aujourd'hui de réaliser une analyse plus approfondie des différents types de chômage. Cette distinction est essentielle pour élaborer des politiques efficaces de lutte contre le chômage, car les mesures appropriées varient selon le type de chômage.

1. Le chômage conjoncturel
2. Le chômage structurel
3. Le chômage technologique
4. Le chômage frictionnel
5. Le chômage saisonnier

Le chômage conjoncturel: À court terme, le niveau d'emploi dépend du niveau de production. Lorsque la production augmente, les entreprises embauchent davantage, et vice versa. La production, à son tour, dépend de la demande des agents économiques. Le chômage lié à ce scénario découle d'une insuffisance de la demande adressée aux producteurs. La réduction de la demande amène les entreprises à revoir à la baisse leurs plans de production, généralement accompagnés de licenciements pour s'adapter au ralentissement économique. Ce sous-emploi affecte l'économie dans son ensemble.

Le chômage structurel: Ce type de chômage est associé aux structures économiques d'un pays et aux caractéristiques des chercheurs d'emploi. Il peut coexister avec des postes vacants. Par exemple, une pénurie de programmeurs peut coexister avec un excédent d'ingénieurs, car il y a un déséquilibre entre les qualifications recherchées par les employeurs et celles disponibles chez les demandeurs d'emploi. D'autres facteurs comprennent le développement économique régional inégal et une forte croissance démographique, qui peut accentuer le chômage structurel.

Le chômage technologique: Le chômage technologique est lié à l'innovation entraînant la substitution du capital au travail. Deux thèses s'opposent quant à l'impact de la technologie sur le chômage. La thèse pessimiste soutient que le progrès technique entraîne le chômage en remplaçant les travailleurs par des machines, tandis que la thèse optimiste souligne les effets positifs, tels que la baisse des prix, l'augmentation de la consommation et la création de nouveaux emplois.

Le chômage frictionnel: Le chômage frictionnel survient lorsqu'un travailleur recherche un nouvel emploi après avoir volontairement quitté son emploi précédent, généralement en quête de meilleures conditions (salaire, perspectives, etc.). Parfois, des individus restent au chômage par choix, attendant des offres plus avantageuses. Il est de courte durée et dépend de la mobilité, de l'information sur les emplois et des allocations de chômage.

Le chômage saisonnier: Le chômage saisonnier résulte des fluctuations saisonnières de la demande, affectant particulièrement les emplois agricoles, du bâtiment, de la restauration, de l'hôtellerie et du tourisme.


#### 6.4. Conséquences du chômage

1. Conséquences économiques
2. Conséquences sociales

Le chômage a des implications économiques et sociales significatives. Il gaspille des ressources de travail, diminue le niveau de vie général, réduit la demande globale de biens et services, dévalue le capital humain des chômeurs de longue durée, augmente les dépenses sociales de l'État, entraîne la migration de jeunes vers des régions plus prospères, et peut accroître les inégalités en poussant davantage de personnes vers la pauvreté relative.

Le chômage a des répercussions graves, notamment des problèmes de santé mentale, des taux de suicide plus élevés, des difficultés à financer l'éducation des enfants, des jeunes contraints de travailler au lieu d'étudier pour soutenir leur famille, des tensions conjugales et même le recours à des activités criminelles pour faire face aux besoins financiers.

### 7. Sources



## Le financement des agents économiques

### 1. Les sources de financement des ménages

Dans une économie capitaliste, les facteurs de production appartiennent à des individus. En échange de leur travail, les ménages reçoivent un salaire des entreprises. Les ménages fournissent également le "capital" aux entreprises, comprenant des biens matériels (bâtiments, terrains, etc.) et du "capital financier" qui génère des revenus tels que loyers, intérêts et dividendes.

#### 1.1. Les revenus primaires

Le revenu primaire des ménages en économie représente les revenus qu'ils gagnent grâce à leur participation à l'activité économique. Il se compose de deux catégories de revenus : les revenus d'activité issus du travail (salaires, bénéfices des entrepreneurs) et les revenus de la propriété découlant du capital (revenus de placements financiers, loyers, etc.).

1. Les revenus du travail
2. Les revenus du capital (revenus de la propriété)

Le revenu du travail représente la rémunération que les individus reçoivent en échange de leur travail. Cette notion permet de le distinguer du revenu du capital, réservé aux propriétaires d'actifs. On identifie trois catégories : le salaire, imposé au Luxembourg par le "salaire social minimum", le bénéfice englobant agriculteurs, artisans, industriels, et commerçants, et les honoraires, correspondant aux professions libérales (médecins, avocats, notaires, architectes).

Les revenus du capital, liés à la détention d'un capital, englobent les revenus immobiliers, tels que les loyers d'une propriété, et les revenus mobiliers. Ces derniers se divisent en dividendes, attribués aux actionnaires d'une entreprise, et en intérêts, gagnés par les détenteurs de comptes d'épargne ou d'investissements financiers.

#### 1.2. Les revenus secondaires

La redistribution des revenus est un processus où l'État et les institutions de sécurité sociale prélèvent une partie des revenus des plus riches au moyen d'impôts et de cotisations sociales, puis redistribuent ces fonds aux moins fortunés sous forme de prestations sociales. Cette pratique vise à réduire les inégalités économiques et sociales en garantissant un niveau de vie décent pour tous. Les transferts sociaux incluent allocations familiales, assurance-chômage, aides au logement, prestations de sécurité sociale, bourses d'études, etc. Plusieurs organismes participent à ce mécanisme, notamment l'État, qui impose davantage les ménages à revenu élevé, et les institutions de sécurité sociale, qui protègent contre divers risques sociaux.

#### 1.3. Le revenu disponible d'un ménage

Le revenu disponible des ménages est la somme totale de leurs revenus. Ils ont deux options pour l'utiliser : soit le dépenser pour acheter des biens et services, c'est la consommation, soit le mettre de côté pour l'avenir, c'est l'épargne. Malgré la pandémie, le revenu moyen par ménage a augmenté de 9,3 % en 2020, mais cela a également entraîné une augmentation du taux de pauvreté. Cependant, les inégalités globales n'ont pas beaucoup augmenté, grâce à des mesures telles que l'augmentation du salaire social minimum et des allocations. La consommation et l'épargne sont des fonctions essentielles des ménages, qui déterminent leur niveau de vie.

#### 1.4. La consommation

1. Les différents types de consommation
2. Les déterminants de la consommation
3. Les lois d'Engel
4. Les instruments de mesure

La consommation finale des ménages se divise en deux catégories. La première est la consommation individuelle de biens et services marchands, financée par les revenus des ménages. Elle profite principalement à l'acheteur. La deuxième est la consommation collective, qui englobe les services non marchands financés par les impôts, bénéficiant à la collectivité. Par ailleurs, il existe également la consommation intermédiaire des entreprises, liée aux biens et services utilisés dans le processus de production, et la consommation finale des ménages, portant sur des biens détruits plus rapidement.

Le niveau de consommation des ménages est influencé par divers facteurs, économiques et non-économiques. Les déterminants économiques incluent le revenu, où une augmentation favorise la consommation, tandis que des taux d'intérêt bas stimulent les dépenses. Les déterminants non-économiques, comme l'appartenance socioprofessionnelle, l'origine sociale, la religion et la composition du ménage, affectent également les choix de consommation.

La composition de la consommation évolue en fonction du pouvoir d'achat. Ernst Engel, statisticien et économiste, a formulé trois lois basées sur l'analyse des budgets ouvriers :
- Loi 1 : La part des dépenses alimentaires diminue avec l'augmentation du revenu, qualifiant ces biens d'inférieurs.
- Loi 2 : Les dépenses pour habillement, logement et chauffage (biens normaux) restent relativement constantes avec l'augmentation du revenu.
- Loi 3 : Les dépenses pour les loisirs, les voyages et la culture (biens supérieurs) augmentent avec le revenu croissant.

L'utilisation de divers agrégats permet de mesurer, interpréter et prédire les tendances de la consommation et de l'épargne. La consommation est évaluée en fonction de la valeur totale des biens et services destinés à satisfaire directement les besoins des ménages. Les coefficients budgétaires par poste expriment la part du revenu consacrée à un type de bien spécifique, facilitant l'analyse de l'évolution de la consommation des ménages.

#### 1.5. L'épargne

1. Les motifs de l'épargne
2. Les formes de l'épargne
3. Instrument de mesure

Les individus font des choix entre consommation immédiate et différée, l'épargne représentant la partie du revenu non dépensée immédiatement. Elle peut servir à la prévoyance (achats planifiés), à la précaution (imprévus) ou à la spéculation (investissements financiers). En 2020, la pandémie a augmenté l'épargne en Europe et aux États-Unis, atteignant des niveaux records. Au Luxembourg, le taux d'épargne est le plus élevé de l'UE (20%), lié à divers facteurs comme le chômage croissant et l'évolution démographique. L'épargne peut être financière (investissements) ou non financière (achats immobiliers) et est exprimée en pourcentage du revenu disponible.

#### 1.6. Le crédit bancaire

Lorsqu'un ménage place son épargne à la banque, les intérêts représentent les revenus qu'il perçoit en tant que prêteur. Cependant, de nombreux ménages ont recours au crédit bancaire pour financer des investissements ou des biens de consommation. Le taux d'intérêt est le coût de ce prêt, remboursé par des mensualités incluant le remboursement de la dette et les intérêts. Il existe des taux d'intérêt variables et fixes. Les banques évaluent le taux d'endettement des ménages lors de la demande de crédit, qui ne devrait généralement pas dépasser 33%. L'endettement des ménages est la somme de leurs crédits, exprimée en pourcentage du revenu disponible annuel.

#### 1.7. Le problème de surendettement

Le surendettement est un problème alarmant au 21e siècle. Dans notre société de consommation, de plus en plus de personnes risquent de s'endetter excessivement. Le taux d'endettement d'un ménage, exprimant le pourcentage des revenus mensuels consacrés aux mensualités, est un indicateur clé. Dépasser 30% représente un risque d'endettement, dépendant du niveau de revenu. 20% peut déjà être dangereux pour les revenus modestes et les familles nombreuses, tandis que 60% augmente fortement le risque de surendettement.

#### 1.8. Conclusion

Les ménages financent leurs dépenses de consommation principalement grâce à leur revenu disponible. L'épargne représente ce qui reste après ces dépenses. La consommation comprend divers types (finale, intermédiaire, individuelle, collective) influencés par des facteurs comme le revenu, les taux d'intérêt, les préférences individuelles, et des facteurs sociologiques et psychologiques. L'épargne est la partie du revenu non dépensée immédiatement, pouvant prendre différentes formes (thésaurisation, épargne financière et non financière). Le crédit bancaire est la deuxième source de financement pour les ménages, souvent nécessaire pour l'achat de biens de consommation ou de biens immobiliers, mais le surendettement devient de plus en plus préoccupant.

### 2. Le financement de l'entreprise

Les entreprises, pour opérer, utilisent divers facteurs de production, générant des recettes dont une partie est réservée pour l'autofinancement. Toutefois, elles nécessitent des capitaux pour investir dans des équipements, des machines, ou des brevets. Pour cela, elles sollicitent des investisseurs via deux sources principales : les marchés financiers, par l'émission d'actions et d'obligations, ou les banques par des emprunts classiques. Les investisseurs deviennent des propriétaires d'entreprise, percevant des dividendes en fonction des bénéfices. Cela permet à l'entreprise de financer son développement malgré les coûts initiaux élevés.

### 3. Le financement de l'Etat

Le Traité de Maastricht identifie le secteur public comme un ensemble comprenant l'Administration centrale (État), les administrations locales (communes), et la Sécurité Sociale. Le secteur public joue un rôle économique majeur, agissant en tant que consommateur, fournisseur de services publics, employeur et investisseur, influençant significativement l'économie et la société par son comportement.

1. Les recettes de l’État
2. Les dépenses de l’État
3. Le solde budgétaire

Le budget de l'État comprend les recettes et les dépenses utilisées par l'État pour son intervention économique et sociale. Chaque année, le gouvernement luxembourgeois établit une loi budgétaire, votée à la Chambre des Députés, qui prévoit l'ensemble des recettes et dépenses pour l'exercice budgétaire à venir.

Les sources de financement de l'État se divisent en trois catégories : les recettes courantes, les recettes en capital et les recettes des opérations financières. Les recettes courantes comprennent principalement les impôts, les taxes, la TVA, les droits d'accises et d'autres sources de revenus. Les impôts sont classés en impôts directs et indirects, avec l'impôt direct considéré comme plus équitable car il tient compte de la capacité contributive des contribuables. Les recettes en capital proviennent de ventes non récurrentes, comme la vente de biens publics ou les droits de succession. Les recettes des opérations financières incluent les revenus provenant de sociétés à participation publique et de la vente de participations publiques.

Les dépenses de l'État se répartissent en dépenses courantes, dépenses en capital et dépenses des opérations financières. Les dépenses courantes couvrent la consommation publique, les transferts aux ménages, aux entreprises et au reste du monde, ainsi que les intérêts de la dette publique. Les dépenses en capital concernent les projets d'investissement publics, tandis que les dépenses des opérations financières englobent l'amortissement de la dette publique.

Le solde budgétaire, différence entre les recettes et les dépenses de l'État, détermine s'il y a un déficit budgétaire (si le solde est négatif), un équilibre budgétaire (si le solde est nul) ou un excédent budgétaire (si le solde est positif). En cas de déficit budgétaire, il peut être financé par l'endettement public ou l'utilisation de réserves budgétaires accumulées. L'endettement public peut être à court terme (bons du Trésor) ou à long terme (obligations d'État) et peut également être effectué auprès du système bancaire, sous réserve de restrictions de l'Union européenne.

Le budget de l'État est un instrument essentiel pour planifier et gérer les finances publiques, influençant ainsi la politique économique et sociale du pays.


### 4. Le système de financement de l'économie

#### 4.1. Besoins et capacités de financement

Chaque agent économique génère ses revenus pour couvrir ses dépenses, présentant soit une capacité de financement (dépenses inférieures aux ressources), soit un besoin de financement (dépenses excédant les ressources). Les entreprises ont souvent des besoins de financement pour des investissements, tout comme les familles qui peuvent demander un crédit immobilier. Les administrations publiques et les entreprises structurellement empruntent. Les ménages dégagent généralement une capacité de financement. À l'échelle macroéconomique, les principaux agents à capacité de financement sont les ménages, tandis que les entreprises, les États et les administrations publiques ont souvent des besoins de financement. La microéconomie étudie le comportement individuel, tandis que la macroéconomie se penche sur les agrégats tels que le PIB et le chômage.

#### 4.2. Le systèeme financier

1. Le système bancaire
2. Le marché des capitaux

Le système financier joue un rôle crucial dans les économies modernes en mettant en relation les agents à capacité de financement et ceux ayant des besoins de financement. Il existe deux principaux circuits de financement : le financement direct et le financement indirect. Le financement indirect implique l'utilisation d'intermédiaires, principalement les banques, pour fournir des ressources financières. Les banques sont des institutions clés qui reçoivent les dépôts des épargnants et accordent des crédits aux emprunteurs, agissant ainsi en tant qu'intermédiaires entre ces deux groupes. Le prêt d'argent génère des intérêts pour les prêteurs, encourageant l'épargne.

Le marché des fonds prêtables représente un marché simplifié où l'offre de capitaux des épargnants rencontre la demande de financement des emprunteurs. Les marchés de capitaux, en revanche, consistent en la vente d'instruments financiers tels que les actions d'entreprises ou les obligations d'États et d'entreprises. Ils offrent une alternative aux prêts bancaires traditionnels pour le financement des entreprises et des États.

Les marchés de capitaux sont souvent associés à des bourses, qui servent de lieux d'échange pour les titres financiers. Cela garantit la transparence des prix et facilite les transactions entre les investisseurs. Les bourses permettent aux entreprises et aux États de trouver des financements auprès d'investisseurs privés et institutionnels.

Les instruments financiers incluent les actions, qui représentent la propriété d'une partie du capital social d'une entreprise, et les obligations, qui sont des titres de créance émis par des entreprises ou des États. Le système financier est essentiel pour mobiliser l'épargne vers les investissements, favorisant ainsi la croissance économique.

### 5. Sources




## La monnaie

### 1. Définition de la monnaie

### 2. Les fonctions de la monnaie

### 3. Les différentes formes de monnaie

#### 3.1. Les formes historiques

#### 3.2. Les formes actuelles

#### 3.3. Conclusion

### 4. Le système bancaire

<!--  Banks as Financial Intermediaries  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/3f7f539864e22aea15dde75381ff7d3f7129b2d9" alt="Socialisation" style="width:50%">
  <figcaption>Fig.X -  Banks act as financial intermediaries because they stand between savers and borrowers. Savers place deposits with banks, and then receive interest payments and withdraw money. Borrowers receive loans from banks and repay the loans with interest. In turn, banks return money to savers in the form of withdrawals, which also include interest payments from banks to savers.</figcaption>
</figure>

#### 4.1. La Banque centrale

#### 4.2. La Banque Centrale Européenne (BCE)

#### 4.3. L'Eurosystème

#### 4.4. La Banque Centrale du Luxembourg

#### 4.5. Les banques commerciales

#### 4.6. Le système de paiement

### 5. La masse monétaire

#### 5.1. Les agrégats monétaires

<!--  The Relationship between M1 and M2 Money  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/4a6d20035187eebdf301afc64af7c17408da03b3" alt="Socialisation" style="width:50%">
  <figcaption>Fig.X -  M1 and M2 money have several definitions, ranging from narrow to broad. M1 = coins and currency in circulation + checkable (demand) deposit + savings deposits. M2 = M1 + money market funds + certificates of deposit + other time deposits.</figcaption>
</figure>


### 6. La création monétaire

#### 6.1. La création de monnaie centrale

#### 6.2. La création de monnaie scripturale

1. Comment fonctionne le mécanisme de création de monnaie scripturale?
2. Le rôle de la Banque centrale dans la création monétaire

#### 6.3 L'équation des transactions

### 7. Les théories monétaires

#### 7.1. La théories quantitative de la monnaie

#### 7.2. La théories keynésienne

1. L'offre de monnaie
2. La demande de liquidités
3. L'équilibre sur le marché de la monnaie
4. Impact de la monnaie sur l'économie réelle

### 8. Les déséquilibres monétaires

#### 8.1. L'inflation

1. Définition
2. Mesure de l'inflation
3. Le salaire nominal et salaire réel
4. Le PIB nominal et le PIB réel
5. Causes de l'inflation
    - L'inflation par la demande
    - L'inflation par les coûts
6. La spirale inflationniste
7. Les conséquences de l'inflation
8. La lutte contre l'inflation

#### 8.2. La déflation

### 9. Fiche pratique: Les indices synthétiques

#### 9.1. Rappel: Les indices simples

#### 9.2. Les indices synthétiques

1. Introduction
2. L'indice des valeurs globales
3. Les indices de prix
    - L'indice de prix de Laspeyres
    - L'indice de prix de Paasche
4. Les indices de quantités

#### 9.3. Les indices synthétiques avec pondération: l'indice des valeurs globales et les indices de Fisher

1. Les indices de Fisher
2. Les indices des valeurs globales

#### 9.4. Les nombres indices - Exercices de synthèse



## Introduction à la sociologie

### 1. Socialisation et agents de socialisation

<!-- Socialisation  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/cede713f709f384a72feb4fb6ced2277d12864e2" alt="Socialisation" style="width:50%">
  <figcaption>Fig.X -  Can you use your hands to eat? Who should pay? Do you stand when someone else gets up, and is that dependent on their gender? The dining manners and customs of different cultures are learned by socialization. (Credit: Kurman Communications/flickr)</figcaption>
</figure>

<br>

<br>

<!-- Socialisation: Twins  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/c36f3935ecdd77324ab552421f098e9093706597" alt="Socialisation" style="width:50%">
  <figcaption>Fig.X -   Identical twins may look alike, but their differences can give us clues to the effects of socialization. These twins chose the same career path, but many twins do not. (Credit: Senior Airman Lauren Douglas/U.S. Air Force)</figcaption>
</figure>


#### 1.1. La notion de socialisation

#### 1.2. Les buts de la socialisation

#### 1.3. Les mécanismes de la socialisation

#### 1.4. Les formes de socialisation

#### 1.5. Les agents de socialisation

1. Agents complémentaires
2. Agents concurrents

<!-- Socialised roles: Father and child  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/81f1bba179012fef785f64bfcf532fea148958e4" alt="Parenting" style="width:50%">
  <figcaption>Fig.X - The socialized roles of parents and guardians vary by society. (Credit: Quaries.com/flickr)</figcaption>
</figure>

<br>

<br>

<!-- Socialised roles: Kindergarden  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/8759dcabdc9966cd77e1e38af6e78a8a65885e3c" alt="Kindergarden" style="width:50%">
  <figcaption>Fig.X - These kindergarteners aren’t just learning to read and write; they are being socialized to norms like keeping their hands to themselves, standing in line, and playing together. (Credit: woodleywonderworks/flickr)</figcaption>
</figure>

<br>

<br>

<!-- Socialised roles: Princess culture  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/d0a48dfe293c484fdf29c8e1f8b854e56eb1d0b0" alt="Princess culture" style="width:50%">
  <figcaption>Fig.X - Some researchers, parents, and children's advocates are concerned about the effects of raising girls within what they call "princess culture." Many place blame on entertainment companies, such as Disney, for its portrayals of girls in its movies.</figcaption>
</figure>


<br>

<br>

<!-- Socialised roles: Army  -->
 <figure>
  <img src="https://openstax.org/apps/image-cdn/v1/f=webp/apps/archive/20230620.181811/resources/8c0434340bcc475dbbe9845c322f4d2d6e248193" alt="Army" style="width:50%">
  <figcaption>Fig.X -  In basic training, members of the Air Force are taught to walk, move, and look like each other. (Credit: Staff Sergeant Desiree N. Palacios, U.S. Air Force/Wikimedia Commons)</figcaption>
</figure>

#### 1.6. Questions de synthèse

#### 1.7. Applications

### 2. Les déterminants de la consommation

#### 2.1. Les différents déterminants de la consommation

1. Le déterminant économique de la consommation: le revenu
2. Les déterminants sociaux de la consommation: critères sociodémographiques

```mermaid
graph TD;
A[déterminants de la consommation] --> A1[déterminant économique];
A --> A2[déterminant sociaux];
A1 --> A11[consommation des ménages <br> est fonction du revenu ];
A11 --> B1[lois d'Engel];
B1 --> C11[biens inférieurs];
B1 --> C12[biens normaux];
B1 --> C13[biens supérieurs];
A2 --> B21[composition <br> démographique <br> du ménage];
A2 --> B22[âge des <br> membres <br> du ménage];
A2 --> B23[localisation <br> des ménages];
```

#### 2.2. La consommation est un acte social

1. La consommation: un acte symbolique
2. La consommation: un habitus
3. La consommation: un résultat d'effets sociaux

```mermaid
graph TD;
A[la consommation <br> un acte social] --> B1[consommation <br> = <br> acte symbolique ];
A --> B2[consommation <br> = <br> habitus];
A --> B3[consommation <br> = <br> résultats d'effets sociaux];
B1 --> C1[afficher le groupe <br> social auquel le <br> consommateur <br> appartient];
C1 --> D1[Jean Baudrillard];
B2 --> C2[transmission des goûts <br> et préférences par <br> les groupes sociaux];
C2 --> D2[Pierre Bourdieu];
B3 --> C3[consommer pour <br> exposer sa <br> richesse];
C3 --> D31[Thorstein Veblen];
C3 --> D32[James Duesenberry];
```

#### 2.3. Questions de synthèse

#### 2.4. Application

### 3. Les facteurs de la pauvreté

```mermaid
graph TD;
A[facteurs à l'origine de la pauvreté] --> B1[absence de travail];
A --> B2[working poor];
A --> B3[situation familiale];
B1 --> C11[enfants vivant <br> dans les <br> ménages pauvres];
B1 --> C12[pauvreté des <br> retraités];
B1 --> C13[chômeurs de <br> longue durée];
B3 --> C31[monoparenté];
B3 --> C32[taille du <br> ménage];
```

#### 3.1. La pauvreté en relation avec le travail

1. L'absence de travail
2. Diverses formes de travail

#### 3.2. La pauvreté en relation avec la situation familiale

1. La monoparentalité
2. La taille du ménage

#### 3.3. Questions de synthèse

#### 3.4. Application






