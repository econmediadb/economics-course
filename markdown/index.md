# Guide de révision pour le cours de sciences économiques

## Que faut-il enseigner au lycée?

Lors du développement du programme et d'un cours, il faut que les points suivants soient vérifiés:

- explication des **concepts fondamentaux** 
- développement des **outils de l’analyse économique**
- susciter la **curiosité des élèves**

Les notions essentielles qu'il faut enseigner aux lycéens (jugées par les experts-économistes):

- **rôle des prix** et **méchanismes du marché**: 
  - les processus de coordination, 
  - les dangers du pouvoir de monopole et de 
  - l’asymétrie d’information, et 
  - les conséquences pour la distribution des revenus  
- **l'offre et la demande** : (cf. l'approche et les graphiques de Robert Frank et Ben Bernanke) 
- **modèle économique** et **marchés économiques** 

**Source**  
- https://doi-org.proxy.bnl.lu/10.3917/comm.157.0025 
- https://doi-org.proxy.bnl.lu/10.3917/leco.072.0018


## Quel est le but d'un guide de révision?

Le guide de révision est conçu pour aider les apprenants à réviser les différents chapitres de leur évaluation. Chaque chapitre de ce guide contient un certain nombre de fonctionnalités communes. Ils commencent par une liste des mots-clés économiques qui aideront les apprenants à se rappeler les sujets abordés pendant le cours. Tout au long du chapitre, il existe de nombreuses cartes conceptuelles (*concept maps*), qui peuvent être utiles pour les apprenants qui apprennent mieux visuellement.

Dans cette partie nous allons présenter les différents sujets, qui sont traités dans le cours, sous forme de **cartes conceptuelles**.

Les **cartes conceptuelles** sont des « outils graphiques pour organiser et représenter les connaissances ». Dans le domaine de l'éducation, ils peuvent capturer les connaissances des experts en la matière, des éducateurs et des apprenants, afin qu'ils puissent être utilisés pour la planification, l'enseignement, l'apprentissage et l'évaluation. Un ensemble de concepts connexes est affiché dans des boîtes (nœuds) avec des lignes les reliant pour indiquer les relations. Ces liens sont étiquetés (contrairement aux cartes mentales) pour décrire les relations entre eux. Par conséquent, en suivant un lien
entre les concepts forme un sens ou une courte proposition. 

## Méthodologie [Méthodologie](https://gitlab.com/econmediadb/economics-course/-/blob/main/markdown/methodology.md)

![Concepts - Overview](https://gitlab.com/econmediadb/economics-course/-/raw/main/concept-maps/20220524-year1-overview.png "Concepts - Overview")

## [Introduction](https://gitlab.com/econmediadb/economics-course/-/blob/main/markdown/introduction-rg.md)

## [Chapitre 1: Comment   crée-t-on   des richesses et comment les mesure-t-on?](https://gitlab.com/econmediadb/economics-course/-/blob/main/markdown/chapter1-rg.md)

## [Chapitre 2: La répartition de la richesse](https://gitlab.com/econmediadb/economics-course/-/blob/main/markdown/chapter2-rg.md)

## [Chapitre 3: Comment se forment les prix sur un marché?](https://gitlab.com/econmediadb/economics-course/-/blob/main/markdown/chapter3-rg.md)

## [Chapitres pour le cours de 2e](https://gitlab.com/econmediadb/economics-course/-/blob/main/markdown/chapter-1-2e.md)

## [Glossaire](https://gitlab.com/econmediadb/economics-course/-/blob/main/markdown/glossaire.md)

## Les notions utilisés dans le cours

Veuillez trouver ci-dessous les principaux mots-clés utilisés dans les différents chapitres.

```mermaid
flowchart TB

    a7 --> b4;
    b7 --> c2;
    c5 --> d5;

    subgraph introduction
     a1[acteur économique] --> a2[rareté];
     a2 --> a3[besoin économique];
     a3 --> a4[utilité];
     a4 --> a5[prise de décision];
     a5 --> a6[problèmes économiques fondamentaux];
     a6 --> a7[frontière des possibilités de production];
     a7 --> a8[système économique];
     a8 --> a9[économie normative et positive];
    end

    subgraph chapitre 1
     b1[revenu national] --> b2[types de biens et services];
     b2 --> b3[production marchande et non-marchande];
     b3 --> b4[facteurs de production];
     b4 --> b5[substitution et complémentarité];
     b5 --> b6[productivité globale des facteurs];
     b6 --> b7[valeur ajoutée];
     b7 --> b8[PIB];
     b8 --> b9[taux de croissance];
     b9 --> b10[PIB par habitant];
     b10 --> b11[progrès technique];
     b11 --> b12[révolution industrielle];
     b12 --> b13[indicateurs complémentaires];
    end

    subgraph chapitre 2
     c1[partage de la richesse] --> c2[valeur ajoutée brute];
     c2 --> c3[revenu primaire];
     c3 --> c4[rémunération des facteurs de production];
     c4 --> c5[partage de la valeur ajoutée brute];
     c5 --> c6[répartition des revenus];
    end

    subgraph chapitre 3
     d1[circuit économique] --> d2[relation demande-prix];
     d2 --> d3[rôle des institutions];
     d3 --> d4[les marchés];
     d4 --> d5[structure de marché];
     d5 --> d6[concurrence parfaite];
     d6 --> d7[oligopole];
     d7 --> d8[monopole];
     d8 --> d9[demande];
     d9 --> d10[offre];
     d10 --> d11[prix et quantité d'équilibre];
    end    

```
