# Chapitre 1 : La science économique – une science de choix

## 1. Concept fondamental : la rareté

1. [Introduction à l'économie](https://www.youtube.com/watch?v=OdcA5_1hAbg)
1. [Les ressources économiques](https://www.youtube.com/watch?v=m_zvi-2PbZs)
1. [Rareté et rivalité (vidéo)](https://www.youtube.com/watch?v=n204qwAoepk)

## 2. Les trois problèmes économiques fondamentaux

1. [La courbe des possibilités de production](https://www.youtube.com/watch?v=6j672b0a2GY)
1. [Le coût d'opportunité](https://www.youtube.com/watch?v=EUndTSXSHaA)

## 3. Les systèmes économiques

1. [L'économie centralisée et l'économie de marché](https://www.youtube.com/watch?v=EV4TQjSugJI)
1. [L'économie, mode d'emploi - Secteur Public, secteur Privé : c'est quoi? Comment ça marche? (vidéo)](https://www.youtube.com/watch?v=On3nlrLJa3w)
1. [Les modèles économiques](https://www.youtube.com/watch?v=9Co2Fk7Rh2s)
1. [Affirmation normative et affirmation positive](https://www.youtube.com/watch?v=C3AdKPrthuo)


# Chapitre 2 : Comment crée-t-on des richesses et comment les mesure-t-on ?

## 1. La richesse d'une économie 


id 

## 2. Le circuit économique

1. [Le circuit économique (vidéo)](https://www.youtube.com/watch?v=06DnEsZJt9M)
1. [Comment les entreprises financent-elles leurs projets ? (vidéo)](https://www.youtube.com/watch?v=g8UVk7EvgqM)

## 3. La diversité de la production

## 4. Comment produire et mesurer la production

1. [Les 4 facteurs de production](https://www.youtube.com/watch?v=XkIxcqrZZ6M)

## 5. Comment mesurer la création de richesses d'une nation?

1. [Qu'est-ce que le Produit Intérieur Brut (PIB) ? (vidéo)](https://www.youtube.com/watch?v=ROpFSrUMs-A)

## 6. Comment répartir la valeur

1. [TVA collectée, TVA déductible... Qui paye réellement ? (vidéo)](https://www.youtube.com/watch?v=fh2Uy5M1NS4)

## 7. Comment le PIB a-t-il évolué dans le monde à long terme?

1. [La croissance (vidéo)](https://www.youtube.com/watch?v=mcMJyHmjvv4)
1. [La décroissance, une solution à la crise ? (vidéo)](https://www.youtube.com/watch?v=TJymoeijdDs)

## 8. Les indicateurs complémentaires

1. [L'indice de développement humain (vidéo)](https://www.youtube.com/watch?v=PLtz8KsyRig)

# Chapitre 3 : Comment se forment les prix sur un marché ?

## 2. Qu'est-ce qu'un marché ?

1. [Le marché du travail (vidéo)](https://www.youtube.com/watch?v=3q_h00oUsLU)
1. [La concurrence (vidéo)](https://www.youtube.com/watch?v=ZYkbXe_lmBo)
1. [Monopoles et oligopoles (vidéo)](https://www.youtube.com/watch?v=RUJg5cZ-Bl0)

## 3. Comment se forme l'équilibre sur un marché concurrentiel ?

1. [L'équilibre de marché](https://www.youtube.com/watch?v=Z895aSaMev4)
1. [Changement dans l'équilibre de marché](https://www.youtube.com/watch?v=-qjMdYnwJZk)

## 4. Gain à l’échange : Surplus du consommateur et du producteur

## 5. Changements d’offre et de demande

[Impact du changement de la demande ou de l’offre sur le prix et la quantité d’équilibre](https://www.youtube.com/watch?v=HGqRzmp6_Hs)

# Source

- [Essentiels de l'économie - Economie politique (BnF Gallica)](https://gallica.bnf.fr/html/und/droit-economie/economie-politique?mode=desktop)
- [KhanAcademyFrancophone](https://www.youtube.com/@KhanAcademyFrancais)
- [Cité de l'Économie](https://www.youtube.com/@citedeleconomie)
- [Dessine-moi l'éco](https://www.youtube.com/@dessinemoileco-sydo)
- [Académie de Strasbourg - sciences économiques et sociales](https://pedagogie.ac-strasbourg.fr/ses/sites-utiles/des-sites-de-videos-et-danimations/)
- [Les SES en vidéos](https://www.youtube.com/@lessesenvideos2745)


<br>

<br>

<br>

# Matériel pour 2e et 1e

1. [Les agents économiques sont-ils rationnels? (vidéo)](https://www.youtube.com/watch?v=tbpNYEUdtT0)
5. [L’inflation à tout prix (vidéo)](https://www.youtube.com/watch?v=lbKSwu_yEnY)
6. [L’euro numérique (vidéo)](https://www.youtube.com/watch?v=zBu-c6tvHVQ)
9. [Comment mesure-t-on le chômage ? (vidéo)](https://www.youtube.com/watch?v=0AJLLsL2mZg)
11. [Comment calcule-t-on l'impôt sur le revenu ? (vidéo)](https://www.youtube.com/watch?v=JExYtPDPb3k)
12. [Immigration : opportunité ou menace pour le marché du travail ? (vidéo)](https://www.youtube.com/watch?v=4NDy8G_KlXo)
13. [Quel statut juridique pour une entreprise ? (vidéo)](https://www.youtube.com/watch?v=1Uts-yOEaVg)
14. [La valeur d'une monnaie peut-elle impacter l'économie d'un pays ? (vidéo)](https://www.youtube.com/watch?v=k8InNv3RfCA)
15. [Pourquoi la déflation peut-elle être dangereuse ? (vidéo)](https://www.youtube.com/watch?v=U0oti-8Wc6E)
18. [Y a-t-il un remède au chômage ? (vidéo)](https://www.youtube.com/watch?v=s3cM2Kx-jUY)
19. [Qu'est-ce que l'avantage comparatif ? (vidéo)](https://www.youtube.com/watch?v=ip9d1UJ4RYg)
20. [Comment mesure-t-on le chômage ? (vidéo)](https://www.youtube.com/watch?v=NhZoVos52Hg)
21. [La création monétaire, un taux d'inflation à contrôler (vidéo)](https://www.youtube.com/watch?v=o2u7Xa57y8A)
22. [Comment un Etat peut-il faire faillite ? (vidéo)](https://www.youtube.com/watch?v=_K7pOATTPqA)
23. [Conséquences d'une mauvaise nouvelle sur notre système économique (vidéo)](https://www.youtube.com/watch?v=U0zl9f5wTzo)
24. [La balance des paiements (vidéo)](https://www.youtube.com/watch?v=9xD4G55MERQ)
26. [Qu'est-ce que la politique monétaire ? Avant 2021 (vidéo)](https://www.youtube.com/watch?v=11e-6CbYOl8)
27. [La transmission de la politique monétaire (vidéo)](https://www.youtube.com/watch?v=_CKg4Io-e80)
28. [Comment fonctionnent les taux directeurs ?](La transmission de la politique monétaire)
29. [Qu’est-ce que la dette publique ? (vidéo)](https://www.youtube.com/watch?v=mB1ufMgEw6Q)


- [Le métier d'économiste vu par des lycéens (vidéo)](https://www.citeco.fr/le-m%C3%A9tier-d%E2%80%99%C3%A9conomiste-vu-par-des-lyc%C3%A9ens-vid%C3%A9o)
- [L'agriculture du futur (vidéo)](https://www.youtube.com/watch?v=GEplt9E7IOg)
- [Créer une carte mentale - ARTE Education](https://www.youtube.com/watch?v=pmuOAAxN-Ps&list=PLsKz9d8OwcfcUuY_HPXMRXf_JDIqxPVMH&index=6)
- [Les tutoriels Educ'ARTE](https://www.youtube.com/playlist?list=PLsKz9d8OwcfcUuY_HPXMRXf_JDIqxPVMH)

