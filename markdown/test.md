# Test

## Embed Youtube Videos


[![](http://img.youtube.com/vi/5BJpXSXQ56A/0.jpg)](http://www.youtube.com/watch?v=5BJpXSXQ56A "1. Swiss Meetup 2021 in January")

```markdown
[![](http://img.youtube.com/vi/5BJpXSXQ56A/0.jpg)](http://www.youtube.com/watch?v=5BJpXSXQ56A "1. Swiss Meetup 2021 in January")
``` 

## Expand/Collapse 

<details>
  <summary markdown="span">This is the summary text, click me to expand</summary>

  This is the detailed text.

  We can still use markdown, but we need to take the additional step of using the `parse_block_html` option as described in the [Mix HTML + Markdown Markup section](#mix-html--markdown-markup).

  You can learn more about expected usage of this approach in the [GitLab UI docs](https://gitlab-org.gitlab.io/gitlab-ui/?path=/story/base-collapse--default) though the solution we use above is specific to usage in markdown.
</details>


