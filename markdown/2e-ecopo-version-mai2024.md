# Économie politique

## Chapitre 1 : Les agents économiques - Leurs fonctions principales et leur financement

### 1. Les sources de financement des ménages

```mermaid
graph LR
A[Sources de financement des ménages]
B[Revenus primaires]
C[Revenus obtenus par <br> le travail ou les investissements]
D[Revenus secondaires]
E[Flux financiers non liés à l'emploi <br> ou aux investissements individuels]
F[Équilibre des budgets et <br> satisfaction des besoins financiers]

A --> B
A --> D
B --> C
D --> E
E --> F

```

<br>

<br>

```mermaid
graph LR
A[Revenu disponible]
B["Consommer (la consommation)"]
C[Constitue la valeur des biens et services <br> acquis pour satisfaire les besoins humains]
D[Postes de consommation : alimentation, transport, <br>communication, logement, loisirs]
E["Épargner (l'épargne)"]
F[Partie du revenu non dépensée, <br> gardée en réserve pour dépenses futures]

A --> B
A --> E
B --> C
B --> D
E --> F
```

<br>

<br>

```mermaid

graph LR
B[Crédit bancaire]
C[Investissement immobilier, <br> biens de consommation]
D["Rémunération de la banque <br> (intérêts)"]
E[Critère d'évaluation]
E1[Taux d'endettement <br> bancaire des ménages]
F["Formule : <br> Taux d'endettement <br> = <br> (Dettes / Revenu disponible) * 100"]

B --> C
B --> D
B --> E
E --> E1
E1 --> F

```

<br>

<br>

```mermaid

graph LR
A[Consommation]
B[Déterminants économiques]
C[Déterminants non-économiques]
E[Niveau de revenu disponible]
F[Effet cliquet]
G[Taux d'intérêt bancaires]
H[Prix de la monnaie]
I[Stimulation de la consommation]
J[Crédits moins onéreux]
L[Goûts et préférences individuelles]
M[Sociologie et psychologie]
N[Catégorie socioprofessionnelle]
O[Origine sociale ou ethnique]
P[Appartenance religieuse]
Q[Traditions culturelles]
R[Composition du ménage]
S[Choix de consommation]

A --> B
A --> C
B --> E
B --> F
B --> G
G --> H
G --> I
G --> J
C --> L
C --> M
L --> S
M --> S
N --> S
O --> S
P --> S
Q --> S
R --> S
C --> N
C --> O
C --> P
C --> Q
C --> R
```

<br>

<br>

```mermaid

graph LR
A[Composition de la consommation]
B[Loi 1 : Alimentation]
C[Diminution si le revenu augmente]
D[Biens inférieurs]
E[Loi 2 : Habillement, logement, chauffage]
F[Reste plus ou moins constante si le revenu augmente]
G[Biens normaux]
H[Loi 3 : Loisirs, voyages, culture]
I[Augmentation avec l'augmentation du revenu]
J[Biens supérieurs]

A --> B
A --> E
A --> H
B --> C
B --> D
E --> F
E --> G
H --> I
H --> J


```

<br>

<br>

```mermaid

graph LR
    A[L’épargne] --> B[Choix entre consommation immédiate et différée]
    A --> C[Motifs de l’épargne]
    A --> D[Formes de l’épargne]
    C --> E[Épargne de prévoyance]
    C --> F[Épargne de précaution]
    C --> G[Épargne de spéculation]
    D --> H[Thésaurisation]
    D --> I[Épargne financière]
    D --> J[Épargne non financière]
    I --> K[Compte d’épargne]
    I --> L[Actions]
    I --> M[Obligations]
    I --> N[Fonds d’investissement]
    I --> O[Assurance-pension]
    J --> P["Immeubles (logements/terrains)"]
    J --> Q["Biens meubles (vins, tableaux, ...)"]
    E --> R[Acquisition d’une voiture]
    E --> S[Préparation des vacances]
    F --> T[Maladie]
    F --> U[Risque de chômage]
    F --> V[Souci de l’avenir des enfants]
    F --> W[Niveau de la retraite]
    G --> X[Achats d’obligations]
    G --> Y[Achats d’actions]

```


<br>

<br>

### 2. Le financement de l’entreprise

<br>

<br>

```mermaid

graph LR
    A[Le financement <br> de l’entreprise] --> B[Utilisation des <br> facteurs de production]
    A --> C[Objectif : Rechercher <br> un profit maximal]
    A --> D[Bilan de l’entreprise]
    D --> E[Sources de <br> financement]
    D --> F[Emplois des fonds]
    E --> G["Financement interne <br> (autofinancement)"]
    E --> H[Financement externe]
    H --> I[Marchés financiers]
    I --> J[Émission d'actions]
    I --> K[Émission d'emprunts <br> obligataires]
    H --> L[Banques]
    L --> M[Présentation du <br> projet de développement]
    M --> N[Santé financière <br> de l'entreprise]
    M --> O[Recettes futures <br> du projet]
    
```

<br>

<br>

```mermaid

graph LR
    A[Le financement de l’État] --> B[Secteur public]
    B --> C["Administration centrale (État)"]
    B --> D["Administrations locales (communes)"]
    B --> E[Sécurité Sociale]
    B --> F[Acteur économique]
    F --> G[Consommateur de <br> biens et services]
    F --> H[Fournisseur de <br> services publics]
    F --> I[Employeur]
    F --> J[Investisseur]
    A --> K[Ressources <br> financières]
    K --> L[Budget de l’État]
    L --> M[Recettes]
    M --> N[Recettes courantes]
    N --> O[Impôts et taxes]
    N --> P[Intérêts de <br> fonds publics]
    N --> Q[Redevances]
    N --> R[Revenus des sociétés <br> à participation publique]
    N --> S[Recettes diverses]
    M --> T[Recettes en capital]
    T --> U[Vente de biens immobiliers]
    M --> V[Recettes des <br> opérations financières]
    V --> W[Vente de <br> participations <br> publiques]
    L --> X[Emprunt public]
    X --> Y[Remboursement]
    L --> Z[Dépenses]
    Z --> AA[Dépenses courantes]
    AA --> AB[Consommation <br> publique]
    AA --> AC[Transferts]
    AA --> AD[Intérêts de la <br> dette publique]
    Z --> AE[Dépenses en capital]
    AE --> AF[Augmentation du <br> patrimoine public]
    Z --> AG[Dépenses des <br> opérations <br> financières]
    AG --> AH[Amortissement de la <br> dette publique]
    A --> AI[Solde budgétaire]
    AI --> AJ[Excédent, déficit ou <br> équilibre budgétaire]
    AI --> AK[Endettement public]
    AK --> AL[Dette intérieure]
    AK --> AM[Dette extérieure]
    AK --> AN[Obligation d'État]
    AK --> AO[Bon du Trésor]
    AN --> AP[Titre de dette <br> à long terme]
    AO --> AQ[Titre de dette <br> à court terme]
    AK --> AR[Financement monétaire]

```

<br>

<br>

```mermaid

graph LR
    A[Concept central : Le système financier]
    B[Agents à <br> capacité de <br> financement]
    C[Ménages]
    D["Entreprises (dans certains cas)"]
    E[Agents à <br> besoin de <br> financement]
    F[Entreprises]
    G[États]
    H[Mécanismes de <br> financement]
    I[Financement direct]
    J[Marché des capitaux]
    K[Actions]
    L[Obligations]
    M[Financement indirect]
    N[Système bancaire]
    O[Dépôts]
    P[Prêts]
    Q[Rôles clés <br> du système <br> financier]
    R[Intermédiaire entre <br> les épargnants et <br> les emprunteurs]
    S[Allocation efficace <br> des ressources <br> financières]
    T[Facilite l'investissement <br> et la croissance <br> économique]
    U[Offre des opportunités <br> de diversification <br> et de rendement <br> aux investisseurs]
    V[Points supplémentaires]
    W[Déséquilibre structurel <br> entre les <br> besoins et les capacités <br> de financement]
    X[Rôle des taux d'intérêt <br> dans l'équilibre de <br> l'offre et de la demande]
    Y[Importance du <br> système financier <br> pour une  <br> économie performante]

    A --> B
    B --> C
    B --> D
    A --> E
    E --> F
    E --> G
    A --> H
    H --> I
    I --> J
    J --> K
    J --> L
    H --> M
    M --> N
    N --> O
    N --> P
    A --> Q
    Q --> R
    Q --> S
    Q --> T
    Q --> U
    A --> V
    V --> W
    V --> X
    V --> Y


```

<br>

<br>

## Chapitre 2 : Emploi, travail et chômage

### 1. Les différentes notions de l’emploi

<br>

<br>

```mermaid

graph LR
    A[Différencier <br> travail et <br> emploi]
    B[Travail: Activité physique <br> ou intellectuelle exercée <br> pour produire des <br> biens ou des services]
    C[Emploi: Contrat liant <br> un employeur <br> à un employé pour <br> une activité rémunérée]
    D[Marché du travail]
    E[Lieu où se rencontrent <br> l'offre de travail et la <br> demande de travail]
    F[Détermine les <br> conditions d'emploi : <br> salaires, horaires, ...]
    G[Population active]
    H[Personnes âgées <br> de 15 à 64 ans <br> exerçant ou cherchant <br> un emploi rémunéré]
    I[Se divise en <br> population active occupée <br>  et population active inoccupée]
    J[Emploi intérieur et <br> emploi national]
    K[Emploi intérieur: <br> Ensemble des personnes <br> travaillant sur <br> le territoire national, <br> incluant les <br> frontaliers étrangers]
    L[Emploi national: <br> Ensemble des personnes <br> résidant au Luxembourg <br> et ayant un emploi, <br> incluant les <br> frontaliers luxembourgeois <br> travaillant à l'étranger]
    M[Taux d'activité et <br> taux d'emploi]
    N[Taux d'activité: <br> Proportion de la population <br> en âge de travailler <br> qui est active]
    O[Taux d'emploi: <br> Proportion de la population <br> en âge de travailler <br> qui a un emploi]
    P[Différences entre les deux taux.]
    Q[Le taux d'emploi <br> ne prend pas en compte <br> les chômeurs, contrairement <br> au taux d'activité]
    R[Le taux d'emploi <br> permet de mesurer <br> l'efficacité du <br> marché du travail <br> à intégrer les actifs <br> dans la vie économique]
    S[Importance de ces notions]
    T[Permettent de comprendre <br> le fonctionnement du marché du travail <br> et les enjeux liés à l'emploi.]
    U[Utilisés pour analyser <br> les politiques publiques et <br> leurs impacts sur l'emploi.]
    V[Fournissent des indicateurs clés <br> pour la comparaison entre pays <br> et la mesure du <br> bien-être économique.]

    A --> B
    A --> C
    D --> E
    D --> F
    G --> H
    G --> I
    J --> K
    J --> L
    M --> N
    M --> O
    P --> Q
    P --> R
    S --> T
    S --> U
    S --> V

```

<br>

<br>

### 2. Mutations structurelles de l’emploi au Luxembourg

<br>

<br>

```mermaid

graph LR
    A[Évolution du taux d'emploi]
    B[Augmentation notable <br> ces dernières décennies, <br> principalement due à <br> l'accroissement de <br> l'emploi féminin]
    C[Réduction de l'écart <br> entre le taux d'emploi <br> des femmes et des hommes, <br> mais persistance d'une différence]
    D[Diminution de la <br> durée de travail]
    E[Baisse continue <br> sur plusieurs niveaux : <br> durée de vie active, <br> nombre de jours travaillés par an, <br> nombre d'heures <br> travaillées par semaine]
    F[Stabilisation récente <br> des durées hebdomadaires et <br> de la part des non-salariés]
    G[Débat actuel sur <br> une réduction du temps de travail <br> sans perte de salaire]
    H[Tertiarisation <br> de l'emploi]
    I[Secteur tertiaire devenu <br> le plus grand pourvoyeur d'emplois, <br> notamment dans les banques, <br> assurances et transports]
    J[Émergence d'activités connexes <br> nécessitant un savoir-faire spécialisé]
    K[Concentration des <br> emplois de services <br> à Luxembourg-Ville <br> et sa périphérie]
    L[Marché de l'emploi <br> de plus en plus <br> cosmopolite]
    M[Augmentation constante <br> de la population luxembourgeoise, <br> avec une part <br> croissante d'étrangers]
    N[Immigration et développement <br> de l'emploi frontalier]
    O[Frontaliers provenant <br> principalement de <br> France, Belgique <br> et Allemagne]
    P[Points clés]
    Q[Mutations structurelles <br> liées à la transition <br> du secondaire vers <br> le tertiaire]
    R[Besoin croissant <br> de main-d'oeuvre qualifiée <br> dans le secteur tertiaire]
    S[Importance de <br> l'immigration et <br> du travail frontalier <br> pour l'économie luxembourgeoise]
    T[Débat sur la conciliation <br> entre vie professionnelle <br> et vie privée]

    A --> B
    A --> C
    D --> E
    D --> F
    D --> G
    H --> I
    H --> J
    H --> K
    L --> M
    L --> N
    L --> O
    P --> Q
    P --> R
    P --> S
    P --> T

```

<br>

<br>


### 3. L’organisation du travail

<br>

<br>


```mermaid


graph LR
    A[Division du travail]
    B[Spécialisation des tâches <br> et gain d'habilité. <br> Utilisation plus efficace <br> du temps de travail.]
    C[Substitution <br> capital-travail]
    D[Remplacement du travail <br> par le capital dans la production. <br> Différences de coût <br> entre les deux facteurs. <br> Nouvelles techniques de <br> production automatisables.]
    E[Organisation <br> scientifique <br> du travail <br> OST]
    F[Ensemble de principes <br> pour optimiser la production industrielle. <br> Frederick W. Taylor <br> Repose sur une double division <br> du travail : verticale et horizontale]
    G[Fordisme]
    H[Application du taylorisme <br> à la chaîne de montage <br> par Henry Ford. <br> salaire à 5 dollars par jour <br> travail d'usine à grande échelle <br> consommation de masse.]
    I[Limites de l'OST et <br> émergence du post-taylorisme]
    J[Critique de la <br> déshumanisation du travail  <br> et du manque de considération <br> pour les ouvriers. <br> toyotisme <br> valorisation de la polyvalence, <br> de l'autonomie et de la <br> responsabilité des salariés <br> intégration de la dimension humaine <br> et des relations entre les travailleurs]
    K[Points clés]
    L[L'organisation du travail <br> vise à optimiser <br> la production et la performance. <br> Le post-taylorisme valorise <br> la coopération, l'autonomie et <br> le bien-être des salariés.]

    A --> B
    C --> D
    E --> F
    G --> H
    I --> J
    K --> L


```

<br>

<br>


### 4. Le fonctionnement du marché du travail – approche théorique

<br>

<br>


```mermaid

graph LR
A[La demande de travail] --> B[Besoins en <br> main-d'oeuvre]
B --> C[Volume de production]
C --> D[Court terme]
D --> E[Augmentation du <br> volume de production]
E --> F[Hausse du <br> volume de travail]
F --> G[Demande de <br> travail accrue]
C --> H[Moyen et <br> long terme]
H --> I[Évolution de <br> la productivité]
I --> J[Hausse de la <br> productivité]
J --> K[Baisse du volume <br> de travail nécessaire]
K --> L[Baisse de la <br> demande de travail]
A --> M[Facteurs influençant <br> la demande de travail]
M --> N[Prix du travail]
N --> O[Salaire à payer]
N --> P[Prix des autres <br> facteurs de production]
M --> Q[Productivité <br> du travail]
Q --> R[Produit marginal]
R --> S[Output supplémentaire]

```

<br>

<br>

```mermaid

graph LR
A[Facteurs de production] --> B[Ressources naturelles]
B --> C[Terre, matières premières, énergie]
A --> D[Ressources humaines]
D --> E[Travail manuel et intellectuel]
A --> F[Ressources en capital]
F --> G[Équipements, machines, outillage]
G --> H[Fonction de production]

```

<br>

<br>

```mermaid

graph LR
H[Fonction de production] --> I["Q = f(N, L, K)"]
I --> J[N = Terre]
I --> K[L = Travail]
I --> L[K = Capital]
```

<br>

<br>

SECTION 4 EST INCOMPLET


### 5. Le chômage

```mermaid
graph LR
A[Le chômage] --> B[Taux de chômage]
B --> C[Nombre de chômeurs]
B --> D[Population active]
D --> E[Résidents]
E --> F[Sans emploi]
E --> G[Disponibles pour le marché du travail]
E --> H[Recherche d'un emploi approprié]
E --> I[Non affectés à une mesure pour l'emploi]
E --> J[Indemnisés ou non indemnisés]
E --> K[Respect des obligations de suivi de l'ADEM]
```

<br>

<br>


```mermaid
graph LR
    A[Chômage] --> B[Types de chômage]
    B --> C[Chômage classique]
    B --> D[Chômage conjoncturel]
    B --> E[Chômage structurel]
    C --> F[Rigidité des salaires]
    D --> G[Demande globale]
    E --> H[Changements technologiques]
    E --> I[Déséquilibres de compétences]
    E --> J[Rigidités du marché du travail]
    A --> K[Conséquences du chômage]
    K --> L[Individus]
    K --> M[Familles]
    K --> N[Économie]
    L --> O[Perte de revenus]
    L --> P[Dégradation de la santé]
    L --> Q[Exclusion sociale]
    M --> R[Pauvreté]
    M --> S[Tensions familiales]
    M --> T[Limitations éducatives]
    N --> U[Perte de production]
    N --> V[Dépenses publiques]
    N --> W[Instabilité sociale]
    A --> X[Politiques de lutte contre le chômage]
    X --> Y[Chômage classique]
    X --> Z[Chômage conjoncturel]
    X --> AA[Chômage structurel]
    Y --> AB[Réduction des rigidités salariales]
    Y --> AC[Formation professionnelle]
    Z --> AD[Politiques fiscales et monétaires]
    Z --> AE[Investissements publics]
    AA --> AF[Éducation et formation]
    AA --> AG[Innovation et entrepreneuriat]
    AA --> AH[Assouplissement des rigidités]
```

<br>

<br>


```mermaid
graph LR
    A[Chômage] --> B[Conséquences économiques]
    B --> C[Gaspillage de ressources]
    B --> D[Diminution du niveau de vie]
    B --> E[Demande agrégée moins élevée]
    B --> F[Dévalorisation du capital humain]
    B --> G[Hausse des allocations de chômage]
    B --> H[Concentration du chômage dans certaines régions]
    B --> I[Hausse des inégalités]
    A --> J[Conséquences sociales]
    J --> K[Problèmes de santé mentale]
    J --> L[Difficultés de financement des études]
    J --> M[Situations de divorce]
    J --> N[Carrière criminelle]
```

<br>

<br>

```mermaid
graph LR
    A[Emploi, Chômage, Inactivité] --> B[Contrats à durée déterminée <br> CDD]
    A --> C[Intérim]
    B --> D[Précarité de l'emploi]
    B --> E[Sous-emploi]
    A --> F[Halo du chômage]
    F --> G[Personnes <br> non comptabilisées <br> comme chômeurs]
    G --> H[Abandon de la <br> recherche d'emploi]
    G --> I[Travail à temps partiel <br> cherchant un emploi <br> à temps plein]
    G --> J[Individus découragés <br> mais souhaitant travailler]
    F --> K[Impact au-delà <br> des chiffres officiels <br> du taux de chômage]
```
<br>

<br>


## Chapitre 3 : La monnaie

<br>

<br>

```mermaid
graph LR
A[Les fonctions de la monnaie] --> B[Paiement]
  A --> C[Mesure de la valeur et du prix]
  A --> D[Réserve de valeur]
  B --> E[Achats de biens et de services]
  C --> F[Comparaison entre les choses]
  D --> G[Transactions dans le temps]
  G --> H[Achats à une date future]
```

<br>

<br>

```mermaid
graph LR
A[Formes historiques de monnaie] --> B[Troc]
A --> C[Monnaie marchandise]
A --> D[Monnaie métallique]
B --> E[Avantages du troc]
B --> F[Inconvénients du troc]
C --> G[Avantages de la monnaie marchandise]
C --> H[Inconvénients de la monnaie marchandise]
D --> I[Avantages de la monnaie métallique]
D --> J[Inconvénients de la monnaie métallique]
E --> K[Spécialisation]
E --> L[Double coïncidence des besoins]
F --> M[Indivisibilité des biens]
F --> N[Problème de conservation]
G --> O[Pas de double coïncidence des besoins]
G --> P[Stockable]
H --> Q[Peu maniable]
H --> R[Peu divisible]
H --> S[Valeur variable]
I --> T[Inaltérable]
I --> U[Divisible]
I --> V[Commode]
I --> W[Fongible]
J --> X[Rareté]
J --> Y[Transport difficile]
```


<br>

<br>

```mermaid
graph LR
Z[Formes actuelles de monnaie]
Z --> AA[Monnaie fiduciaire]
Z --> AB[Monnaie scripturale]
AA --> AC[Avantages de la monnaie fiduciaire]
AA --> AD[Inconvénients de la monnaie fiduciaire]
AB --> AE[Avantages de la monnaie scripturale]
AB --> AF[Inconvénients de la monnaie scripturale]
AC --> AG[Transport facile]
AC --> AH[Titres au porteur]
AC --> AI[Confiance élevée]
AD --> AJ[Insuffisance des billets]
AD --> AK[Stockage et transport difficiles]
AE --> AL[Sécurité]
AE --> AM[Utilisation simple]
AE --> AN[Encaisse improductive]
AF --> AO[Crise de confiance]
```

<br>

<br>

```mermaid
graph LR
A[La banque centrale] --> B[Émission de la monnaie fiduciaire]
A --> C[Définition et mise en œuvre de la politique monétaire]
A --> D[Supervision du système bancaire]
A --> E[Prêteur en dernier ressort]

B --> F[Billets de banque]
B --> G[Pièces de monnaie]

C --> H[Stabilité des prix]
C --> I[Croissance économique]

D --> J[Solidité financière des banques commerciales]
D --> K[Stabilité du système financier]

E --> L[Prêts aux banques commerciales en difficulté]

F --> M[Monnaie fiduciaire]

H --> N[Inflation maîtrisée]
H --> O[Pouvoir d'achat préservé]

I --> P[Investissements stimulés]
I --> Q[Consommation accrue]

J --> R[Adéquation des fonds propres]
J --> S[Gestion des risques prudente]

K --> T[Confiance des acteurs économiques]
K --> U[Fluidité des transactions financières]

L --> V[Éviter les faillites bancaires]
L --> W[Préserver la stabilité du système financier]

A --> X[L'Eurosystème]
X --> Y[Banque centrale européenne <br> BCE]
X --> Z[Banques centrales nationales <br> BCN]

Y --> AA[Définition de la politique monétaire unique]
Y --> AB[Supervision du système bancaire de la zone euro]
Y --> AC[Coopération avec les BCN]

Z --> AD[Mise en œuvre de la politique monétaire unique]
Z --> AE[Contribution à la stabilité du système financier]

A --> AF[Les banques commerciales]
AF --> AG[Acceptation des dépôts]
AF --> AH[Octroi de crédits]
AF --> AI[Offre de services de paiement]

AG --> AJ[Comptes courants]
AG --> AK[Comptes d'épargne]
AG --> AL[Dépôts à terme]

AH --> AM[Prêts à la consommation]
AH --> AN[Prêts immobiliers]
AH --> AO[Prêts aux entreprises]

AI --> AP[Virements]
AI --> AQ[Prélèvements automatiques]
AI --> AR[Cartes de paiement]

AF --> AS[Intermédiation financière]
AS --> AT[Transfert des fonds des épargnants vers les emprunteurs]
AS --> AU[Financement des investissements]
```

<br>

<br>

```mermaid
graph LR
A[La masse monétaire] --> B[Agrégats monétaires]
B --> C[M1 <br> Monnaie au sens strict]
B --> D[M2]
B --> E[M3]

C --> F[Monnaie fiduciaire <br> billets et pièces]
C --> G[Monnaie scripturale <br> comptes courants]

D --> H[Dépôts d'épargne à vue]
D --> I[Dépôts à terme court]

E --> J[Devises]
E --> K[Bons de caisse]
E --> L[Titres d'OPCVM]

A --> M[Politique monétaire]
M --> N[Inflation]
M --> O[Croissance économique]

M --> P[Instruments de politique monétaire]
P --> Q[Ajustement de la masse monétaire]
Q --> R[Objectifs de la banque centrale]
```


<br>

<br>

```mermaid
graph LR
A[Banque centrale <br> BCE] --> B[Monnaie centrale]
B --> C[Pièces et billets]
B --> D[Réserves obligatoires]
D --> E[Banques commerciales]
E --> F[Monnaie scripturale]
F --> G[Dépôts à vue]
E --> H[Crédits]
H --> G
G --> I[Remboursement de crédit]
I --> F
A --> J[Politique monétaire]
J --> K[Besoins de l'économie]
K --> B
A --> L[Prêts aux gouvernements]
L --> B
A --> M[Prêts aux banques commerciales]
M --> B
A --> N[Achat de devises étrangères]
N --> B
E --> O[Nécessité de refinancement]
O --> P[Marché interbancaire]
O --> Q[BCE <br> prêteur en dernier ressort]
P --> E
Q --> E
A --> R[Réserves obligatoires]
R --> E
A --> S[Taux d'intérêt directeur]
S --> E

```

GRAPHIQUE DOIT ETRE CORRIGE


<br>

<br>

```mermaid
graph LR
A[Flux monétaires <br> MV] --> B[Valeur totale des transactions <br> PT]
B --> C[Flux réels]
D[Théorie quantitative de la monnaie] --> E[Vitesse de circulation de la monnaie <br> V]
E --> F[Niveau des transactions <br> T]
F --> G[Masse monétaire <br> M]
G --> H[Niveau général des prix <br> P]
I[Théorie keynésienne] --> J[Demande de monnaie <br> L]
J --> K[Revenu <br> Y]
J --> L[Taux d'intérêt <br> i]
J --> M[Anticipations des agents économiques]
M --> N[Politique monétaire expansionniste]
N --> O[Investissements]
O --> P[Croissance économique]
P --> H
```
GRAPHIQUE DOIT ETRE SIMPLIFIE

<br>

<br>

```mermaid
graph LR
A[Inflation] --> B[Définition]
B --> C[Hausse généralisée et continue <br> du niveau général des prix]
B --> D[Ne se limite pas à un ou deux biens]
B --> E["Hyperinflation (augmentation des prix > 50% par mois)"]
```

<br>

<br>

```mermaid
graph LR
A[Inflation] 
A --> F[Mesure de l'inflation]
F --> G[Indices de prix <br> IPC]
G --> H[Évolution du niveau des prix]
G --> I[Panier représentatif de biens et services]
G --> J[Indice de Laspeyres <br> quantités fixées à la période de référence]
```


<br>

<br>

```mermaid
graph LR
A[Inflation] 
A --> K[Salaires nominal et réel]
K --> L[Salaire nominal <br> rémunération en euros]
K --> M[Salaire réel <br> pouvoir d'achat]
M --> N["Calcul du salaire réel (salaire nominal / IPC)"]
```


<br>

<br>

```mermaid
graph LR
A[Inflation] 
A --> O[PIB nominal et réel]
O --> P[PIB nominal <br> valeur de la production à prix courants]
O --> Q[PIB réel <br> PIB nominal déflaté par IPC]
Q --> R[Analyse de la croissance économique en volume]
Q --> S[Élimination de l'effet de l'inflation]
```

<br>

<br>

```mermaid
graph LR
A[Inflation] 
A --> T[Causes de l'inflation]
T --> U[Inflation par la demande <br> demand-pull inflation ]
U --> V[Excès de demande par rapport à l'offre]
V --> W[Hausse des prix jusqu'à l'équilibre]
U --> X[Facteurs accroissant la demande]
X --> Y[Accès facile au crédit]
X --> Z[Baisses d'impôts]
X --> AA[Augmentation des investissements des entreprises]
X --> AB[Hausses des dépenses publiques]
X --> AC[Hausses des salaires]
X --> AD[Hausses des exportations]
X --> AE[Hausse de la masse monétaire]
T --> AF[Inflation par les coûts <br> cost-push inflation ]
AF --> AG[Augmentation des coûts de production des entreprises]
AG --> AH[Hausse du prix des matières premières]
AG --> AI[Hausse du prix de l'énergie]
AG --> AJ[Hausse des salaires]
AG --> AK[Hausse des charges sociales]
AG --> AL[Hausse des impôts]
AG --> AM[Hausse du taux d'intérêt]
AG --> AN[Répercussion sur les prix de vente]
```


<br>

<br>

```mermaid
graph LR
A[Inflation] 
A --> AO[Spirale inflationniste]
AO --> AP[Hausse des prix]
AP --> AQ[Hausse des salaires]
AQ --> AR[Nouvelle augmentation des prix]
A --> AS[Conséquences de l'inflation]
AS --> AT[Perte de pouvoir d'achat des ménages]
AS --> AU[Découragement de l'épargne]
AS --> AV[Baisse de la compétitivité des entreprises nationales]
AS --> AW[Tensions sociales]
```


<br>

<br>

```mermaid
graph LR
A[Inflation] 
A --> AX[Lutte contre <br> l'inflation]
AX --> AY[BCE <br> objectif d'inflation proche de 2%]
AY --> AZ[Taux d'intérêt directeur]
AZ --> BA[Hausse du <br> taux d'intérêt]
BA --> BB[Renchérissement <br> du crédit]
BA --> BC[Frein à la demande]
BA --> BD[Réduction de la pression sur les prix]
AX --> BE[Autres mesures]
BE --> BF[Augmentation du <br> coefficient de réserve <br> obligatoire des banques]
BE --> BG[Lutte contre les  revendications <br> salariales excessives]
```



<br>

<br>

```mermaid
graph LR
A[Inflation] 
A --> BH[La déflation]
BH --> BI[Baisse du <br> niveau général <br> des prix]
BH --> BJ[Gain de <br> pouvoir d'achat <br> aux ménages]
BH --> BK[Conséquences néfastes <br> pour l'économie]
A --> BL[Conséquences de <br> la déflation]
BL --> BM[Report des achats]
BL --> BN[Réduction de <br> la consommation et <br> de l'investissement]
BL --> BO[Aggravation de <br> la situation financière <br> des emprunteurs]
BO --> BP[Augmentation du <br> coût réel <br> de la dette]
A --> BQ[Spirale déflationniste]
BQ --> BR[Baisse <br> des prix]
BR --> BS[Baisse de <br> la demande]
BS --> BT[Baisse de <br> la production]
BT --> BU[Baisse des <br> salaires]
BT --> BV[Baisse de <br> l'investissement]
BV --> BW[Renforcement <br> de la baisse <br> des prix]
A --> BX[Lutte contre <br> la déflation]
BX --> BY[Difficulté à <br> combattre la déflation]
BY --> BZ[Inefficacité des <br> politiques monétaires <br> traditionnelles]
BY --> CA[Exemple du Japon <br> déflation]

```



<br>

<br>


## Chapitre 4 : Introduction à la sociologie

<br>

<br>

```mermaid
graph LR
A[La sociologie] --> B[Étude des sociétés humaines]
A --> C[Interactions sociales]
B --> D[Individus]
B --> E[Groupes]
B --> F[Institutions]
```

<br>

<br>


```mermaid
graph LR
G[Concepts clés en sociologie] --> H[Socialisation]
G --> I[Structure sociale]
G --> J[Culture]
G --> K[Institutions sociales]
```
<br>

<br>


```mermaid
graph LR
L[Domaines de la sociologie] --> M[Inégalités sociales]
L --> N[Criminologie]
L --> O[Sociologie de l'éducation]
L --> P[Sociologie médicale]
```
<br>

<br>


```mermaid
graph LR
Q[Applications de la sociologie] --> R[Pauvreté]
Q --> S[Criminalité]
Q --> T[Éducation]
Q --> U[Soins de santé]
```

<br>

<br>


<br>

<br>

```mermaid

graph LR


```

