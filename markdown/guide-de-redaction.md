# Rédaction d'une question problématisée

Une grande partie du travail des économistes consiste à communiquer de manière
écrite, sous forme de livre ou d’article. Pour les apprenants, les épreuve en
économie exigent que les idées soient communiquées sous forme écrite. Dans
cette partie, nous présentons divers conseils sur la manière de rédiger
des réponses.

## Les objectifs de l'évaluation

Les épreuves de contrôle continu ont pour objectif d'évaluer l'aptitude de 
l'apprenant à:

- mobiliser des connaissances théoriques pour la compréhension des phénomènes économiques
- rédiger des réponses construites et argumentées, montrant une maîtrise correcte de la langue

## Les bases

En général, il s'agit de développer le contenu économique, tout en améliorant
les capacités rédactionnelles. Ces compétences peuvent être amélioré en lisant
des journaux, des blogs, … .

Voici dix conseils pour améliorer les capacités rédactionnelles:

1. Soyez clair et précis dans votre rédaction. Utilisez des mots que vous
   comprenez et lorsque vous utilisez des termes techniques, soyez précis.
   Ceci est important car l'économie a de nombreux termes très similaires.
2. N'oubliez pas d'adapter votre écriture à votre public. Dans la plupart des
   cas, ce sera votre enseignant. Ils chercheront à lire des documents rédigés
   de manière pertinente et utilisant des termes et des concepts économiques
   appropriés.
3. Écrivez de manière impersonnelle. En d'autres termes, évitez le «je» 
   ou «nous» dans vos productions écrites. Cela s'applique particulièrement 
   lorsqu'on vous demande de faire une évaluation d'une question ou d'une 
   argumentation économique. 
4. Réfléchissez bien au temps auquel vous écrivez. Essayez d'être cohérent. 
5. Faites attention aux phrases et à la ponctuation. En général, essayez 
   d'écrire des phrases courtes, allant à l'essentiel et contenant des mots 
   que vous comprenez et savez utiliser. 
6. Utilisez une idée par paragraphe et développez vos idées. Cela permet à 
   une personne lisant votre travail de reconnaître plus facilement sur 
   quoi porte votre réponse et de voir comment votre idée a été développée.
7. Renforcez vos arguments. En d'autres termes, ne faites jamais de 
   déclarations que vous ne pouvez pas fonder. Essayez toujours d'élaborer 
   ou de développer vos déclarations et vos arguments.
8. Évitez l'humour. En cas de doute, laissez l'humour et l'esprit en dehors 
   de vos réponses écrites. De nombreux économistes ont le sens de l'humour,
   mais les épreuves, les examens et les productions écrites ne sont pas 
   le lieu pour le démontrer. 
9. Soyez professionnel. Évitez d’offenser, même si vous n'êtes pas d'accord 
   avec ce que vous avez pu lire ou avec la question d'examen. Il existe 
   d'autres moyens de faire connaître votre opinion. 
10. Continuez à travailler sur vos capacités rédactionnelles. Ne soyez 
    jamais satisfait et continuez à réfléchir à la façon dont vous pouvez 
    écrire de manière claire et pertinente. 

## Identifier le type de question

Les questions d'examen contiennent généralement deux instructions très importantes : 

Les mots clés dans la question indiquent la **forme** que doit prendre la réponse. Par exemple, cela pourrait être sous la forme d'une description, d'une discussion, d'une explication ou simplement d'une déclaration. Ces mots sont là pour un but, à savoir qu'ils sont destinés à guider les compétences à utiliser lorsque vous répondez à une question particulière. 

D’autres mots clés indiquent le **contenu** de votre réponse. Ces mots clés sont de nature beaucoup plus diversifiée puisqu'ils couvrent l'ensemble de la matière du programme. Leur objectif est de clarifier l'objet de la question et ce sur quoi vous devez écrire. 


| Mots-clés | Explication |
| --- | --- |
| _Calculer_ ... | Entraînez-vous à l'aide des informations fournies |
| _Définissez_ ... | Donnez le sens exact |
| _Décrire_ ... | Donnez une description de ... |
| _Identifiez_ ... | Donnez un exemple ou un point clé |
| _Illustrez_ ... | Donnez des exemples ou utilisez un diagramme |
| _Aperçu_ ... | Décrivez le point clé sans détail |
| _Analyser_ ... | Énoncez les points principaux et montrez comment ils sont liés et connectés | 
| _Comparez_ ... | Expliquez les similitudes et les différences |
| _Expliquez_ ... | Donnez des raisons claires ou exprimez clairement |
| _Envisagez_ ... | Donnez votre avis sur, avec une certaine justification |
| _Évaluez_ ... | Montrez à quel point quelque chose est important |
| _Discutez_ ... | Donnez les arguments importants, pour et contre, idéalement avec une conclusion |
| _Justifiez_ ... | Expliquez pourquoi les arguments pour une opinion sont plus forts que les arguments contre |
| _Évaluer_ ... | Discutez de l'importance de, en essayant de pondérer vos opinions |
| _Dans quelle mesure_ ... | Donnez des raisons pour et contre, arrivez à une conclusion avec une justification des arguments les plus forts et des plus faibles |

## Méthode pour la construction d'un brouillon

Afin d'éviter les hors-sujets, il faut prévoir quelques minutes pour faire un premier brouillon.

1. Analyser la question posée
   1. Identifier la partie du cours. En général, la question porte sur l'un 
      des cours étudiés depuis le début de l'année.
   1. Les éléments théoriques.
   1. Les mots clés
2. Organiser la réponse
   1. Choisir un plan: le nombre de parties.
   1. Mobiliser ses connaissances: mots clés, théories, ...

## Rédaction du texte

- Présenter le(s) document(s) en faisant le lien avec le sujet proposé.
- Reprendre la problématique rédigée à l'étape 1 du brouillon.
- Selon le temps disponible, annoncer rapidement le plan.
- Il comporte deux ou trois parties, selon le plan suggéré par la consigne.
- Chaque partie commence par une ou deux phrases pour exposer l'idée générale.
- Chaque argument s'appuie sur un ou des éléments du ou des document(s) et est enrichi par un élément de connaissance précis renvoyant aux théories, acteurs et événements.
- La conclusion répond à la question posée.  

## En résumé

<div class="center">

```mermaid
graph TD
    A[Question économique] --> B[Analyse]
    B --> C[Résultat]
    C --> D[Évaluations]
```
</div>

- **Question économique**: énoncé d'un problème ou d'une question économique particulière
- **Analyse**: application des concepts et des techniques utilisées en sciences économiques
- **Résultat**: présentation des résultats de l'analyse économique
- **Évaluation**: une évaluation de l'impact des résultats sur les individus ou les groupes

## Sources

- Colin Bamford et Susan Grant. _Cambridge International AS and A Level Economics Coursebook with CD-ROM_. Cambridge University Press, 2014.
- Michel Hagnerelle. _Géographie Tle_. Magnard. 
