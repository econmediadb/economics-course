# Chapitre 1: Le fonctionnement des marchés

## Situer le chapitre 1 par rapport au cours de l'enseignement secondaire classique

```mermaid
flowchart LR
  subgraph MARCHES-DES-BIENS-ET-SERVICES
    direction TB
    subgraph SCIENCES-ECO-3e
        direction TB
            A[L'économie en tant que <br> science sociale]-->B[Affirmations économiques <br> positives et normatives]
            B --> C[problème économique]
            C --> D[Frontières des <br> possibilités de production]
            D --> E[division du travail]
            E --> F[Économie de marché, <br> économie mixte et <br> économie planifiée]
    end
    direction LR
    subgraph MARCHES-2e
        direction TB
            c1[décision rationnelle]-->c2[demand et offre]
            c2-->c3[élasticité]
            c3-->c4[mécanisme des prix]
            c4 --> c5[surplus du <br> consommateur et <br> du producteur]
            c5 --> c6[impôts et subventions]
    end
    subgraph DEFAILLANCE-INTERVENTION-1e
        direction TB
            b1[défaillance du marché]-->b2[intervention publique]
    end
  end
  SCIENCES-ECO-3e --> MARCHES-2e
  MARCHES-2e --> DEFAILLANCE-INTERVENTION-1e
```

## Les compétences qui seront acquises dans ce chapitre :
- Compréhension des hypothèse sous-jacentes de la prise de décision économique rationnelle. Les consommateurs cherchent à maximiser leur utilité, tandis que les entreprises cherchent à maximiser leurs profits.
- Compréhsion de la demande économique :
    1. La distinction entre les mouvements *le long d'une courbe* de demande et les *déplacements* d'une courbe de demande.
    2. Les facteurs qui peuvent provoquer un déplacement de la courbe de la demande (les conditions de la demande)
    3. Le concept d'utilité marginale décroissante et son influence sur la forme de la courbe de demande
- Compréhsion de l'offre économique : 
    1. La distinction entre les mouvements le long d'une courbe d'offre et les déplacements d'une courbe d'offre.
    2. Les facteurs qui peuvent provoquer un déplacement de la courbe d'offre (les conditions d'offre).       
- Compréhension du notion de l'élasticité :
    1. Compréhension de l'élasticité de la demande par rapport au prix, au revenu et à l'élasticité croisée.
    2. Utiliser des formules pour calculer les élasticités de prix, de revenu et les élasticités croisées de la demande.
    3. Interpréter les valeurs numériques de :
        - élasticité de la demande par rapport au prix : élastique unitaire, parfaitement et relativement élastique, et parfaitement et relativement inélastique
        - élasticité de la demande par rapport au revenu : produits inférieurs, normaux et de luxe ; relativement élastique et relativement inélastique
        - élasticité croisée de la demande : biens substituables, complémentaires et non liés.
    4. Les facteurs qui influencent les élasticités de la demande.
    5. La signification des élasticités de la demande pour les entreprises et le gouvernement en termes de :
        - l'imposition d'impôts indirects et de subventions
        - des changements dans le revenu réel
        - des changements dans les prix des biens de substitution et des biens complémentaires.
    6. La relation entre l'élasticité de la demande par rapport au prix et le revenu total (y compris le calcul).
    1. (option) Compréhension de l'élasticité de l'offre par rapport au prix
    2. (option)Utiliser une formule pour calculer l'élasticité de l'offre par rapport au prix
    3. (option)Interpréter les valeurs numériques de l'élasticité de l'offre par rapport au prix :
        - parfaitement et relativement élastique, et parfaitement et relativement inélastique.
    4. (option)Facteurs qui influencent l'élasticité de l'offre par rapport au prix
    5. (option)La distinction entre le court terme et le long terme en économie et sa signification pour l'élasticité de l'offre.   
- Compréhension du méchanisme des prix :
    1. Fonctions du mécanisme des prix pour allouer les ressources : 
        - rationnement
        - incitation
        - signalisation
    2. (option) Le mécanisme des prix dans le contexte de différents types de marchés, y compris les marchés locaux, nationaux et mondiaux.
- Compréhension de la notion du surplus du consommateur et du producteur :
   1. La distinction entre le surplus du consommateur et le surplus du producteur
    2. L'utilisation de diagrammes d'offre et de demande pour illustrer le surplus du consommateur et du producteur
    3. Comment les changements de l'offre et de la demande peuvent-ils affecter le surplus du consommateur et du producteur ?

## Objectifs du chapitre :
- La prise de décision économique rationnelle des consommateurs et des entreprises en utilisant les concepts de
  - préférences
  - rationalité et utilité
  - coûts d'opportunité
  - contrainte budgétaire 
  - maximisation et raisonnnement à la marge
  - coût de production: coût total, coût moyen, coût marginal
  - courbe de demande
  - recette marginale
  - égalisation du coût marginal et de la recette marginale
  - courbe d'offre (tbc)
- Le déplacement des courbes (de demande et d'offre) et déplacement sur les courbes
  - variation de l'offre
  - variation de la demande
  - variation du prix
- Elasticité
  - Elasticité demande-prix
  - Courbe d'offre (tbc)
  - Elasticité offre-prix
  - Equilibre sur le marché concurrentiel
- Le surplus du consommateur et du producteur
  - coût de production
  - décision de la quantité à produire
  - courbe d'offre du producteur

## 1.1. La prise de décision rationnelle

### 1.1.1. Les préférences et choix économiques

[source (2.2)](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/text/02.html#22-les-pr%C3%A9f%C3%A9rences-et-choix-%C3%A9conomiques)


En tant qu’élève, tous les jours vous devez choisir le nombre d’heures que vous passerez à étudier. De nombreux facteurs peuvent influencer votre choix : votre intérêt pour une matière, la difficulté de celle-ci, le nombre d’heures que vos amis passent à étudier, et ainsi de suite. Une part de votre motivation pour vos études vient peut-être de la conviction selon laquelle plus vous passerez de temps à étudier, plus les notes que vous obtiendrez seront élevées.

Dans ce chapitre, nous construisons un modèle simple de choix de nombre d’heures travaillées par un élève, fondé sur l’hypothèse que la note finale sera d’autant plus élevée que le temps consacré à étudier sera important. Nous supposons une relation positive entre le nombre d’heures travaillées et la note finale, mais est-elle vérifiée dans les faits ?

Un groupe de psychologues de l’éducation a étudié le comportement de 84 étudiants de l’université d’État de Floride afin d’identifier les facteurs ayant affecté leurs performances.

<!--
Elizabeth Ashby Plant, Karl Anders Ericsson, Len Hill et Kia Asberg. 2005. ‘Why study time does not predict grade point average across college students: Implications of deliberate practice for academic performance’. Contemporary Educational Psychology 30 (1): pp. 96–116.--> 

Par exemple, les conditions dans lesquelles ils étudient devraient être examinées : étudier pendant une heure dans une pièce bruyante remplie de personnes n’est probablement pas aussi efficace qu’étudier pendant une heure à la bibliothèque.

Ainsi, après avoir pris en compte l’environnement et d’autres facteurs pertinents (y compris la moyenne générale au semestre précédent, le nombre d’heures consacrées au travail salarié, ou à faire la fête), les psychologues ont estimé qu’une heure supplémentaire passée à étudier chaque semaine augmentait la moyenne générale à la fin du semestre de 0,24 point en moyenne.

Imaginons maintenant un élève, que nous appellerons Alexeï. Il peut faire varier le nombre d’heures qu’il passe à étudier. Dans ce modèle, le temps passé à étudier fait référence à la totalité du temps qu’Alexeï consacre à son apprentissage, que ce soit en classe ou individuellement, quotidiennement (et non par semaine, comme c’était le cas pour les étudiants en Floride). La relation entre le nombre d’heures passées à étudier et la note finale est représentée dans les colonnes du Tableau 2.2.

**Tableau 2.2** Comment le temps passé à étudier affecte-t-il la note d’Alexeï ? 
| Heures passées à étudier | 0 | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9  | 10 | 11 | 12 | 13 | 14 | 15 ou plus |
|--------------------------|---|----|----|----|----|----|----|----|----|----|----|----|----|----|----|------------|
| Note                     | 0 | 22 | 33 | 42 | 50 | 57 | 63 | 69 | 74 | 78 | 81 | 84 | 86 | 88 | 89 | 90         |

En réalité, la note finale peut aussi être affectée par des éléments imprévisibles. Dans la vie de tous les jours, nous rassemblons ces événements sous le terme de « chance ». Pour ce faire, nous utilisons l’expression « en gardant les autres choses constantes », ou, plus souvent, l’expression latine, ceteris paribusceteris paribus La signification littérale de l’expression est « toutes choses égales par ailleurs ». Dans un modèle économique, elle signifie que l’analyse « garde les autres facteurs inchangés ».

, qui signifie « toutes choses étant égales par ailleurs ». Nous allons supposer que, comme dans l’étude menée en Floride, les heures qu’il passe à étudier au cours du semestre vont augmenter sa note finale, ceteris paribus. Le Tableau 2.2 montre comment sa note variera avec le nombre d’heures passées à étudier, si tous les autres facteurs – comme sa vie sociale – sont maintenus constants.

Si Alexeï étudie quatre heures, sa note sera 50 et s’il étudie 10 heures, il obtiendra une note de 81. Avec 15 heures passées à étudier, Alexeï obtient sa note maximale, 90. Alexeï fait donc face à un arbitrage : à combien de points est-il prêt à renoncer, afin de pouvoir consacrer du temps à autre chose qu’aux études ?

La décision dépend de ses préférences – c’est-à-dire de ce qui lui importe. Si Alexeï ne se préoccupait que de ses notes, il étudierait 15 heures par jour. Mais, comme d’autres personnes, Alexeï souhaite aussi avoir du temps libre – il aime dormir, sortir et aussi regarder la télévision.

Imaginons maintenant une fermière autosuffisante, que nous appellerons Angela. Elle produit des céréales qu’elle mange et ne vend à personne. Son choix est cependant contraint : pour produire des céréales, il faut travailler. Le Tableau 2.3 indique comment le montant de céréales produit dépend du nombre d’heures travaillées chaque jour.

**Tableau 2.3** Comment le temps passé à travailler affecte-t-il la production d’Angela ?
| Heures travaillées | 0 | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9  | 10 | 11 | 12 | 13 | ... | 18 | 24 |
|--------------------------|---|----|----|----|----|----|----|----|----|----|----|----|----|----|----|------------| ---- |
| Céréales                     | 0 | 9 | 18 | 26 | 33 | 40 | 46 | 51 | 55 | 58 | 60 | 62 | 64 | 66 | ... | 69         | 71 |

Angela choisit son nombre d’heures de travail. Par exemple, si elle travaille 12 heures par jour, elle produira 64 unités de céréales. Nous faisons l’hypothèse que si elle produit trop peu de céréales, elle mourra de faim. Pour quelle raison ne peut-elle pas produire le plus de céréales possible ?

Tout comme Alexeï, Angela accorde de la valeur au temps libre : elle doit faire un choix entre consommer des céréales et consommer du temps libre.

#### Exercice 2.2 Hypothèses ceteris paribus

On vous a demandé de conduire dans votre lycée une étude similaire à celle menée à l’université d’État de Floride.

 1. Hormis l’environnement de travail, quels facteurs selon vous devraient être maintenus constants dans un modèle portant sur la relation entre les heures passées à étudier et la note finale ?
 2. Outre les conditions pour étudier (environnement de travail), quelles autres informations voudriez-vous collecter sur les lycéens ?

### 1.1.2. La rationalité et l'utilité individuelle

[source 2.4](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/text/02.html#24-la-rationalit%C3%A9-et-lutilit%C3%A9-individuelle)

Une journée compte 24 heures. Alexeï doit diviser ce temps entre ses études (le temps qu’il passe à apprendre) et son temps libre (c’est-à-dire le reste de son temps). Nous illustrons ses préférences dans le Graphique 2.2, représentant le temps libre en abscisse et la note finale en ordonnée.

[insert graph here](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/images/web/graphique-02-02.jpg)

Le temps libre fait référence au temps qui n’est pas passé à étudier. Chaque point du graphique représente une combinaison différente de temps libre et d’une note finale.

Nous pouvons supposer :

- Alexeï préfère plus de temps libre à moins de temps libre : les combinaisons A et B donnent toutes les deux une note de 84, mais Alexeï préférera A car la combinaison donne plus de temps libre.
- Alexeï préfère une note élevée à une note faible : avec les combinaisons C et D, Alexeï a 20 heures de temps libre par jour, mais il préfère D car cela lui donne une note plus élevée.

Comparons toutefois les points A et D dans Graphique 2.2. Alexeï préférerait-il D (note faible et beaucoup de temps libre) ou A (note élevée et moins de temps libre) ? Une manière de le découvrir serait de lui demander.

Supposons qu’il se déclare indifférent entre A et D, ce qui implique qu’il tirerait la même satisfaction des deux résultats. Nous disons alors que ces deux résultats lui procurent la même utilité. Et nous savons qu’il préfère A à B, donc que B lui procure une utilité moindre que A ou D.

Une manière systématique de représenter les préférences d’Alexeï serait de commencer par déterminer l’ensemble des combinaisons qui lui procurent la même utilité que A et D.

Nous pourrions poser une autre question à Alexeï : « Imagine que tu puisses avoir la combinaison A (15 heures de temps libre, 84 points). Combien de points serais-tu prêt à sacrifier pour obtenir une heure supplémentaire de temps libre ? » Supposons qu’après mûre réflexion sa réponse soit « 9 ». Nous saurions alors qu’il serait indifférent entre A et E (16 heures de temps libre, 75 points). Nous pourrions poser la même question pour la combinaison E, et ainsi de suite jusqu’au point D. En continuant ainsi, nous pourrions établir une liste comme celle du Tableau 2.4.

Tableau 2.4 Représentation des préférences d’Alexeï. 
|                       | A  | E  | F  | G  | H  | D  |
|-----------------------|----|----|----|----|----|----|
| Heures de temps libre | 15 | 16 | 17 | 18 | 19 | 20 |
| Note finale           | 84 | 75 | 67 | 60 | 54 | 50 |

Alexeï est indifférent entre A et E, entre E et F, et ainsi de suite – ce qui signifie qu’il est indifférent entre l’ensemble des combinaisons situées entre A et D.

Pour décrire les préférences, il n’y a pas besoin de connaître l’utilité exacte de chaque option ; nous avons seulement besoin de connaître quelles combinaisons procurent une utilité supérieure ou inférieure à celle apportée par les autres. Dans notre modèle des préférences d’un lycéen, les biens sont « la note finale » et « le temps libre ». Dans d’autres modèles, il s’agira souvent de biens de consommation, comme la nourriture ou les vêtements, et nous appelons l’individu un consommateur.

Pour comprendre le comportement économique, nous avons besoin de connaître les préférences des gens. En économie, nous nous figurons les personnes comme prenant des décisions selon leurs préférences, ce par quoi nous désignons les goûts, les dégoûts, les attitudes, les sentiments et les croyances qui les animent et les motivent.

Si vous demandez à quelqu’un s’il aime les glaces, il répondra probablement honnêtement. En revanche, la réponse à la question : « À quel point êtes-vous altruiste ? » peut être un mélange de vérité, d’autopromotion et de vœu pieux. C’est pourquoi les économistes utilisent parfois des expériences afin d’observer le comportement des individus sous des conditions bien définies. Les expériences mesurent ce que les gens font, plutôt que ce qu’ils disent.

Parfois, il est possible de mener des expériences « sur le terrain » : c’est-à-dire de changer délibérément les conditions économiques dans lesquelles les individus prennent des décisions, et d’observer comment leur comportement se modifie. Une illustration est donnée par le travail de Juan Camilo Cárdenas, un économiste de l’université des Andes à Bogota, en Colombie. Dans notre vidéo Économiste en action, il décrit son recours à l’économie expérimentale dans des situations réelles, et comment cela nous aide à comprendre pourquoi les individus coopèrent, même lorsqu’il existe des incitations manifestes à ne pas le faire.

#### Vidéo
[Dans notre vidéo Économiste en action «  Invisible hands working together », Juan Camilo Cárdenas parle de son usage innovant de l’économie expérimentale dans des situations de la vie réelle.](https://tinyurl.com/se37rca)

#### Exercice 2.3 Quelles sont vos préférences ? [lien](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/text/02.html#24-la-rationalit%C3%A9-et-lutilit%C3%A9-individuelle)


#### Exercice 2.4 Préférences [lien](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/text/02.html#24-la-rationalit%C3%A9-et-lutilit%C3%A9-individuelle)

### 1.1.3. Les coûts d’opportunité

[source 2.5](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/text/02.html#25-les-co%C3%BBts-dopportunit%C3%A9)

Revenons maintenant au problème d’Alexeï, qui doit arbitrer entre ses notes et son temps libre. Cette fois-ci nous montrerons comment la note finale dépend de la quantité de temps libre plutôt que du temps passé à étudier. Le Tableau 2.5 montre la relation entre sa note finale et le nombre d’heures de temps libre par jour — soit l’image inversée du Tableau 2.2.

Tableau 2.5 Comment le temps passé à étudier affecte-t-il la note d’Alexeï ? 
| Heures de temps libre par jour | 24 | 23 | 22 | 21 | 20 | 19 | 18 | 17 | 16 | 15 | 14 | 13 | 12 | 11 | 10 | 9 ou moins |
|--------------------------------|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|------------|
| Note                           | 0  | 20 | 33 | 42 | 50 | 57 | 63 | 69 | 74 | 78 | 81 | 84 | 86 | 88 | 89 | 90         |

Si Alexeï travaille pendant 24 heures, cela signifie qu’il n’a pas de temps libre et que sa note finale est de 90. S’il choisit d’avoir 24 heures de temps libre par jour, il aura une note de zéro. Une autre façon d’exprimer cela est de dire que le temps libre a un coût d’opportunité : pour avoir plus de temps libre, Alexeï doit renoncer à l’opportunité d’avoir une note plus élevée.

Le temps libre a un coût d’opportunité, sous la forme d’une perte de points de sa note (de façon équivalente, il est possible de dire que les points ont un coût d’opportunité – à savoir le temps libre auquel Alexeï doit renoncer pour les obtenir). Cela représente l’arbitrage qu’il doit faire entre la note et le temps libre.

#### (Option) Illustration plus avancée

[... renoncer à une heure de temps libre et elle retire à la fois de l’utilité du temps libre et de la consommation de céréales. ...](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/text/02.html#25-les-co%C3%BBts-dopportunit%C3%A9)

### 1.1.4. La contrainte budgétaire

[source 2.7](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/text/02.html#27-la-contrainte-budg%C3%A9taire)

Imaginons que vous cherchiez un emploi juste après votre sortie de l’université. Pris ensemble, le salaire et les heures de travail déterminent la quantité de temps libre dont vous bénéficiez et vos revenus totaux.

Comme pour Angela, nous allons réfléchir en termes de consommation et de temps libre, par jour et en moyenne. Nous supposons que vos dépenses – c’est-à-dire votre consommation moyenne de nourriture, de logement et d’autres biens et services – ne peuvent pas excéder vos revenus (donc, par exemple, vous ne pouvez pas emprunter pour augmenter votre consommation) :

Consommation=salaire(24−temps libre)

Nous appellerons cette équation votre contrainte budgétaire, parce qu’elle permet de calculer ce que vous pouvez vous permettre d’acheter.

Vous vous attendez à gagner un salaire de 15 euros par heure. Dans les colonnes du Tableau 2.6a nous avons calculé votre temps libre pour des heures de travail variant entre 0 et 16 heures par jour, et votre consommation maximale, quand votre salaire horaire est de 15 euros. L’équation de la contrainte budgétaire est :

Consommation=15(24−temps libre)

Tableau 2.6a Comment le temps passé à travailler affecte-t-il votre consommation quotidienne ? 
| Heures de travail | Temps libre | Consommation |
|-------------------|-------------|--------------|
| 0                 | 24          | 0 €          |
| 2                 | 22          | 30 €         |
| 4                 | 20          | 60 €         |
| 6                 | 18          | 90 €         |
| 8                 | 16          | 120 €        |
| 10                | 14          | 150 €        |
| 12                | 12          | 180 €        |
| 14                | 10          | 210 €        |
| 16                | 8           | 240 €        |

Les emplois diffèrent par le nombre d’heures que vous devez passer à travailler — quel serait donc votre nombre d’heures travaillées idéal ?

Tout comme Alexeï, vous cherchez un équilibre entre deux arbitrages :

- Le montant de consommation que vous êtes désireux(se) d’échanger contre une heure de temps libre.
- Le montant de consommation que vous pouvez obtenir en renonçant à une heure de temps libre, qui est égal au salaire.

Alors que vous étudiez cette possibilité, vous recevez un courriel. Un mystérieux bienfaiteur souhaiterait vous proposer, à vie, un revenu quotidien de 50 euros (la seule chose qu’il vous demande de faire est de fournir vos coordonnées bancaires). Vous réalisez immédiatement que cela modifiera votre choix d’emploi.

La nouvelle situation est décrite dans le Tableau 2.6b : pour chaque niveau de temps libre, votre revenu total (vos revenus augmentés du don mystérieux) est supérieur de 50 euros au niveau précédent. Votre contrainte budgétaire est maintenant :

Consommation=15(24−temps libre)+50

Tableau 2.6b Comment le temps passé à travailler affecte-t-il votre consommation quotidienne ? 
| Heures de travail | Temps libre | Consommation |
|-------------------|-------------|--------------|
| 0                 | 24          | 50 €         |
| 2                 | 22          | 80 €         |
| 4                 | 20          | 110 €        |
| 6                 | 18          | 140 €        |
| 8                 | 16          | 170 €        |
| 10                | 14          | 200 €        |
| 12                | 12          | 230 €        |
| 14                | 10          | 260 €        |
| 16                | 8           | 290 €        |

Remarquez que le revenu additionnel de 50 euros ne change pas votre coût d’opportunité du temps : chaque heure de temps libre supplémentaire réduit toujours votre consommation de 15 euros (le salaire).

Vous réalisez soudain qu’il ne serait probablement pas sage de donner au mystérieux étranger vos coordonnées bancaires (il s’agit peut-être d’un canular). Une année plus tard, votre chance tourne : votre employeur augmente votre salaire de 10 euros par heure, et vous laisse la possibilité de renégocier vos heures. Votre contrainte budgétaire est désormais égale à :

Consommation=25(24−temps libre)

Dans le Tableau 2.6c, vous pouvez voir comment la contrainte budgétaire évolue lorsque le salaire augmente.

Tableau 2.6c Comment le temps passé à travailler affecte-t-il votre consommation quotidienne ? 
| Heures de travail | Temps libre | Consommation |
|-------------------|-------------|--------------|
| 0                 | 24          | 0 €          |
| 2                 | 22          | 50 €         |
| 4                 | 20          | 100 €        |
| 6                 | 18          | 150 €        |
| 8                 | 16          | 200 €        |
| 10                | 14          | 250 €        |
| 12                | 12          | 300 €        |
| 14                | 10          | 350 €        |
| 16                | 8           | 400 €        |

Comparez les résultats dans le Tableau 2.6a et le Tableau 2.6c. Avec 24 heures de temps libre (et pas de travail), votre consommation serait de 0, quel que soit le salaire. Mais pour chaque heure de temps libre à laquelle vous renoncez, votre consommation augmente désormais de 25 euros, au lieu de 15 euros précédemment.

### 1.1.5. La maximisation et le raisonnement à la marge

[source 2.9](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/text/02.html#29-quelle-quantit%C3%A9-produire-la-maximisation-et-le-raisonnement-%C3%A0-la-marge)

Apple fixe des prix élevés pour ses iPhone et iPad afin d’augmenter ses profits, plutôt que de baisser les prix pour toucher plus de clients. D’autres entreprises adoptent des stratégies plus ou moins différentes. Par exemple, entre septembre 2017 et septembre 2018, le profit unitaire de Ford se situait entre 2,7 et 6,1 %. À la même période, le profit unitaire réalisé sur la vente des produits d’Apple représentait entre 38,4 et 38,9 % du prix.

Dans cette section, nous allons étudier la manière dont les entreprises qui fabriquent des produits différenciés fixent un prix et choisissent la quantité à produire afin de maximiser leurs profits. Cela dépend de la demande qui lui est adressée, c’est-à-dire de la disposition des clients potentiels à payer pour le produit, et des coûts de production.

Prenez l’exemple d’une entreprise qui fabrique des voitures. Toutes les voitures ne sont pas identiques. Ce sont des produits différenciés. Chaque marque et chaque modèle sont produits par une seule et même entreprise et ont des caractéristiques uniques en termes de conception, de performances, qui les différencient des voitures fabriquées par d’autres entreprises. Par rapport à Ford, qui produit environ 6,6 millions de véhicules par an, notre entreprise fictive produit des voitures pour un marché de niche et est plutôt petite : nous l’appellerons donc Beautiful Cars.

#### Les coûts de production

Pensez aux coûts de fabrication et de vente des voitures. L’entreprise a besoin de locaux (une usine) équipés de machines pour le moulage, le forgeage, le montage et le soudage des carrosseries. Elle peut les louer à une autre entreprise ou rassembler le capital financier nécessaire pour investir dans ses propres locaux et équipements. Ensuite, elle doit acheter les matières premières et les composants et rémunérer les ouvriers qui font fonctionner les équipements. Elle a aussi besoin de salariés pour gérer les processus d’achat, de production et pour vendre les voitures.

L’une des composantes du coût de production des voitures est le montant à verser aux propriétaires de l’entreprise (les actionnaires) afin de couvrir le coût d’opportunité du capitalcoût d’opportunité du capital La quantité de revenu qu’un investisseur aurait pu obtenir en investissant l’unité de capital ailleurs.

, c’est-à-dire pour les inciter à continuer à investir dans les actifs dont l’entreprise a besoin pour produire des voitures. En général, les actionnaires n’investiront pas s’ils peuvent faire un meilleur usage de leur argent en investissant et en réalisant des profits ailleurs. Ce qu’ils pourraient recevoir ailleurs, pour chaque euro investi, est un autre exemple de coût d’opportunité (abordé dans la Section 2.5), appelé ici coût d’opportunité du capital.

Certains coûts ne varient pas avec le nombre de voitures. On les appelle coûts fixes. Suivez les étapes de l’analyse du Graphique 2.3 pour voir que plus il y a de voitures produites, plus les coûts totaux (les coûts fixes et les coûts variables, comme le nombre d’heures travaillées à rémunérer aux employés) sont élevés.

Graphique 2.3 Coût total de production. 
[insert image here](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/images/web/graphique-02-03-b.jpg)

**Les coûts totaux sont croissants**. Les coûts totaux augmentent avec la quantité d’unités produites, en partie parce que l’entreprise a besoin d’employer plus d’ouvriers. 

Suivez les étapes de l’analyse du Graphique 2.4 pour voir que, à partir du coût total de production, nous avons déterminé le coût moyen d’une voiture, et la manière dont il varie avec la production. La partie supérieure du Graphique 2.4 montre la manière dont les coûts totaux dépendent de la quantité de voitures produites chaque jour. La variation du coût moyen est présentée sur la partie inférieure.

Graphique 2.4 Beautiful Cars : variation du coût total et moyen. 
[insert image here](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/images/web/graphique-02-04-d.jpg)

**Variation du coût moyen**. Nous pouvons calculer le coût moyen pour chaque nombre de voitures pour représenter la variation du coût moyen d’une voiture produite du graphique du bas.

Nous pouvons voir sur le Graphique 2.4 que les coûts moyens de Beautiful Cars sont décroissants pour de faibles niveaux de production (jusqu’à 40 voitures produites). Pour des niveaux de production élevés (plus de 40 voitures produites), les coûts moyens augmentent. Cela peut être dû au fait que l’entreprise doit augmenter le nombre de roulements quotidiens des équipes sur la chaîne de montage. Elle doit peut-être aussi payer des heures supplémentaires et l’équipement peut tomber en panne plus fréquemment lorsque la chaîne de montage fonctionne plus longtemps.

Le coût marginal est le coût de production additionnel dû à une unité supplémentaire. Suivez les étapes de l’analyse du Graphique 2.5 pour voir que la production des Beautiful Cars présente des coûts marginaux croissants.

Graphique 2.5 Coût marginal d’une voiture. 
[insert image here](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/images/web/graphique-02-05-c.jpg)

**Variation du coût marginal**. Si nous calculons le coût marginal à chaque niveau de production de Beautiful Cars, nous pouvons représenter la variation du coût marginal. 

#### La courbe de demande

Afin de fixer un prix, une entreprise a besoin d’informations concernant la demande, notamment le nombre de clients potentiels prêts à payer pour son produit. Pour un modèle simple de la demande de Beautiful Cars, imaginez qu’il y ait 100 clients potentiels qui achèteraient chacun une Beautiful Cars aujourd’hui, si le prix était assez bas. Supposez que nous ordonnons sur une droite l’ensemble des clients potentiels par ordre décroissant de leur disposition à payer, et que nous représentons graphiquement comment la disposition à payer varie le long de cette droite (Graphique 2.6). Alors, pour tout niveau de prix, disons égal à 3 200 euros, le graphique montre le nombre de clients dont la disposition à payer serait supérieure ou égale au prix. Dans ce cas, 60 clients sont prêts à payer 3 200 euros ou plus, donc la demande de voitures pour un prix de 3 200 euros est 60.

Graphique 2.6 Demande de voitures (par jour). 
[insert image here](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/images/web/graphique-02-06.jpg)

Les courbes de demande sont souvent dessinées sous la forme de lignes droites, comme dans cet exemple, bien qu’il n’y ait aucune raison de penser qu’elles soient droites en réalité : nous avons bien vu que la courbe de demande pour les Cheerios pomme-cannelle n’était pas droite. En revanche, nous pouvons nous attendre à ce que les courbes de demande soient descendantes : lorsque le prix augmente, la quantité demandée par les consommateurs diminue. Réciproquement, quand la quantité disponible est faible, le produit peut être vendu à un prix élevé.

On peut s’attendre à ce qu’une entreprise qui vend un produit différencié connaisse une courbe de demande descendante. Nous en avons vu un exemple empirique avec le cas des Cheerios pomme-cannelle (un autre produit différencié). Si le prix d’une voiture Beautiful Cars est élevé, la demande sera faible, car seuls les consommateurs qui préfèrent Beautiful Cars aux autres marques l’achèteront. Si le prix baisse, davantage de consommateurs seront attirés par Beautiful Cars, ceux-là mêmes qui auraient sûrement acheté une Ford ou une Volvo sinon.

### 1.1.6. Quelle quantité produire ? L’égalisation entre coût marginal et recette marginale 

[source 2.10](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/text/02.html#210-quelle-quantit%C3%A9-produire-l%C3%A9galisation-entre-co%C3%BBt-marginal-et-recette-marginale)

Quel est le meilleur choix de prix et de quantité pour le producteur ? Rappelez-vous que les recettes sont données par le prix de vente multiplié par la quantité vendue (Graphique 2.6). La recette marginale correspond à l’augmentation des recettes induite par l’augmentation de la quantité produite. Le Tableau 2.7 montre comment calculer la recette marginale avec 20 voitures produites, c’est-à-dire l’augmentation des recettes lorsque la quantité passe de 20 à 21 voitures produites.

Tableau 2.7 Calcul de la recette marginale. 
| Quantité                     | Prix                     | Recette                                                                           |
|------------------------------|--------------------------|-----------------------------------------------------------------------------------|
| Quantité = 20                | Prix = 6 400 €           | Recette = 128 000 €                                                               |
| Quantité = 21                | Prix = 6 320 €           | Recette = 132 720 €                                                               |
| Variation de la quantité = 1 | Variation du prix = 80 € | Recette margionale = variation de la recette / variation de la quantité = 4 720 € |


*Gain de recettes* (21e voiture) : 	6 320 € <br>
*Perte de recettes* (80 € sur chacune des 20 autres voitures) :	-1 600 € <br>
*Recette marginale* 	4 720 €

Quand la production passe de 20 à 21 voitures, les recettes varient pour deux raisons. Une voiture supplémentaire est vendue, mais, étant donné que le nouveau prix de vente est plus faible avec 21 voitures produites, il y a aussi une perte de 80 euros sur chacune des 20 autres voitures vendues au nouveau prix. Les recettes marginales correspondent à l’effet net de ces deux changements.

Sur le Tableau 2.8, nous trouvons la variation des recettes marginales, et l’utilisons pour trouver le point correspondant à un profit maximal. Les deux premières colonnes décrivent la courbe de demande et la troisième colonne décrit la variation du coût marginal. Vous pouvez voir la manière dont le profit varie en fonction de la production dans la dernière colonne du Tableau 2.8.

Tableau 2.8 Recette marginale, coût marginal et profit. 
| Prix  | Quantité | Coût marginal | Recette marginale | Profit  |
|-------|----------|---------------|-------------------|---------|
| 8 000 | 0        | 1 000         | 8 000             | -48 000 |
| 7 200 | 10       | 1 600         | 6 413             | 11 063  |
| 6 400 | 20       | 2 200         | 4 825             | 48 250  |
| 5 600 | 30       | 2 800         | 3 238             | 63 563  |
| 4 800 | 40       | 3 400         | 1 650             | 57 000  |
| 4 000 | 50       | 4 000         | 63                | 28 563  |
| 3 200 | 60       | 4 600         | -1 525            | -21 750 |

Les trois dernières colonnes dans le Tableau 2.8 suggèrent que le point maximisant les profits se trouve là où la recette marginale égalise le coût marginal. Pour comprendre pourquoi, souvenez-vous que le profit est la différence entre les recettes et les coûts, de sorte que, pour n’importe quel niveau de production, la variation des profits lorsque la production augmente d’une unité (le profit marginal) est égale à la différence entre la variation des recettes et la variation des coûts :

profit=recette totale−coût total

profit marginal=recette marginale−coût marginal

Ainsi :

- Si la recette marginale est supérieure au coût marginal, l’entreprise pourrait augmenter son profit en augmentant la production.
- Si la recette marginale est inférieure au coût marginal, le profit marginal est négatif. Il serait préférable de diminuer la production.

Dans notre exemple :

 - Quand la production est inférieure à 32, la recette marginale est supérieure au coût marginal : le profit marginal est positif, donc le profit augmente avec la production.
 - Quand la production est supérieure à 32, la recette marginale est inférieure au coût marginal : le profit marginal est négatif, donc le profit diminue avec la production.
 - Quand la production est égale à 32, la recette marginale est égale au coût marginal : le niveau de profit atteint son maximum.

Le prix et la quantité maximisant les profits sont 5 440 euros et 32 voitures, correspondant à un profit de 63 360 euros. Rappelez-vous que le profit de l’entreprise est la différence entre ses recettes et ses coûts totaux :

profit=recette totale−coût total=prix×quantité−coût total

De manière équivalente, le profit est le nombre d’unités produites multiplié par le profit par unité, qui est la différence entre le prix et le coût moyen.

profit=quantité(prix−coût 
totalquantité)=quantité(prix−coût moyen)

Cette formule correspond au profit économique. Le profit économique est le profit supplémentaire par rapport au rendement minimal exigé par les actionnaires, qui est appelé le profit normal. Rappelez-vous que les coûts de production prennent en compte le coût d’opportunité du capital (les paiements faits aux actionnaires pour les inciter à prendre des parts). Si le prix est égal au coût moyen, le profit économique de l’entreprise est zéro.

Le succès d’une entreprise ne dépend pas seulement de la détermination du bon prix. Le choix du produit et sa capacité à attirer les clients, à produire à moindres coûts en proposant une qualité supérieure à celle de la concurrence importent tout autant. Elle doit aussi être en mesure de recruter et de garder les salariés qui rendent possibles toutes ces choses.

## 1.2. La demande

[source 1.3](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/text/01.html#13-comment-la-confrontation-de-loffre-et-la-demande-dun-bien-ou-dun-service-d%C3%A9termine-t-elle-l%C3%A9quilibre-sur-un-march%C3%A9-concurrentiel)

Objectif : Comprendre que la courbe de demande est une fonction décroissante du prix et représenter graphiquement une courbe de demande.

La demande (dans notre exemple, la demande quotidienne totale de pains de la part de l’ensemble des consommateurs de la ville) est une fonction décroissante du prix. Plus le prix est élevé, moins les consommateurs (demandeurs) sont disposés à acheter du pain. La demande de marché, qui correspond à la somme des demandes individuelles, baisse donc. Un prix plus élevé réduit le pouvoir d’achat des consommateurs et diminue la quantité demandée. En effet, un prix plus élevé annule ou reporte la demande de certains consommateurs et réduit la quantité consommée des autres.

De même, ce phénomène est plus marqué pour certains biens ou services qui ont des proches substituts : lorsque le prix du bien ou du service augmente alors que celui d’un proche substitut reste inchangé, les consommateurs peuvent consommer une quantité moins importante du bien ou du service dont le prix relatif s’est accru (par exemple, les fraises et les framboises, la viande blanche et la viande rouge, une place de cinéma et une vidéo à la demande via une box).

Les déterminants de la demande sont les suivants : le prix, le nombre d’acheteurs, les anticipations de ces acheteurs, la variation du prix d’un bien ou d’un service substituable ou complémentaire, le revenu, les goûts des consommateurs.

Tableau 1.3 Prix et quantité de pains demandée. 
| Prix en euro | Quantité de pains demandée |
|--------------|----------------------------|
| 0,85         | 9 000                      |
| 1,7          | 6 000                      |
| 2            | 5 000                      |
| 2.5          | 4 000                      |
| 3,25         | 2 000                      |
| 4,25         | 0                          |


Graphique 1.2 La courbe de demande sur le marché du pain. 
[insert image here](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-02.jpg)


On peut interpréter la pente de la courbe de demande.

Objectif : Interpréter la pente de la courbe de demande.

La pente d’une courbe de demande nous renseigne sur sa sensibilité par rapport aux variations des prix. Quand une variation des prix entraîne une variation plus que proportionnelle des quantités demandées, on dit que la demande est très sensible à la variation des prix (pente de la courbe peu importante). Quand une variation des prix entraîne une variation moins que proportionnelle des quantités demandées, on dit que la demande est peu sensible à la variation des prix (pente de la courbe très importante). Enfin, si une variation des prix n’entraîne pas de modification des quantités demandées, on dit que la demande est insensible à la variation des prix.

**TBC include insteresting exercices here (from CORE)**

### 1.2.1. Interprétation des déplacements *des* courbes ?

[source 1.4](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/text/01.html#14-comment-interpr%C3%A9ter-les-variations-des-quantit%C3%A9s-offertes-et-des-quantit%C3%A9s-demand%C3%A9es)

L’offre et la demande augmentent ou baissent, indépendamment de la variation du prix de marché, sous l’effet de chocs positifs ou négatifs (modifications des conditions d’offre et de demande). Ces variations se traduisent graphiquement par un déplacement des courbes d’offre et de demande vers la droite (en cas de choc positif) ou vers la gauche (en cas de choc négatif). Ce déplacement des courbes se traduit lui-même par un déséquilibre (une surproduction ou une pénurie) à l’ancien prix d’équilibre.

**Objectif** : Montrer qu’une variation de l’offre (de la demande) indépendamment de la variation du prix provoque un déplacement de la courbe d’offre (de demande).

Appuyons-nous de nouveau sur l’exemple du marché du pain, envisagé à la Section 1.3. Supposons que le coût de la farine qui entre dans la composition du pain baisse en raison d’une situation climatique favorable à la production de blé : cela incite les boulangers à produire davantage, à tous niveaux de prix du pain, parce que leurs perspectives de profit augmentent. On parle alors de choc d’offre (modification des conditions de l’offre, ici du coût de production) positif (puisque l’offre augmente alors que le niveau général des prix reste constant). Graphiquement, cette hausse de l’offre se traduit par le déplacement de la courbe d’offre vers la droite, parallèlement à la courbe d’offre initiale.

Graphique 1.4a Effet d’un choc d’offre positif sur le marché du pain. 
[insert image here](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-04-a.jpg)

- Exercice 1 : Effet d’un choc d’offre positif sur le marché du pain
  - À la suite du choc d’offre positif, quelles sont la quantité demandée et la quantité offerte de pains à l’ancien prix d’équilibre ?
  - Comment qualifie-t-on ce déséquilibre ?
- Exercice 2 : Effet d’un choc de demande positif sur le marché du pain
  - Appuyez-vous sur les acquis de la Section 1.3 sur les déterminants de la demande pour proposer une explication d’un choc de demande positif sur le marché du pain.
  - Représentez graphiquement le déplacement de la courbe pour tous niveaux de prix.
  - Quel déséquilibre apparaît sur le marché du pain en cas de choc de demande positif ?
- Exercice 3 : Effet d’un choc négatif sur le marché du pain
  - Comment les courbes d’offre et de demande se déplacent-elles en cas de chocs négatifs ?
  - Quel déséquilibre apparaît en cas de choc d’offre négatif ? En cas de choc de demande négatif ?

### 1.2.1. Interprétation des déplacements *sur les* courbes ?

[source 1.4](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/text/01.html#14-comment-interpr%C3%A9ter-les-variations-des-quantit%C3%A9s-offertes-et-des-quantit%C3%A9s-demand%C3%A9es)

Lorsque le marché est à l’équilibre, un changement du prix et de la quantité d’équilibre ne peut être dû qu’à un choc : la variation (baisse dans le cas d’un choc négatif, hausse dans le cas d’un choc positif) de l’offre ou de la demande provoque le déplacement des courbes à tous niveaux de prix. Cette variation de l’offre ou de la demande induit l’apparition d’un déséquilibre (surproduction ou pénurie). Celui-ci se traduit par une variation du prix (à la baisse en cas de surproduction, à la hausse en cas de pénurie), qui donne elle-même lieu à un ajustement des quantités offertes et demandées (déplacement sur les courbes), permettant le retour à l’équilibre (nouveau prix et nouvelle quantité d’équilibre). Grâce à la flexibilité des prix, des quantités offertes et demandées, le marché est ainsi autorégulateur : il revient spontanément à l’équilibre.

Objectif : Montrer qu’un déséquilibre se résorbe par la variation du prix et un déplacement sur les courbes d’offre et de demande.

Puisque les chocs se traduisent par l’apparition de déséquilibres, la question qui se pose dès lors concerne le retour à l’équilibre.

Dans le cas du choc d’offre positif sur le marché du pain étudié précédemment, une surproduction apparaît, car l’offre (6 800 pains) est supérieure à la demande (5 000 pains) à l’ancien prix d’équilibre de 2 euros. Vous avez compris dans la Section 1.3 que, dans ce cas, le prix baisse sur le marché du fait de la concurrence entre les offreurs souhaitant tous écouler leur production. En raison de cette baisse du prix du pain, l’offre de pain baisse et la demande de pain augmente. Le Graphique 1.4b montre que cette situation se traduit par un déplacement sur les courbes d’offre et de demande.

Graphique 1.4b Déplacements sur les courbes d’offre et de demande à la suite d’un choc d’offre positif
[insert image here](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-04-b.jpg)

- Exercice : L’effet d’une baisse des prix sur l’offre et la demande
  - Appuyez-vous sur les acquis de cette section pour expliquer pourquoi, lorsque le prix baisse, l’offre de pain baisse et la demande de pain augmente.
  - Décrivez les variations de l’offre et de la demande à la suite de la baisse du prix du pain.
  - Quels sont le nouveau prix et la nouvelle quantité d’équilibre ?

Graphique 1.5 Déplacements sur les courbes d’offre et de demande à la suite d’un choc de demande positif. 
[insert image here](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-05.jpg)

### 1.2.3. (Option) Etude de cas  : Taxe forfaitaire

[source](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/text/01.html#14-comment-interpr%C3%A9ter-les-variations-des-quantit%C3%A9s-offertes-et-des-quantit%C3%A9s-demand%C3%A9es)



## 1.3. Elasticités de la demande par rapport au prix, au revenu

[source 12.9, 12.10](https://www.sciencespo.fr/department-economics/econofides/premiere-stmg/text/12.html#129-l%C3%A9lasticit%C3%A9-prix-demande)

L’élasticité prix de la demande est une mesure de la sensibilité des consommateurs à un changement de prix. Elle est définie comme la variation (en pourcentage) de la demande en réponse à une augmentation du prix de 1 %. Par exemple, supposez que lorsque le prix d’un produit augmente de 10 %, nous observions une baisse de 5 % de la quantité vendue. Nous pouvons alors calculer l’élasticité, ε, de la manière suivante :

ε = − variation de la demande (en %) ÷ variation du prix (en %)

ε est la lettre grecque epsilon, souvent utilisée pour représenter l’élasticité. Lorsque le changement du prix est positif le changement de demande est négatif et vice versa. Le signe moins (-) dans la formule de l’élasticité garantit donc que la mesure de la sensibilité au prix soit un nombre positif. Dans cet exemple, nous obtenons donc :

ε = −5 ÷ 10 = 0,5

On dit que la demande est élastique lorsque l’élasticité est supérieure à 1 et inélastique lorsqu’elle est inférieure à 1.

**TBC Elasticité demande-revenu**

L’élasticité-prix croisée de la demande fait référence à la variation en pourcentage de la quantité demandée d’un produit donné en raison de la variation en pourcentage du prix d’un autre produit « proche ». Si tous les prix peuvent varier, la quantité demandée du produit A dépend non seulement de son propre prix (voir élasticité de la demande) mais aussi des prix des autres produits.

Ce qui est fascinant concernant les prix déterminés par les marchés est que ce ne sont pas les individus qui envoient les messages, mais plutôt les interactions anonymes impliquant parfois des millions de personnes. Lorsque les conditions changent – un nouveau mode de production de pain moins onéreux, par exemple –, personne n’a besoin de modifier le message : « Proposez du pain au lieu des pommes de terre à table ce soir. » Un changement de prix résulte d’un changement des coûts des entreprises. Un prix du pain en baisse parle de lui-même.

### 1.3.1. Élasticité de la demande (avancé: présentation mathématique)

[source](https://www.core-econ.org/the-economy/book/fr/text/leibniz-07-08-01.html)

L’élasticité-prix de la demande mesure la sensibilité de la quantité demandée au prix : elle nous donne la variation en pourcentage de la quantité demandée lorsque le prix varie de 1 %. Dans ce Leibniz nous définissons l’élasticité de manière algébrique et nous montrons comment la décision de fixation de prix d’une entreprise dépend de l’élasticité de la demande à laquelle elle fait face.

Il y a deux manières d’écrire une fonction de demande. Précédemment nous avons décrit la demande de Beautiful Cars en utilisant la fonction de demande *inverse* :

𝑃=𝑓(𝑄)

où 𝑓(𝑄) est le prix auquel l’entreprise peut vendre exactement 𝑄 voitures. Pour définir l’élasticité, il est plus commode d’écrire la fonction de demande sous sa forme directe :

𝑄=𝑔(𝑃)

𝑔(𝑃) est la quantité de Beautiful Cars demandée quand le prix est 𝑃. (La fonction 𝑔 est la fonction inverse de 𝑓 ; mathématiquement, on peut écrire 𝑔(𝑃)=𝑓−1(𝑃) .)

La dérivée de la fonction de demande est 𝑑𝑄/𝑑𝑃=𝑔′(𝑃)
. C’est une manière de mesurer à quel point la demande du consommateur (𝑄) change en réponse à un changement du prix. Cependant, ce n’est pas une mesure très utile parce qu’elle dépend des unités dans lesquelles sont exprimés 𝑃 et 𝑄. 

Par exemple, on obtiendrait une réponse différente selon que le prix soit en euros ou en dollars.

À la place, nous définissons l’élasticité-prix de la demande dans le texte comme :

**TBC: change fbox to mbox or equivalent ...**

$-\frac{ \fbox{variation en \% de la quantité demandée} }{ \fbox{variation en \% du prix} }$

C’est une mesure plus utile de la sensibilité de la demande au prix. Vous pouvez déduire de la définition qu’elle est indépendante des unités de mesure. Cependant elle est étroitement liée à la dérivée 𝑔′(𝑃) — pour le voir, supposez que le prix change de 𝑃 à 𝑃+Δ𝑃, entraînant une variation de la quantité demandée de 𝑄=𝑔(𝑃) à 𝑄+Δ𝑄. Le changement en pourcentage du prix est 100Δ𝑃/𝑃, et le changement en pourcentage de la quantité est 100Δ𝑄/𝑄. En substituant cela dans l’expression de l’élasticité, l’on obtient :

$−\frac{\Delta 𝑄}{𝑄}/\frac{\Delta 𝑃}{𝑃} = − \frac{𝑃}{𝑄} \frac{\Delta Q}{\Delta P}$

En prenant la limite de cette expression quand $\Delta P \longrightarrow 0$, on obtient la définition algébrique de l’élasticité-prix de la demande, que nous dénotons par 𝜀 comme dans le texte :

$\varepsilon = - \frac{P}{Q} \frac{dQ}{dP}$

Et puisque 𝑄=𝑔(𝑃), l’élasticité peut aussi être écrite comme :

$\varepsilon = \frac{P g'(P)}{g(P)}$

Remarquez que la valeur de l’élasticité est normalement positive, puisque selon la Loi de la Demande, la dérivée de la fonction de demande sera négative.

Lorsqu’elle est définie de manière algébrique, 𝜀
est seulement approximativement identique à notre définition originale de l’élasticité selon laquelle il s’agit de la variation en pourcentage de la quantité lorsque le prix augmente de 1 %. Cependant, si l’on suppose que l’augmentation de 1 % correspond à une faible augmentation (ce qui est une hypothèse réaliste), c’est une bonne approximation, et nous l’interprétons souvent de cette manière.

Prenez la fonction de demande suivante :

$𝑄=100𝑃^{−0,8}$

Ici,
$\varepsilon = - \frac{P}{Q} \frac{dQ}{dP}=-\frac{P}{100𝑃^{−0,8}} \times -80P^{-1,8} = 0,8 $

Dans ce cas particulier, l’élasticité de la demande est constante – elle est égale à 0,8

en tous points de la courbe de demande.

En général les élasticités ne sont pas constantes. Elles varient lorsque l’on se déplace le long de la courbe de demande. L’exemple ci-dessus illustre un cas spécial. Si la forme de la fonction de demande est $Q=aP^{-c}$, où 𝑎 et 𝑐 sont des constantes positives, l’élasticité de la demande est 𝑐. C’est la seule classe de fonctions de demande pour laquelle l’élasticité est constante.

## 1.4. L'offre

L’offre détermine les quantités que les vendeurs sont prêts à offrir sur le marché pour chaque niveau de prix donné. L’offre de marché s’obtient en additionnant les offres individuelles. L’offre est une fonction croissante des prix, car plus le prix augmente et plus les perspectives de profit sont importantes pour les vendeurs. La pente d’une courbe d’offre nous renseigne sur la sensibilité de l’offre à la suite d’une variation des prix.

[source 1.3](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/text/01.html#13-comment-la-confrontation-de-loffre-et-la-demande-dun-bien-ou-dun-service-d%C3%A9termine-t-elle-l%C3%A9quilibre-sur-un-march%C3%A9-concurrentiel)

Objectif : Comprendre pourquoi l’offre est une fonction croissante du prix et représenter graphiquement une courbe d’offre.

Pour un modèle simple d’un marché comptant de nombreux acheteurs et vendeurs (on parle ici d’atomicité), prenons l’exemple d’une ville où un grand nombre de petites boulangeries fabriquent du pain et le vendent directement aux consommateurs. La courbe d’offre de marché indique la quantité totale produite par l’ensemble des boulangeries à chaque niveau de prix donné.

L’offre (dans notre exemple, l’offre quotidienne totale de pains de la part de l’ensemble des boulangers de la ville) est une fonction croissante du prix : plus le prix est élevé, plus les boulangers (offreurs) sont disposés à produire et à vendre, car cela constitue une perspective de gain plus élevé, augmentant l’offre de marché. L’offre de marché est donc la somme des offres de tous les producteurs. De plus, ces prix plus élevés incitent de nouveaux individus à devenir boulangers, augmentant le nombre d’offreurs et l’offre totale.

Voici les déterminants qui influencent les quantités offertes : les coûts de production (par exemple, l’évolution du prix des matières premières ou bien le changement de prix des facteurs de production), le nombre d’offreurs, les évolutions technologiques, un changement des anticipations des producteurs.

- Exercice : L’offre et le prix
  - Qu’est-ce que l’offre ? 
  - Qu’est-ce que l’offre de marché ?
  - Pourquoi l’offre est-elle une fonction croissante du prix ? 

Le Tableau 1.2 et le Graphique 1.1 montrent les quantités offertes en fonction du prix du pain qui vont permettre de construire la courbe d’offre.

Tableau 1.2 Prix et quantité de pains offerte. 
| Prix en euro | Quantité de pains offerte |
|--------------|---------------------------|
| 1,1          | 1 000                     |
| 1,5          | 3 000                     |
| 2            | 5 000                     |
| 2.35         | 6 000                     |
| 3,77         | 9 000                     |


Graphique 1.1 La courbe d’offre sur le marché du pain. 
[insert image here](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-01.jpg)


On peut interpréter la pente de la courbe d’offre.

Objectif : Interpréter la pente de la courbe d’offre.

La pente d’une courbe d’offre nous renseigne sur la sensibilité de l’offre par rapport aux variations des prix.

Quand une variation des prix entraîne une variation plus que proportionnelle des quantités offertes, on dit que l’offre est très sensible à la variation des prix (pente de la courbe peu importante). Quand une variation des prix entraîne une variation moins que proportionnelle des quantités offertes, on dit que l’offre est peu sensible à la variation des prix (pente de la courbe très importante). Enfin, si une variation des prix n’entraîne pas de modification des quantités offertes, on dit que l’offre est insensible à la variation des prix.

**TBC: Bonne application dans CORE**




## 1.5. Elasticités de l'offre


## 1.6. Détermination des prix

L’interaction entre l’offre et la demande détermine un équilibre de marché tel que les acheteurs et les vendeurs sont preneurs de prix et réalisent l’échange à la quantité qui correspond au prix du marché ; on parle d’équilibre concurrentiel. Si la demande est supérieure à l’offre (situation de pénurie) le prix va augmenter et si l’offre est supérieure à la demande (situation de surproduction) le prix va baisser pour que l’équilibre soit de nouveau atteint.

[source ](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/text/01.html#13-comment-la-confrontation-de-loffre-et-la-demande-dun-bien-ou-dun-service-d%C3%A9termine-t-elle-l%C3%A9quilibre-sur-un-march%C3%A9-concurrentiel)

Objectif : Comprendre comment se forme l’équilibre sur un marché concurrentiel.

Nous connaissons maintenant la courbe d’offre et la courbe de demande (voir Graphique 1.1 et Graphique 1.2) sur le marché du pain. Le Graphique 1.3 montre que le prix d’équilibre est exactement de 2 euros. À ce prix, le marché est à l’équilibre : les consommateurs demandent 5 000 pains par jour et les entreprises en fournissent 5 000 par jour.

Si de nombreuses entreprises (offreurs) fabriquent des produits identiques et si les clients (demandeurs) peuvent choisir n’importe quelle entreprise pour effectuer leurs achats, alors les entreprises sont preneuses de prix à l’équilibre. Elles n’auraient aucun avantage à vendre à un prix différent de celui en vigueur.

Graphique 1.3 Équilibre sur le marché du pain. 
[insert image here](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-03.jpg)

## 1.7. Le mécanisme des prix


## 1.8. Le surplus du consommateur et du producteur

[source 1.5](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/text/01.html#15-comment-le-producteur-maximise-t-il-son-profit)

Objectif : Découvrir la notion de coût de production et comprendre quelle quantité le producteur décide de produire pour un niveau de prix donné.

### 1.8.1. La notion de coût de production

Prenons l’exemple du marché des avions privés et imaginons une entreprise fictive qui produit des avions similaires à ceux des grandes marques, comme Cessna, Bombardier, Gulfstream ou encore Dassault. Nous appellerons cette entreprise Supers Jets. Si cette entreprise produit des avions identiques à ses concurrents, rappelez-vous qu’elle est preneuse de prix : elle ne peut pas vendre un avion, de la même gamme, à un prix supérieur à celui de ses concurrents.

Pensez aux coûts de production et de vente des avions. Tout d’abord, l’entreprise a besoin de mettre en œuvre un processus de recherche et développement pour concevoir son appareil. Elle a également besoin de locaux (une usine) équipés de machines pour la confection des appareils. Elle peut les louer à une autre entreprise ou rassembler le capital financier nécessaire pour investir dans ses propres locaux et équipements. Ensuite, elle doit acheter les matières premières et les composants, et rémunérer les salariés (des ouvriers notamment) qui font fonctionner les équipements. Elle a aussi besoin d’autres salariés (des cadres, des professions intermédiaires ou des employés) pour gérer le processus d’achat et de production ainsi que pour vendre les avions.

Pour produire, l’entreprise fait donc face d’une part à des coûts fixes qui ne dépendent pas de son activité ou encore des quantités produites, d’autre part à des coûts variables qui dépendent des quantités produites. Les coûts fixes sont donc à acquitter même si aucune quantité n’a été produite et aucun chiffre d’affaires n’a été réalisé.

Le coût total noté CT se compose de l’ensemble des coûts supportés par l’entreprise.

Le coût moyen (CM) de production de Supers Jets correspond au coût total pour une quantité d’avions produits CT divisé par cette quantité (Q).

$CM(Q) = \frac{CT(Q)}{Q}$

Le coût marginal (Cm) correspond au coût de production d’une unité supplémentaire (ici l’unité est l’avion) ; la quantité produite passe alors de Q à Q+1.

$Cm(Q+1)=CT(Q + 1)−CT(Q)$

### 1.8.2. Quelle quantité le producteur décide-t-il de produire en situation de « preneur de prix » ?

[source 1.5](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/text/01.html#15-comment-le-producteur-maximise-t-il-son-profit)

Le profit total augmente tant que le coût marginal est inférieur au prix de marché, car le producteur réalise un profit marginal (le solde « prix de marché – coût marginal » est positif). À la quantité pour laquelle le coût marginal égalise le prix de marché, le profit total est à son maximum.

Une entreprise preneuse de prix produit donc une quantité telle que Cm = P* (ou P* est le prix de marché). En effet, si l’entreprise augmentait sa production jusqu’à un niveau où Cm > P*, le coût de fabrication de la dernière unité produite serait supérieur à P*, de telle sorte que l’entreprise réaliserait une perte sur cette unité. Si l’entreprise produisait à un niveau où Cm < P*, elle pourrait produire davantage, tant qu’elle réalise un profit marginal positif. À la quantité pour laquelle Cm = P*, le profit total est maximal.


Supposez maintenant que vous soyez le chef d’entreprise de Supers Jets et que vous deviez déterminer la quantité d’avions à produire. Comme tout chef d’entreprise, vous avez comme objectif non seulement de faire des profits, mais également de maximiser votre profit. Le profit d’une entreprise est la différence entre ses recettes et ses coûts totaux.

$profit=recettes−coûts =𝑃𝑄−𝐶𝑇(𝑄)$

De manière équivalente, le profit est le nombre d’unités produites multiplié par le profit unitaire, qui est la différence entre le prix et le coût moyen.

$profit=Q \left( P - \frac{CT(Q)}{Q} \right)  $

On suppose que compte tenu de l’offre et de la demande globale d’avions privés le prix d’équilibre du marché est de 340 000 euros. On rappelle que vos principaux concurrents produisent des avions strictement identiques aux vôtres. Si vous choisissiez un prix plus élevé, les clients potentiels se tourneraient vers d’autres concurrents. Vous êtes donc preneur de prix.

- Exercice : Pour un prix de marché de 340 000 euros, quelle quantité allez-vous accepter de produire ? Déterminez la quantité à produire pour laquelle votre profit est maximal.

| Prix du marché (€) | Quantité produite | Coût moyen (€) | Coût marginal (€) | Chiffres d'affaires (€) | Coût total (€) | Profit (€) |
|--------------------|-------------------|----------------|-------------------|-------------------------|----------------|------------|
| 340 000            | 1                 | 750 000        | 300 000           | 340 000                 | 750 000        | −410 000   |
| 340 000            | 2                 | 475 000        | 200 000           | 680 000                 | 950 000        | −270 000   |
| 340 000            | 3                 | 367 000        | 151 000           | 1 020 000               | 1 101 000      | -81 000    |
| 340 000            | 4                 | 305 500        | 121 000           | 1 360 000               | 1 222 000      | 138 000    |
| 340 000            | 5                 | 273 000        | 143 000           | 1 700 000               | 1 365 000      | 335 000    |
| 340 000            | 6                 | 256 500        | 174 000           | 2 040 000               | 1 539 000      | 501 000    |
| 340 000            | 7                 | 250 000        | 211 000           | 2 380 000               | 1 750 000      | 630 000    |
| 340 000            | 8                 | 250 000        | 250 000           | 2 720 000               | 2 000 000      | 720 000    |
| 340 000            | 9                 | 255 000        | 295 000           | 3 060000                | 2 295 000      | 765 000    |
| 340 000            | 10                | 263 500        | 340 000           | 3 400 000               | 2 635 000      | 765 000    |
| 340 000            | 11                | 275 000        | 390 000           | 3 740 000               | 3 025 000      | 715 000    |
| 340 000            | 12                | 288 750        | 440 000           | 4 080 000               | 3 465 000      | 615 000    |
| 340 000            | 13                | 305 000        | 500 000           | 4 420 000               | 3 965 000      | 455 000    |
| 340 000            | 14                | 324 000        | 571 000           | 4 760 000               | 4 536 000      | 224 000    |
| 340 000            | 15                | 346 000        | 654 000           | 5 100 000               | 5 190 000      | −90 000    |
| 340 000            | 16                | 370 500        | 738 000           | 5 440 000               | 5 928 000      | −488 000   |
| 340 000            | 17                | 398 000        | 838 000           | 5 780 000               | 6 766 000      | −986 000   |
| 340 000            | 18                | 428 000        | 938 000           | 6 120 000               | 7 704 000      | −1 584 000 |
| 340 000            | 19                | 461 000        | 1 055 000         | 6 460 000               | 8 759 000      | −2 299 000 |
| 340 000            | 20                | 497 000        | 1 181 000         | 6 800 000               | 9 940 000      | −3 140 000 |


- Exercice : On suppose désormais que compte tenu de l’offre et de la demande globale le prix d’équilibre du marché soit de 500 000 euros.

| Prix du marché   (€) | Quantité produite | Coût moyen   (€) | Coût marginal   (€) | Chiffres d’affaires   (€) | Coût total   (€) | Profit   (€) |
|----------------------|-------------------|------------------|---------------------|---------------------------|------------------|--------------|
| 500 000              | 1                 | 750000           | 300000              | 500 000                   | 750 000          | −250 000     |
| 500 000              | 2                 | 475000           | 200000              | 1 000 000                 | 950 000          | 50 000       |
| 500 000              | 3                 | 367000           | 151000              | 1 500 000                 | 1 101 000        | 499 000      |
| 500 000              | 4                 | 305 500          | 121 000             | 2 000000                  | 1 222 000        | 778 000      |
| 500 000              | 5                 | 273 000          | 143 000             | 2 500 000                 | 1 365 000        | 1 135 000    |
| 500 000              | 6                 | 256 500          | 174 000             | 3 000 000                 | 1 539 000        | 1 461 000    |
| 500 000              | 7                 | 250 000          | 211 000             | 3 500 000                 | 1 750 000        | 1 750 000    |
| 500 000              | 8                 | 250 000          | 250 000             | 4 000 000                 | 2 000 000        | 2 000000     |
| 500 000              | 9                 | 255 000          | 295 000             | 4 500 000                 | 2 295 000        | 2 205 000    |
| 500 000              | 10                | 263 500          | 340 000             | 5 000 000                 | 2 635 000        | 2 365 000    |
| 500 000              | 11                | 275 000          | 390 000             | 5 500 000                 | 3 025 000        | 2 475 000    |
| 500 000              | 12                | 288 750          | 440 000             | 6 000 000                 | 3 465 000        | 2 535 000    |
| 500 000              | 13                | 305 000          | 500 000             | 6 500 000                 | 3 965 000        | 2 535 000    |
| 500 000              | 14                | 324 000          | 571 000             | 7 000 000                 | 4 536 000        | 2 464 000    |
| 500 000              | 15                | 346 000          | 654 000             | 7 500 000                 | 5 190 000        | 2 310 000    |
| 500 000              | 16                | 370 500          | 738 000             | 8 000 000                 | 5 928 000        | 2 072 000    |
| 500 000              | 17                | 398 000          | 838 000             | 8 500 000                 | 6 766 000        | 1 734 000    |
| 500 000              | 18                | 428 000          | 938 000             | 9 000 000                 | 7 704 000        | 1 296 000    |
| 500 000              | 19                | 461 000          | 1 055 000           | 9 500 000                 | 8 759 000        | 741 000      |
| 500 000              | 20                | 497 000          | 1 181 000           | 10 000 000                | 9 940 000        | 60 000       |




Tableau 1.4 La maximisation du profit pour un niveau de prix donné (P* = 340 000 euros) 
| Prix du marché   (€) | Quantité produite | Coût moyen   (CM, €) | Coût marginal   (Cm, €) | Chiffres d’affaires   (€) | Coût total   (CT, €) | Profit   (€) |
|----------------------|-------------------|----------------------|-------------------------|---------------------------|----------------------|--------------|
| 340 000              | 8                 | 250 000              | 250 000                 | 2 720 000                 | 2 000 000            | 720 000      |
| 340 000              | 9                 | 255 000              | 295 000                 | 3 060000                  | 2 295 000            | 765 000      |
| 340 000              | 10                | 263 500              | 340 000                 | 3 400 000                 | 2 635 000            | 765 000      |
| 340 000              | 11                | 275 000              | 390 000                 | 3 740 000                 | 3 025 000            | 715 000      |
| 340 000              | 12                | 288 750              | 440 000                 | 4 080 000                 | 3 465 000            | 615 000      |



Tableau 1.5 La maximisation du profit pour un niveau de prix donné (P* = 500 000 euros) 
| Prix du marché   (€) | Quantité produite | Coût moyen   (CM, €) | Coût marginal   (Cm, €) | Chiffres d’affaires   (€) | Coût total   (CT, €) | Profit   (€) |
|----------------------|-------------------|----------------------|-------------------------|---------------------------|----------------------|--------------|
| 500 000              | 10                | 263 500              | 340 000                 | 5 000 000                 | 2 635 000            | 2 365 000    |
| 500 000              | 11                | 275 000              | 390 000                 | 5 500 000                 | 3 025 000            | 2 475 000    |
| 500 000              | 12                | 288 750              | 440 000                 | 6 000 000                 | 3 465 000            | 2 535 000    |
| 500 000              | 13                | 305 000              | 500 000                 | 6 500 000                 | 3 965 000            | 2 535 000    |
| 500 000              | 14                | 324 000              | 571 000                 | 7 000 000                 | 4 536 000            | 2 464 000    |
| 500 000              | 15                | 346 000              | 654 000                 | 7 500 000                 | 5 190 000            | 2 310 000    |

### 1.8.3. Comment déduire la courbe d’offre du producteur de la maximisation du profit ?

Pour une entreprise en situation de preneuse de prix, la courbe d’offre débute au minimum du coût moyen et se confond avec la courbe de coût marginal. La courbe d’offre détermine donc à partir du coût moyen minimal la quantité à produire pour laquelle le profit est maximal.

Objectif : Comprendre que la courbe d’offre détermine, pour tous niveaux de prix, la quantité à produire par l’entrepreneur afin que son profit soit maximal.

Rappel : le producteur cherche à faire du profit et à le maximiser. Le profit est maximal lorsque la dernière unité produite rapporte autant que ce qu’elle coûte.

Lorsque les prix baissent (du fait de la concurrence que se livrent les entreprises présentes sur le marché), vous choisissez de produire des quantités différentes. Vous choisissez la quantité pour laquelle Cm = prix de marché ; en effet, nous avons vu que pour un prix de marché donné (P*) le profit est maximal pour P* = Cm.

On remarque néanmoins que si le prix baissait jusqu’à devenir inférieur à 250 000 euros, votre entreprise Supers Jets accuserait des pertes. En effet, le coût moyen minimal est de 250 000 euros, ce qui suppose qu’en deçà de ce prix votre entreprise ferait forcément des pertes. Vous refuseriez alors de produire au-dessous de ce prix. D’autres entreprises, plus compétitives, pourraient continuer à produire. Le Graphique 1.8 représente les courbes de coût marginal et de coût moyen de l’entreprise Supers Jets.

Graphique 1.8 La représentation graphique de la courbe d’offre de Supers Jets. 
[insert image here](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-08.jpg)

### 1.8.4. Que sont le surplus du producteur et le surplus du consommateur ?

Quand le marché du pain est à l’équilibre avec la quantité de pains offerte sur le marché égalisant la quantité demandée, le surplus total est représenté par l’aire sous la courbe de demande et au-dessus de la courbe d’offre.

La quantité d’équilibre de 5 000 pains en situation de concurrence est telle que le surplus total est maximisé. Si une quantité inférieure à 5 000 pains était produite, les gains totaux issus de l’échange seraient plus faibles.

À l’équilibre, tous les gains potentiels à l’échange sont exploités. Cette propriété – le fait que le surplus combiné des consommateurs et des producteurs soit maximisé au point où l’offre égale la demande – est vraie de manière générale dans ce modèle : si les acheteurs comme les vendeurs sont preneurs de prix, la quantité d’équilibre maximise la somme des gains issus de l’échange sur le marché.

[source 1.6](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/text/01.html#16-quels-sont-les-gains-%C3%A0-l%C3%A9change-sur-un-march%C3%A9-concurrentiel)

Les participants à ce marché sont preneurs de prix, les acheteurs et vendeurs sont bien sûr libres de choisir un autre prix, mais ils n’en tireraient aucun avantage.

Cependant, dans l’exemple du marché du pain, même si tous les agents acceptent le prix de 2 euros par pain, certains vont tirer plus de gains de cet échange. En effet, certains consommateurs (la demande) étaient prêts à payer plus de 2 euros et certains producteurs (l’offre) étaient prêts à vendre moins cher que 2 euros.

#### Le surplus du consommateur

Objectif : Comprendre ce que le consommateur gagne à l’échange.

Souvenez-vous que la courbe de demande indique la disposition à payer de chaque client potentiel. Un client disposé à payer plus que le prix de vente achètera le pain et gagnera à cet échange un surplus, car la valeur qu’il attribue au pain est supérieure au prix qu’il doit payer pour l’acquérir. Le Graphique 1.9a illustre le surplus du consommateur.

Graphique 1.9a Le surplus du consommateur. 
[insert image here](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-09-a-d.jpg)

**TBC: Graphique interactif dans CORE** 

Le surplus total des consommateurs est l’aire colorée qui représente le gain à l’échange des consommateurs au prix de marché donné, c’est-à-dire la différence entre ce que ces consommateurs étaient disposés à payer et le prix (ce qu’ils payent vraiment).

#### Le surplus du producteur

Objectif : Comprendre ce que le producteur gagne à l’échange

Souvenez-vous que la courbe d’offre correspond à la courbe de coût marginal, c’est-à-dire le coût de la dernière unité produite. Un producteur supportant un coût marginal inférieur au prix continue à produire pour vendre à un prix qui lui permet de gagner un profit, le surplus du producteur.

Comme le prix d’équilibre est unique et que le coût marginal est croissant, le surplus du producteur va être de plus en plus réduit. Il est même nul lorsque le coût marginal a rejoint le niveau du prix. Le Graphique 1.9b illustre le surplus du producteur.

Graphique 1.9b Le surplus du producteur 
[insert image here](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-09-b-c.jpg)

Le surplus total du producteur est l’aire colorée qui représente le gain à l’échange des producteurs au prix de marché donné, c’est-à-dire la différence entre le prix payé pour chaque pain et le coût marginal de chacun de ces pains.


#### Pourquoi le surplus est-il maximisé à l’équilibre ?

Objectif : Comprendre qu’à l’équilibre le gain à l’échange est maximal.

Les consommateurs (demandeurs) et les producteurs (offreurs) de pain qui décident volontairement d’échanger tirent tous des gains de cet échange. Les consommateurs qui sont prêts à payer un prix supérieur ou égal au prix du marché gagnent le surplus du consommateur. Les producteurs qui ont des coûts marginaux inférieurs ou égaux au prix du marché gagnent le surplus du producteur. Le surplus total mesure les gains à l’échange ou gains générés par le commerce pour tous les agents économiques qui y participent.

Le Graphique 1.9c montre comment calculer le surplus total (les gains tirés de l’échange) à l’équilibre concurrentiel du marché du pain.

Graphique 1.9d La perte sèche. 
[insert image here](https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-09-d-b.jpg)

**TBC: Graphique interactif dans CORE** 

Ces deux exemples de perte sèche montrent que le surplus total est réduit s’il s’éloigne du prix d’équilibre de 2 euros et de la quantité d’équilibre de 5 000 pains. Pour un prix supérieur (prix plancher), les producteurs sont prêts à vendre plus de pains, mais il y a moins de demandes. Pour un prix inférieur (prix plafond), les consommateurs sont disposés à acheter plus de pains, mais les boulangers en offrent moins.




## 1.9. Impôts indirects et subventions
