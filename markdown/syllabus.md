
# Syllabus examples

## 1. Schola Europea

### 1.1. Competences

| Competency | Key Concepts |
| --- | --- |
| Knowledge | The student displays a comprehensive knowledge of facts and concepts |
| Comprehension | The student displays a thorough command and use of concepts and principles in economics |
| Application | The student makes connections between different parts of the syllabus and applies concepts to a wide variety of unfamiliar situations and makes appropriate links |
| Analysis | The student is capable of detailed and critical analysis and explanations of complex contexts |
| Digital and information Competences | The student can consistently and independently find information on economic topics and assess the reliability of information, on- and offline and can independently use digital resources for economic tasks |
| Communication (oral and written) | The student can communicate logically and concisely using correct economic terminology and is able to give a presentation |
| Teamwork | The student is able to work in a team |
|  |  |
|  |  |

### 1.2. Topics 

| Sub-topic | Suggested % teaching time in S4&S5 |
| --- | --- |
| **S4**| |
| Nature and principles of economics | 10% |
| Consumption of goods and services | 20% |
| Production of goods and services | 25% |
| Markets and price-determination | 30% |
| Distribution of goods and services | 15% |
| **S5** | |
| Payments for goods and services | 30% |
| National economy | 40% |
| International trade  | 30% |

### 1.3. Learning objectives

1. Nature and principles of economics
    1. **explain** what is meant by:
        - the basic economic problem
        - opportunity cost
        - incentives
        - goods and services
        - the factors of production
        - production, consumption, economic agents (households, firms)
2. Consumption of goods and services
    1. **explain** the economic functions and objectives of households
    2. **explain** the component parts of a family’s budget (e.g. sources of income, types of expenditure) and the constraints imposed upon it
    3. **explain** the idea of a family’s purchasing power and describe the operations of a family budget
    4. **understand** personal finance: savings vs. excessive indebtedness
    5. **show** how the interests of consumers may be safeguarded
3. Production of goods and services
    1. **explain** the economic functions and objectives of producers
    2. **identify** the different sectors of production
    3. **explain** the productive combination (including added value and productivity) and how the different factors of production are rewarded
    4. **explain** the concepts of specialisation and division of labour
    5. **calculate** revenue, indicate the difference between fixed and variable costs and calculate the break-even point and profit / loss levels
    6. **explain** the concept of integration and economies of scale
    7. **identify** legal types of business organization and present multinational corporations
    8. **identify** and explain the different sources of business finance
4. Markets and price-determination
    1. **indicate** the factors influencing demand, and draw individual and market demand curves
    2. **indicate** the factors influencing supply, and draw individual and market supply curves
    3. **explain** simple elasticities of demand and supply
    4. **determine** equilibrium price and the quantity traded, and show how these may change
5. Distribution of goods and services
    1. **show** the different ways in which goods from the producer reach the consumer, and the importance of e.g. advertising, wholesaling, retailing, online shopping, shared economy storage and transport
6. Payments for goods and services
    1. **explain** the forms, functions and characteristics of money
    2. **explain** the creation of money by the commercial banks
    3. **explain** the different methods of making payments
    4. **describe** the services offered by banks
    5. **indicate** the principal activities of a central bank
7. National economy
    1. **explain** the circular flow of income with five economic agents including injections and leakages
    2. **define** GDP and GNP and distinguish between GDP and GDP per capita
    3. **distinguish** between nominal and real GDP and explain the different methods of measuring national output
    4. **explain** the weaknesses of GDP as a measure of growth and welfare
    5. **explain** different types of market failures e.g. externality, merit goods, public goods etc.
    6. **calculate** economic growth and explain the main causes of inflation, the concept of deflation and the types of unemployment
    7. **explain** the policies governments and central banks have to remedy lack of growth, unemployment and inflation/deflation
8. International trade
    1. **distinguish** between absolute and comparative advantage
    2. **explain** the advantages and disadvantages of international trade
    3. **show** how international trade may be limited by tariff and non-tariff barriers and explain why trade restrictions are put in place
    4. **identify** the efforts to promote international trade by organisations such as the EU, WTO, IMF, World Bank, EIB, OECD, G20 etc
    5. **define** imports and exports and explain the relationship to a trade deficit and a trade surplus
    6. **define** the term “exchange rate” and explain the concepts of appreciation and depreciation of a currency
    7. **discuss** the benefits and constraints of a common currency such as the Euro

### 1.4. Assessment objectives

- Objective 1: **Knowledge and comprehension**
    - Recall and select relevant economic terminology
    - Demonstrate understanding of economic concepts
    - Demonstrate an understanding of economic principles, cause and effect
- Objective 2: **Application**
    - Make a summary of different concepts
    - Apply economic theory to practical situations
    - Make links between different parts of the syllabus
    - Make links between theory and unfamiliar situations 
- Objective 3: **Analysis**
    - Make detailed and critical analyses
    - Explain complex contexts 
- Objective 4: **Digital and information competences**
    - Find information on economic topics
    - Assess the reliability of information, on- and offline
    - Independently use appropriate software for economic tasks
- Objective 5: **Communication (oral and written) skills**
    - Present balanced and focused economic explanations both orally and in written form
    - Demonstrate evidence of research skills and organization
- Objective 6: **Teamwork**
    - Demonstrate ability to work in a group

## Sources

- [Economics Syllabus](www.eursc.eu/Syllabuses/2021-07-D-12-en-3.pdf)
