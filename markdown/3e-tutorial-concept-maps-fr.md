# Les cartes conceptuelles (ou *concept maps*)

## 1. Définition et utilisation des cartes conceptuelles

### 1.1. Quelle est la signification de la *cartographie conceptuelle* (*concept mapping*)?

La cartographie conceptuelle est la stratégie employée pour développer une carte conceptuelle. Une carte conceptuelle se compose de nœuds ou de cellules qui contiennent un concept, un élément ou une question, et de liens. Les liens sont étiquetés et indiquent la direction à l'aide d'une flèche. Les liens étiquetés expliquent la relation entre les nœuds.
La flèche décrit la direction de la relation et se lit comme une phrase.

Voici un exemple de schéma conceptuel général ([source](https://www.a-z.lu/discovery/fulldisplay?docid=cdi_proquest_miscellaneous_1642318663&context=PC&vid=352LUX_BIBNET_NETWORK:BIBNET_UNION&search_scope=DN_and_CI&tab=DiscoveryNetwork&lang=fr))

```mermaid
graph TD
    A[carte conceptuelle] --> |est un|B[graphique]
    B --> |est composé de| B1[liens]
    B --> |est composé d'| B2[éléments]
    B1 --> |relient des pairs de|B2
    B1 --> |ont|C1[étiquettes]
    B2 --> |représentent|C2[concepts]
    C1 --> |explique <br> les relations|B2
```


### 1.2. Pourquoi utiliser la *cartographie conceptuelle* ?

La schématisation conceptuelle facilite un apprentissage significatif et, par conséquent, des scores plus élevés peuvent être obtenus lors des évaluations lorsque des schémas conceptuels sont utilisés en classe.

Les cartes conceptuelles ont également une fonction sociale. La construction d'une carte conceptuelle rassemble les gens si l'instructeur structure la cartographie conceptuelle comme une activité de groupe coopérative.

Les cartes conceptuelles peuvent être utilisées pour améliorer la compréhension d'un sujet par les étudiants et comme outil d'évaluation pour les enseignants afin de tester la maîtrise de la matière par les étudiants.

Les cartes conceptuelles permettent de:

1. de réfléchir de manière abstraite à la matière et d'être en mesure d'appliquer les concepts théoriques à leur vie quotidienne,
2. de retenir les concepts économiques à long terme, et
3. d'avoir une meilleure perception de l'utilité et de l'accessibilité de l'économie.

Les cartes conceptuelles nous permettent d'illustrer la pensée complexe d'un apprenant et nous donnent un aperçu de la manière dont les apprenants structurent leur pensée et leur compréhension d'un certain contenu.

### 1.3. Comment présenter les cartes conceptuelles aux apprenants

Une fois que certaines parties du matériel théorique ont été introduites, une série de questions à faire à la maison sera fournie.

Ces questions hebdomadaires peuvent impliquer la construction d'une carte conceptuelle. L'apprenant construira sa carte conceptuelle à partir des concepts fournis par la question devant la classe lors d'une présentation et ses camarades de classe feront des commentaires, seront d'accord ou non avec les liens, ou fourniront leur propre carte conceptuelle alternative.

Grâce à ce processus, la classe construit une carte conceptuelle, faisant du processus d'apprentissage et de compréhension un exercice productif et intéressant en raison de la discussion des liens entre les concepts et de la finalisation de la construction d'une carte conceptuelle "de classe".

### 1.4. Composants clés d'une carte conceptuelle

- Concepts (associés à un sujet et essentiels à la compréhension)
- Mots de liaison (soulignant les relations entre les concepts pour former des propositions).
- Liens croisés (montrant les connexions entre les domaines disciplinaires)
- Hiérarchie (des concepts généraux et englobants aux concepts spécifiques)
- Question centrale : qui peut être utilisée ou non pour guider le processus de création de la carte conceptuelle.

## 2. Exemples

### 2.1. Exemple 1 : Facteurs influençant la courbe d'offre

```mermaid
graph TD
    A[courbe d'offre] --> |mouvement le <br> long de la <br> courbe d'offre|B1[propre <br> prix]
    B1 --> B11[variation de la quantité <br> fournie et du prix]
    A --> |déplacement de la <br> courbe d'offre| B2[les facteurs <br> à l'origine <br> du changement]
    B2 --> B21[prix des <br> produits <br> connexes]
    B2 --> B22[nombre de <br> fournisseurs]
    B2 --> B23[prix des <br> intrants]
    B2 --> B24[attentes]
    B2 --> B25[technologie]
    B21 --> B3[une quantité différente est fournie au même prix]
    B22 --> B3
    B23 --> B3
    B24 --> B3
    B25 --> B3
```

### 2.2. Exemple 2 : Concepts associés à la concurrence parfaite en tant que structure de marché

```mermaid
graph TD
    A[caractéristiques du marché] --> B1[de nombreux <br> acheteurs et <br> vendeurs]
    A --> B2[pas de barrières <br> à l'entrée]
    A --> B3[preneur de prix]
    A --> B4[parfaite <br> information]
    A --> B5[produits <br> homogènes]
    B1 --> C[concurrence parfaite]
    B2 --> C
    B3 --> C 
    B4 --> C 
    B5 --> C 
    C --- D[structure du marché]
    D --- D1[monopole]
    D --- D2[concurrence <br> imparfaite]
    C --> E1[court terme]
    E1 --> E11[bénéfice <br> économique <br> positif]
    E1 --> E12[profit <br> économique <br> nul]
    E1 --> E13[bénéfice <br> économique <br> négatif]
    C --> E2[long terme]
    E2 --> E21[profit <br> économique nul]
    style D fill:#0080FF
    style C fill:#CCFFFF
    style E1 fill:#CCFFFF
    style E11 fill:#CCFFFF
    style E12 fill:#CCFFFF
    style E13 fill:#CCFFFF
    style E2 fill:#CCFFFF
    style E21 fill:#CCFFFF
    style D1 fill:#CCE5FF
    style D2 fill:#CCCCFF    

```


## Sources

- [The Effectiveness of collaborative problem-solving tutorials in introductory microeconomics](https://www.a-z.lu/discovery/fulldisplay?docid=cdi_proquest_miscellaneous_38977969&context=PC&vid=352LUX_BIBNET_NETWORK:BIBNET_UNION&search_scope=DN_and_CI&tab=DiscoveryNetwork&lang=fr)
- [Effectiveness of concept maps in economics: Evidence from Australia and USA](https://www.a-z.lu/discovery/fulldisplay?docid=cdi_crossref_primary_10_1016_j_lindif_2007_03_003&context=PC&vid=352LUX_BNL:BIBNET_UNION&search_scope=DN_and_CI_UCV&tab=DiscoveryNetwork_UCV&lang=fr)
- [ Enseigner autrement avec le mind mapping : cartes mentales et conceptuelles
](https://www.a-z.lu/discovery/fulldisplay?docid=alma990016038310107255&context=L&vid=352LUX_EDU:BIBNET_UNION&search_scope=DN_and_CI_UCV&tab=DiscoveryNetwork_UCV&lang=fr)
- [Concept Mapping Tutorial](https://wiki.ubc.ca/Documentation:Concept_Mapping_Tutorial)
- [The Theory Underlying Concept Maps and How to Construct and Use Them](https://cmap.ihmc.us/docs/theory-of-concept-maps)
- [Concept maps: A useful and usable tool for computer-based knowledge assessment? A literature review with a focus on usability.](https://www.a-z.lu/discovery/fulldisplay?docid=cdi_proquest_miscellaneous_1642318663&context=PC&vid=352LUX_BIBNET_NETWORK:BIBNET_UNION&search_scope=DN_and_CI&tab=DiscoveryNetwork&lang=fr)
- [Concept Maps (SCRIPT)](https://script.lu/fr/activites/innovation/concept-maps)
- [OASYS: un outil graphique numérique développé de l'uni.lu](https://www.oasys4schools.lu/conceptmaps/)
