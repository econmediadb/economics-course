## 1.2. La demande

**Mots-clés** de la section 1.2 : *courbe de demande, déplacement de la courbe de demande, déplacement sur la courbe de demande, (option) taxe forfaitaire ...*

*La demande est une fonction décroissante du prix. Plus le prix est élevé, moins les consommateurs (demandeurs) sont disposés à acheter du pain. La demande de marché, qui correspond à la somme des demandes individuelles, baisse donc. Un prix plus élevé réduit le pouvoir d’achat des consommateurs et diminue la quantité demandée. En effet, un prix plus élevé annule ou reporte la demande de certains consommateurs et réduit la quantité consommée des autres.*

De même, ce phénomène est plus marqué pour certains biens ou services qui ont des proches substituts : lorsque le prix du bien ou du service augmente alors que celui d’un proche substitut reste inchangé, les consommateurs peuvent consommer une quantité moins importante du bien ou du service dont le prix relatif s’est accru (par exemple, les fraises et les framboises, la viande blanche et la viande rouge, une place de cinéma et une vidéo à la demande via une box).

Les déterminants de la demande sont les suivants : le prix, le nombre d’acheteurs, les anticipations de ces acheteurs, la variation du prix d’un bien ou d’un service substituable ou complémentaire, le revenu, les goûts des consommateurs.

<!-- Tableau 1.3 -->
Tableau 1.8 Prix et quantité de pains demandée. 
| Prix en euro | Quantité de pains <br> demandée |
|:------------:|---------------------------:|
| 0,85         | 9 000                      |
| 1,7          | 6 000                      |
| 2            | 5 000                      |
| 2.5          | 4 000                      |
| 3,25         | 2 000                      |
| 4,25         | 0                          |

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-02.jpg" />
    <br>
    Graphique 1.2 La courbe de demande sur le marché du pain.

On peut interpréter la pente de la courbe de demande.


### 1.2.1. Interprétation de la courbe de demande

La pente d’une courbe de demande nous renseigne sur sa sensibilité par rapport aux variations des prix. 

Quand une variation des prix entraîne une variation plus que proportionnelle des quantités demandées, on dit que la demande est très sensible à la variation des prix (pente de la courbe peu importante). 

Quand une variation des prix entraîne une variation moins que proportionnelle des quantités demandées, on dit que la demande est peu sensible à la variation des prix (pente de la courbe très importante). 

Enfin, si une variation des prix n’entraîne pas de modification des quantités demandées, on dit que la demande est insensible à la variation des prix.

### 1.2.2. Interprétation des déplacements *des* courbes ?

*L’offre et la demande augmentent ou baissent, indépendamment de la variation du prix de marché, sous l’effet de chocs positifs ou négatifs (modifications des conditions d’offre et de demande). Ces variations se traduisent graphiquement par un déplacement des courbes d’offre et de demande vers la droite (en cas de choc positif) ou vers la gauche (en cas de choc négatif). Ce déplacement des courbes se traduit lui-même par un déséquilibre (une surproduction ou une pénurie) à l’ancien prix d’équilibre.*

Appuyons-nous de nouveau sur l’exemple du marché du pain. Supposons que le coût de la farine qui entre dans la composition du pain baisse en raison d’une situation climatique favorable à la production de blé : cela incite les boulangers à produire davantage, à tous niveaux de prix du pain, parce que leurs perspectives de profit augmentent. On parle alors de *choc d’offre* (modification des conditions de l’offre, ici du coût de production) positif (puisque l’offre augmente alors que le niveau général des prix reste constant). Graphiquement, cette hausse de l’offre se traduit par le déplacement de la courbe d’offre vers la droite, parallèlement à la courbe d’offre initiale.

--- 
**EXERCICE**

- Exercice 1 : Effet d’un choc d’offre positif sur le marché du pain
  - À la suite du choc d’offre positif, quelles sont la quantité demandée et la quantité offerte de pains à l’ancien prix d’équilibre ?
  - Comment qualifie-t-on ce déséquilibre ?
- Exercice 2 : Effet d’un choc de demande positif sur le marché du pain
  - Appuyez-vous sur les acquis de la Section 1.3 sur les déterminants de la demande pour proposer une explication d’un choc de demande positif sur le marché du pain.
  - Représentez graphiquement le déplacement de la courbe pour tous niveaux de prix.
  - Quel déséquilibre apparaît sur le marché du pain en cas de choc de demande positif ?
- Exercice 3 : Effet d’un choc négatif sur le marché du pain
  - Comment les courbes d’offre et de demande se déplacent-elles en cas de chocs négatifs ?
  - Quel déséquilibre apparaît en cas de choc d’offre négatif ? En cas de choc de demande négatif ?

---




<!--
---
APPLICATION

Montrer qu’une variation de l’offre (de la demande) indépendamment de la variation du prix provoque un déplacement de la courbe d’offre (de demande).
TBC
---
-->


### 1.2.3. Interprétation des déplacements *sur les* courbes ?

Lorsque le marché est à l’équilibre, un changement du prix et de la quantité d’équilibre ne peut être dû qu’à un choc : la variation (baisse dans le cas d’un choc négatif, hausse dans le cas d’un choc positif) de l’offre ou de la demande provoque le déplacement des courbes à tous niveaux de prix. Cette variation de l’offre ou de la demande induit l’apparition d’un déséquilibre (surproduction ou pénurie). Celui-ci se traduit par une variation du prix (à la baisse en cas de surproduction, à la hausse en cas de pénurie), qui donne elle-même lieu à un ajustement des quantités offertes et demandées (déplacement sur les courbes), permettant le retour à l’équilibre (nouveau prix et nouvelle quantité d’équilibre). Grâce à la flexibilité des prix, des quantités offertes et demandées, le marché est ainsi autorégulateur : il revient spontanément à l’équilibre.

Puisque les chocs se traduisent par l’apparition de déséquilibres, la question qui se pose dès lors concerne le retour à l’équilibre.

Dans le cas du choc d’offre positif sur le marché du pain étudié précédemment, une surproduction apparaît, car l’offre (6.800 pains) est supérieure à la demande (5.000 pains) à l’ancien prix d’équilibre de 2 euros. Le prix baisse sur le marché du fait de la concurrence entre les offreurs souhaitant tous écouler leur production. En raison de cette baisse du prix du pain, l’offre de pain baisse et la demande de pain augmente. Le Graphique 1.4b montre que cette situation se traduit par un déplacement sur les courbes d’offre et de demande.

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-04-b.jpg" />
    <br>
    Graphique 1.4b Déplacements sur les courbes d’offre et de demande à la suite d’un choc d’offre positif

---
**APPLICATION**

- Exercice : L’effet d’une baisse des prix sur l’offre et la demande
  - Appuyez-vous sur les acquis de cette section pour expliquer pourquoi, lorsque le prix baisse, l’offre de pain baisse et la demande de pain augmente.
  - Décrivez les variations de l’offre et de la demande à la suite de la baisse du prix du pain.
  - Quels sont le nouveau prix et la nouvelle quantité d’équilibre ?

---

<p align="center">
    <img src="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/graphique-01-05.jpg" />
    <br>
    Graphique 1.5 Déplacements sur les courbes d’offre et de demande à la suite d’un choc de demande positif.


### 1.2.4. (Option) Etude de cas  : Taxe forfaitaire

TBC
