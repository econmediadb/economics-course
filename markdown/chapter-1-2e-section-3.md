## 1.3. Elasticités de la demande par rapport au prix

**Mots-clés** de la section 1.3 : *élasticité-prix de la demande, élasticité-prix croisée de la demande, fonction de demande inverse, fonction de demande directe, Loi de la Demande, ...*

### 1.3.1. Élasticité de la demande (présentation simple)

L’**élasticité prix de la demande** est une mesure de la sensibilité des consommateurs à un changement de prix. Elle est définie comme la variation (en pourcentage) de la demande en réponse à une augmentation du prix de 1 %. Par exemple, supposez que lorsque le prix d’un produit augmente de 10 %, nous observions une baisse de 5 % de la quantité vendue. Nous pouvons alors calculer l’élasticité, ε, de la manière suivante :

ε = − variation de la demande (en %) ÷ variation du prix (en %)

ε est la lettre grecque epsilon, souvent utilisée pour représenter l’élasticité. Lorsque le changement du prix est positif le changement de demande est négatif et vice versa. Le signe moins (-) dans la formule de l’élasticité garantit donc que la mesure de la sensibilité au prix soit un nombre positif. Dans cet exemple, nous obtenons donc :

ε = −5 ÷ 10 = 0,5

On dit que la demande est *élastique* lorsque l’élasticité est supérieure à 1 et *inélastique* lorsqu’elle est inférieure à 1.

L’**élasticité-prix croisée de la demande** fait référence à la variation en pourcentage de la quantité demandée d’un produit donné en raison de la variation en pourcentage du prix d’un autre produit « proche ». Si tous les prix peuvent varier, la quantité demandée du produit A dépend non seulement de son propre prix (voir élasticité de la demande) mais aussi des prix des autres produits.

### 1.3.2. Élasticité prix croisée de la demande (présentation simple)

TBD