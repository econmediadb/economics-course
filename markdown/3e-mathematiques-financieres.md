# Mathématiques financières (classes de 3CD/3CG)

## Vue d'ensemble

```mermaid
graph TD;
    A[mathématiques financières] --> A1[pourcentages]
    A --> A2[intérêts <br> simples]
    A --> A3[intérêts <br> composés]
    A --> A4[indices <br> simples]
```

## 1. Les pourcentages 

```mermaid
graph TD;
    A[pourcentages] --> A1[définition]
    A --> A2[majorations <br> de prix]
    A --> A3[réductions <br> de prix]
```

### 1.1. Définition 

On appelle pourcentage le *rapport constant* de mesure de *deux grandeurs proportionnelles* quand la mesure du second est de 100:

```math
\frac{x}{100}
```

### 1.2. Majorations de prix 

Le bénéfice (**B**) est la différence entre le prix de vente (**PV**) et le prix d'achat (**PA**) d'une marchandise:

```math
B = PV - PA
```

```mermaid
graph TD;
    A[Hyp: PV proportionnel au PA] --> B[Bénéfice]
    B --> |peut être exprimé en|B1[pourcentage sur le PA]
    B --> |peut être exprimé en|B2[pourcentage sur le PV]

```

#### 1.2.1. Bénéfice exprimé en un pourcentage du prix d'achat

```mermaid
graph TD;
    A[réaliser un bénéfice de x% sur le PA] --> |signifie pour un PA <br> de 100 euro|B[B = x]
    A --> |signifie pour un PA <br> de 100 euro|C[PV = 100 + x ]

```

<!-- blank line -->
<br>
<!-- blank line -->
<!-- blank line -->
<br>
<!-- blank line -->
<!-- blank line -->
<br>
<!-- blank line -->

Point de départ: $PA = 100$

```math
PA = 100 \\
PA + B = 100 + B \\
```
comme nous avons $B = x$, nous pouvons déduire

```math
PA + B = 100 + x \\
```
Comme $PV = B + PA$, nous pouvons écrire:

```math
PV = 100 + x
```


Comme $PV$ et $PA$ sont *directement proportionnels*,
```math
\frac{PV}{PA} = \frac{100+x}{100}
```
alors (en multipliant des deux côtés par $PA$), nous obtenons:
```math
\textcolor{red}{PV = PA \cdot \frac{100+x}{100}}
```
et
```math
\textcolor{blue}{PA = PV \cdot \frac{100}{100+x}}
```

En utilisant l'expression en rouge ci-dessus il est possible de déduire l'expression:
```math
\textcolor{red}{B=\frac{x}{100} \cdot PA}
```

En remplaçant l'expression bleu ci-dessus dans la dernière expression, nous pouvons déduire l'expression suivante:
```math
\textcolor{blue}{B=\frac{x}{100+x} \cdot PV}
```

<br>

<br>

<br>

$`\textcolor{red}{\text{!!! \textbf{Devoir à domicile (pour vendredi 21/4/2023)}: Refaire les mêmes calculs pour la section \textbf{1.2.2. Bénéfice exprimé en un pourcentage du PV} !!!}}`$ 



### 1.3. Les réductions de prix 


La réduction de prix (**R**) est la différence entre le prix de vente brut (**PB**) et le prix de vente net (**PN**) d'une marchandise:

```math
R = PB - PN
```

```mermaid
graph TD;
    A[Hyp: PN proportionnel au PB] --> B[Réduction]
    B --> |peut être exprimé en|B1[pourcentage sur le PB]
    B --> |peut être exprimé en|B2[pourcentage sur le PN]
```
 
<br>

<br>



#### 1.3.1. Réduction exprimée en un pourcentage du prix de vente brut (PB)

Accorder une réduction de $x$% sur le $PB$ de 100, la réduction est de $x$ et le $PN$ est de $100-x$

```mermaid
graph TD;
    A[accorder une réduction de x% sur le PB] --> |signifie pour un PB <br> de 100 euro|B[R = x]
    A --> |signifie pour un PB <br> de 100 euro|C[PN = 100 - x ]
```

<!-- blank line -->
<br>
<!-- blank line -->
<!-- blank line -->
<br>
<!-- blank line -->
<!-- blank line -->
<br>
<!-- blank line -->

Point de départ: $PB = 100$

```math
PB = 100 \\
PB - R = 100 - R \\
```
comme nous avons $R = x$, nous pouvons déduire

```math
PB - R = 100 - x \\
```
Comme $PN = PB - R$, nous pouvons écrire:

```math
PN = 100 - x
```


Comme $PB$ et $PN$ sont *directement proportionnels*,
```math
\frac{PB}{PN} = \frac{100}{100-x}
```
alors (en multipliant des deux côtés par $PN$), nous obtenons:
```math
\textcolor{red}{PB = PN \cdot \frac{100}{100-x}}
```
et
```math
\textcolor{blue}{PN = PB \cdot \frac{100-x}{100}}
```

En utilisant l'expression en rouge ci-dessus il est possible de déduire l'expression:
```math
\textcolor{red}{R=\frac{x}{100} \cdot PB}
```

En remplaçant l'expression bleu ci-dessus dans la dernière expression, nous pouvons déduire l'expression suivante:
```math
\textcolor{blue}{R=\frac{x}{100-x} \cdot PN}
```



<br>

<br>

<br>

$`\textcolor{red}{\text{!!! \textbf{Devoir à domicile (pour mercredi 17/5/2023)}: Refaire les mêmes calculs pour la section \textbf{1.3.2. Réduction exprimée en un pourcentage du PN} !!!}}`$ 


<!--- $$                             --->
<!--- \begin{aligned}                --->
<!--- \dot{x} & = \sigma(y-x) \\     --->
<!--- \dot{y} & = \rho x - y - xz \\ --->
<!--- \dot{z} & = -\beta z + xy      --->
<!--- \end{aligned}                  --->
<!--- $$                             --->


## 2. Intérets simples

$$
I = \frac{C \cdot t \cdot n}{36000}
$$

où $I$ est l'intérêt, $C$ le capital prêté ou placé, $t$ le taux d'intérêt et $n$ le nombre de jours de placement



#### 2.2.2 Durée de placement en mois, en années

Si la durée de placement est exprimé en *mois* ($m$)

$$
I = \frac{C \cdot t \cdot m}{1200}
$$

Si la durée de placement est exprimé en *années* ($a$)
$$
I = \frac{C \cdot t \cdot a}{100}
$$



## 2.3. Valeur acquise (ou valeur définitive) d'un capital ($A$)

La valeur acquise d'un capital pendant $n$ jours :

$A  = C + I$

$A  = C + \frac{C \cdot t \cdot n}{36000}$

# 3. Les intérêts composés

```mermaid
graph TD;
    A[placement] --> B1[à intérêts simples]
    A --> B2[à intérêts composés]
    B1 --> B1a[capital placé]
    B1a --> |reste|B1a1[invariable]
    B1a --> |produit|B1a2[des intérêts <br> égaux à chaque période]
    B2 --> B2a[intérêts forment <br> un nouveau placement <br> à chaque période ]
```

<br>

<br>

<br>

## 3.2. Détermination de la valeur acquise ($A$)

Valeur acquise :
$$
A = C + I_{\text{composé}} 
$$

<br>

<br>

<br>

Période 1:
$$
\begin{aligned}
A_1 & = \underbrace{C}_{capital} + \underbrace{C \cdot i}_{intérêt}   \\
    & = C(1 + i)
\end{aligned} 
$$

Période 2:
$$
\begin{aligned}
A_2 & = \underbrace{C(1 + i)}_{capital} + \underbrace{[C(1 + i)] \cdot i}_{intérêt}   \\
    & = C(1 + i) \cdot (1 + i) \\
    & = C(1 + i)^2
\end{aligned} 
$$

Période 3:
$$
\begin{aligned}
A_3 & = \underbrace{C(1 + i)^2}_{capital} + \underbrace{[C(1 + i)^2] \cdot i}_{intérêt}   \\
    & = C(1 + i)^2 \cdot (1 + i) \\
    & = C(1 + i)^3
\end{aligned} 
$$

$$
\vdots
$$

Période $n$:
$$
\begin{aligned}
A_n & = \underbrace{C(1 + i)^{n-1}}_{capital} + \underbrace{[C(1 + i)^{n-1}] \cdot i}_{intérêt}   \\
    & = C(1 + i)^{n-1} \cdot (1 + i) \\
    & = C(1 + i)^n
\end{aligned} 
$$

### 3.2.2. Formule de la valeur acquise (d'un capital placé à *intérêts composés*) 

Valeur acquise :
$$
A = C(1+i)^n 
$$

<br>

<br>

<br>


## 3.3. Détermination de la valeur actuelle ($C$)

$$
\begin{aligned}
A & = C(1+i)^n  ~ ~ ~ ~ \mid \cdot (1+i)^{-n}  \\
C & = A(1+i)^{-n} \\
\end{aligned} 
$$

## 3.4. Recherche du taux de placement ($i$)

$$
\begin{aligned}
A & = C(1+i)^n  ~ ~ ~ ~ \mid \cdot C^{-1}  \\
(1+i)^n & = \frac{A}{C} ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \mid \sqrt[n]{\cdot}  \\
(1+i) & = \sqrt[n]{\frac{A}{C}} ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \mid -1 \\
i & = \sqrt[n]{\frac{A}{C}} - 1
\end{aligned} 
$$

## 3.5. Recherche du temps de placement ($n$)

$$
\begin{aligned}
A & = C(1+i)^n  ~ ~ ~ ~ \mid \cdot C^{-1}  \\
(1+i)^n & = \frac{A}{C} ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \mid \log(\cdot)  \\
n \log(1+i) & = \log \frac{A}{C} ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  \mid \div \log(1+i) \\
n & = \frac{ \log \frac{A}{C} }{ \log(1+i) }
\end{aligned} 
$$

**Note**: En 3e vous n'avez pas encore eu d'introduction aux logaritmes. Regardez les vidéos (Khan Academy) suivants:
- [Introduction aux logarithmes (7 min)](https://www.youtube.com/watch?v=D9fovL5H7GI)
- [Introduction aux propriétés des logarithmes 1/2 (8 min)](https://www.youtube.com/watch?v=69SO_oxK0vI)
- [Introduction aux propriétés des logarithmes 2/2 (8 min)](https://www.youtube.com/watch?v=FQlQVphVg_k)

## 3.6. Taux proportionnels, taux équivalents

$$
\begin{aligned}
1 + i_a = (1 + i_s)^2
\end{aligned}
$$

où $i_a$ est le _taux annuel_ et $i_s$ est le taux semestriel

De même,

$$
\begin{aligned}
1 + i_a = (1 + i_m)^{12}
\end{aligned}
$$

où $i_a$ est le _taux annuel_ et $i_m$ est le taux mensuel

En utilisant ces formules, il est possible d'obtenir $i_s$ en fonction de $i_a$ (par réarr
angement).


