# Notions et concepts utilisés dans le cours de 2e

## Chapitre sur la sociologie

### 1. Les inégalités socioéconomiques

Dans cette section, nous discutons la perception des inégalités dans la société.

```mermaid
graph TD;
A[inégalités économiques] --> |s'observe par la présence de| B[3 phénomènes];
B --> B1[élite économique];
B --> B2[classes moyennes];
B --> B3[pauvreté];
B1 --> |observable à l'| C[échelle];
B2 --> |observable à l'| C;
B3 --> |observable à l'| C;
C --> C1[mondiale];
C --> C2["nationale (Luxembourg)"];
A --> |sont analysées par quels| D[acteurs];
D --> D1[chercheurs en <br> sciences sociales];
D --> D2[statisticiens officiels];
D --> D3[chargés d'études];
D1 --> E[indicateurs utilisés];
D2 --> E;
D3 --> E;
E --> F1[seuils de faible revenu];
E --> F2[quintiles de revenu];
E --> F3[coefficient de Gini];
F1 --> |peuvent mesurer| G[2 types de pauvretés];
G --> G1[pauvreté relative];
G --> G2[pauvreté absolue];
```

#### 1.1. La richesse et la pauvreté dans le monde

Vidéos educ'arte:
- Evolution de l'extrême pauvreté dans le monde 1990-2015
- La mesure des écarts de richesse : l'indicateur de Gini
- Pays de l'OCDE: l'augmentation des revenus des plus riches
- Inégalités de richesse : la lutte contre l'extrême pauvreté


Le concept d'"inégalités socioéconomiques" se réfère essentiellement aux différences en termes de revenus, de biens, de pouvoir et de conditions de vie entre individus ou groupes sociaux. D'après les informations publiées par diverses organisations, de telles inégalités existent, que ce soit entre pays riches et pays pauvres ou même à l'intérieur d'un même pays.

Ces inégalités socioéconomiques sont évidentes à l'échelle mondiale. On observe des différences frappantes entre le mode de vie des pays développés, centrés sur la consommation, et celui des pays en développement, où la survie quotidienne est un défi. Ces disparités sont régulièrement mises en lumière dans les médias, à travers des images de dirigeants d'entreprises prospères, de familles de la classe moyenne occidentale confrontées à des difficultés, de célébrités d'Hollywood, et de camps de réfugiés. Elles sont tellement ancrées dans notre réalité quotidienne qu'elles ne nous surprennent plus.

1. La richesse dans le monde

Les personnes riches attirent l'attention et suscitent divers sentiments : fascination, curiosité, envie, admiration, mais parfois aussi du mépris. Leur mode de vie exceptionnel les rend distincts et donne l'impression qu'ils exercent une influence considérable sur le monde, y compris sur les décisions politiques. Depuis 1996, le nombre de millionnaires a fortement augmenté, et leur part dans la richesse globale s'est accrue également. Aujourd'hui, un nombre croissant de personnes riches tirent leurs revenus principalement de leurs salaires, surtout dans le secteur financier.

Ces individus fortunés constituent une élite internationale cosmopolite. Ils ont souvent grandi dans un contexte de mondialisation, fréquenté des écoles internationales privées, et ont tissé des réseaux mondiaux tout en partageant une culture d'élite commune. Cette élite se distingue par ses capitaux à plusieurs niveaux : économique, culturel et social, comme le décrit la théorie de Bourdieu.

 2. La pauvreté dans le monde

 L'extrême richesse est le privilège d'un très petit nombre de personnes, tandis que la majorité de la population mondiale vit dans la pauvreté. Les inégalités socioéconomiques sont fréquemment analysées à travers le prisme de la pauvreté par des organisations internationales, telles que l'ONU, qui s'efforcent de lutter contre ce fléau dans le cadre des Objectifs du millénaire pour le développement. On note des avancées dans la réduction de la pauvreté en Asie de l'Est, particulièrement en Chine et en Inde. Cependant, la pauvreté reste un problème majeur dans certaines régions d'Afrique subsaharienne, d'Asie du Sud et des Caraïbes, où une proportion importante de la population vit avec moins de 1,25 dollar par jour.

Combattre la faim fait aussi partie de ces objectifs, mais les progrès ont été limités depuis le début des années 2000. Environ 16% des habitants des régions en développement souffrent de sous-alimentation. Même les pays riches ne sont pas entièrement à l'abri de la pauvreté, bien que celle-ci y soit moins fréquente. Selon l'OCDE, plus de 10% de la population de ses pays membres vit dans la pauvreté, avec des différences notables entre les différents pays et régions du monde.


#### 1.2. Les inégalités au Luxembourg

Des études récentes révèlent l'ampleur des inégalités socioéconomiques au Luxembourg et en Europe, confirmant leur présence dans toutes les sociétés.

[Indicateurs d'inégalité dans la répartition des revenus](https://lustat.statec.lu/vis?fs[0]=Th%C3%A8mes%2C1%7CConditions%20sociales%23C%23%7CConditions%20de%20vie%23C1%23&pg=0&fc=Th%C3%A8mes&df[ds]=ds-release&df[id]=DF_C1102&df[ag]=LU1&df[vs]=1.0&pd=%2C2019&dq=A.&vw=tb)

[Indicateurs de risque de pauvreté (en %)](https://lustat.statec.lu/vis?fs[0]=Th%C3%A8mes%2C1%7CConditions%20sociales%23C%23%7CConditions%20de%20vie%23C1%23&pg=0&fc=Th%C3%A8mes&df[ds]=ds-release&df[id]=DF_C1103&df[ag]=LU1&df[vs]=1.0&pd=%2C2020&dq=A.)

[Niveau de vie selon les caractéristiques de la personne de référence du ménage (en EUR)](https://lustat.statec.lu/vis?fs[0]=Th%C3%A8mes%2C1%7CConditions%20sociales%23C%23%7CConditions%20de%20vie%23C1%23&pg=0&fc=Th%C3%A8mes&df[ds]=ds-release&df[id]=DF_C1101&df[ag]=LU1&df[vs]=1.0&pd=%2C2019&dq=.A)

[Revenu mensuel disponible et niveau de vie (en EUR)](https://lustat.statec.lu/vis?fs[0]=Th%C3%A8mes%2C1%7CConditions%20sociales%23C%23%7CConditions%20de%20vie%23C1%23&pg=0&fc=Th%C3%A8mes&df[ds]=ds-release&df[id]=DF_C1100&df[ag]=LU1&df[vs]=1.0&pd=%2C&dq=A.)

[Structure moyenne du budget des ménages](https://lustat.statec.lu/vis?fs[0]=Th%C3%A8mes%2C1%7CConditions%20sociales%23C%23%7CConditions%20de%20vie%23C1%23&pg=0&fc=Th%C3%A8mes&df[ds]=ds-release&df[id]=DF_C1300&df[ag]=LU1&df[vs]=1.0&pd=%2C&dq=C02..A&ly[rw]=SPECIFICATION&ly[cl]=TIME_PERIOD)

[inclure ici le Graphique 1 à la page 89 du [Rapport Travail et Cohésion Sociale 2023](https://statistiques.public.lu/fr/publications/series/analyses/2023/analyses-02-231.html)]

1. Les plus fortunés

 Au Luxembourg, les personnes les plus riches ont tendance à rester discrètes sur leur richesse financière. Une étude réalisée en 2012 sur les 100 dirigeants d'entreprises les mieux payés a révélé une grande disparité dans leurs revenus. En moyenne, leur revenu annuel s'échelonne de 4,2 à 75,5 millions de dollars, avec une moyenne impressionnante de 12,8 millions de dollars. Cela représente plus de 250 fois le revenu moyen national.

    Selon le "World Wealth Report 2023" publié par Capgemini, le nombre de millionnaires au Luxembourg a diminué de 0,7 % en 2022, passant de 46 200 à 45 800 individus. La richesse totale des millionnaires luxembourgeois a également reculé de 1,6 % pour atteindre 135,5 milliards de dollars. Toutefois, la fortune moyenne d'un millionnaire luxembourgeois est restée relativement stable à 2,95 millions de dollars.

    Le rapport attribue cette baisse de la richesse à plusieurs facteurs, notamment la guerre en Ukraine, l'inflation, la hausse des taux d'intérêt et la pression sur les prix de l'immobilier. Malgré ces difficultés, la population aisée du Luxembourg reste importante, les millionnaires détenant une part importante de la richesse nationale. En outre, le nombre de milliardaires luxembourgeois a diminué de 17 à 16 en 2022, et leur fortune totale a chuté de 2,3 % pour atteindre 61 milliards de dollars. Bien que la richesse des individus aisés du Luxembourg ait diminué, ils continuent de constituer un segment important de la population du pays.

    World Wealth Report 2023, Capgemini

Cette tendance à l'accroissement de la richesse des plus fortunés se confirme au fil des ans au Luxembourg. L'écart de revenus entre les dirigeants d'entreprises et la population moyenne devient de plus en plus important. Les inégalités socioéconomiques demeurent, malgré le statut du Luxembourg en tant que centre financier et économique important en Europe. Les personnes aisées du pays continuent d'amasser des fortunes importantes, tandis que d'autres groupes de la population peuvent faire face à des défis économiques.

Il est important de souligner que le Luxembourg, en tant que petit pays avec une économie diversifiée, présente des caractéristiques uniques en matière de disparités de revenus et de patrimoine. Ces disparités peuvent être influencées par divers facteurs économiques et sociaux spécifiques à ce pays.

 2. La classe moyenne

Au Luxembourg, la classe moyenne, qui représente une grande partie de la population, se distingue par sa diversité. Elle se divise en plusieurs catégories, telles que les employés de bureau (cols blancs), les ouvriers (cols bleus), les travailleurs indépendants et les salariés. Des études menées par des sociologues et chercheurs locaux ont permis de mieux comprendre les changements dans le niveau de vie de cette classe moyenne.

La classe moyenne au Luxembourg est généralement définie comme les ménages dont les revenus se situent entre 75 % et 150 % du revenu médian du pays. Au fil du temps, cette classe a connu des évolutions notables. Par exemple, de 1990 à 2010, la part de la population luxembourgeoise appartenant à la classe moyenne est restée relativement stable, représentant environ la moitié de la population. Cependant, certaines tendances sont apparues, comme une augmentation du niveau d'éducation au sein de la classe moyenne et une hausse du nombre de ménages composés de couples sans enfants. 

Il est important de noter qu'une proportion significative des ménages de la classe moyenne au Luxembourg tire désormais ses revenus de deux sources de travail. Cette situation est devenue plus courante au fil des ans, indiquant que de plus en plus de familles dépendent de deux salaires pour maintenir leur niveau de vie et éviter de basculer dans la pauvreté.

Ces tendances soulignent l'influence des facteurs économiques et sociodémographiques sur la définition et l'évolution de la classe moyenne au Luxembourg, reflétant ainsi les spécificités de ce pays européen.

 3. La pauvreté

Malgré la prospérité économique générale du Luxembourg, une importante diversité économique existe, et une part significative de la population vit dans des conditions de pauvreté. Environ 40 % des résidents du Luxembourg ont un revenu annuel inférieur à un seuil critique. La pauvreté dans ce pays peut se manifester de différentes manières, allant de la pauvreté chronique à la pauvreté temporaire. Il y a une forte corrélation entre la pauvreté et le chômage, et des événements comme la fermeture d'entreprises peuvent plonger des communautés entières dans la pauvreté.

Depuis les années 1980, le phénomène des "travailleurs pauvres" est devenu plus visible. Il s'agit de personnes ayant des emplois mal rémunérés, souvent insuffisants pour les sortir de la pauvreté. Cette catégorie comprend principalement des femmes, des jeunes immigrants et des membres de minorités ethniques, travaillant généralement dans les secteurs des services, du commerce de détail et de la restauration, souvent pour un salaire minimum.

La pauvreté au Luxembourg est également liée à l'itinérance, avec des individus ou des familles incapables de se permettre un logement. Cette situation peut entraîner un cercle vicieux de pauvreté, en particulier pour ceux qui souffrent de problèmes de santé mentale ou de dépendance. Bien que les statistiques précises sur les sans-abri soient difficiles à obtenir, on estime qu'il y a un nombre important de personnes sans domicile fixe, surtout dans la ville de Luxembourg et ses alentours.

Le manque de logement stable rend difficile l'accès aux aides sociales et aux opportunités d'emploi, contribuant à maintenir les personnes dans des situations précaires au Luxembourg. Ces observations mettent en évidence les différentes facettes de la pauvreté dans ce pays européen prospère.



### 4. La socialisation

```mermaid
graph TD;
A[socialisation] --> |est un| B[processus continu];
B --> |et aussi un| C[processus changeant];
C --> |quant à| D1[durée des étapes];
C --> |quant à| D2[rites de passage];
D1 --> |qui se prolongent <br> plus ou mois <br> selon les| D11[facteurs];
D11 --> D111[facteurs économiques];
D11 --> D112[facteurs sociaux];
D2 --> |qui sont| D21[formels];
D2 --> |qui sont| D22[collectifs];
D21 --> E[cultures]
D22 --> E;
```

<br>

<br>

<br>

<br>

```mermaid
graph TD;
A[processus de socialisation] --> |se divise en | B[5 étapes];
B --> B1["enfance <br> (0-12 ans)"];
B --> B2["adolescence <br> (13-17 ans)"];
B --> B3["âge adulte <br> (22-39 ans)"];
B --> B4["maturité <br> (45-59 ans)"];
B --> B5["vieillesse <br> (65 ans et plus)"]; 
B2 --> C[périodes de transition];
B3 --> C;
B4 --> C;
B5 --> |se subdivise en deux| D[groupes];
D --> D1["3e âge <br> (65-74 ans)"];
D --> D2["4e âge <br> (75 ans et plus)"];
A --> |souvent marqué par des| E[changements qui affectent];
E --> E1[environnement social];
E --> E2[rôle ou statut social];
C --> E;
E1 --> F[resocialisation];
E2 --> F;
```


<br>

<br>

<br>

<br>


```mermaid
graph TD;
A[socialisation] --> |se fait au moyen de| B[4 mécanismes];
B --> B1[miroir <br> réfléchissant];
B --> B2[jeu de rôle];
B --> B3[maniement <br> des impressions];
B --> B4[socialisation <br> par anticipation];
B1 --> |se divise en| B11[3 phases];
B11 --> B111[phase 1 <br> prise de <br> conscience <br> de l'autre];
B11 --> B112[phase 2 <br> prise de <br> conscience <br> du jugement que <br> l'autre porte <br> sur soi];
B11 --> B113[phase 3 <br> intégration <br> du jugement <br> de l'autre];
B113 --- B12["«je deviens <br> tel que l'on <br> me perçoit»"];
B2 --> |se divise en| B21[3 étapes];
B21 --> B211[étape 1 <br> préparation];
B211 --- B211a[reproduction <br> des rôles]
B21 --> B212[étape 2 <br> appropriation];
B212 --- B212a[adaptation <br> progressive <br> des rôles];
B21 --> B213[étape 3 <br> jeu];
B213 --- B213a[interprétation <br> complète des <br> rôles];
B213a --- B213b["«je deviens ce <br> que je vois»"];
B3 --> |nécessite une <br> adaptation <br> constante des| B31["attitudes, <br> comportements, <br> ..."];
B31 --> |afin qu'ils <br> soient <br> adéquats <br> selon| B311[contexte, <br> circonstances, <br> public, etc.];
B311 --- B32["«je deviens tel que <br> je veux être vu»"];
B4 --> |d'ordre| B41[professionnel];
B4 --> |d'ordre| B42[personnel];
B41 --- B5["«je deviens tel que <br> je veux être un jour»"];
B42 --- B5;
```


### 4.1. Qu’est-ce que la socialisation ?

#### 4.1.1. Un processus de construction de l'identité 

La socialisation, c'est quand on apprend les règles et les comportements acceptés par la société, ce qui aide à former notre personnalité.

Il y a deux sortes d'identité : l'identité personnelle, qui est basée sur ce qu'on pense de nous-mêmes, et l'identité sociale, qui se forme selon les groupes auxquels on appartient. L'identité sociale nous aide à nous repérer dans la société. Elle se compose de trois parties : notre indépendance personnelle, notre appartenance à un groupe et notre différence avec les autres. Ces trois parties se complètent de façon surprenante. Notre identité personnelle et notre identité de groupe se construisent aussi en comparaison avec les autres. Par exemple, faire partie d'une équipe de sport ne crée pas tout de suite un sentiment d'appartenance, mais cela peut se développer lors de matchs contre d'autres équipes.

Notre identité sociale, ce qui nous définit en tant que groupe, nous vient dès la naissance et dépend de choses qu'on ne choisit pas, comme notre genre, la classe sociale de nos parents, ou notre nom. Notre identité se construit en se comparant et en interagissant avec les autres.

La société attend de nous certaines identités toutes faites, ce qui rend difficile d'être vraiment soi-même. Cela peut aussi nous isoler des autres. Avoir plusieurs groupes auxquels on appartient peut rendre notre identité collective moins claire et moins riche. Trouver sa propre identité, à la fois personnelle et sociale, est devenu un défi complexe.

Nos identités personnelle et sociale sont étroitement liées. La façon dont on se voit est aussi influencée par notre environnement social. Par exemple, le rôle qu'on joue dans la société, notre confiance en nous, notre spontanéité et notre capacité à penser par nous-mêmes sont tous influencés par notre entourage. Notre identité personnelle vient de nos propres perceptions, mais aussi des normes sociales. Par exemple, une personne obèse peut être vue négativement par la société, ce qui affecte son image de soi. Ainsi, on ne contrôle pas complètement notre identité, car elle est formée par nos expériences de socialisation.

#### 4.1.2 Un processus d’adaptation et d’intégration

La socialisation est un processus où on apprend à interagir avec différentes personnes. En observant et en communiquant avec les autres, on apprend les comportements et les idées qui sont acceptés dans notre société. Notre famille, nos amis, nos professeurs, mais aussi les gens qu'on rencontre par hasard et les médias jouent un rôle important dans notre développement. Si les gens autour de nous approuvent ou désapprouvent notre comportement, et la façon dont nous réagissons à leurs réactions, tout cela participe à notre socialisation.

La socialisation mélange notre culture, notre être physique et notre environnement. Elle combine notre identité personnelle et notre identité sociale, qui deviennent une partie intégrante de notre personnalité.

La socialisation, c'est aussi apprendre le système culturel dans lequel on vit (la langue, les règles de comportement, les croyances, etc.). Elle nous aide à intégrer ces éléments dans notre personnalité, ce qui nous permet de vivre en société. C'est un échange entre l'individu et la culture.

Ce processus inclut trois éléments : la culture, notre organisme et le milieu social. Leur interaction forme notre identité personnelle. Il y a toujours un débat entre l'importance de l'hérédité et celle de l'environnement social dans notre développement, mais il est important de comprendre le rôle de chacun.

#### 4.1.3 Les approches théoriques de la socialisation

Il est compliqué de mesurer exactement comment chacun des différents systèmes (génétique, culturel, social) influence notre développement personnel. Sommes-nous simplement le résultat de nos gènes, de notre culture ou de notre environnement social ? Les théories de la socialisation tentent de répondre à cette question importante sur la liberté et la contrainte, et le rôle de l'individu dans la société. Chaque théorie propose une vision différente.

    1. Les théories déterministes de la socialisation :

        1.1. Durkheim voit la socialisation comme un processus où la société a plus d'influence que l'individu. Selon lui, la socialisation est une manière pour les adultes de transmettre les comportements et la culture à la jeunesse, principalement par la famille et l'école. Pour Durkheim, cela prépare l'individu à vivre en société et vise à maintenir l'ordre et la solidarité.

        1.2. La vision marxiste de la socialisation est également déterministe mais la considère comme une source d'aliénation, où l'individu perd son authenticité. Dans cette vision, les groupes sociaux dominants imposent leurs valeurs et idéologies aux autres, créant ainsi des inégalités sociales liées au sexe, à la classe sociale ou à l'origine ethnique, en particulier à travers l'éducation.

    2. Les théories interactionnistes de la socialisation :

    À l'opposé des théories déterministes, l'interactionnisme place l'individu au cœur du processus de socialisation, en mettant l'accent sur ses interactions dans des contextes variés. Cette approche considère que l'individu joue un rôle actif dans sa socialisation.

    Margaret Mead a introduit l'idée de socialisation mutuelle, en prenant l'exemple de la famille. Les comportements d'un enfant sont influencés par ses parents, mais l'enfant influence aussi les comportements de ses parents. Cette interaction conduit à des changements rapides dans les sociétés, car elle expose les générations plus âgées à de nouvelles coutumes et valeurs.

    L'individu, relativement autonome, s'adapte à différentes situations en modifiant son comportement et ses attitudes pour collaborer avec les autres. Pour être bien accepté socialement, il évite généralement les conflits. Anatol Rapoport (1965) a souligné que le maintien des relations sociales dépend de la volonté de coopérer et d'éviter les comportements provocateurs. Cette attitude peut même conduire à pardonner rapidement ou ignorer des comportements socialement inappropriés pour maintenir la coopération.

### 4.2. Le déroulement du processus de socialisation

Le développement de la personnalité sociale est un processus continu tout au long de la vie, commençant dès la naissance et se poursuivant jusqu'à la mort. On distingue la socialisation primaire, intense dans l'enfance et l'adolescence, de la socialisation secondaire, une adaptation sociale continue ultérieure. C'est un processus persistant mais variable.

#### 4.2.1. Un processus continu, mais changeant

Le développement social varie en fonction de l'individu et du contexte culturel. Certaines cultures célèbrent des étapes importantes de la vie par des rites de passage, transformant des moments potentiellement stressants en épreuves solidaires et symbolisant le passage à un nouveau statut, comme l'exemple des jeunes femmes aborigènes d'Australie célébrant leurs premières menstruations.

Dans le monde occidental, les étapes de la socialisation ne sont plus aussi rigides qu'auparavant. Les cérémonies officielles et collectives comme la première communion ou le mariage ont perdu de leur signification sociale. Les valeurs sont devenues plus personnelles. Malgré cela, certaines traditions subsistent, comme le mariage, bien qu'il soit moins fréquent qu'auparavant. En 2020, au Luxembourg, il y a eu 1 803 mariages.

[Mariages et divorces au Luxembourg (1950-2022)](https://lustat.statec.lu/vis?fs[0]=Th%C3%A8mes%2C1%7CPopulation%20et%20emploi%23B%23%7CMouvement%20de%20la%20population%23B3%23&pg=0&fc=Th%C3%A8mes&df[ds]=ds-release&df[id]=DF_B2100&df[ag]=LU1&df[vs]=1.0&pd=%2C2022&dq=A.SL03%2BSL02&vw=tl)

[Code Source du graphique.](../julia/08-mariages-divorces-luxembourg.ipynb)

Les rites de passage globaux sont devenus moins évidents en raison de la complexité des dynamiques familiales et sociales, ainsi que de la perte de consensus sur les valeurs. Cependant, la recherche d'identité persiste chez les jeunes à travers des rites tels que la première consommation d'alcool, les sorties accompagnées, les bals de fin d'année, les initiations dans les écoles, et l'entrée sur le marché du travail.

Les étapes de la vie et de la socialisation ont connu des changements importants au cours des cinquante dernières années, notamment en raison de l'allongement de l'espérance de vie. Les transitions de vie se produisent maintenant plus tard, telles que quitter le foyer parental, achever les études, se marier et avoir des enfants. Par exemple, l'âge moyen au premier mariage augmente, tout comme l'âge des mères au premier enfant, tandis que le nombre de mères adolescentes diminue considérablement. Ces évolutions sont liées à l'augmentation de l'espérance de vie, qui a atteint 80.5 ans pour les hommes et 84.8 ans pour les femmes en 2023.

[Les femmes au Luxembourg deviennent maman de plus en plus tard (publication STATEC)](https://statistiques.public.lu/dam-assets/catalogue-publications/regards/2019/regards-02-19.pdf)

[Naissances vivantes selon la situation du couple et l'âge de la mère](../julia/09-age-des-mamans-a-premiere-naissance.ipynb)

| Année | Naissances dans le mariage | Moins de 20 ans | 20-24 ans | 25-29 ans | 30-34 ans | 35-39 ans | 40-44 ans | 45 ans et plus | Âge inconnu | Naissances hors mariage | Moins de 20 ans | 20-24 ans | 25-29 ans | 30-34 ans | 35-39 ans | 40-44 ans | 45 ans et plus | Âge inconnu |
|--------:|---------------------------:|----------------:|----------:|----------:|----------:|----------:|----------:|---------------:|------------:|------------------------:|----------------:|----------:|----------:|----------:|----------:|----------:|---------------:|------------:|
| **1955**    | 4824                       | 144             | 1302      | 1762      | 1054      | 412       | 143       | 7              |             | 138                     | 34              | 63        | 22        | 13        | 4         | 2         |                |             |
| **1975**    | 3814                       | 236             | 1276      | 1385      | 613       | 255       | 44        | 5              |             | 168                     | 51              | 54        | 27        | 21        | 7         | 1         |                | 7           |
| **1995**    | 4710                       | 73              | 683       | 1851      | 1553      | 487       | 62        | 1              |             | 711                     | 42              | 169       | 220       | 166       | 92        | 16        | 1              | 5           |
| **2015**    | 3660                       | 12              | 215       | 884       | 1435      | 919       | 177       | 18             |             | 2455                    | 79              | 311       | 653       | 807       | 498       | 99        | 6              | 2           |
| **2022**    | 3765                       | 6               | 162       | 736       | 1561      | 1031      | 250       | 19             |             | 2724                    | 40              | 244       | 672       | 1001      | 584       | 167       | 16             |            |

L'adolescence est fortement influencée par ces changements et s'allonge considérablement. Certains estiment qu'elle peut s'étendre jusqu'à l'âge de 30 ans ou plus. La transition vers le monde du travail prend désormais sept ans au lieu de cinq, avec la moitié des jeunes terminant leurs études et entrant sur le marché du travail vers l'âge de 23 ans.

Il existe une grande diversité dans la définition de la vie familiale et professionnelle des individus, sans un cheminement standard vers l'âge adulte. Cette diversification, principalement de 15 à 35 ans, est en grande partie influencée par des facteurs socioéconomiques. Les aspirations à former un couple sont freinées par la difficulté à obtenir un emploi stable et à développer une carrière, ainsi que par l'importance accordée à la vie professionnelle. Les départs du domicile parental varient en fonction de la structure familiale et du niveau de scolarité. L'augmentation de la scolarisation des femmes retarde la formation d'union, tandis que l'allongement de la scolarité retarde les transitions vers la vie adulte.

L'allongement de la période de transition vers la vie adulte varie selon le milieu socioéconomique. Dans les catégories sociales aisées, les jeunes peuvent rester en dehors du marché du travail plus longtemps. En revanche, ceux issus de milieux défavorisés commencent généralement à travailler plus tôt et quittent les études précocement, soit pour aider financièrement leur famille, rechercher rapidement leur indépendance financière, ou en raison de valeurs familiales différentes.

#### 4.2.2. Les étapes du processus de socialisation

Les étapes de la vie se redéfinissent de manière variée en fonction de l'époque et du lieu, marquant une diversité croissante par rapport aux chemins traditionnels.

Le cheminement de la socialisation est devenu très diversifié dans de nombreuses sociétés, rendant difficile la description d'étapes uniformes vécues de la même manière par tous les individus d'une même culture.

Le processus de socialisation peut être divisé en grandes étapes pour mieux le comprendre. Certaines identifient des moments clés liés aux cycles de vie, tandis que d'autres, comme Daniel J. Levinson, découpent le processus en étapes plus précises. Ce modèle s'applique principalement aux sociétés occidentales actuelles, avec des variations significatives entre individus.

    1. L'enfance

    L'enfance constitue la première étape de la socialisation, caractérisée par une intense période d'apprentissage qui tend à s'étendre dans l'adolescence. Durant cette période, les habitudes de vie, les règles d'hygiène, la moralité, et l'expression des sentiments se développent. L'enfant acquiert sa confiance envers autrui, son autonomie, son sens du travail bien fait, son estime de soi, et ses compétences intellectuelles, motrices, et socio-affectives. Ces apprentissages, influencés par les récompenses et sanctions, laissent des empreintes durables dans la vie de l'individu.

    2. L’adolescence

    L'adolescence, deuxième étape de la socialisation, s'étend généralement de 13 à 17 ans, bien que sa durée puisse varier. C'est une période marquée par la quête d'identité, une crise où l'individu cherche à se définir au sein de la société. Les adolescents cherchent à être reconnus, se distancient de leurs parents, et se tournent vers leurs amis pour construire leur image. Les changements physiques liés à la puberté renforcent ce désir d'affirmation, y compris l'identité sexuelle. Les adolescents veulent exercer leurs droits et libertés, ce qui peut s'exprimer par leur apparence et leur style vestimentaire. C'est une période de remise en question et de construction de soi.

    3. L’âge adulte

    Une fois passées les étapes de l'enfance et de l'adolescence, l'individu continue sa socialisation. La troisième étape, l'âge adulte, se situe approximativement entre 22 et 40 ans. Pendant cette période, l'individu développe au maximum sa capacité de travail. Il consacre beaucoup d'énergie à construire un monde matériel et émotionnel autour de lui, établissant des relations professionnelles, amicales et amoureuses solides. Certains moments sont plus marquants en matière de socialisation, tels que l'intégration dans un nouvel emploi, la vie en couple et la naissance du premier enfant. Cette étape est marquée par une synchronie avec la société en termes d'objectifs et de comportements, variant en fonction de l'engagement et du statut socio-économique de l'individu. Plus le statut est élevé, plus cette étape de socialisation est significative.

    4. La maturité

    La quatrième étape de la socialisation, qui dure environ 15 ans (de 45 à 60 ans), se caractérise par un sentiment de savoir, de sagesse et une perspective élargie sur la vie. C'est une période de grande satisfaction et de créativité selon Levinson. Sur le plan professionnel, les rôles sociaux deviennent moins contraignants, tandis que la satisfaction au travail et l'influence atteignent leur apogée. L'individu se tourne vers d'autres aspects de la vie, notamment la famille et les relations. Cependant, certains peuvent faire face à un stress professionnel, tandis que d'autres entreprennent une nouvelle carrière. Sur le plan personnel, les relations conjugales sont généralement plus satisfaisantes, et l'amitié gagne en importance.

    5. La vieillesse

    La dernière étape de la socialisation selon Levinson est la vieillesse, mais elle varie considérablement d'une personne à l'autre en termes de santé, de satisfaction, de bien-être financier et de solitude. Certains experts ont même divisé cette étape en deux groupes : le troisième âge (65 à 74 ans) regroupant les retraités actifs et préretraités, et le quatrième âge (75 ans et plus) pour les personnes en perte d'autonomie. La vieillesse implique généralement la retraite, ce qui peut être difficile pour certains qui se sentent inutiles et dépendants de la société. Cependant, ceux qui ont planifié cette étape, pour qui le travail n'était pas la priorité ou qui s'engagent dans de nouvelles activités, comme le bénévolat ou les loisirs, l'abordent plus facilement, surtout lorsque la santé, les finances et le soutien social sont présents.

#### 4.2.3. Les périodes de transition

Le modèle de Levinson divise la vie en cinq étapes de socialisation. Entre 18 et 21 ans, se produit la première période de transition vers la vie adulte, marquée par des choix de carrière, d'autonomie financière et d'engagement sentimental. De 41 à 44 ans, la deuxième période de transition, souvent appelée "crise de la quarantaine", incite à faire un bilan personnel et professionnel. Entre 61 et 64 ans, la troisième période de transition est la plus difficile, mettant en question la valorisation au travail et la recherche de sens dans les lois de la nature et de la société. Ces transitions sont des moments de bouleversement et de remise en question, mais aussi l'opportunité de redéfinir ses aspirations et d'entreprendre un nouveau départ.

#### 4.2.4. La resocialisation

La resocialisation se produit lorsqu'un individu réoriente sa vie ou apprend de nouvelles normes et valeurs. Elle peut découler d'une évolution sociale ou survenir spontanément. Par exemple, des périodes de transition telles que la naissance d'un premier enfant peuvent entraîner une resocialisation des nouveaux parents. De même, l'immigration vers un nouveau pays nécessite une adaptation à un nouvel environnement culturel. Dans certains cas, la resocialisation se produit de manière plus intense et sur une période limitée, par exemple lorsqu'un individu est placé dans une institution fermée telle qu'une prison ou l'armée, où tous les aspects de la vie sont contrôlés sous une seule autorité. Ces institutions forment des microsociétés caractérisées par des règles strictes et une orientation commune des activités.

Les institutions fermées, telles que les prisons, peuvent sacrifier l'individualité des individus qui y sont placés. Les détenus perdent leur identité personnelle, deviennent des numéros de matricule et voient leur liberté de choix et d'initiative grandement restreinte. Une expérience menée par Philip Zimbardo a révélé que même des étudiants, lorsqu'ils sont placés en tant que gardiens dans une simulation de prison, peuvent rapidement adopter des comportements cruels envers les prisonniers, montrant ainsi l'influence perturbante de ces institutions sur le comportement humain.

De plus, une expérience réalisée lors de l'ouverture d'un établissement pénitentiaire a montré que des citoyens, après seulement quelques jours d'incarcération, ont développé des attitudes inhabituelles telles que la soumission, la méfiance, la résistance ou le défi de l'autorité. Cela souligne à quel point l'environnement institutionnel fermé peut influencer rapidement les comportements et les dynamiques de groupe.

La resocialisation peut également toucher la société dans son ensemble, comme cela a été observé lors des bouleversements politiques en Europe de l'Est à partir de 1989. Ces changements ont entraîné des transformations majeures dans la vie quotidienne des habitants, avec des conséquences positives et négatives.

En fin de compte, la resocialisation se produit lorsque les individus subissent la pression d'un nouvel environnement, ce qui les oblige à réexaminer leurs habitudes de vie, à apprendre de nouvelles normes et valeurs, et à redéfinir leur identité.

### 4.3. Les mécanismes de socialisation

La socialisation est un processus qui se déroule tout au long de la vie, mais comment s'intègrent les comportements et valeurs de la société dans notre personnalité ? Certains sociologues, comme Cooley, Mead et Goffman, ont exploré comment l'individu développe sa personnalité en interagissant avec les autres, s'adaptant aux normes sociales. Leurs travaux ont identifié quatre mécanismes de socialisation, que nous allons maintenant expliquer.

#### 4.3.1. Le miroir réféchissant

Charles Horton Cooley, au début du XXe siècle, avance que notre perception de nous-mêmes provient en grande partie des interactions avec les autres. Il décrit ce processus comme un "miroir réfléchissant", où nous devenons le reflet de l'image que les autres nous renvoient.

Cooley identifie trois phases de ce mécanisme de socialisation. La première consiste à prendre conscience de la manière dont nous sommes perçus par les autres, que ce soit les commentaires de nos parents, le comportement de nos amis ou les regards d'étrangers. La deuxième phase implique la compréhension du jugement que les autres portent sur nous, comme notre charme, notre intelligence, etc. Enfin, dans la troisième phase, nous intégrons ces jugements à notre identité, ce qui peut susciter une affirmation de soi ou de la honte.

Cooley soutient que notre "moi" est façonné par cette perception, parfois basée sur une fausse compréhension de la manière dont les autres nous voient. Des études montrent également que les attentes des enseignants peuvent influencer les performances des élèves, un phénomène connu sous le nom d'"effet Pygmalion". Lorsqu'on attribue de manière faussement élevée un quotient intellectuel à des élèves, ceux-ci améliorent leurs performances en réponse aux attentes positives des enseignants.

Ainsi, notre perception de nous-mêmes est fortement influencée par la manière dont les autres nous perçoivent, et cela peut avoir un impact significatif sur notre identité et nos performances.


#### 4.3.2. Le jeu de rôle

George Herbert Mead, sociologue, a développé les idées de Cooley sur la socialisation en se concentrant sur le développement de l'image de soi à travers les interactions sociales. Selon lui, ce processus comporte trois étapes.

La première étape, la préparation, implique que les enfants imitent simplement les gestes des personnes qui les entourent, comme les membres de leur famille.

La deuxième étape, l'appropriation des rôles, se produit lorsque les enfants commencent à différencier les actions et les gestes des différents acteurs sociaux et à les associer à des rôles spécifiques, tels que père, enseignant, policier, etc. Ils apprennent également les symboles culturels de leur société, ce qui leur permet d'interpréter correctement les rôles et de s'adapter à de nouvelles situations.

Enfin, à l'étape du jeu, les enfants développent une compréhension plus profonde de leurs relations sociales. Ils sont capables d'assumer plusieurs rôles et de comprendre les responsabilités des autres personnes autour d'eux. Ils reconnaissent également que de nombreuses personnes peuvent exercer le même métier, et ils comprennent les différences de valeur et d'importance attribuées à différents rôles sociaux.

Au cours de cette dernière étape, les enfants deviennent plus conscients des attitudes, des opinions, des points de vue et des attentes de la société dans son ensemble. Ils apprennent à se conformer aux normes sociales et à considérer l'ensemble du groupe qui les entoure dans leurs interactions.

En résumé, Mead décrit comment les enfants développent leur identité en imitant d'abord les autres, puis en comprenant les rôles sociaux, et enfin en devenant conscients des normes et des attentes de la société.

#### 4.3.3. Le maniement des impressions

Erving Goffman, sociologue, explique la socialisation comme un processus d'intégration des comportements sociaux, guidé par une adhésion sincère aux règles de la société. Il soutient que la socialisation se produit au cours d'activités quotidiennes où des efforts sont déployés pour être mieux acceptés socialement, ce qui conduit à l'apprentissage des normes et des valeurs par l'expérimentation de comportements conformes ou non aux normes.

Goffman utilise le concept de "maniement des impressions" pour décrire la manière dont les individus adaptent leur présentation de soi pour satisfaire différents publics. Il compare ces comportements sociaux au jeu des acteurs de théâtre, où les individus ajustent leur comportement en fonction de leur audience. Par exemple, un employé peut paraître plus occupé lorsque son patron le regarde, ou une serveuse peut "ne pas voir" un client qui attend pour commander.

La communication verbale et non verbale joue un rôle essentiel dans le maintien de son image sociale, permettant de dissimuler des émotions telles que la souffrance ou l'embarras. Les individus utilisent des techniques pour vaincre la timidité ou préserver leur image, par exemple en venant accompagnés d'amis à une soirée pour célibataires.

Goffman note que les personnes tolèrent généralement les erreurs et les maladresses des autres, surtout si elles semblent bien intentionnées. Il qualifie ce comportement poli d'"indulgence subtile".

En fin de compte, Goffman souligne que bien que nous puissions consciemment façonner notre image sociale, nous ne sommes pas enfermés dans des rôles fixes, et chacun a la possibilité de modifier l'image qu'il projette en fonction de la situation, tout en conservant un certain degré de spontanéité.

#### 4.3.4. La socialisation par anticipation

La socialisation anticipée est un processus essentiel qui permet à un individu de se préparer mentalement aux rôles qu'il devra assumer à l'avenir et de répondre aux attentes de son environnement. Cela se fait par une expérimentation précoce de ces rôles. Cette anticipation aide l'individu à mieux appréhender les fonctions associées aux nouveaux rôles, qu'il s'agisse de devenir adulte, parent ou professionnel.

La préparation mentale précède la véritable expérience de ces nouveaux rôles, et elle est particulièrement efficace lorsqu'elle est accompagnée d'un encadrement adéquat par un adulte ou un mentor. Cette socialisation anticipée joue un rôle clé dans la préparation des individus à leur futur rôle professionnel. Ils intègrent à l'avance les normes, les valeurs et les stéréotypes du milieu qu'ils ambitionnent d'occuper.

Par exemple, un étudiant qui souhaite devenir avocat cherchera à vivre des expériences et à développer des comportements en adéquation avec ce métier, comme discourir en public, adopter un style vestimentaire particulier, ou fréquenter des lieux associés à la profession. Les attentes du milieu professionnel sont transmises par les parents, les enseignants et les médias, ce qui aide l'individu à se préparer mentalement à son futur rôle.

Certains auteurs associent la socialisation anticipée à la socialisation par le travail, qui se déroule en plusieurs phases. La première phase consiste en le choix de carrière et la sélection des études appropriées. La deuxième phase, la socialisation anticipée proprement dite, peut durer de quelques mois à quelques années, et elle peut se faire dès l'enfance et l'adolescence en idéalisant le travail de ses parents ou en fixant des objectifs de vie précoces.

La troisième phase implique le conditionnement et l'engagement, où la personne s'adapte aux aspects négatifs de son travail tout en acceptant les aspects positifs. Enfin, la quatrième phase est celle de l'engagement continu, où le travail devient un cadre stimulant et respecté, notamment envers les règles établies.

La socialisation anticipée montre que la vie s'organise souvent en fonction de ce qui est anticipé pour l'avenir, et non seulement en fonction du présent. Cela souligne l'importance de se préparer mentalement aux futurs rôles et responsabilités que la société attend de chaque individu.

### 4.4. Les agents de socialisation

La socialisation évolue en plusieurs étapes dans la société nord-américaine. Notre environnement, composé d'individus, de groupes et d'institutions, influence nos choix et notre image de nous-mêmes via les agents de socialisation.

#### 4.4.1. La famille

La socialisation commence dès la naissance au sein de la famille, qui est le principal agent de socialisation primaire. Les parents jouent un rôle essentiel dans la transmission des rôles sociaux, des attentes de la société, et des normes de genre. Cependant, l'individualisme croissant dans les sociétés contemporaines remet en question le rôle de la famille dans la socialisation. Les besoins individuels des membres de la famille prévalent désormais sur les responsabilités collectives. D'autres instances de socialisation, telles que l'État, l'école et la garderie, assument certaines fonctions traditionnelles de la famille.

La famille est devenue plus axée sur l'individu, et son existence dépend de l'épanouissement personnel et de la réalisation de soi. Les choix de mariage et de parentalité sont désormais basés sur le bonheur individuel plutôt que sur des obligations traditionnelles. Cette évolution a également modifié les relations au sein de la famille, passant d'une éducation autoritaire à une approche plus souple et valorisante de l'enfant.

La place de la famille en tant qu'agent de socialisation a été remise en question, car d'autres agents, tels que les services de garde, les écoles, l'État et les réseaux sociaux, ont pris de l'importance. Certains estiment que la famille n'est plus à la hauteur de sa tâche en matière de socialisation, en raison de problèmes tels que la délinquance, le décrochage scolaire, le divorce et la monoparentalité.

Cependant, pour d'autres, la famille reste un point de référence et un cadre où les individus développent leurs racines et leurs souvenirs. La socialisation familiale continue, mais elle se fait désormais par le biais de stratégies visant à construire l'autonomie tout en maintenant des liens sociaux. La famille n'est plus la seule source de transmission de modèles générationnels, car elle subit des influences diverses qui élargissent le cadre des relations sociales.

#### 4.4.2. L'école

L'école joue un rôle majeur en tant qu'agent de socialisation en préparant les enfants à la société plus vaste et en leur transmettant les valeurs et les normes de cette société. Elle enseigne des compétences de base telles que la lecture, l'écriture et le calcul, ainsi que des valeurs comme la compétition, le respect des règles et de l'autorité, et la réussite individuelle, qui sont essentielles pour le futur travail des enfants. De plus, l'école favorise les interactions avec les pairs, ce qui est important dans le processus de socialisation.

Cependant, l'école peut aussi refléter et renforcer les divisions économiques, intellectuelles, sociales, raciales et sexuelles de la société. Certains enseignants et manuels scolaires peuvent transmettre des stéréotypes et des valeurs qui favorisent certains groupes au détriment d'autres. De plus, malgré l'augmentation de l'accès au collégial, de nombreux facteurs contribuent à maintenir l'inégalité des chances, et tous les jeunes n'ont pas les mêmes opportunités éducatives.

L'école peut structurer la stratification des jeunes en créant des distinctions entre différents domaines d'études, entre les écoles publiques et privées, et entre les sexes. Par exemple, des études montrent des écarts de réussite scolaire entre les sexes, avec une réussite généralement plus élevée pour les filles. Cela peut avoir un impact sur les choix de carrière futurs et perpétuer des inégalités dans le marché du travail.

En outre, il semble que les enseignants puissent avoir des attentes différentes envers les garçons et les filles, ce qui peut influencer le comportement des élèves. Les garçons sont parfois confrontés à une pression de groupe pour éviter les matières perçues comme "féminines", tandis que les filles sont encouragées à réussir dans ces domaines.

En résumé, l'école est un agent de socialisation essentiel qui prépare les enfants à la société en leur transmettant des compétences et des valeurs. Cependant, elle peut également refléter et renforcer les inégalités sociales, économiques et de genre, ce qui soulève des défis en matière d'égalité des chances et de justice sociale.

#### 4.4.3. Les pairs

À mesure qu'un enfant grandit et s'intègre à l'école, la famille perd de son exclusivité en tant qu'agent influant sur son développement social. Les pairs, c'est-à-dire les personnes de son âge et de son milieu social, prennent un rôle de plus en plus prépondérant. Ces groupes de pairs, tels que les gangs ou les groupes d'amis chez les adolescents, aident ces derniers à gagner en indépendance par rapport à leurs parents et à d'autres figures d'autorité. Les adolescents ont tendance à imiter leurs amis, car le groupe impose des sanctions positives ou négatives.

De plus, le groupe de pairs influence les ambitions et les aspirations d'un adolescent quant à ses futurs rôles dans la société. Il facilite la transition vers la responsabilité d'adulte. À la maison et à l'école, les jeunes sont soumis à l'autorité des adultes, mais au sein de leur groupe de pairs, ils bénéficient d'une plus grande liberté pour exprimer leurs opinions et établir leurs propres règles.

Le groupe de pairs agit comme un catalyseur pour le désir de solidarité, de relations chaleureuses et d'autonomie de l'adolescent, tout en intégrant ces notions dans un système de normes et de valeurs qui semble émaner d'eux-mêmes, sans autorité extérieure. Ils apprennent la camaraderie et l'entraide au sein de ce groupe.

L'influence du groupe de pairs sur l'intégration d'un individu dans la société peut être positive ou négative. Les pairs peuvent encourager des comportements socialement acceptables, comme le bénévolat, mais aussi inciter à des comportements allant à l'encontre des normes et des valeurs de la société, tels que la délinquance ou la conduite imprudente. Cette influence peut donc prendre des tournures favorables ou défavorables, ce dernier cas étant appelé "socialisation négative".

#### 4.4.4. Le monde du travail

À l'âge adulte (22-40 ans), l'individu développe un fort engagement envers son travail, qui détermine en grande partie son sentiment de réussite. En début de carrière, il cherche à s'intégrer dans l'entreprise en répondant à ses exigences. Le travail agit comme un puissant agent de socialisation en façonnant les comportements, la pensée et les ambitions personnelles. Il incite à la production, à la réalisation de soi et à la consommation, et il impose une standardisation des attitudes.

La socialisation par le travail est influencée par les expériences de socialisation vécues durant l'enfance et l'adolescence, ainsi que par les représentations des rôles professionnels véhiculées par l'entourage, les médias et les pressions sociales. Elle s'opère au sein de la culture d'entreprise, qui vise à intégrer les individus dans la structure économique de la société. Les entreprises déploient des efforts pour que les employés adoptent les valeurs de l'organisation et stimulent leur productivité.

Cependant, la longue durée du travail, malgré les efforts pour favoriser la conciliation travail-famille, peut empiéter sur la vie sociale et familiale. Les objectifs économiques des entreprises influencent la personnalité de l'individu et conditionnent son rendement, parfois même dans sa vie personnelle.

Les relations avec les collègues et le respect de règles relationnelles sont également des facteurs importants de socialisation au travail. Les relations interpersonnelles au travail contribuent à l'apprentissage du respect de l'autorité, mais aussi à l'exercice du pouvoir sur d'autres individus, modifiant ainsi l'identité sociale de l'individu.

Ainsi, la socialisation par le monde du travail ne se limite pas à l'acquisition de compétences professionnelles, mais englobe aussi les relations sociales et la manière dont l'individu s'intègre dans la société.

#### 4.4.5. Les médias

Au cours des derniers cent ans, l'explosion des innovations technologiques a considérablement étendu les moyens par lesquels les individus peuvent transmettre et recevoir des messages. Parmi ces innovations, les médias tels que la presse, la radio, la télévision, Internet, le cinéma, etc., jouent un rôle de plus en plus prépondérant dans le processus de socialisation.

L’utilisation du smartphone, en particulier, a un impact considérable sur la socialisation des enfants. Selon une étude de l’Institut national de la statistique et des études économiques (STATEC), un tiers des jeunes âgés de 16 à 24 ans passent en moyenne plus de 5 heures par jour sur leur téléphone en 2021. Les activités sur le smartphone peuvent être très diverses, telles que l’écoute de musique, le visionnage de films et de séries. Cependant, c’est principalement l’utilisation de messageries instantanées (WhatsApp, Viber, Snapchat, etc.), de réseaux sociaux (Facebook, Twitter, Instagram, etc.) et de sites de partage (YouTube, TikTok, etc.) qui jouent un rôle important dans la socialisation.

Cependant, il est essentiel de se poser des questions sur la complexité des formes d'apprentissage que ces médias encouragent et sur leur capacité à participer à une socialisation négative. Par exemple, la violence représentée dans les médias, souvent déformée et amplifiée, a été liée à des comportements agressifs ou antisociaux chez les enfants qui y sont exposés régulièrement, même lorsqu'il s'agit de dessins animés. 

Selon une revue de recherche de l’American Psychological Association, l’exposition à la violence médiatique peut entraîner une augmentation du comportement agressif, une désensibilisation à la violence et une diminution du comportement prosocial. Une étude publiée dans le Journal of Youth and Adolescence a révélé que l’exposition aux médias violents était associée à une augmentation de l’agression chez les enfants. Une autre étude publiée dans le Journal of Abnormal Child Psychology a révélé que l’exposition à la violence médiatique était associée à une augmentation de l’anxiété et de la dépression chez les enfants. Une revue de recherche du National Center for Biotechnology Information a révélé que l’exposition à la violence médiatique était associée à une gamme de résultats négatifs, notamment l’agression, la désensibilisation et la peur.

Les médias ne se limitent pas à la violence ; la montée de la téléréalité brouille les frontières entre fiction et réalité, contribuant au voyeurisme et à l'exhibitionnisme, tout en remettant en question la notion de "vrai". De plus, les médias influencent la construction de l'identité de genre des jeunes en établissant des stéréotypes de comportement et de sexualité, affectant ainsi leurs valeurs et leurs comportements.

Malgré ces critiques, certains médias éducatifs peuvent aider les enfants à développer des compétences essentielles pour l'école, telles que l'écoute, l'échange et la curiosité. Internet offre également un accès illimité à l'information, permettant aux individus de communiquer au sein de groupes sociaux.

En outre, la montée de l’intelligence artificielle a également un impact sur la socialisation des jeunes. Les algorithmes de recommandation utilisés par les plateformes de médias sociaux peuvent renforcer les stéréotypes et les préjugés existants, ce qui peut avoir des conséquences négatives sur la façon dont les jeunes perçoivent les autres et sur leur propre estime de soi.

En conclusion, les médias jouent un rôle incontournable dans la socialisation des individus, avec des influences potentiellement positives et négatives. Il est essentiel de prendre en compte cette dimension pour comprendre la socialisation des jeunes.

#### 4.4.6. L'Etat

L'État joue un rôle prépondérant dans nos vies, exerçant un contrôle significatif sur nos actions et responsabilités. Cela se manifeste clairement dans des domaines liés au développement et à la maturité individuelle. Par exemple, l'État fixe les règles concernant l'âge minimum pour voter, consommer de l'alcool, conduire une voiture ou se marier sans le consentement des parents. Il assume également des responsabilités croissantes qui relevaient autrefois de la sphère familiale, notamment dans le domaine du développement des jeunes et du bien-être des personnes âgées.

Depuis 1945, avec l'avènement de l'État providence, l'État s'est étendu dans tous les pays industrialisés. En plus de son rôle traditionnel de maintien de l'ordre et de sécurité, l'État intervient dans la régulation des relations sociales, fournissant une assistance aux plus démunis, garantissant les besoins sociaux et la protection sociale, et stimulant l'offre et la demande économiques. Cette évolution se fait par le biais d'institutions diverses telles que les services de santé, les services sociaux, l'éducation, les garderies, etc. L'État intervient même dans des domaines traditionnellement réservés à la famille ou à l'Église, comme la prise en charge des personnes âgées ou l'intervention dans les cas de violence conjugale.

L'État justifie son intervention en tant que substitut de la famille en soutenant que les parents font parfois des choix idéologiques qui les empêchent de remplir pleinement leur rôle dans la société moderne. De plus, en l'absence d'une exécution adéquate par la famille des obligations qu'il a lui-même définies, l'État intervient pour redéfinir les règles au sein de la famille, y compris par le biais du système judiciaire.

Au-delà de son rôle vis-à-vis de la famille, l'État influe largement sur la socialisation des individus à l'âge adulte. Les citoyens agissent à travers des instruments tels que les syndicats et les partis politiques pour influencer les objectifs et les décisions de l'État qui façonnent leur personnalité sociale. Par exemple, l'État détermine la place d'un individu dans la société en fixant le revenu minimum, bien qu'il ne laisse pas toujours le choix à l'individu d'être ce qu'il souhaite.

Cependant, l'État est confronté à des défis financiers croissants et doit réduire ses services malgré les besoins croissants de la population. Cette réduction de l'engagement de l'État peut favoriser le secteur privé et suscite des débats sur la privatisation de services qui devraient être assurés. Ainsi, la question de l'intervention de l'État dans la vie des individus reste un enjeu majeur dans son rôle de socialisation.

#### 4.4.7. Les autres agents de socialisation

De nombreux autres agents de socialisation interviennent dans le développement social de l'individu, notamment la classe sociale, le groupe ethnique et le groupe religieux. La classe sociale contribue à l'acquisition d'un habitus spécifique, tandis que le groupe ethnique influence les normes, les valeurs et l'environnement social de l'individu. Cependant, nous nous concentrerons ici sur l'influence du groupe religieux en tant qu'agent de socialisation.

Bien que la religion semble en déclin dans les sociétés occidentales, de nouveaux mouvements religieux ou idéologiques émergent pour répondre à la désaffection envers le catholicisme, par exemple. Les croyances jouent un rôle essentiel dans le développement social de chaque individu, en les aidant à élaborer une nouvelle vision du monde à travers les expériences de rupture, d'échec et de succès dans leur vie. Les croyances, transmises par les religions et les groupes religieux, contribuent à l'intégration des valeurs, des modèles de vie et de la culture des sociétés et des groupes.

Sur le plan individuel, les croyances sont à la base même de la socialisation, offrant des réponses et des explications lors de moments difficiles ou de désillusions. Sur le plan collectif, les croyances ont une importance historique majeure. Le catholicisme, par exemple, a longtemps été le fondement de la vie communautaire au Luxembourg, et bien que son institution ne soit plus aussi présente, le “catholicisme culturel” persiste encore. Cependant, le nombre de personnes se réclamant de croyances et pratiques religieuses traditionnelles, en particulier du catholicisme, a fortement diminué, passant de 75% à 48% entre 2008 et 2021. En revanche, la part des personnes athées et sans religion a fortement augmenté. Malgré cette évolution, de nombreux Luxembourgeois continuent à observer des rituels de passage catholiques pour marquer des moments importants de leur vie, tels que les baptêmes, les mariages et les funérailles.

En conclusion, le processus de socialisation est complexe, mais il est clair que le lien entre l'individu et les groupes auxquels il appartient joue un rôle déterminant dans la manière dont il se comporte dans la société. Ce lien n'implique pas un contrôle total de l'individu, et les structures des groupes sont malléables et peuvent être modifiées par l'action, ce qui peut entraîner des transformations dans l'environnement social des individus.



### Applications

#### Section 4.1.

1. Quels sont les deux types de représentation qui interviennent dans la construction de l’identité ?
2. Quels sont les trois systèmes qui jouent un rôle dans le processus de socialisation ?
3. Lequel de ces trois systèmes semble avoir une infuence primordiale ? Pourquoi ?
4. En quoi les visions déterministes et interactionnistes de la socialisation s’opposent-elles ?

#### Section 4.2. 

5. En quoi la socialisation est-elle un processus continu ? En quoi est-elle un processus changeant ?
6. Quelles sont les étapes de la socialisation selon Levinson ? Quelles sont les principales caractéristiques de chacune de ces étapes ?
7. Par quoi se caractérisent les diverses périodes de transition ?
8. Qu’est-ce que la resocialisation ? En quoi une institution fermée constitue-t-elle un milieu favorable à ce processus ?

#### Section 4.3.

9. Qu’est-ce qu’un mécanisme de socialisation ?
10. Selon Cooley, l’individu développe son identité sociale principalement grâce au mécanisme du miroir réféchissant. De quoi s’agit-il ?
11. Pour sa part, Mead s’intéresse surtout au mécanisme du jeu de rôle. Quelles sont les étapes de ce mécanisme et en quoi consistent-elles ?
12. Pour Goffman, le mécanisme de socialisation primordial est le maniement des impressions. Comment fonctionne-t-il ?
13. Enfin, il existe un quatrième mécanisme : la socialisation par anticipation. En quoi peut-on rapprocher ce mécanisme de la socialisation par le travail ?

#### Section 4.4.

14. Qu’est-ce qu’un agent de socialisation ?
15. Quels sont les six principaux agents de socialisation ?
16. Pour chacun de ces agents, nommez deux caractéristiques essentielles.

### Sources

1.1.

- Organisation des Nations Unies (ONU). (2022). Rapport sur le développement humain. New York, NY : ONU. https://hdr.undp.org/content/human-development-report-2021-22
- Organisation de coopération et de développement économiques - OCDE (2019), Panorama de la société 2019: Les indicateurs sociaux de l'OCDE, OECD Publishing, Paris, https://doi.org/10.1787/e9e2e91e-fr. 
- [World Wealth Report 2023 (Capgemini)](https://www.capgemini.com/insights/research-library/world-wealth-report/)

1.2.

- STATEC. (2023). Rapport travail et cohésion sociale 2023. Luxembourg : STATEC.
- OGBL. (2023). Dossier – Les inégalités se creusent encore. Luxembourg : OGBL.
- https://www.csl.lu/fr/pages-economiques/inegalites-et-pauvrete/

4.

- Denis, C. et al., 2013. Individu et société. Chenelière éducation.
- [Le smartphone et son usage selon l’âge (Statec Infograpie)](https://statistiques.public.lu/dam-assets/catalogue-publications/infographie/2022/infographie-tic-smartphone-18082022-a4.pdf)
- [Net recul des pratiques religieuses et montée des spiritualités alternatives au Luxembourg (STatec Regards)](https://statistiques.public.lu/dam-assets/catalogue-publications/regards/2023/regards-03-23.pdf)
- American Psychological Association: “Violent Video Games: Myths, Facts, and Unanswered Questions” by Craig A. Anderson et al1.
- Journal of Youth and Adolescence: “The Effects of Media Violence on Adolescent Health” by Huesmann et al2.
- Journal of Abnormal Child Psychology: “The Effects of Media Violence on Desensitization in Children and Adolescents” by Funk et al3.
- National Center for Biotechnology Information: “The Effects of Media Violence on Society” by Anderson et al4.
