# Economics Course

## Description of the Project

This project is inspired by [COREecon](https://www.core-econ.org/), an international, collaborative project that aims at rethinking the way economics is taught in the classroom. The COREecon material is designed to help learners to understand modern-day society.

The COREecon material is already taught at universities around the world. This project attempts to adapt the original COREecon material to the secondary school curriculum. A similar attempt has already been made in France ( see [Econofides](https://www.sciencespo.fr/department-economics/econofides/) ). That project has mainly been designed for the French educational system. Our approach will try to respond to the needs of learners, who live in a multilingual country and where the language of teaching is not the native language of the learners. Although this may be less an an issue at university, it can cause major issues to younger learners who are at secondary school level. 

The concepts that will be taught in the first year can be found at the [**Concept Maps for the economics course**](https://gitlab.com/econmediadb/economics-course/-/blob/main/markdown/index.md) page.


The [Wiki](https://gitlab.com/econmediadb/economics-course/-/wikis/home) of this project shows the different components of the the course, such as concept maps, sources of the paragraphs, ... .

## Tools

This project relies on several **open-source tools**:

### GitLab

- [x] [GitLab](https://gitlab.com/)
- [ ] [GitLab for Education](https://about.gitlab.com/solutions/education/)
- [ ] [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) is a tool for software development and more using continuous integration (CI), continuous delivery and deployment (CD)
- [ ] [GitLab Milestone](https://about.gitlab.com/handbook/marketing/project-management-guidelines/milestones/) and a [tutorial video](https://www.youtube.com/watch?v=Uw249U2U34w&ab_channel=GitLabUnfiltered) 
- [x] [Gitlab Mermaid](https://handbook.gitlab.com/handbook/tools-and-tips/mermaid/)
- [x] [edulink](https://portal.education.lu/cgie/APPS/EDULINK)
- [ ] or bit.ly

### Language

- [ ] [kramdown : fast, pure-Ruby Markdown-superset converter](https://kramdown.gettalong.org/quickref.html)
- [x] [asciidoctor](https://asciidoctor.org/)
- [ ] [asciidoctor-question](https://github.com/hobbypunk90/asciidoctor-question)
- [x] [Markdown language](https://www.markdownguide.org/) ( [10 minute tutorial](https://www.youtube.com/watch?v=hpAJMSS8pvs&ab_channel=NicholasCifuentes-Goodbody) ).
- [x] [mermaid: diagramming and charting tool](https://github.com/mermaid-js/mermaid)
- [x] [Render PDF from Markdown that contains mermaid](https://gist.github.com/letientai299/2c974b4f5e7b05be52d369ff8693c29a) 

The following command converts a markdown file, including mermaid, into a pdf file.

```bash
    pandoc  -F mermaid-filter -o output.pdf input.md 
```

- [ ] [plantuml](https://plantuml.com/)
- [ ] [TikZ examples](https://texample.net/tikz/examples/tag/diagrams/) (Some applications using [TikZ package](https://sites.google.com/site/kochiuyu/Tikz#TOC-Monopoly) in economics.)
- [x] [The TikZ and PGF Packages](https://tikz.dev/)
- [ ] [PGFPlots](http://pgfplots.sourceforge.net/) - A LaTeX Package to create plots in two and three dimensions. (see the following article: [Using pgfplots to make economic graphs in LaTeX](https://towardsdatascience.com/using-pgfplots-to-make-economic-graphs-in-latex-bcdc8e27c0eb) ) and the [complete PGF user manual](https://mirror.foobar.to/CTAN/graphics/pgf/base/doc/pgfmanual.pdf)

### Tools for converting 

- [ ] [reveal.js](https://github.com/hakimel/reveal.js) is an open source HTML presentation framework.
- [ ] [pandoc converts files from one markup format into another](https://pandoc.org/)
- [ ] [Tables generator (Markdown, LaTeX, ...)](https://www.tablesgenerator.com/markdown_tables#)
- [ ] [How to Display Images on Your Website Correctly](https://sites.google.com/view/usefulweb-devtips/home)



### Publishing tools

- [ ] LaTeX through [Overleaf](https://www.overleaf.com/) or [directly in GitLab](https://gitlab.com/islandoftex/images/texlive/-/wikis/Compiling-LaTeX-documents-with-GitLab-CI).
- [ ] [wowchemy - Easy scientific & technical publishing](https://wowchemy.com/)
- [ ] [wowchemy template](https://hugo-documentation-theme.netlify.app/)
- [ ] [Making slides from anywhere for anyone using Marp, GitLab Pages and Gitpod](https://medium.com/linkbynet/making-slides-from-anywhere-for-anyone-using-marp-gitlab-pages-and-gitpod-35001daf1c93)
- [ ] [Fundamentals of Data Visualization](https://clauswilke.com/dataviz/) with the [source code](https://github.com/clauswilke/dataviz)
- [ ] Use [font awesome](https://fontawesome.com/v4/icons/) to include icons/illustration in visual explanations
- [ ] Use Markdown in [narakeet](https://www.narakeet.com/news/2020/03/14/markdown-support.html) to create scripts easily

### Misc

- [x] [Visual Studio Code](https://code.visualstudio.com/)
- [ ] [atom.io](https://atom.io/)
- [ ] [youtube-dl](https://github.com/ytdl-org/youtube-dl/blob/master/README.md#readme)
- [ ] [edX](https://edx.readthedocs.io/projects/edx-partner-course-staff/en/latest/index.html)
- [ ] [oasys](https://www.oasys4schools.lu/conceptmaps/) and the [project webpage Script](https://www.script.lu/fr/activites/innovation/concept-maps). *This option is being compared to Mermaid and may be substituted*.
- [ ] [How to Convert Markdown Files in Linux](https://linuxhint.com/convert-markdown-files-linux/)
- [ ] [Unicode Character Table](https://unicode-table.com/en/blocks/enclosed-alphanumeric-supplement/)

The **design of the content** relies on:

- [x] [CORE](https://www.core-econ.org/)
- [ ] [Alternatives Economiques](https://www.alternatives-economiques.fr/)
- [x] [FRED](https://fred.stlouisfed.org/)
- [x] [Le manuel scolaire collaboratif](https://www.lelivrescolaire.fr/)
- [ ] [EDUC'arte](https://educ.arte.tv)
- [ ] [Media content in Economics](https://tarikgit.github.io/posters/01_Poster_IFEN_Landscape.pdf) together with the [database](https://drive.google.com/file/d/1G6oxgboywzi4rEmuShdcNWmS6MlX__Vf/view?usp=sharing)
- [ ] [Principles of Economics (University of Minnesota)](https://open.lib.umn.edu/principleseconomics/)
- [x] [Principles of Economics (OpenStax)](https://opentextbc.ca/principlesofeconomics/)
- [ ] [Developing and Applying Economics as a Science in Europe (EEA)](https://eeassoc.org/course-design-0)
- [ ] [Khan Academy: Macroéconomie](https://fr.khanacademy.org/economics-finance-domain/macroeconomics/macro-basic-economics-concepts)

Economic and social **data** is taken from:
- [x] [Our World in Data](https://ourworldindata.org/) 
- [x] [OECD data](https://data.oecd.org/)
- [ ] [OECD Data Explorer (new)](https://data-explorer.oecd.org/)
- [ ] [IMF data](https://data.imf.org/)
- [ ] [European Central Bank Statistics](https://www.ecb.europa.eu/stats/html/index.en.html)
- [ ] [Observatory of Economic Complexity (MIT)](https://oec.world/en/home-b)
- [ ] [The Gloge of Economic Complexity](http://globe.cid.harvard.edu/)
- [ ] [Federal Reserve Economic Data (FRED)](https://fred.stlouisfed.org/)
- [ ] [Visual Capitalist](https://www.visualcapitalist.com/)
- [ ] [econlowdown](https://www.econlowdown.org/)
- [ ] [World Inequality Database (for Luxembourg)](https://wid.world/country/luxembourg/)
- [ ] [Stanford Center on Poverty and Inequality](https://inequality.stanford.edu/)

Newspaper **articles**:
- [ ] [Financial Times](https://www.ft.com/) through [FT Schools](https://enterprise.ft.com/en-gb/services/group-subscriptions/secondary-education/)
- [ ] [Les Echos](https://www.lesechos.fr/) through [BNL](https://bnl.public.lu/)
- [ ] [Frankfurter Allgemeine Zeitung](https://www.faz.net/) through [BNL](https://bnl.public.lu/)

National curriculum of ... :
- [ ] [Programmes et ressources en sciences économiques et sociales - voie GT (France)](https://eduscol.education.fr/1658/programmes-et-ressources-en-sciences-economiques-et-sociales-voie-gt)
- [ ] [Questionnements et objectifs d'apprentissage (France)](https://www.cours-thales.fr/lycee/premiere/programme-sciences-economiques-sociales)
- [ ] [La spécialité Sciences Économiques et Sociales, pour qui ? (France)](http://lyceeernestperochon.cc-parthenay.fr/spip.php?article1074)

### Websites and material that should be included 

- [ ] [Map of Worldwide Croplands](https://www.usgs.gov/media/images/map-worldwide-croplands) : This map shows cropland distribution across the world in a nominal 30-meter resolution derived primarily with Landsat imagery for the year 2015. The map uses machine learning algorithms on Google Earth Engine cloud computing platform. This is the baseline product of the GFSAD30 Project. There is a total of 1.874 billion hectares (roughly 12.6 percent of the global terrestrial area) of croplands in the world.

### Graphics used in GT EcoPo project

- [x] [Python generated diagrams](https://gitlab.com/econmediadb/economics-course/-/tree/main/julia?ref_type=heads)
- [ ] [picryl - Public Domain Collections](https://picryl.com/)
- [ ] [Pexels -  free stock photos, royalty free images & videos shared by creators](https://www.pexels.com)
- [ ] [Library of Congress (free to use)](https://www.loc.gov/free-to-use/)
- [ ] [Library of Congress](https://www.loc.gov/pictures/)
- [ ] [pixabay - PublicDomainPictures](https://pixabay.com/users/publicdomainpictures-14/)
- [ ] [Free Public Domain/CC0 Images](https://free-images.com/)
- [ ] [flickr - Public Domain](https://www.flickr.com/groups/publicdomain/)
- [ ] [Getty - Open Content Program](https://www.getty.edu/projects/open-content-program/)
- [ ] [National Archives - Catalog](https://catalog.archives.gov/)
- [ ] [National Gallery of Art - Open Access Data](https://www.nga.gov/open-access-images.html)
- [ ] [New York Public Library](https://www.nypl.org/research/collections/digital-collections/public-domain)
- [ ] [United States Antarctic Program](https://photolibrary.usap.gov/)
- [ ] [The New York Public Library - Digital Collections](https://digitalcollections.nypl.org/)
- [ ] [openphoto](https://openphoto.net/)
- [ ] [Wikipedia:Public domain image resources](https://en.wikipedia.org/wiki/Wikipedia:Public_domain_image_resources)
- [ ] [Unsplash](https://unsplash.com/)
- [ ] [kaboompics](https://kaboompics.com/gallery)

- Graphics standard : #1f77b4 default color in Python

### To do ...

Explain the following concepts and show plot : 

- [ ] taux de pauvreté
- [ ] chômage
- [ ] dette publique en % du PIB ([IMF](https://data.imf.org/))
- [ ] taux souverain à dix ans, en % par an ([IMF](https://data.imf.org/))
- [ ] écart entre taux d'intérêt et taux de croissance (r-g), en % par an ([IMF](https://data.imf.org/))
- [ ] charge d'intérêt, en % du PIB ([IMF](https://data.imf.org/))

### Pedagogy

- [ ] [The Big Book of Computing Pedagogy](https://helloworld.raspberrypi.org/books/big_book_of_pedagogy)
- [ ] [The Big Book of Computing Content](https://helloworld.raspberrypi.org/books/big_book_of_computing_content)

### Meetings

- [ ] [Open Education Conference](https://openeducationconference.org)
- [ ] [ICDE World Conference](https://www.icde.org/icde-news/tag/ICDE+World+Conference)
- [ ] [Open Educational Global Conference](https://conference.oeglobal.org/)
https://conference.oeglobal.org/
- [ ] [Association of European Economics Education](https://www.economicseducation.eu)




## Our Approach

In order to keep as much as possible the existing CORE approach for teaching Economics, we try to keep as much as possible the _composing blocks_ of the original text.

It is also important to highlight the _main ideas_ for each chapter. All the _technical concepts_ will be introduced around these main ideas.

This approach will make it easier to:
- [ ] adapt the material to the national curriculum requirements
- [ ] track and update the different components of the text when an update is available from [CORE](https://www.core-econ.org/)
- [ ] track and analyse the parts that work well with learners and the parts that require further explanation

![Components of chapter 1](https://gitlab.com/econmediadb/economics-course/-/raw/main/concept-maps/20220524-year1-overview.png "Concept map of the first three chapters")

The [source code](https://gitlab.com/econmediadb/economics-course/-/blob/main/concept-maps/20220524-year1-overview.nxfc) of the previous diagram.

## Development stages

1. Define the concepts used in the course: 
   - Identify which concepts are presented in the CORE material?
   - Identify which concepts are defined in the national curriculum?
   - Construct a concept map for the new course (see the concept map above)
2. Define the applications used in the course:
   - Identify applications 
   - Find relevant articles, videos, and audio that goes with each chapter
   - Design sample answers
3. Define visual support for learner and teacher:
   - construct diagrams (economic models, demand/supply, ...)
   - choose concept map that will be included      

## Final material

The final course will be composed of three main blocks:
1. A pdf coursebook, that contains:
   - theory
   - examples (short extracts) 
2. Self-study material:
   - videos, longer articles
   - MCQ (with solutions)
3. Practice material
   - exercises
   - introduction to essay-writing   

## Multidisciplinarity and Interdisciplinarity

Multidisciplinarity draws on knowledge from different disciplines but stays within their boundaries. Interdisciplinarity analyzes, synthesizes and harmonizes links between disciplines into a coordinated and coherent whole. ([source](https://pubmed.ncbi.nlm.nih.gov/17330451/))

If the number of hours taught in economics are limited, one has to rely on a multidisciplinary approach in order to teach all the required competencies.

| Topic                |     | Subject                 |
|----------------------|-----|-------------------------|
| Microeconomics       | --> | Maths                   |
| Essay-writing        | --> | French                  |
| Article analysis     | --> | English, German, French |
| Inequality           | --> | Sociology               |
| History of economics | --> | History                 |
| Macroeconomics       | --> | Geography               |
| Economic thought     | --> | Philosophy              |

TODO: Analyse the program/syllabus of these subjects and try to establish links ... 

## Compiling using asciidoctor

The adoc files are compiled using the following command.
```
asciidoctor-latex -b html -a toc index.adoc
```
The `-a toc` attribute generates an automatic table of contents.

## Updating the Repository

```
cd economics-course
git add *
git status
git commit -m "ADD COMMENT HERE"
git push
```

When updating the repository, you may get the following error message:

```
error: failed to push some refs to ...
```

Try `git status` to identify the issue. Then try:

```
git pull --rebase origin main
```

## Using GitLab CI/CD

TBC

## Collaborate with your Team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:eaa33bbb7247234266cff9fecba23dd2?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:eaa33bbb7247234266cff9fecba23dd2?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:eaa33bbb7247234266cff9fecba23dd2?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:eaa33bbb7247234266cff9fecba23dd2?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:eaa33bbb7247234266cff9fecba23dd2?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)



***


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
All the material here is for non-commercial and educational purpose.
CORE Economics Education (CORE) and is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License. For more details visit the [terms of use section](https://www.core-econ.org/terms-of-use/). 
FRED material is licenses under the [following conditions](https://fred.stlouisfed.org/legal/).

## Project status
This project has started in September 2021.

