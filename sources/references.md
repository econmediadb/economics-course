# Sources

- [eduscol (Ministère de l'éducation nationale, de la jeunesse et du sport (France))](https://eduscol.education.fr/1658/programmes-et-ressources-en-sciences-economiques-et-sociales-voie-gt)
- [Croissance du PIB (La Banque Mondiale)](https://donnees.banquemondiale.org/indicateur/NY.GDP.MKTP.KD.ZG?end=2020&start=2000&view=chart)
- [Landesabitur-Themen für den Grundkurs PoWi](https://gymnasium.bildung.hessen.de/gym_sek_ii/Gesellschaft/powi/kl/fapa/La-gk/index.html)
- [Staatsinstitut für Schulqualität und Bildungsforschung Münschen (Bayern)](https://www.isb.bayern.de/schulartspezifisches/leistungserhebungen/musterabitur-2011-gymnasium/wirtschaft-und-recht/)
- [Basisinhalte Wirtschaft (Baden-Würtenberg)](https://rp.baden-wuerttemberg.de/fileadmin/RP-Internet/Tuebingen/Abteilung_7/Fachberater/_DocumentLibraries/Documents/rpt-75-wi-begriffsinventar-schwerpunktthemen-wirtschaft.pdf)
- [Seminarkurs Wissenschaftliches Arbeiten (Berlin)](https://www.humboldtschule-berlin.de/119-humboldt-gymnasium/faecher/faecher-m-z/wirtschaftswissenschaft)
- [Bildungsserver (Berlin-Brandenburg)](https://bildungsserver.berlin-brandenburg.de/unterricht/pruefungen/abitur-brandenburg)
- [Basisinhalte Wirtschaft 
Schwerpunktthemen Abiturprüfung (Baden-Württemberg)](https://www.lis.bremen.de/sixcms/media.php/13/WIR_GyQ_2008.pdf)
- [Rahmenplan für die 
Fachrichtung Wirtschaft (Hamburg)](https://hibb.hamburg.de/wp-content/uploads/sites/33/2015/09/Rahmenplan-Fachrichtung-Wirtschaft.pdf)
- [Kerncurriculum berufliches Gymnasium
 (Hessen)](https://kultusministerium.hessen.de/sites/kultusministerium.hessen.de/files/2021-07/kcbg_wirtschaft_ausgabe2018.pdf)


- Smith, Peter., Adam. Wilby, and Mila. Zasheva. Cambridge International AS and a Level Economics Second Edition. 2021. Web.
