
* utiliser des extraits vidéo, du texte, de l'image, du son, des cartes mentales
* des fiches pédagogiques
* des tutoriels
* enseignement et apprentissage
* annoter des videos avec du texte, de l'image, du son
* création de carte mentale: permet d'organiser les idées en faisant des liens visuels entre différents éléments
* ressources du Web
* transcription interactive (disponible dans Microsoft Stream, Youtube, ...) dans la langue choisie
* possibilité de télécharger la transcription, chercher des mots clés, travailler le texte sans l'image
* travailler la créativité des élèves en les faisant imaginer des scènes ou la suite du scénario
* la diffusion de vidéos en classe est efficace pour introduire ou conclure un cours, illustrer des exemples ou amener un débat
* utilisez des cartes mentales (_concept maps_) comme fil conducteur du cours, ou pour conclure ou proposer une synthèse de révision
* à chaque visonnage de vidéos proposé aux élèves, associez une activité avant ou après (p.ex. recherche d'une biographie, recherche des informations contenues dans la vidéo, débat à partir d'un passage d'une vidéo, répondre à un questionnaire sur la vidéo, apprendre à prendre des notes en faisant des synthèses, préparer une interview en s'inspirant de la structure d'une vidéo, écriture d'un scénario à partir des deux premières minutes d'une vidéo - description d'une crise suivi d'une politique économique appliquée par le gouvernement ou la banque centrale, ...)
* *pédagogie différenciée*: partager avec les élèves des vidéos différentes selon leurs niveaux
* mettre à disposition la transcription textuelle afin d'aider l'élève à la compréhension
* *travailler en collaboration*: partager des créations (extraits, cartes mentales) avec des collègues grâce au réseau (education), possibilité de travailler dans différentes disciplines sur les mêmes vidéos
* *classe inversée*: (i) faire visionner une liste de vidéos aux élèves en dehors du temps de classe, (ii) paratager avec les élèves des extraits ou cartes mentales complètes ou partielles qu'ils pourront modifier et compléter (oasys est une plateforme idéale pour ceci), (iii) Microsoft Stream et EDUC'arte permet de suivre si les élèves ont effectivement visionné les vidéos, (iii) les élèves font une courte synthèse après le visionnage de chaque vidéo, en classe les travaux des élèves sont partagés et commentés 
* création de fiches pédagogiques
* vocabulaire




Source: Guide d'utisation EDUC'arte
