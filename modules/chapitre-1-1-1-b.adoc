// Ibn Battuta //

Au 14e siècle, l’érudit marocain Ibn Battûta (voir l’encadré) décrivit la région indienne du Bengale comme « un pays de grande taille où le riz est très abondant. Je n’ai, en effet, jamais vu une région du monde recelant une telle abondance de provisions ».

|===
|_Ibn Battûta (1304–1368)_

était un voyageur et marchand marocain. Ses voyages, qui se sont poursuivis durant trente ans, le conduisirent en Afrique du Nord et de l’Ouest, en Europe de l’Est, au Moyen-Orient, en Asie du Sud et centrale et en Chine.

|===

Il avait pourtant parcouru une grande partie du monde, voyageant à travers la Chine, l’Afrique de l’Ouest, le Moyen-Orient et l’Europe. Trois siècles plus tard, le même sentiment fut exprimé par le diamantaire français du 17e siècle Jean-Baptiste Tavernier, qui écrivit à propos de ce pays :

[quote, Jean-Baptiste Tavernier, Travels in India (1676).]
____
Même dans les plus petits villages, on peut se procurer en abondance du riz, de la farine, du beurre, du lait, des haricots et autres légumes, du sucre, des confiseries, sous forme de poudre et de liquide.
____

À l’époque des voyages d’Ibn Battûta, l’Inde n’était pas plus riche que les autres parties du monde. Mais elle n’était pas non plus plus pauvre. Un observateur à cette époque aurait remarqué que les gens, en moyenne, étaient mieux lotis en Italie, en Chine et en Angleterre qu’au Japon ou en Inde. Mais les grandes différences entre les riches et les pauvres, que le voyageur aurait remarquées partout où il se serait rendu, sautaient bien plus aux yeux que les différences entre les régions. Riches et pauvres portaient souvent des titres distincts : dans certains lieux, ils étaient seigneurs féodaux et serfs, dans d’autres, majestés et sujets, propriétaires d’esclaves et esclaves, ou marchands et commis. À l’époque, comme aujourd’hui, vos perspectives futures dépendaient de la position économique de vos parents et de votre genre. À la différence d’aujourd’hui, au 14e siècle, la partie du monde où vous étiez né(e) importait beaucoup moins.
