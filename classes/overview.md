
# Calendar

## 3CD

|Semaine |Date |Sujet               | Devoir | 
| ---  | ---  | ---                 | -------- | 
| T1a  |      | - Compt. Ch. 1-3 <br> - Eco. Intro. & Ch.1  <br> - **DEC I,1** |          | 
| T1b  |      | - Compt. Ch. 4  <br> - Eco. Ch.1 (suite) <br> - **DEC I,2**  |          | 
| T2a  |      | - Compt. Ch.5-6 <br> - Eco. Ch.2  <br> - **DEC II,1**  |          | 
| T2b  |      | - Compt. Ch.7  <br> - Eco. Ch.3  <br> - **DEC II,2**   |          | 
| T3a  |      | - Compt. Ch.8  <br> - Eco. Articles & applications <br> - **DEC III,1**   |          | 
| T3b  |      | - Math.fi. <br> - Eco. Articles & applications   <br> - **DEC III,2**      |          | 

