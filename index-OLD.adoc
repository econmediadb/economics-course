= Introduction aux sciences économiques

////
Compile this using:
asciidoctor-latex -b html -a toc index.adoc
////


:toc:
:toc-title: Table des Matières
:toclevels: 2

== Chapitre 1: L’activité économique et niveau de vie

[#img-concept-map]
.Les différentes notions traitées dans le chapitre 1 (link:https://gitlab.com/econmediadb/economics-course/-/raw/main/concept-maps/20220116-cours-et-applications-priorites.nxfc[code source])
[link=https://gitlab.com/econmediadb/economics-course/-/raw/main/concept-maps/20220116-cours-et-applications-priorites.png]
image::https://gitlab.com/econmediadb/economics-course/-/raw/main/concept-maps/20220116-cours-et-applications-priorites.png[ConceptMapChap1,400,200]


////
image::https://gitlab.com/econmediadb/economics-course/-/raw/main/concept-maps/20220115-cours-et-applications.png[Concept Map]
////

=== A. Le Cours

==== 0. Introduction

// L'amélioration du niveau de vie moyen est devenue une caractéristique permanente de la vie économique dans de nombreux pays. //
// link:html/01-Chapitre1-short.html#1-introduction[_lire la suite ..._] //
include::modules/chapitre-1-1-1-c.adoc[]

////
Note: The point here is to illustrate for the learner why we are collecting data - in order to understand the economy.
Note: chapitre-1-1-1-c.adoc has been deleted (20220116)
////

==== 1. La distribution des revenus
////
La distribution des revenus, au sein de chaque pays, a changé depuis 1980.
link:html/01-Chapitre1-short.html#1-1-distribution-des-revenus[_lire la suite ..._] +
////
include::modules/chapitre-1-1-2-a.adoc[]

==== 2. Les inégalités +
////
Les inégalités au sein des pays ont augmenté.
link:html/01-Chapitre1-short.html#1-2-inegalites[_lire la suite ..._]
////

include::modules/chapitre-1-1-2-b.adoc[]

// Vue historique des inégalités //
include::modules/chapitre-1-1-2-c.adoc[]


==== 3. Mesure des revenus et du niveau de vie

////
Il existe plusieurs mesures pour estimer le niveau de vie au sein d'une économie.
link:html/01-Chapitre1-short.html#1-3-mesures-revenus-niveau-de-vie[_lire la suite ..._] +
////

include::modules/chapitre-1-2-1-a.adoc[]

==== 4. Méthode de calcul du PIB

////
Nous explorons plus en détail la méthode de calcul du PIB, afin de pouvoir comparer les valeurs dans le temps ou entre pays.
link:html/01-Chapitre1-short.html#1-4-calcul-pib[_lire la suite ..._] +
////

// Méthode de calcul du PIB //
include::modules/chapitre-1-2-1-b.adoc[]

---

// Dessine-moi l'éco: Le PIB
include::modules/chapitre-stmg-5-2-a.adoc[]

---

// Calcul du PIB et explication du concept (avec analogie de la baignoire) //
include::modules/chapitre-stmg-5-4-a.adoc[]

==== 5. Calcul du taux de croissance

////
Une autre manière d'analyser les données est d'utiliser le taux de croissance du PIB au lieu du niveau du PIB.
link:html/01-Chapitre1-short.html#1-5-taux-de-croissance[_lire la suite ..._] +
////

// Calcul du taux de croissance //
include::modules/chapitre-1-2-2-a.adoc[]

// Crosse de hockey, taux de croissance: Inde, Chine, Amérique latine //
include::modules/chapitre-1-2-2-b.adoc[]

// Adam Smith //
include::modules/chapitre-1-2-2-c.adoc[]


==== 6. La statistique nationale et la comptabilité nationale

////
L'évolution du PIB permet d'identifier les phases d’expansion et de récession d'une économie.
link:html/01-Chapitre1-short.html#1-6-statistique-national[_lire la suite ..._] +
////

// La statistique nationale et la comptabilité nationale //
include::modules/chapitre-stmg-5-3-a.adoc[]

---

// Exercice pour l'utilisation de FRED //
include::modules/chapitre-stmg-5-3-b.adoc[]


==== 7. Les indicateurs complémentaires

////
A côté du PIB, il existe des mesures alternatives pour mesurer le niveau de vie au sein d'une économie.
link:html/01-Chapitre1-short.html#1-7-indicateurs-complementaires[_lire la suite ..._] +
////

// Indicateurs complémentaires //
include::modules/chapitre-stmg-5-6-a.adoc[]
include::modules/chapitre-stmg-5-6-b.adoc[]

==== 8. La révolution industrielle

////
Le progrès technologique, qui a eu lieu lors de la révolution industrielle, a joué un rôle important dans l'augmentation du PIB.
link:html/01-Chapitre1-short.html#1-8-revolution-industrielle[_lire la suite ..._] +
////


// Introduction: La révolution industrielle //
include::modules/chapitre-1-3-1-b.adoc[]

===== 8.1. Illustration du changement technologique: l'éclairage

// Illustration de changement technologique: éclairage //
include::modules/chapitre-1-3-1-c.adoc[]

===== 8.2. Illustration du changement technologique: la communication

// Illustration de changement technologique: communication //
include::modules/chapitre-1-3-1-d.adoc[]


==== 9. Conclusion


=== B. Applications

==== 1. Comment crée-t-on des richesses et comment les mesure-t-on ?
Qui produit des richesses?
Comment les richesses sont-elles produites?
Que mesure le PIB et que ne mesure-t-il pas.
Comment a évolué le PIB dans le monde sur le long terme?
[%hardbreaks]
link:https://www.lelivrescolaire.fr/page/6214613[... _aller directement aux questions_ ...]
[%hardbreaks]
_Mots clés: ressources naturelles, pays riche, évaluation de la richesse, rôle de l'État, rôle du secteur privé, richesses produits par la nature_

==== 2. Comment analyser la diversité des producteurs et des productions ?

Les entreprises produisent des biens et services marchands, c’est-à-dire vendus avec l’objectif de réaliser un bénéfice. Cependant, toute la production n’est pas faite par les entreprises : les administrations publiques réalisent une production non marchande et jouent un rôle économique important. Le secteur de l’économie sociale et solidaire repose sur des règles de fonctionnement spécifiques. +
[%hardbreaks]
link:https://www.lelivrescolaire.fr/page/6214617[... _aller directement aux questions_ ...]
[%hardbreaks]
_Mots clés: production marchande, production non-marchande, éducation, secteur public, économie sociale_

==== 3. Comment produire et mesurer la production ?

La production de biens et services nécessite la combinaison de différents facteurs : du travail, du capital, des ressources naturelles ou encore de la technologie. L’entreprise subit des coûts quand elle produit et cherche à réaliser un bénéfice. La création de richesse d’une entreprise peut être mesurée par sa valeur ajoutée.
[%hardbreaks]
link:https://www.lelivrescolaire.fr/page/6214618[... _aller directement aux questions_ ...] +
[%hardbreaks]
_Mots clés: facteur de production, chiffre d'affaire, valeur ajoutée, bénéfice, produit, coût fixe, coût variable, coût moyen_

==== 4. Que mesure le PIB et que ne mesure-t-il pas ?
Le PIB est un indicateur économique, c’est-à-dire qu’il a été construit pour mesurer la production de richesses et la croissance économique.

Il permet de résumer de façon très synthétique de nombreuses informations sur l’état de la production dans un pays.

Néanmoins, il ne peut pas tout mesurer : il ne compte par exemple pas les tâches domestiques comme une production, ne peut pas bien mesurer l’économie souterraine et ne nous apprend rien sur la répartition des richesses. Enfin, le PIB mesure une quantité de production, mais pas la qualité de vie.
[%hardbreaks]
link:https://www.lelivrescolaire.fr/page/6214619[... _aller directement aux questions_ ...] +
[%hardbreaks]
_Mots clés: définition du PIB, difficultés avec le calcul du PIB, inégalités de revenu, évolution du PIB, étude d'un graphique_

==== 5. Comment le PIB a-t-il évolué dans le monde sur le long terme ?

Depuis le XIXe siècle, le PIB a connu une très forte croissance, notamment dans les pays occidentaux. Les pays émergents ont connu récemment une très forte croissance de leur PIB. Néanmoins, cette croissance s’essouffle, et n’est pas sans poser de problèmes, notamment du point de vue écologique.
[%hardbreaks]
link:https://www.lelivrescolaire.fr/page/6214622[... _aller directement aux questions_ ...] +
[%hardbreaks]
_Mots clés: Trente Glorieuse, évolution du PIB à long terme, évolution régionale du PIB, distribution du PIB, évolution du PIB en Chine et en Inde, conséquence d'une croissance rapide_

==== 6. Comment crée-t-on des richesses et comment les mesure-t-on ?

link:https://www.lelivrescolaire.fr/page/6214625[... _aller directement aux questions_ ...] +
_Mots clés: chiffres d'affaires, valeur ajoutée, PIB par habitant, mesure des inégalités_

==== 7. Comment calculer et utiliser le PIB ?

link:https://www.lelivrescolaire.fr/page/6214626[... _aller directement aux questions_ ...] +
_Mots clés: calcul du PIB, utilisation du PIB et du PIB par habitant_

==== 8. *Argumenter* : les limites écologiques de la croissance

link:https://www.lelivrescolaire.fr/page/6214628[... _aller directement aux questions_ ...] +
_Mots clés: croissance économique, conséquence d'une croissance élevée, épuisement des ressources_

==== 9. *Argumenter*: Analyser et évaluer (_facultatif_)

link:modules/chapitre-1-workbook.html[... _aller directement aux questions_ ...] +
_Mots clés:_

=== C. Guide de révision pour l'élève
